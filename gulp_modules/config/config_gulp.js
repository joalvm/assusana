module.exports = {
    browserSync : {
        proxy: { target: "http://assusana.local" },
        port: 82,
        open: true,
        notify: true
    },
    htmlMinifier : {
        collapseWhitespace: true,
        collapseInlineTagWhitespace:true,
        keepClosingSlash: true,
        removeComments: true,
        removeEmptyAttributes: true,
        removeAttributeQuotes:true,
        minifyCSS: true,
        minifyJS:true
    },
    Rename: { 
        suffix: '.min' 
    },
    bSyncReload : { 
        stream: true 
    },
    sass : { 
        errLogToConsole: true,
        outputStyle: 'compressed' // compressed - expanded
    },
    pumblerJS : function(notify) {
        return {
            errorHandler: function(err) {
                var mensaje = "Error: " + (err.message.split(/js: /gi))[1] + "\n";
                mensaje += "Line: " + err.lineNumber + "\n";
                mensaje += "File: " + err.fileName;

                notify.onError({
                    title:    "Error de javascript",
                    subtitle: "Aparece del todo",
                    message:  mensaje,
                })(err);
                this.emit('end');
            }
        };
    }
}
