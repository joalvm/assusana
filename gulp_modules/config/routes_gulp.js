module.exports = {
    //TODOS LOS ARCHIVOS QUE CORRESPONDEN A CodeIgniter
    codeigniter : [
        "./lib/CodeIgniter/**/application/**/*",
        "!./lib/CodeIgniter/application/controllers/Welcome.php",
        "!./lib/CodeIgniter/application/views/welcome_message.php",
        "./lib/CodeIgniter/**/system/**/*",
        "./lib/CodeIgniter/index.php"
    ],

    restFULL : [
    	"./vendor/chriskacerguis/codeigniter-restserver/**/application/libraries/*",
    	"./vendor/chriskacerguis/codeigniter-restserver/**/application/config/*",
		"./vendor/chriskacerguis/codeigniter-restserver/**/application/language/**/*.php"
    ],
	PHPExcel : [
    	"./vendor/phpoffice/phpexcel/Classes/**/*",
    ],
	plugins : {
		scripts : [
            './lib/jquery/dist/jquery.min.js',
			'./lib/magnific-popup/dist/jquery.magnific-popup.min.js',
			'./lib/moment/min/moment-with-locales.min.js',
			'./lib/jquery-editable-select/source/jquery.editable-select.min.js',
			'./src/external/canvasjs.min.js'
        ],
		styles : [
            './lib/magnific-popup/dist/magnific-popup.css'
        ],
		fonts : [
            './lib/Materialize/fonts/**/roboto/*'
        ]
	},

	materialize : {
		base : [
			"./lib/Materialize/js/initial.js",/*[0]*/
			"./lib/Materialize/js/jquery.easing.1.3.js",/*[1]*/
			"./lib/Materialize/js/animation.js",/*[2]*/
			"./lib/Materialize/js/velocity.min.js",/*[3]*/
			"./lib/Materialize/js/hammer.min.js",/*[4]*/
			"./lib/Materialize/js/jquery.hammer.js",/*[5]*/
			"./lib/Materialize/js/global.js"/*[6]*/
		],
		tools : {
			"collapsible": "./lib/Materialize/js/collapsible.js",/*[0]*/
			"dropdown": "./lib/Materialize/js/dropdown.js",/*[1]*/
			"leanModal": "./lib/Materialize/js/leanModal.js",/*[2]*/
			"materialbox": "./lib/Materialize/js/materialbox.js",/*[3]*/
			"parallax": "./lib/Materialize/js/parallax.js",/*[4]*/
			"tabs": "./lib/Materialize/js/tabs.js",/*[5]*/
			"tooltip": "./lib/Materialize/js/tooltip.js",/*[6]*/
			"waves": "./lib/Materialize/js/waves.js",/*[7]*/
			"toasts": "./lib/Materialize/js/toasts.js",/*[8]*/
			"sideNav": "./lib/Materialize/js/sideNav.js",/*[9]*/
			"scrollspy": "./lib/Materialize/js/scrollspy.js",/*[10]*/
			"forms": "./lib/Materialize/js/forms.js",/*[11]*/
			"slider": "./lib/Materialize/js/slider.js",/*[12]*/
			"cards": "./lib/Materialize/js/cards.js",/*[13]*/
			"chips": "./lib/Materialize/js/chips.js",/*[14]*/
			"pushpin": "./lib/Materialize/js/pushpin.js",/*[15]*/
			"buttons": "./lib/Materialize/js/buttons.js",/*[16]*/
			"transitions": "./lib/Materialize/js/transitions.js",/*[17]*/
			"scrollFire": "./lib/Materialize/js/scrollFire.js",/*[18]*/
			"picker": "./lib/pickadate/lib/picker.js",/*[19]*/
			"picker_date": "./lib/pickadate/lib/picker.date.js",/*[20]*/
			"picker_time" : "./lib/pickadate/lib/picker.time.js",
			"picker_legacy": "./lib/pickadate/lib/legacy.js",
			"picker_trans" : "./lib/pickadate/lib/translations/es_ES.js",
			"character_counter": "./lib/Materialize/js/character_counter.js",/*[21]*/
			"carousel": "./lib/Materialize/js/carousel.js"/*[22]*/
		},
		components : function(names_arr) {
			// EN CASO DE NO PASAR VARIABLES DEVUELVE TODOS LOS CAMPOS
			if( typeof names_arr == 'undefined' ) {

				return this.base.concat(this.tools);

			} else {

				var tools_arr = [],
		            _tools    = this.tools;

				// FILTRANDO CAMPOS EN BASE A LAS POSICIONES
				names_arr.forEach(function(val, index) {
					if ( _tools[val] != undefined )
						tools_arr.push( _tools[val] );
				});

				return this.base.concat(tools_arr);
			}
		}
	}

};
