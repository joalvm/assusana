module.exports = function(gulp, opt) {
	var _moduleName = "html",
		_srcHTML 	= "./src/source/application/views/**/*";

	var _html = function(obj, e) { 
		var path = obj.path.replace(/c:\\xampp\\htdocs\\assusana\\/gi, './'),
            path_arr = [],
            npath = "";

        path = path.replace(/\\/g, '/');
        path_arr = path.split('/');

        path_arr.forEach(function(value, index) {
            if (index > 2 && index < (path_arr.length - 1))
                npath += "/" + value;
        });

		gulp.src(path)
			.pipe(opt.phpmin())
			.pipe(opt.htmlmin({
				collapseWhitespace: true,
				collapseInlineTagWhitespace:true,
				keepClosingSlash: true,
				removeComments: true,
				removeEmptyAttributes: true,
				removeAttributeQuotes:true,
				minifyCSS: true,
				minifyJS:true
			}))
			.pipe(gulp.dest("./dist" + npath))
			.pipe(opt.reload(opt.config.bSyncReload));
	};

	return {
		tasks: function() {
			gulp.task( _moduleName + ':watch', _html );
		},

		watches : function() {
			gulp.watch( _srcHTML, _html);
		},

		name : _moduleName
	}
};