module.exports = function(gulp, opt) {
	// Componentes que se uniran a nuestro codigo JS
	var _componentsJS 		= [ 
								'collapsible', 'dropdown', 'tooltip',
								'toasts', 'sideNav', 'forms', 'buttons', 'transitions',
								'picker', 'picker_date', 'picker_time', 'picker_trans'
							],
		_srcSCSS 			= [ 
								'src/scss/sistema/inscription/inscription.scss', /* Main */
								'src/scss/sistema/inscription/_index.scss',
								'src/scss/sistema/inscription/_manage.scss',
								'src/scss/sistema/_custom.scss',
								'src/scss/sistema/_app.scss',
								'src/scss/sistema/_header.scss' 
							];
	var _moduleName 		= 'inscription',
		_outputJS 			= 'dist/static/js/application',
		_outputCSS 			= 'dist/static/css/application',
		_moduleNameConcat 	= _moduleName + ".js",
		_srcJavascript 		= opt.routes.materialize.components(_componentsJS);

	// TAREA PARA LOS JAVASCRIPTS
	var _scripts = function() {
	  _srcJavascript.push('src/js/_core.js');
	  _srcJavascript.push('src/js/sistema/_app.js');
	  _srcJavascript.push('src/js/sistema/inscription/*.js');

	  gulp.src( _srcJavascript )
	  	.pipe(opt.plumber({
	  		errorHandler: function(err) {
	            var mensaje = "Error: " + (err.message.split(/js: /gi))[1] + "\n";
	            mensaje += "Line: " + err.lineNumber + "\n";
	            mensaje += "File: " + err.fileName;

	            opt.notify.onError({
	                title:    "Error de javascript",
	                subtitle: "Aparece del todo",
	                message:  mensaje,
	            })(err);

	            this.emit('end');
	        }
	        
	  	}))
	    .pipe( opt.concat( _moduleNameConcat ) )
	    .pipe(opt.uglify())
	    .pipe( opt.rename( opt.config.Rename ) )
	    .pipe( gulp.dest( _outputJS ) )
	    .pipe( opt.reload( opt.config.bSyncReload ) );
	};

	// TAREA PARA LOS ESTILOS
	var _styles = function() {
		
		opt.config.sass.includePaths = [
			'./',
			'./lib/Materialize/scss/components', 
			'./src/scss', 
			'./src/scss/sistema', 
			'./src/scss/sistema/inscription'
		];
		
		gulp.src( _srcSCSS[0] )
		    .pipe( opt.sass(opt.config.sass).on( 'error', opt.sass.logError ) )
		    .pipe( opt.rename( opt.config.Rename ) )
		    .pipe( gulp.dest( _outputCSS ) )
		    .pipe( opt.reload( opt.config.bSyncReload ) );
	};

	// PUBLICAR
	return {

		// TAREAS
		tasks : function() {
			gulp.task(_moduleName + ':scripts', _scripts);
			gulp.task(_moduleName + ':styles', _styles);

			gulp.task( _moduleName, [_moduleName + ':scripts', _moduleName + ':styles'] );
		},

		// VISORES DE CAMBIOS
		watches : function() {
			gulp.watch( _srcJavascript, [(_moduleName + ':scripts')] );
  			gulp.watch( _srcSCSS, [(_moduleName + ':styles')] );
		},

		// NOMBRE DEL MODULO
		name : _moduleName

	};

};