module.exports = function( gulp, opt ) {

	// Componentes que se uniran a nuestro codigo JS
	var _componentsJS 		= ['waves','dropdown', 'forms','buttons', 'toasts', 'transitions'];

	// Variables de entorno
	var _moduleName 		= 'login',
		_outputJS 			= 'dist/static/js',
		_outputCSS 			= 'dist/static/css',
		_moduleNameConcat 	= _moduleName + ".js",
		_srcSCSS 			= 'src/scss/login/login.scss',
		_srcJavascript 		= opt.routes.materialize.components( _componentsJS );

	// TAREA PARA LOS JAVASCRIPTS
	var _scripts = function() {
		// Agregando los scripts del modulo
		_srcJavascript.push('src/js/_core.js');
		_srcJavascript.push('src/js/login/login.js');

		gulp.src( _srcJavascript )
			.pipe( opt.concat( _moduleNameConcat ) )
			.pipe(opt.uglify() )
			.pipe( opt.rename( opt.config.Rename ) )
			.pipe( gulp.dest( _outputJS ) )
			.pipe( opt.reload( opt.config.bSyncReload ) );
	};

	// TAREA PARA LOS ESTILOS
	var _styles = function() {
		gulp.src(_srcSCSS)
			.pipe( opt.sass({
				errLogToConsole: true,
				outputStyle: 'compressed'
			}).on('error', opt.sass.logError) )
			.pipe( opt.rename( opt.config.Rename ) )
			.pipe( gulp.dest( _outputCSS ) )
			.pipe( opt.reload( opt.config.bSyncReload ) );
	};

	// PUBLICAR
	return {

		// TAREAS
		tasks : function() {
			gulp.task(_moduleName + ':scripts', _scripts);
			gulp.task(_moduleName + ':styles', _styles);

			gulp.task( _moduleName, [_moduleName + ':scripts', _moduleName + ':styles'] );
		},

		// VISORES DE CAMBIOS
		watches : function(){
			gulp.watch( _srcJavascript, [_moduleName + ':scripts'] );
  			gulp.watch( _srcSCSS, [_moduleName + ':styles'] );
		},

		// NOMBRE DEL MODULO
		name : _moduleName

	};

};
