"use strict";
require('events').EventEmitter.prototype._maxListeners = 100;

////////////////////////////////////////
//  REQUERIMIENTOS
////////////////////////////////////////
var gulp        = require('gulp'),
    htmlmin     = require('gulp-html-minifier'),
    imagemin    = require('gulp-imagemin'),
    pngquant    = require('imagemin-pngquant'),
    bSync       = require('browser-sync'),
    run         = require('run-sequence'),
    fsi         = require('fs'),
    config      = require('./gulp_modules/config/config_gulp.js'),
    routes      = require('./gulp_modules/config/routes_gulp.js');
var path = require('path');

var options = {
    routes: routes,
    config: config,
    concat: require('gulp-concat'),
    del: require('del'),
    plumber: require('gulp-plumber'),
    notify: require("gulp-notify"),
    uglify: require('gulp-uglify'),
    rename: require('gulp-rename'),
    copy: require('gulp-copy'),
    sass: require('gulp-sass'),
    phpmin: require('gulp-php-minify'),
    imagemin: imagemin,
    htmlmin: htmlmin,
    pngquant: pngquant,
    optipng: require('imagemin-optipng'),
    reload: bSync.reload
};

var _workshops          = require('./gulp_modules/modules/workshops_gulp.js')(gulp, options);

var _login 				= require('./gulp_modules/modules/login_gulp.js')(gulp, options);

var _dashboard 			= require('./gulp_modules/modules/admin/dashboard_gulp.js')(gulp, options);
var _coordinator 		= require('./gulp_modules/modules/admin/coordinator_gulp.js')(gulp, options);
var _inscription 		= require('./gulp_modules/modules/admin/inscription_gulp.js')(gulp, options);
var _tr_register 		= require('./gulp_modules/modules/admin/training_register_gulp.js')(gulp, options);
var _tr_program	 		= require('./gulp_modules/modules/admin/training_program_gulp.js')(gulp, options);
var _type_training      = require('./gulp_modules/modules/admin/type_training_gulp.js')(gulp, options);
var _entities 			= require('./gulp_modules/modules/admin/entities_gulp.js')(gulp, options);
var _themes             = require('./gulp_modules/modules/admin/themes_gulp.js')(gulp, options);
var _reports 			= require('./gulp_modules/modules/admin/reports_gulp.js')(gulp, options);

var _doc_dashboard 		= require('./gulp_modules/modules/docente/dashboard_gulp.js')(gulp, options);
var _doc_inscription 	= require('./gulp_modules/modules/docente/inscription_gulp.js')(gulp, options);
var _doc_attendance 	= require('./gulp_modules/modules/docente/attendance_gulp.js')(gulp, options);
var _doc_evaluation 	= require('./gulp_modules/modules/docente/evaluation_gulp.js')(gulp, options);
var _doc_reports 		= require('./gulp_modules/modules/docente/reports_gulp.js')(gulp, options);
var _doc_certified      = require('./gulp_modules/modules/docente/certified_gulp.js')(gulp, options);

var _html 				= require('./gulp_modules/helpers/html_gulp.js')(gulp, options);

////////////////////////////////////////
//  LLAMANDO A LAS TARES DE LOS
//  MODULOS QUE SE HAN INSTANCIADO
////////////////////////////////////////
_login.tasks();
_dashboard.tasks();
_workshops.tasks();
_type_training.tasks();
_coordinator.tasks();
_tr_register.tasks();
_entities.tasks();
_themes.tasks();
_inscription.tasks();
_tr_program.tasks();
_reports.tasks();

_doc_dashboard.tasks();
_doc_attendance.tasks();
_doc_inscription.tasks();
_doc_evaluation.tasks();
_doc_reports.tasks();
_doc_certified.tasks();

_html.tasks();

gulp.task('build:StaticFiles', function() {
    gulp.src(routes.plugins.scripts)
        .pipe(gulp.dest('dist/static/js'));

    gulp.src(routes.plugins.styles)
        .pipe(gulp.dest('dist/static/css'));

    gulp.src(routes.plugins.fonts)
        .pipe(gulp.dest('dist/static/fonts'));
});

gulp.task('build:ImagesFiles', function() {
    return gulp.src([
        './src/img/*.jpg', 
        './src/img/*.png', 
        './src/img/*.jpeg',
        './src/img/*.svg'
        ])
        .pipe(imagemin())
        .pipe(gulp.dest('dist/static/img'));
});

gulp.task('build:DeveloperFiles', function() {
    var files = [
        './src/source/**/application/**/config/**/*.php',
        './src/source/**/application/**/controllers/**/*.php',
        './src/source/**/application/**/helpers/*.php',
        './src/source/**/application/**/libraries/*.php',
        './src/source/**/application/**/models/**/*.php',
        //'./src/source/**/application/**/third_party/**/*',
        './src/source/.htaccess'
    ];
    return gulp.src(files)
        .pipe(options.phpmin())
        .pipe(gulp.dest('dist'));
});

gulp.task('build:ViewsFiles', function() {
    return gulp.src('./src/source/**/application/**/views/**/*.php')
        .pipe(options.phpmin())
        .pipe(htmlmin(config.htmlMinifier))
        .pipe(gulp.dest('dist'));
});

gulp.task('build:RestFiles', function(){
    return gulp.src(routes.restFULL)
        .pipe(gulp.dest('dist'));
});

gulp.task('build:PHPExcel', function(){
    return gulp.src(routes.PHPExcel)
        .pipe(gulp.dest('dist/application/third_party/PHPExcel'));
});

gulp.task('build:BaseFiles', function() {
    return gulp.src(routes.codeigniter)
        .pipe(gulp.dest('dist'));
});

gulp.task('clean', function(){
    options.del([
        'dist/*', 
        'dist/.htaccess'
    ]);
});

gulp.task('build', function(){
    run(
      'build:BaseFiles', 
      'build:RestFiles', 
      'build:PHPExcel',
      'build:DeveloperFiles', 
      'build:ViewsFiles', 
      'build:StaticFiles', 
      'build:ImagesFiles'
    );
});

///////////////////////////////////////////////////////
// WATCHER PARA LA SUPERVICION DE CAMBIOS  
// EN ARCHIVOS PHP QUE PERTENECEN AL SISTEMA
///////////////////////////////////////////////////////
var filesDev = [
    './src/source/application/config/*.php',
    './src/source/application/controllers/**/*.php',
    './src/source/application/models/**/*_model.php',
    './src/source/application/helpers/*_helper.php',
    './src/source/application/language/**/*.php',
    './src/source/application/libraries/*.php'
];

gulp.task('filesconfig', function(){
    gulp.watch(filesDev, function(obj) {
        var opath = obj.path.replace(path.normalize(__dirname + '/'), './'),
            path_arr = [],
            npath = "";

        opath = opath.replace(/\\/g, '/');
        path_arr = opath.split('/');

        path_arr.forEach(function(value, index) {
            if (index > 2 && index < (path_arr.length - 1))
                npath += "/" + value;
        });

        if ( fsi.lstatSync(opath).isFile() )
        {
            return gulp.src(opath)
                    .pipe(options.notify({
                        title: "Archivo modificado",
                        message: "file: <%= file.relative %> @ <%= options.date %>"
                    }))
                    .pipe(options.phpmin())
                    .pipe(gulp.dest('dist' + npath))
                    .pipe( options.reload( options.config.bSyncReload ) );
        }
    });
});

////////////////////////////////////////
// VISOR DE CAMBIOS DE ARCHIVOS
////////////////////////////////////////
gulp.task('watch', function() {
    
    //login
    _login.watches();

    //administrador
    _dashboard.watches();
    _workshops.watches();
    _type_training.watches();
    _coordinator.watches();
    _tr_register.watches();
    _entities.watches();
    _themes.watches();
    _inscription.watches();
    _tr_program.watches();
    _reports.watches();

    //docente
    _doc_dashboard.watches();
    _doc_attendance.watches();
    _doc_inscription.watches();
    _doc_evaluation.watches();
    _doc_reports.watches();
    _doc_certified.watches();

    _html.watches();
});

////////////////////////////////////////
// SINCRONIZADOR DEL BROWSER
////////////////////////////////////////
gulp.task('browser-sync', function() { bSync(config.browserSync); });


////////////////////////////////////////
// TAREAS POR DEFECTO
////////////////////////////////////////
gulp.task('default', [ 
    _login.name, 
    _dashboard.name, 
    _workshops.name,
    _coordinator.name,
    _type_training.name,
    _entities.name,
    _themes.name,
    _inscription.name,
    _reports.name,
    _tr_register.name,
    _tr_program.name,
    _doc_dashboard.name,
    _doc_attendance.name,
    _doc_inscription.name,
    _doc_evaluation.name,
    _doc_inscription.name,
    _doc_reports.name,
    _doc_certified.name,
    'filesconfig', 
    'browser-sync', 
    'watch'
]);

/*gulp.task('docente', [
    _doc_dashboard.name,
    _doc_inscription.name,
    _doc_attendance.name,
    'filesconfig', 
    'browser-sync', 
    'watch'
]);*/
