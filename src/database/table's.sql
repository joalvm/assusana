/*
SQLyog Ultimate v11.11 (32 bit)
MySQL - 5.5.45-cll-lve : Database - assusana_db
*********************************************************************
*/


/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
CREATE DATABASE /*!32312 IF NOT EXISTS*/`assusana_db` /*!40100 DEFAULT CHARACTER SET latin1 */;

USE `assusana_db`;

/*Table structure for table `tema` */

DROP TABLE IF EXISTS `tema`;

CREATE TABLE `tema` (
  `temaID` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `tema_title` varchar(255) NOT NULL,
  `tema_description` text,
  `tema_date_created` datetime NOT NULL,
  `tema_date_modified` datetime DEFAULT NULL,
  `tema_status` bit(1) NOT NULL DEFAULT b'1',
  PRIMARY KEY (`temaID`)
) ENGINE=InnoDB AUTO_INCREMENT=17 DEFAULT CHARSET=utf8;

/*Table structure for table `sub_tema` */

DROP TABLE IF EXISTS `sub_tema`;

CREATE TABLE `sub_tema` (
  `suteID` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `tema_ID` int(11) unsigned NOT NULL,
  `sute_name` varchar(250) NOT NULL,
  `sute_description` text,
  `sute_date_created` datetime DEFAULT NULL,
  `sute_date_modified` datetime DEFAULT NULL,
  `sute_status` bit(1) NOT NULL DEFAULT b'1',
  PRIMARY KEY (`suteID`),
  KEY `fk_subtema_tema_1` (`tema_ID`),
  CONSTRAINT `fk_subtema_tema_1` FOREIGN KEY (`tema_ID`) REFERENCES `tema` (`temaID`)
) ENGINE=InnoDB AUTO_INCREMENT=22 DEFAULT CHARSET=utf8;

/*Table structure for table `capacitacion_tipo` */

DROP TABLE IF EXISTS `capacitacion_tipo`;

CREATE TABLE `capacitacion_tipo` (
  `catiID` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `cati_name` varchar(150) NOT NULL,
  `cati_description` text,
  `cati_date_created` datetime DEFAULT NULL,
  `cati_date_modified` datetime DEFAULT NULL,
  `cati_status` bit(1) NOT NULL DEFAULT b'1',
  PRIMARY KEY (`catiID`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8;

/*Table structure for table `capacitacion` */

DROP TABLE IF EXISTS `capacitacion`;

CREATE TABLE `capacitacion` (
  `capaID` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `cati_ID` int(11) unsigned NOT NULL,
  `tema_ID` int(11) unsigned NOT NULL,
  `capa_name` varchar(255) NOT NULL,
  `capa_description` text,
  `capa_date_created` datetime DEFAULT NULL,
  `capa_date_modified` datetime DEFAULT NULL,
  `capa_status` bit(1) NOT NULL DEFAULT b'1',
  PRIMARY KEY (`capaID`),
  KEY `fk_capacitacion_capacitacion_tipo_1` (`cati_ID`),
  KEY `fk_capacitacion_tema_1` (`tema_ID`),
  CONSTRAINT `fk_capacitacion_capacitacion_tipo_1` FOREIGN KEY (`cati_ID`) REFERENCES `capacitacion_tipo` (`catiID`),
  CONSTRAINT `fk_capacitacion_tema_1` FOREIGN KEY (`tema_ID`) REFERENCES `tema` (`temaID`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;

/*Table structure for table `departamento` */

DROP TABLE IF EXISTS `departamento`;

CREATE TABLE `departamento` (
  `depaID` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `depa_key` char(2) NOT NULL,
  `depa_name` varchar(150) NOT NULL,
  PRIMARY KEY (`depaID`)
) ENGINE=InnoDB AUTO_INCREMENT=26 DEFAULT CHARSET=utf8;

/*Table structure for table `provincia` */

DROP TABLE IF EXISTS `provincia`;

CREATE TABLE `provincia` (
  `provID` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `depa_ID` int(11) unsigned NOT NULL,
  `prov_key` char(2) NOT NULL,
  `prov_name` varchar(150) NOT NULL,
  PRIMARY KEY (`provID`),
  KEY `fk_provincia_departamento_1` (`depa_ID`),
  CONSTRAINT `fk_provincia_departamento_1` FOREIGN KEY (`depa_ID`) REFERENCES `departamento` (`depaID`)
) ENGINE=InnoDB AUTO_INCREMENT=197 DEFAULT CHARSET=utf8;

/*Table structure for table `distrito` */

DROP TABLE IF EXISTS `distrito`;

CREATE TABLE `distrito` (
  `distID` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `prov_ID` int(11) unsigned NOT NULL,
  `dist_key` char(2) NOT NULL,
  `dist_name` varchar(100) NOT NULL,
  PRIMARY KEY (`distID`),
  KEY `fk_distrito_provincia_1` (`prov_ID`),
  CONSTRAINT `fk_distrito_provincia_1` FOREIGN KEY (`prov_ID`) REFERENCES `provincia` (`provID`)
) ENGINE=InnoDB AUTO_INCREMENT=1840 DEFAULT CHARSET=utf8;

/*Table structure for table `entidad_tipo` */

DROP TABLE IF EXISTS `entidad_tipo`;

CREATE TABLE `entidad_tipo` (
  `entpID` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `entp_name` varchar(100) NOT NULL,
  `entp_description` varchar(255) DEFAULT NULL,
  `entp_date_created` datetime DEFAULT NULL,
  `entp_date_modified` datetime DEFAULT NULL,
  `entp_status` bit(1) NOT NULL DEFAULT b'1',
  PRIMARY KEY (`entpID`)
) ENGINE=InnoDB AUTO_INCREMENT=17 DEFAULT CHARSET=utf8;

/*Table structure for table `entidad_subtipo` */

DROP TABLE IF EXISTS `entidad_subtipo`;

CREATE TABLE `entidad_subtipo` (
  `ensuID` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `entp_ID` int(11) unsigned NOT NULL,
  `ensu_name` varchar(100) NOT NULL,
  `ensu_description` text,
  `ensu_date_created` datetime DEFAULT NULL,
  `ensu_date_modified` datetime DEFAULT NULL,
  `ensu_status` bit(1) NOT NULL DEFAULT b'1',
  PRIMARY KEY (`ensuID`),
  KEY `fk_entidad_subtipo_entidad_tipo_1` (`entp_ID`),
  CONSTRAINT `fk_entidad_subtipo_entidad_tipo_1` FOREIGN KEY (`entp_ID`) REFERENCES `entidad_tipo` (`entpID`)
) ENGINE=InnoDB AUTO_INCREMENT=27 DEFAULT CHARSET=utf8;

/*Table structure for table `entidad` */

DROP TABLE IF EXISTS `entidad`;

CREATE TABLE `entidad` (
  `entiID` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `ensu_ID` int(11) unsigned NOT NULL,
  `enti_ubigeo` varchar(6) NOT NULL,
  `enti_cue` varchar(10) DEFAULT NULL,
  `enti_name` varchar(200) NOT NULL,
  `enti_description` text,
  `enti_date_created` datetime DEFAULT NULL,
  `enti_date_modified` datetime DEFAULT NULL,
  `enti_status` bit(1) NOT NULL DEFAULT b'1',
  PRIMARY KEY (`entiID`),
  KEY `fk_entidad_entidad_subtipo_1` (`ensu_ID`),
  CONSTRAINT `fk_entidad_entidad_subtipo_1` FOREIGN KEY (`ensu_ID`) REFERENCES `entidad_subtipo` (`ensuID`)
) ENGINE=InnoDB AUTO_INCREMENT=3046 DEFAULT CHARSET=utf8;

/*Table structure for table `docente` */

DROP TABLE IF EXISTS `docente`;

CREATE TABLE `docente` (
  `doceID` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `enti_ID` int(11) unsigned NOT NULL,
  `doce_name` varchar(100) NOT NULL,
  `doce_lastname` varchar(100) NOT NULL,
  `doce_dni` varchar(8) NOT NULL,
  `doce_sexo` bit(1) NOT NULL,
  `doce_specialty` varchar(200) DEFAULT NULL,
  `doce_email` varchar(150) DEFAULT NULL,
  `doce_active` bit(1) NOT NULL DEFAULT b'0',
  `doce_request_number` varchar(60) NOT NULL,
  `doce_username` varchar(15) NOT NULL,
  `doce_password` varchar(60) NOT NULL,
  `doce_locked` bit(1) NOT NULL DEFAULT b'0',
  `doce_pin` varchar(4) DEFAULT NULL,
  `doce_last_access` datetime DEFAULT NULL,
  `doce_date_created` datetime DEFAULT NULL,
  `doce_date_modified` datetime DEFAULT NULL,
  `doce_status` bit(1) NOT NULL DEFAULT b'1',
  PRIMARY KEY (`doceID`),
  KEY `fk_docentes_entidad_1` (`enti_ID`),
  CONSTRAINT `fk_docentes_entidad_1` FOREIGN KEY (`enti_ID`) REFERENCES `entidad` (`entiID`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8;

/*Table structure for table `programa` */

DROP TABLE IF EXISTS `programa`;

CREATE TABLE `programa` (
  `progID` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `capa_ID` int(11) unsigned NOT NULL,
  `prog_date` date NOT NULL,
  `prog_time_start` time NOT NULL,
  `prog_time_end` time DEFAULT NULL,
  `prog_place` varchar(200) NOT NULL,
  `prog_address` text NOT NULL,
  `dist_ID` int(11) unsigned NOT NULL,
  `prog_is_private` bit(1) NOT NULL DEFAULT b'0',
  `prog_limit` int(2) unsigned DEFAULT '0',
  `prog_date_created` datetime DEFAULT NULL,
  `prog_date_modified` datetime DEFAULT NULL,
  `prog_is_canceled` bit(1) NOT NULL DEFAULT b'0',
  `prog_reason_canceled` text,
  `prog_folder` varchar(255) DEFAULT NULL,
  `prog_file_name` varchar(100) DEFAULT NULL,
  `prog_file_size` decimal(9,2) unsigned DEFAULT NULL,
  `prog_status` bit(1) NOT NULL DEFAULT b'1',
  PRIMARY KEY (`progID`),
  KEY `fk_programa_capacitacion_1` (`capa_ID`),
  KEY `fk_programa_distrito_1` (`dist_ID`),
  CONSTRAINT `fk_programa_capacitacion_1` FOREIGN KEY (`capa_ID`) REFERENCES `capacitacion` (`capaID`),
  CONSTRAINT `fk_programa_distrito_1` FOREIGN KEY (`dist_ID`) REFERENCES `distrito` (`distID`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;

/*Table structure for table `programa_docente` */

DROP TABLE IF EXISTS `programa_docente`;

CREATE TABLE `programa_docente` (
  `prdoID` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `prog_ID` int(11) unsigned NOT NULL,
  `doce_ID` int(11) unsigned NOT NULL,
  `sute_ID` int(11) unsigned NOT NULL,
  `prdo_is_coordinator` bit(1) NOT NULL DEFAULT b'0',
  `prdo_date_created` datetime DEFAULT NULL,
  `prdo_date_modified` datetime DEFAULT NULL,
  `prdo_status` bit(1) NOT NULL DEFAULT b'1',
  PRIMARY KEY (`prdoID`),
  KEY `fk_programa_docente_docente_1` (`doce_ID`),
  KEY `fk_programa_docente_programa_1` (`prog_ID`),
  KEY `fk_programa_docente_sub_tema_1` (`sute_ID`),
  CONSTRAINT `fk_programa_docente_docente_1` FOREIGN KEY (`doce_ID`) REFERENCES `docente` (`doceID`),
  CONSTRAINT `fk_programa_docente_programa_1` FOREIGN KEY (`prog_ID`) REFERENCES `programa` (`progID`),
  CONSTRAINT `fk_programa_docente_sub_tema_1` FOREIGN KEY (`sute_ID`) REFERENCES `sub_tema` (`suteID`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8;

/*Table structure for table `evaluacion` */

DROP TABLE IF EXISTS `evaluacion`;

CREATE TABLE `evaluacion` (
  `evalID` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `prdo_ID` int(11) unsigned NOT NULL,
  `eval_record` int(2) unsigned NOT NULL,
  `eval_observation` text,
  `eval_level_satisfaction` int(3) unsigned DEFAULT NULL,
  `eval_date_created` datetime DEFAULT NULL,
  `eval_date_modified` datetime DEFAULT NULL,
  `eval_status` bit(1) NOT NULL DEFAULT b'1',
  PRIMARY KEY (`evalID`),
  KEY `fk_evaluacion_programa_docente_1` (`prdo_ID`),
  CONSTRAINT `fk_evaluacion_programa_docente_1` FOREIGN KEY (`prdo_ID`) REFERENCES `programa_docente` (`prdoID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Table structure for table `participantes` */

DROP TABLE IF EXISTS `participantes`;

CREATE TABLE `participantes` (
  `partID` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `enti_ID` int(11) unsigned DEFAULT NULL,
  `dist_ID` int(11) unsigned DEFAULT NULL,
  `part_name` varchar(100) NOT NULL,
  `part_lastname` varchar(100) NOT NULL,
  `part_dni` varchar(8) NOT NULL,
  `part_gender` bit(1) NOT NULL,
  `part_email` varchar(150) NOT NULL,
  `part_profesion` varchar(150) DEFAULT NULL,
  `part_cellphone` varchar(15) DEFAULT NULL,
  `part_phone` varchar(10) DEFAULT NULL,
  `part_annex` varchar(6) DEFAULT NULL,
  `part_position` varchar(150) DEFAULT NULL COMMENT 'cargo',
  `part_dispacity` bit(1) NOT NULL DEFAULT b'0',
  `part_details_dispacity` text,
  `part_date_created` datetime DEFAULT NULL,
  `part_date_modified` datetime DEFAULT NULL,
  `part_status` bit(1) NOT NULL DEFAULT b'1',
  PRIMARY KEY (`partID`),
  KEY `fk_participantes_entidad_1` (`enti_ID`),
  KEY `fk_participantes_distrito_1` (`dist_ID`),
  CONSTRAINT `fk_participantes_distrito_1` FOREIGN KEY (`dist_ID`) REFERENCES `distrito` (`distID`),
  CONSTRAINT `fk_participantes_entidad_1` FOREIGN KEY (`enti_ID`) REFERENCES `entidad` (`entiID`)
) ENGINE=InnoDB AUTO_INCREMENT=16 DEFAULT CHARSET=utf8;

/*Table structure for table `inscripcion` */

DROP TABLE IF EXISTS `inscripcion`;

CREATE TABLE `inscripcion` (
  `inscID` int(22) unsigned NOT NULL AUTO_INCREMENT,
  `part_ID` int(22) unsigned DEFAULT NULL,
  `prog_ID` int(11) unsigned NOT NULL,
  `insc_registry_data` text,
  `insc_request_number` varchar(60) NOT NULL,
  `insc_email_confirmed` bit(1) DEFAULT b'0',
  `insc_assistance` bit(1) DEFAULT b'0',
  `insc_time_assistance` time DEFAULT NULL,
  `insc_nota` int(2) unsigned DEFAULT NULL,
  `insc_satisfaction` int(3) unsigned DEFAULT NULL,
  `insc_date_created` datetime DEFAULT NULL,
  `insc_date_modified` datetime DEFAULT NULL,
  `insc_status` bit(1) DEFAULT b'1',
  PRIMARY KEY (`inscID`),
  KEY `fk_inscripcion_participantes_1` (`part_ID`),
  KEY `fk_inscripcion_capacitacion_programacion_fecha_1` (`prog_ID`),
  CONSTRAINT `fk_inscripcion_capacitacion_programacion_fecha_1` FOREIGN KEY (`prog_ID`) REFERENCES `programa` (`progID`),
  CONSTRAINT `fk_inscripcion_participantes_1` FOREIGN KEY (`part_ID`) REFERENCES `participantes` (`partID`)
) ENGINE=InnoDB AUTO_INCREMENT=16 DEFAULT CHARSET=utf8;

/*Table structure for table `programa_material` */

DROP TABLE IF EXISTS `programa_material`;

CREATE TABLE `programa_material` (
  `prmaID` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `prog_ID` int(11) unsigned NOT NULL,
  `prma_description` varchar(100) NOT NULL,
  `prma_quantity` int(4) unsigned NOT NULL,
  `prma_cost` decimal(9,2) unsigned NOT NULL,
  `prma_date_created` datetime DEFAULT NULL,
  `prma_date_modified` datetime DEFAULT NULL,
  `prma_status` bit(1) NOT NULL DEFAULT b'1',
  PRIMARY KEY (`prmaID`),
  KEY `fk_programa_material_programa_1` (`prog_ID`),
  CONSTRAINT `fk_programa_material_programa_1` FOREIGN KEY (`prog_ID`) REFERENCES `programa` (`progID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Table structure for table `programa_private` */

DROP TABLE IF EXISTS `programa_private`;

CREATE TABLE `programa_private` (
  `prprID` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `prog_ID` int(11) unsigned NOT NULL,
  `enti_ID` int(11) unsigned NOT NULL,
  PRIMARY KEY (`prprID`),
  KEY `fk_programa_private_programa_1` (`prog_ID`),
  KEY `fk_programa_private_entidad_1` (`enti_ID`),
  CONSTRAINT `fk_programa_private_entidad_1` FOREIGN KEY (`enti_ID`) REFERENCES `entidad` (`entiID`),
  CONSTRAINT `fk_programa_private_programa_1` FOREIGN KEY (`prog_ID`) REFERENCES `programa` (`progID`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
