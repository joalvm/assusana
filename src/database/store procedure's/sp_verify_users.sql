DELIMITER $$

USE `assusana_db`$$

DROP PROCEDURE IF EXISTS `sp_verify_users`$$

CREATE PROCEDURE `sp_verify_users`(
    _role INT,
    _username VARCHAR(50),
    _userpass VARCHAR(60)
)
BEGIN
    DECLARE _id INT;
    
    IF (_role = 2) THEN
    
		SELECT 
			d.`doceID` INTO _id 
		FROM
			docente d 
		WHERE d.`doce_status` = 1 
			AND d.`doce_active` = 1 
			AND d.`doce_locked` = 0 
			AND d.`doce_username` = _username 
			AND d.`doce_password` = _userpass;
			
		IF (_id IS NOT NULL) THEN 
			UPDATE 
				`docente` 
			SET
				`doce_last_access` = NOW() 
			WHERE `doceID` = _id ;
			SELECT 
				d.`doceID` id,
				d.`doce_name` 'name',
				d.`doce_lastname` lastname,
				d.`doce_username` username,
				d.`doce_sexo` gender,
				d.`doce_locked` is_locked,
				d.`doce_pin` pin 
			FROM
				docente d 
			WHERE d.`doce_status` = 1 
				AND d.`doceID` = _id ;
		ELSE 
			SELECT 
				TRUE error,
				"El usuario no se encuentra registrado o no tiene acceso" message ;
		END IF ;
    END IF ;
END$$

DELIMITER ;