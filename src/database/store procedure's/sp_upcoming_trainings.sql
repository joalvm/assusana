DELIMITER $$

USE `assusana_db`$$

DROP PROCEDURE IF EXISTS `sp_upcoming_trainings`$$

CREATE PROCEDURE `sp_upcoming_trainings`()
BEGIN
	SELECT 
		p.`progID` id, 
		c.`capa_name` training_name,
		p.`prog_place` place,
		p.`prog_date` date_realization,
		p.`prog_time_start` time_start,
		p.`prog_time_end` time_finish,
		`get_NumberRegisters`(p.`progID`) number_reg
	FROM `programa` p
	INNER JOIN `capacitacion`c ON c.`capaID` = p.`capa_ID`
	WHERE p.`prog_status` = 1
	AND p.`prog_is_canceled` = 0
	AND CAST(CONCAT(p.`prog_date`, ' ', p.`prog_time_end`) AS DATETIME) > NOW()
	ORDER BY CAST(CONCAT(p.`prog_date`, ' ', p.`prog_time_end`) AS DATETIME) ASC;
END$$

DELIMITER ;