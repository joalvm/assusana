DELIMITER $$

USE `assusana_db`$$

DROP PROCEDURE IF EXISTS `sp_valid_register_participante`$$

CREATE PROCEDURE `sp_valid_register_participante`(
	_request_number VARCHAR(40),
	_program_id INT,
	_participante_id INT,
	_inscription_id INT
)
BEGIN
	DECLARE _exists INT;
	
	SELECT 
		i.`inscID` INTO _exists 
	FROM `inscripcion` i
	WHERE `inscID` = _inscription_id
	AND `part_ID` = _participante_id
	AND `prog_ID` = _program_id
	AND `insc_request_number` = _request_number
	AND `insc_email_confirmed` = 0
	AND `insc_status` = 1;
	
	IF _exists IS NOT NULL THEN 
		
		UPDATE 
			`inscripcion` 
		SET 
			`insc_email_confirmed` = 1, 
			`insc_date_modified` = NOW()
		WHERE `inscID` = _exists;
		
		SELECT 
			i.`inscID` id,
			i.`insc_request_number` request_number,
			p.`part_name` 'name',
			p.`part_lastname` last_name,
			p.`part_dni` dni,
			p.`part_email` email,
			c.`capa_name` training_name,
			ct.`cati_name` type_training_name,
			pr.`prog_date` date_realization,
			pr.`prog_time_start` time_start,
			pr.`prog_time_end` time_end
		FROM `inscripcion` i
		INNER JOIN `participantes` p ON p.`partID` = i.`part_ID`
		INNER JOIN `programa` pr ON pr.`progID` = i.`prog_ID`
		INNER JOIN `capacitacion` c ON c.`capaID` = pr.`capa_ID`
		INNER JOIN `capacitacion_tipo` ct ON ct.`catiID` = c.`cati_ID`
		WHERE `inscID` = _exists;
	
	ELSE
	
		SELECT TRUE error, "Validación no permitada puede que el usuario ya este validado o que este haya sido eliminado" message;
	
	END IF;
END$$

DELIMITER ;