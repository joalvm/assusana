DELIMITER $$

USE `assusana_db`$$

DROP PROCEDURE IF EXISTS `sp_valid_register_docente`$$

CREATE PROCEDURE `sp_valid_register_docente`(
		_username VARCHAR(60),
		_dni VARCHAR(8),
		_role INT,
		_request VARCHAR(60),
		_request_id INT
    )
BEGIN
		DECLARE _exists INT;
		
		SELECT `doceID` INTO _exists 
		FROM docente d
		WHERE d.`doceID` = _request_id
			AND d.`doce_request_number` = _request
			AND d.`doce_dni` = _dni
			AND d.`doce_username` = _username
			AND d.`doce_locked` = 0
			AND d.`doce_active` = 0
			AND d.`doce_status` = 1;
		
		IF (_exists IS NOT NULL) THEN
		
			UPDATE `docente` 
			SET `doce_active` = 1
			WHERE `doceID` = _exists;
			
			SELECT 
				d.`doceID` id,
				d.`doce_name` 'name',
				d.`doce_lastname` lastname,
				d.`doce_sexo` gender,
				d.`doce_email` email,
				d.`doce_dni` dni,
				d.`doce_date_created` date_created,
				d.`doce_specialty` profession,
				d.`doce_username` username,
				d.`doce_pin` pin,
				et.`entp_name` type_entity,
				es.`ensu_name` subtype_entity,
				e.`enti_name` entity
			FROM docente d
				INNER JOIN `entidad` e ON e.`entiID` = d.`enti_ID`
				INNER JOIN `entidad_subtipo` es ON es.`ensuID` = e.`ensu_ID`
				INNER JOIN `entidad_tipo` et ON et.`entpID` = es.`entp_ID`
			WHERE d.`doceID` = _exists;
			
		ELSE 
		
			SELECT TRUE error, "Validación no permitada puede que el usuario ya este validado o que este haya sido eliminado" message;
		
		END IF;
		
    END$$

DELIMITER ;