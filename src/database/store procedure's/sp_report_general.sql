DELIMITER $$

USE `assusana_db`$$

DROP PROCEDURE IF EXISTS `sp_report_general`$$

CREATE PROCEDURE `sp_report_general` () 
BEGIN
    SELECT 
        p.`progID` id,
        p.`prog_date` date_realization,
        p.`prog_time_start` time_start,
        p.`prog_time_end` time_finish,
        ct.`cati_name` type_training,
        t.`tema_title` theme,
        c.`capa_name` training_name,
        d.`dist_name` distrito,
        `get_Departamento` ( p.`dist_ID` ) departamento,
        `get_Provincia` ( p.`dist_ID` ) provincia,
        IF(
            pr.`part_gender` = 0,
            'MASCULINO',
            'FEMENINO'
        ) gender,
        IF(i.`insc_assistance` = 0, 'NO', 'SI') assistance,
        CONCAT(
            pr.`part_name`,
            ' ',
            pr.`part_lastname`
        ) 'names',
        pr.`part_position` cargo 
    FROM
        `inscripcion` i 
        INNER JOIN `participantes` pr 
            ON pr.`partID` = i.`part_ID` 
        INNER JOIN `programa` p 
            ON p.`progID` = i.`prog_ID` 
        INNER JOIN `capacitacion` c 
            ON c.`capaID` = p.`capa_ID` 
        INNER JOIN `distrito` d 
            ON d.`distID` = p.`dist_ID` 
        INNER JOIN `capacitacion_tipo` ct 
            ON ct.`catiID` = c.`cati_ID` 
        INNER JOIN `tema` t 
            ON t.`temaID` = c.`tema_ID` 
    WHERE p.`prog_status` = 1 
        AND i.`insc_status` = 1 
    ORDER BY p.`progID` DESC,
        pr.`part_lastname` ASC ;
END $$

DELIMITER ;