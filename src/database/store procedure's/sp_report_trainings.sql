DELIMITER $$

USE `assusana_db`$$

DROP PROCEDURE IF EXISTS `sp_report_trainings`$$

CREATE PROCEDURE `sp_report_trainings`()
BEGIN
    SELECT 
        p.`prog_date` date_realization,
        p.`prog_time_start` time_start,
        p.`prog_time_end` time_finish,
        p.`prog_place` place,
        p.`prog_address` address,
        d.`dist_name` distrito,
        get_Provincia (d.`distID`) provincia,
        get_Departamento (d.`distID`) departamento,
        c.`capa_name` training_name,
        ct.`cati_name` type_training,
        GROUP_CONCAT(
            CONCAT(
                dc.`doce_name`,
                ' ',
                dc.`doce_lastname`
            ) SEPARATOR ','
        ) coordinador,
        get_NumberInscriptions (p.`progID`, 0) inscriptions,
        get_NumberInscriptions (p.`progID`, 1) attendance 
    FROM
        `programa` p 
        INNER JOIN `programa_docente` pd 
            ON pd.`prog_ID` = p.`progID` 
        INNER JOIN `docente` dc 
            ON dc.`doceID` = pd.`doce_ID` 
        INNER JOIN `capacitacion` c 
            ON c.`capaID` = p.`capa_ID` 
        INNER JOIN `capacitacion_tipo` ct 
            ON ct.`catiID` = c.`cati_ID` 
        INNER JOIN `tema` t 
            ON t.`temaID` = c.`tema_ID` 
        INNER JOIN `distrito` d 
            ON d.`distID` = p.`dist_ID` 
    WHERE p.`prog_status` = 1 
    GROUP BY p.`progID` 
    ORDER BY p.`prog_date` DESC;
END$$

DELIMITER ;