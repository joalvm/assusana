DELIMITER $$

USE `assusana_db`$$

DROP VIEW IF EXISTS `v_inscriptions`$$

CREATE VIEW `assusana_db`.`v_inscriptions` AS 
(SELECT 
    i.inscID inscription_id,
    i.insc_assistance assistance,
    i.insc_email_confirmed email_confirmed,
    i.insc_request_number request_number,
    i.insc_date_created date_inscription,
    p.partID participant_id,
    p.part_name participant_name,
    p.part_lastname participant_lastname,
    p.part_dni participant_dni,
    p.part_email participant_emails,
    p.part_position participant_position,
    p.part_profesion participant_profesion,
    pg.progID programming_id,
    pg.prog_is_canceled its_canceled,
    pg.`prog_is_private` its_customized,
    IF(
        (pg.`prog_date` < CAST(NOW() AS DATE)),
        1,
        0
    ) its_finish,
    c.capaID training_id,
    c.capa_name training_title,
    ct.catiID type_training_id,
    ct.cati_name type_training_name 
FROM
    inscripcion i 
    INNER JOIN participantes p 
        ON p.partID = i.part_ID 
        AND p.part_status = 1 
    INNER JOIN programa pg 
        ON pg.progID = i.prog_ID 
        AND pg.prog_status = 1 
    INNER JOIN capacitacion c 
        ON c.capaID = pg.capa_ID 
        AND c.capa_status = 1 
    INNER JOIN capacitacion_tipo ct 
        ON ct.catiID = c.cati_ID 
WHERE i.`insc_status` = 1) $$

DELIMITER ;
