DELIMITER $$

USE `assusana_db`$$

DROP VIEW IF EXISTS `v_programming_min`$$

CREATE VIEW `v_programming_min` AS
	SELECT 
		p.`progID` programming_id,
		ca.`capaID` training_id,
		ct.`catiID` typetraining_id,
		ca.`capa_name` training_title,
		ca.`capa_description` training_description,
		ct.`cati_name` type_training,
		p.`prog_place` place,
		p.`prog_address` address,
		dp.depaID departamento_id,
		dp.`depa_name` departamento,
		pr.provID provincia_id,
		pr.`prov_name` provincia,
		ds.distID distrito_id,
		ds.`dist_name` distrito,
		p.`prog_date` date_realization,
		p.`prog_time_start` time_start,
		p.`prog_time_end` time_finish,
		p.`prog_limit` limit_max,
		p.`prog_is_canceled` its_canceled,
		p.`prog_is_private` its_customized,
		IF(p.`prog_date` < DATE(NOW()), 1, 0) its_finish,
		en.`entiID` entity_id,
		en.`enti_name` entity_name,
		get_NumberRegisters(p.`progID`) nregisters,
		`get_NumberInscriptions`(p.`progID`, 1) nassistents,
		p.`prog_folder` folder,
		p.`prog_file_name` file_name,
		p.`prog_file_size` file_size,
		p.`prog_date_modified` last_modified
	FROM `programa` p
	INNER JOIN `capacitacion` ca ON ca.`capaID` = p.`capa_ID` AND ca.`capa_status` = 1
	INNER JOIN `capacitacion_tipo` ct ON ct.`catiID` = ca.`cati_ID` AND ct.`cati_status` = 1
	INNER JOIN `distrito` ds ON ds.`distID` = p.`dist_ID`
	INNER JOIN `provincia` pr ON pr.`provID` = ds.`prov_ID`
	INNER JOIN `departamento` dp ON dp.`depaID` = pr.`depa_ID`
	LEFT JOIN `programa_private` pp ON pp.`prog_ID` = p.`progID`
	LEFT JOIN `entidad` en ON en.`entiID` = pp.`enti_ID`
	WHERE p.`prog_status` = 1
	ORDER BY p.`prog_date` DESC;$$
	
DELIMITER ;
