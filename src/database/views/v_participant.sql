DELIMITER $$

USE `assusana_db`$$

DROP VIEW IF EXISTS `v_participant`$$

CREATE VIEW `assusana_db`.`v_participant` AS 
(SELECT 
    p.`partID` id,
    p.`part_name` 'name',
    p.`part_lastname` lastname,
    p.`part_dni` dni,
    p.`part_gender` gender,
    p.`part_email` email,
    p.`part_profesion` profesion,
    p.`part_cellphone` cellphone,
    p.`part_phone` phone,
    p.`part_annex` annex,
    p.`part_position` 'position',
    p.`part_dispacity` dispacity,
    p.`part_details_dispacity` details_dispacity,
    e.`entiID` entity_id,
    e.`enti_name` entity_name,
    es.`ensuID` subtype_entity_id,
    es.`ensu_name` subtype_entity_name,
    et.`entpID` type_entity_id,
    et.`entp_name` type_entity_name,
    di.`distID` distrito_id,
    di.`dist_name` distrito_name,
    pr.`provID` provincia_id,
    pr.`prov_name` provincia_name,
    de.`depaID` departamento_id,
    de.`depa_name` departamento_name,
    p.`part_date_created` date_created,
    p.`part_date_modified` date_modified 
FROM
    `participantes` p 
    INNER JOIN `entidad` e 
        ON e.`entiID` = p.`enti_ID` 
    INNER JOIN `entidad_subtipo` es 
        ON es.`ensuID` = e.`ensu_ID` 
    INNER JOIN `entidad_tipo` et 
        ON et.`entpID` = es.`entp_ID` 
    INNER JOIN `distrito` di 
        ON di.`distID` = p.`dist_ID` 
    INNER JOIN `provincia` pr 
        ON pr.`provID` = di.`prov_ID` 
    INNER JOIN `departamento` de 
        ON de.`depaID` = pr.`depa_ID` 
WHERE p.`part_status` = 1) $$

DELIMITER ;
