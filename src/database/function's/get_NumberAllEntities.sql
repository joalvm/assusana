DELIMITER $$

USE `assusana_db`$$

DROP FUNCTION IF EXISTS `get_NumberAllEntities`$$

CREATE FUNCTION `get_NumberAllEntities`() 
RETURNS INT(11)
DETERMINISTIC
BEGIN
	
	DECLARE _num INT(11);
	
	SELECT COUNT(entiID) INTO _num FROM entidad WHERE `enti_status` = 1;
	
	RETURN _num;
	
END$$

DELIMITER ;