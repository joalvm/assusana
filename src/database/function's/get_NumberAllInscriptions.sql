DELIMITER $$

USE `assusana_db`$$

DROP FUNCTION IF EXISTS `get_NumberAllInscriptions`$$

CREATE FUNCTION `get_NumberAllInscriptions` (
	_attendance BIT
) RETURNS INT (11)
DETERMINISTIC
BEGIN
    DECLARE _num INT (11) ;
    
    IF _attendance = 1 THEN
		SELECT 
			COUNT(inscID) INTO _num 
		FROM
			`inscripcion` 
		WHERE `insc_status` = 1 
			AND `insc_email_confirmed` = 1
			AND `insc_assistance` = 1;
    ELSE
		SELECT 
			COUNT(inscID) INTO _num 
		FROM
			`inscripcion` 
		WHERE `insc_status` = 1
		AND `insc_email_confirmed` = 1;

    END IF;
    
    RETURN _num ;
END $$

DELIMITER ;