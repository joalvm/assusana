DELIMITER $$

USE `assusana_db`$$

DROP FUNCTION IF EXISTS `get_NumberRegisters`$$

CREATE FUNCTION `get_NumberRegisters`(
	_programming_id INT
) RETURNS INT(11)
BEGIN

	DECLARE _count INT;
	
	SELECT 
		COUNT(`inscID`) INTO _count 
	FROM `inscripcion` 
	WHERE `insc_status` = 1
	AND `prog_ID` = _programming_id;
	
	RETURN _count;
	
END$$

DELIMITER ;