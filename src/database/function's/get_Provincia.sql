DELIMITER $$

USE `assusana_db`$$

DROP FUNCTION IF EXISTS `get_Provincia`$$

CREATE FUNCTION `get_Provincia`(
	_idDistrito INT (11)
)
RETURNS VARCHAR(250) 
CHARSET utf8
DETERMINISTIC
BEGIN
	DECLARE _depart VARCHAR (250);
    SELECT 
        p.prov_name INTO _depart 
    FROM
        `distrito` di 
        INNER JOIN `provincia` p 
            ON p.`provID` = di.`prov_ID`
    WHERE di.`distID` = _idDistrito 
    GROUP BY di.`distID`;
    RETURN _depart;
END$$

DELIMITER ;