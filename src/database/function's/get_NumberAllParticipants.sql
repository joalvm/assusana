DELIMITER $$

USE `assusana_db`$$

DROP FUNCTION IF EXISTS `get_NumberAllParticipants`$$

CREATE FUNCTION `get_NumberAllParticipants`() 
RETURNS INT(11)
DETERMINISTIC
BEGIN
	
	DECLARE _num INT(11);
	
	SELECT 
		COUNT(p.partID) INTO _num 
	FROM `participantes` p
	INNER JOIN 
		inscripcion i ON i.`part_ID` = p.`partID`
	WHERE 
	p.`part_status` = 1
	AND i.`insc_status` = 1;
	
	RETURN _num;
	
END$$

DELIMITER ;