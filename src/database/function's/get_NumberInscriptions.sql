DELIMITER $$

USE `assusana_db`$$

DROP FUNCTION IF EXISTS `get_NumberInscriptions`$$

CREATE FUNCTION `get_NumberInscriptions` (
    _id_programming INT (11),
    _attendance BIT
) RETURNS INT (11)
DETERMINISTIC
BEGIN
    DECLARE _num INT (11) ;
    IF _attendance = 1 
    THEN 
    SELECT 
        COUNT(inscID) INTO _num 
    FROM
        `inscripcion` 
    WHERE `insc_status` = 1 
        AND `insc_email_confirmed` = 1 
        AND `insc_assistance` = 1 
        AND `prog_ID` = _id_programming ;
    ELSE 
    SELECT 
        COUNT(inscID) INTO _num 
    FROM
        `inscripcion` 
    WHERE `insc_status` = 1 
        AND `insc_email_confirmed` = 1 
        AND `prog_ID` = _id_programming ;
    END IF ;
    RETURN _num ;
END $$

DELIMITER ;