DELIMITER $$

USE `assusana_db`$$

DROP FUNCTION IF EXISTS `get_NumberProgramming`$$

CREATE FUNCTION `get_NumberProgramming` (_with_cancel BIT) 
RETURNS INT (11) 
DETERMINISTIC
BEGIN

    DECLARE _num INT (11) ;
    
    IF (_with_cancel = 1) THEN 
		SELECT 
			COUNT(`progID`) INTO _num 
		FROM
			`programa` 
		WHERE `prog_status` = 1 ;
    ELSE 
		SELECT 
			COUNT(`progID`) INTO _num 
		FROM
			`programa` 
		WHERE `prog_status` = 1 
			AND `prog_is_canceled` = 0 ;
    END IF ;
    
    RETURN _num ;
    
END $$

DELIMITER ;