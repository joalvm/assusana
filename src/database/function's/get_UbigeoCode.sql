DELIMITER $$

USE `assusana_db`$$

DROP FUNCTION IF EXISTS `get_UbigeoCode`$$

CREATE FUNCTION `get_UbigeoCode`(`_distID` INT)
RETURNS VARCHAR(6) 
CHARSET utf8
DETERMINISTIC
BEGIN
    DECLARE _ubigeo VARCHAR (6) ;
    SELECT 
        CONCAT(
            dp.`depa_key`,
            p.`prov_key`,
            d.`dist_key`
        ) INTO _ubigeo 
    FROM
        `distrito` d 
        INNER JOIN `provincia` p 
            ON p.`provID` = d.`prov_ID` 
        INNER JOIN `departamento` dp 
            ON p.`depa_ID` = dp.`depaID` 
    WHERE d.distID = _distID 
    ORDER BY depa_name ;
    RETURN _ubigeo ;
END$$

DELIMITER ;