DELIMITER $$

USE `assusana_db`$$

DROP FUNCTION IF EXISTS `get_NumberGenderForProgramming`$$

CREATE FUNCTION `get_NumberGenderForProgramming`(
		_gender BIT,
		_id_programming INT(11)
) RETURNS INT(11)
DETERMINISTIC
BEGIN
		DECLARE _num INT(11);
		
		SELECT 
			COUNT(`partID`) INTO _num 
		FROM `participantes` pr 
		INNER JOIN `inscripcion` i ON i.`inscID` = pr.`partID`
		WHERE pr.`part_status` = 1 
		AND pr.`part_gender` = _gender
		AND i.`prog_ID` = _id_programming;
		
		RETURN _num;
    END$$

DELIMITER ;