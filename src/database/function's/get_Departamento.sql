DELIMITER $$

USE `assusana_db`$$

DROP FUNCTION IF EXISTS `get_Departamento`$$

CREATE FUNCTION `assusana_db`.`get_Departamento` (_idDistrito INT (11)) RETURNS VARCHAR (250) 
BEGIN
    DECLARE _depart VARCHAR (250) ;
    SELECT 
        de.`depa_name` INTO _depart 
    FROM
        `distrito` di 
        INNER JOIN `provincia` p 
            ON p.`provID` = di.`prov_ID` 
        INNER JOIN `departamento` de 
            ON de.`depaID` = p.`depa_ID` 
    WHERE di.`distID` = _idDistrito 
    GROUP BY de.`depaID` ;
    RETURN _depart ;
END $$

DELIMITER ;