<?php if (! defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class Login_model extends CI_Model
{
    public $decripter_length = 6;

    public function __construct()
    {
        parent::__construct();
        date_default_timezone_set('America/Lima');
        setlocale(LC_ALL, array('es_PE.UTF-8','es_PE@amer','es_PE','peru'));
    }

    public function verify_users($username, $password, $role)
    {
        $parameters = ['_role' => $role, '_username' => $username, '_userpass' => $password];

        $result = $this->db->query("CALL sp_verify_users(?, ?, ?)", $parameters)->result();

        if (! isset($result[0]->error)) {
            $_array = array(
                'id' => $result[0]->id,
                'name' => $result[0]->name,
                'lastname' => $result[0]->lastname,
                'username' => $result[0]->username,
                'gender' => $result[0]->gender,
                'pin' => $result[0]->pin,
                'role' => $role,
                'logged_in' => true
            );

            $this->session->set_userdata($_array);

            return [
                "error" => false,
                "message" => "Usuario autentificado... Redireccionando.",
                "url" => base_url('/docente/dashboard.html')
            ];
        } else {
            return [
                "error" => true,
                "message" => $result[0]->message
            ];
        }
    }
}

/* End of file Login_model.php */
/* Location: ./application/models/Login_model.php */
