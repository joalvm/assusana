<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Attendance_model extends CI_Model {

	public $data_program;

	public function __construct()
	{
		parent::__construct();
		date_default_timezone_set('America/Lima');
		setlocale(LC_ALL, array('es_PE.UTF-8','es_PE@amer','es_PE','peru'));
	}

	public function insert($data, $programming_id)
	{
		$time = (new DateTime())->format('H:i:s');
		$date = (new Datetime())->format('Y-m-d H:i:s');
		$program = $this->data_program;

		$update_arr = [];

		foreach ($data as $key => $value) 
		{
			$time_finish = new DateTime($program->date_realization . " " . $program->time_finish);
			$time_start =  new DateTime($program->date_realization . " " . $program->time_start);
			$now = new DateTime();
			$time_assist = NULL;

			if( $now > $time_finish ) 
			{
				$time_assist = $program->time_start;
			}
			else if( $now < $time_start ) 
			{
				$time_assist = $program->time_start;
			}
			else
			{
				$time_assist = $now->format('H:i:s');
			}

			if ( isset($value['time']) && ((boolean)$value['assi']) ) 
			{
				$time_assist = $value['time'];
			} 
			else if( !((boolean)$value['assi']) ) 
			{
				$time_assist = NULL;
			}

			array_push($update_arr, [
				'inscID' => $value['insc'],
				'insc_assistance' => (boolean)$value['assi'],
				'insc_time_assistance' => $time_assist,
				'insc_date_modified' => $date
			]);	
		}

		return $this->db->update_batch('inscripcion', $update_arr, 'inscID');
	}

	public function get_programming()
	{
		$keys = [
			'pd.prdoID id',
			'pd.prog_ID programming_id',
			'pd.doce_ID docente_id',
			'c.capa_name training_title',
			'p.prog_date date_realization',
			'p.prog_is_canceled its_canceled',
			'p.prog_time_start time_start',
			'p.prog_time_end time_end',
			'p.prog_folder folder',
			'p.prog_file_name file_name',
			'p.prog_file_size file_size'
		];

		$this->db->select(implode(',', $keys))
			->from('programa_docente pd')
				->join('programa p', 'p.progID = pd.prog_ID', 'inner')
				->join('capacitacion c', 'c.capaID = p.capa_ID', 'inner')
			->where([
					'p.prog_status' => TRUE,
					'p.prog_is_canceled' => FALSE,
					'pd.doce_ID' => $this->session->id
				])
				->group_by('pd.prog_ID')
				->order_by('p.prog_date DESC, p.prog_time_start DESC');

		$table = $this->db->get()->result();

		foreach ($table as $row => &$cell) {
			$cell->folder = base_url($cell->folder);
		}

		return $table;
	}

	public function get_participants($programming_id)
	{
		$keys = [
			"i.inscID inscription_id",
			"i.prog_ID programming_id",
			"i.part_ID participant_id",
			"p.part_name 'name'",
			"p.part_lastname last_name",
			"p.part_profesion profesion",
			"p.part_dni dni",
			"p.part_email email",
			"p.part_gender gender",
			"p.part_dispacity dispacity",
			"i.insc_assistance attendance",
			"i.insc_time_assistance time_attendance",
			"i.insc_nota nota",
			"i.insc_satisfaction satisfaction",
			"e.enti_name entity_name"
		];

		$this->db->select(implode(',', $keys))
			->from('inscripcion i')
				->join('participantes p', 'p.partID = i.part_ID AND p.part_status = 1', 'inner')
				->join('entidad e', 'e.entiID = p.enti_ID', 'inner')
				->join('programa pg', 'pg.progID = i.prog_ID AND pg.prog_status = 1', 'inner')
			->where([
				'i.insc_status' => TRUE,
				'i.insc_email_confirmed' => TRUE,
				'pg.prog_is_canceled' => FALSE,
				'i.prog_ID' => $programming_id
			])
			->order_by('p.part_lastname ASC');

		$table = $this->db->get()->result();

		foreach ($table as $row => &$cell) {
			$cell->entity_name = ucfirst(mb_strtolower($cell->entity_name, 'UTF-8'));
			$cell->inscription_id = (int) $cell->inscription_id;
			$cell->programming_id = (int) $cell->programming_id;
			$cell->participant_id = (int) $cell->participant_id;
			$cell->nota = is_null($cell->nota) ? NULL : ((int) $cell->nota);
			$cell->satisfaction = is_null($cell->satisfaction) ? NULL : (int) $cell->satisfaction;
			$cell->gender = (boolean) $cell->gender;
			$cell->attendance = (boolean) $cell->attendance;
			$cell->dispacity = (boolean) $cell->dispacity;
			
			if ( ! is_null($cell->time_attendance) )
				$cell->time_attendance = date('Y-m-d') . " " . $cell->time_attendance;
		}

		return $table;
	}

}

/* End of file Attendance_model.php */
/* Location: ./application/models/Attendance_model.php */