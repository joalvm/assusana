<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Evaluation_model extends CI_Model {

	public $_base_path = "public/";
	public $_rel_path = "";
	public $_folder = "";


	public function __construct()
	{
		parent::__construct();
		$this->load->library('upload');
	}

	public function save_file( $file, $fullPath )
	{
		$folder = "";

		$fullPath = $this->_base_path . $fullPath;

		foreach (explode("/", $fullPath) as $key => $value) 
		{
			if ( !empty($value) ) 
				$folder = $this->create_folder($folder.$value);
		}
		
        $config = [
        	'upload_path' 		=> $folder,
        	'allowed_types' 	=> 'pdf|doc|docx',
        	'max_size' 			=> '6000',
        	'overwrite' 		=> FALSE,
        	'encrypt_name' 		=> TRUE,
        	'remove_spaces' 	=> TRUE,
        	'file_ext_tolower'	=> TRUE
        ];
 
        $this->load->library( 'upload', $config );
        $this->upload->initialize( $config );

        $confirm = $this->upload->do_upload( $file );

        if ( ! $confirm ) 
        {
        	$this->output
				->set_content_type('application/json')
				->set_status_header(400)
        		->set_output( json_encode([
	        			"error" => TRUE,
	        			"message" => $this->upload->display_errors()
	        		])
        		)->_display();
        		
            exit();
        } 
        else 
        {
            $fileInfo = $this->upload->data();

            $result = [
            	"folder" => $folder,
            	"data" => $fileInfo
            ];
        }

        return $result;
	}

	public function insert_file($programming_id, $folder, $file_name, $file_size)
	{
		$date = (new DateTime())->format('Y-m-d H:i:s');

		$params = [
			"prog_folder" => $folder,
			"prog_file_name" => $file_name,
			"prog_file_size" => number_format($file_size, 2, '.', ''),
			"prog_date_modified" => $date
		];

		$this->db->where(["progID" => $programming_id]);

		$result = $this->db->update('programa', $params);
		
		return $result;
	}

	public function insert_evaluation($post_data)
	{
		$date = (new Datetime())->format('Y-m-d H:i:s');

		$update_arr = [];

		foreach ($post_data as $key => $value) 
		{
			array_push($update_arr, [
				'inscID' => $key,
				'insc_nota' => empty($value) ? NULL : $value,
				'insc_date_modified' => $date
			]);
		}

		$result = $this->db->update_batch('inscripcion', $update_arr, 'inscID');

		return $result;
	}

	public function create_folder( $xpath, $index = 0 )
    {
        $path = trim( $xpath );
        $path = rtrim( $path, "/" );

        if ( ! file_exists($path) ) {
            if ( mkdir( $path, 0777, TRUE ) )
                return $path . "/";
        }
        else 
        {
            if ( is_dir($path))
                return $path . "/";
            else
                $this->create_folder( ( $path. '_' . $index . '/'), ($index + 1) );
        }
    }

}

/* End of file Evaluation_model.php */
/* Location: ./application/models/Evaluation_model.php */