<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Inscription_model extends CI_Model {

	public $variable;

	public function __construct()
	{
		parent::__construct();
		date_default_timezone_set('America/Lima');
		setlocale(LC_ALL, array('es_PE.UTF-8','es_PE@amer','es_PE','peru'));
	}

}

/* End of file Inscription_model.php */
/* Location: ./application/models/Inscription_model.php */