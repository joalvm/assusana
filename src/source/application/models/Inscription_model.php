<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Inscription_model extends CI_Model {

	public $ID = 0;
	public $PGID = 0;
	public $data = [];

	public function __construct()
	{
		parent::__construct();
		date_default_timezone_set('America/Lima');
		setlocale(LC_ALL, array('es_PE.UTF-8','es_PE@amer','es_PE','peru'));
	}

	public function delete()
	{
		$date = (new DateTime())->format('Y-m-d H:i:s');

		$params = [
			'insc_status' => FALSE,
			'insc_date_modified' => $date
		];

		$this->db->where(['inscID' => $this->ID]);
		
		return $this->db->update('inscripcion', $params);
	}

	public function active()
	{
		$date = (new DateTime())->format('Y-m-d H:i:s');

		$params = [
			'insc_email_confirmed' => TRUE,
			'insc_date_modified' => $date
		];

		$this->db->where(['inscID' => $this->ID]);
		
		return $this->db->update('inscripcion', $params);
	}

	public function get_ALLInscriptions() 
	{
		$where = [
			"i.insc_status" => TRUE,
		];

		return $this->qResults($where);
	}

	public function get_inscription_from_programming()
	{
		$where = [
			"i.insc_status" => TRUE,
			"pg.progID" => $this->PGID
		];

		return $this->qResults($where);
	}

	public function check_ifs_canceled_programming()
	{
		$this->db->select('prog_is_canceled')
			->from('programa')
			->where(['progID' => $this->PGID, "prog_status" => TRUE]);

		$res = $this->db->get()->result();

		return (!empty($res)) ? (boolean)$res[0]->prog_is_canceled : FALSE;
	}

	public function update_info_participant($participant_id)
	{
		$date = (new DateTime())->format('Y-m-d H:i:s');
		$result = new stdClass;

		$param = [
			'part_name' => my_ucwords($this->data['name']), 
			'part_lastname' => my_ucwords($this->data['lastname']),
			'part_dni' => $this->data['dni'],
			'part_date_modified' => $date
		];

		$this->db->where(['partID' => $participant_id]);
		$confirm = $this->db->update('participantes', $param);

		if( $confirm )
		{
			$result->error = FALSE;
			$result->message = "El participante ha sido modificado";
		}
		else
		{
			$result->error = TRUE;
			$result->message = "Error al momento de modificar, vuelva a intentarlo.";
		}

		return $result;
	}

	public function qResults($where = [], $groupby = '') 
	{
		$keys = [
			"i.inscID inscription_id",
			"i.insc_assistance assistance",
			"i.insc_email_confirmed email_confirmed",
			"i.insc_request_number request_number",
			"i.insc_date_created date_inscription",
			"p.partID participant_id",
			"p.part_name participant_name",
			"p.part_lastname participant_lastname",
			"p.part_dni participant_dni",
			"p.part_email participant_emails",
			"p.part_position participant_position",
			"p.part_profesion participant_profesion",
			"p.part_dispacity dispacity",
			"pg.progID programming_id",
			"pg.prog_is_canceled its_canceled",
			"c.capaID training_id",
			"c.capa_name training_title",
			"ct.catiID type_training_id",
			"ct.cati_name type_training_name"
		];

		$this->db->select(implode(',', $keys))
			->from('inscripcion i')
				->join('participantes p', 'p.partID = i.part_ID AND p.part_status = 1', 'inner')
				->join('programa pg', 'pg.progID= i.prog_ID AND pg.prog_status = 1', 'inner')
				->join('capacitacion c', 'c.capaID = pg.capa_ID AND c.capa_status = 1', 'inner')
				->join('capacitacion_tipo ct', 'ct.catiID = c.cati_ID', 'inner')
				->order_by('p.part_lastname ASC');

		$this->db->where($where);

		if ( !empty($groupby) )
			$this->db->group_by($groupby);

		$table = $this->db->get()->result();

		foreach ($table as $row => &$cell) {
			$cell->dispacity = (boolean) $cell->dispacity;
			$cell->assistance = (boolean) $cell->assistance;
			$cell->email_confirmed = (boolean) $cell->email_confirmed;
			$cell->its_canceled = (boolean) $cell->its_canceled;
			$cell->inscription_id = (int) $cell->inscription_id;
			$cell->participant_id = (int) $cell->participant_id;
			$cell->programming_id = (int) $cell->programming_id;
			$cell->training_id = (int) $cell->training_id;
			$cell->type_training_id = (int) $cell->type_training_id;
		}

		return $table;
	}

}

/* End of file Inscription_model.php */
/* Location: ./application/models/Inscription_model.php */