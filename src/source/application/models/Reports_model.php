<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Reports_model extends CI_Model {

	public $variable;

	public function __construct()
	{
		parent::__construct();
		date_default_timezone_set('America/Lima');
		setlocale(LC_ALL, array('es_PE.UTF-8','es_PE@amer','es_PE','peru'));
	}

	public function get_info_certified( $ID, $one = TRUE )
	{
		$where = [
			'i.insc_email_confirmed' => TRUE,
			'i.insc_status' => TRUE,
			'i.insc_assistance' => TRUE
		];

		if( $one ) {
			$where['i.inscID'] = $ID;
		} else {
			$where['i.prog_ID'] = $ID;
		}

		$strQuery = "c.capa_name training_name,
					ct.cati_name type_training_name,
					p.prog_date date_realization,
					get_Departamento(p.dist_ID) departamento,
					pr.part_name participant_name,
					pr.part_lastname participant_lastname";

		$this->db->select($strQuery)
			->from('inscripcion i')
				->join('programa p', 'p.progID = i.prog_ID', 'inner')
				->join('participantes pr', 'pr.partID = i.part_ID', 'inner')
				->join('capacitacion c', 'c.capaID = p.capa_ID', 'inner')
				->join('capacitacion_tipo ct', 'ct.catiID = c.cati_ID', 'inner')
			->where($where);

		$table = $this->db->get()->result();

		foreach ($table as $row => &$cell) {
			$cell->departamento = ucfirst(mb_strtolower($cell->departamento, 'UTF-8'));
		}

		return $table;
	}

	public function get_cant_participants_for_type_entity()
	{
		$this->db->select('et.entpID id, et.entp_name name, COUNT(i.inscID) cant')
			->from('inscripcion i')
				->join('participantes p', 'p.partID = i.part_ID', 'inner')
				->join('entidad e', 'e.entiID = p.enti_ID', 'inner')
				->join('entidad_subtipo es', 'es.ensuID = e.ensu_ID', 'inner')
				->join('entidad_tipo et', 'et.entpID = es.entp_ID', 'inner')
			->where([
				'i.insc_status' => TRUE,
				'i.insc_email_confirmed' => TRUE,
				'i.insc_assistance' => TRUE
			])
			->group_by('et.entpID');

		$table = $this->db->get()->result();

		foreach ($table as $row => &$cell)
		{
			$cell->id 	= (int) $cell->id;
			$cell->cant = (int) $cell->cant;
			$cell->name = my_ucwords_ns($cell->name);
		}

		return $table;
	}

	public function get_report_general() 
	{
		return $this->db->query('CALL sp_report_general()')->result();
	}

	public function get_report_training() 
	{
		$table = $this->db->query('CALL sp_report_trainings()')->result();

		foreach ($table as $row => &$cell) {
			$docentes_arr = explode(',', $cell->coordinador);
			$docentes_unique = array_unique($docentes_arr);
			$cell->coordinador = implode(', ', $docentes_unique);
			$cell->time_start = date_format((new DateTime($cell->time_start)), 'h:ia');
			$cell->time_finish = date_format((new DateTime($cell->time_finish)), 'h:ia');
			$cell->date_realization = date_format((new DateTime($cell->date_realization)), 'd/m/Y');
		}

		return $table;
	}
}

/* End of file Reports_model.php */
/* Location: ./application/models/Reports_model.php */
