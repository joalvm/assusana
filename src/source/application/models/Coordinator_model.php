<?php if (! defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class Coordinator_model extends CI_Model
{
    public $ID;
    public $data = [];
    public $usuario = "";
    public $nrequest = "";
    public $data_find = [];

    public function __construct()
    {
        parent::__construct();
        $this->load->library('email');

        date_default_timezone_set('America/Lima');
        setlocale(LC_ALL, array('es_PE.UTF-8','es_PE@amer','es_PE','peru'));
    }

    public function show()
    {
        $strQuery = "d.doceID id, d.doce_name 'name', d.doce_lastname lastname, d.doce_dni dni, 
		IF(d.doce_sexo, 'Mujer', 'Hombre') gender, d.doce_email email, d.doce_locked locked, 
		d.doce_date_modified last_modified";

        $this->db->select($strQuery)
            ->from('docente d')
            ->where(["d.doce_status" => true]);

        return $this->db->get()->result();
    }

    public function find($ID)
    {
        return $this->consult_db(['d.doceID' => $ID, 'd.doce_status' => true]);
    }

    public function insert($data = [])
    {
        $date = (new DateTime())->format('Y-m-d H:i:s');

        $lastname_arr = explode(" ", $data["lastname"]);
        
        $search = ['á','é','í','ó','ú','Á','É','Í','Ó','Ú','ü','Ü','Ë','ë','Ï','ï','Ö','ö','Ä','ä','@','-','/','$','%'];
        $replace = ['a','e','i','o','u','A','E','I','O','U','u','U','E','e','I','i','O','o','A','a','','','','',''];
        
        $lastname = str_replace($search, $replace, $lastname_arr[0]);
        
        $this->usuario = "c" . mb_strtolower($lastname, 'UTF-8') . rand(100, 1000);
        
        $this->nrequest = random_string("sha1");
        
        $queryData = [
            "enti_ID" => $data["entity"],
            "doce_name" => my_ucwords_ns($data["name"]),
            "doce_lastname" => my_ucwords_ns($data["lastname"]),
            "doce_dni" => $data["dni"],
            "doce_sexo" => (boolean)$data["gender"],
            "doce_specialty" => !empty($data["profession"]) ? my_ucfirst($data["profession"]) : null,
            "doce_email" => $data["email"],
            "doce_active" => ($data['send_email'] == 0) ? true : false,
            "doce_request_number" => $this->nrequest,
            "doce_username" => $this->usuario,
            "doce_password" => sha1("123456"),
            "doce_date_created" => $date,
            "doce_date_modified" => $date
        ];

        $this->db->insert("docente", $queryData);

        return $this->db->insert_id();
    }

    public function update()
    {
        $date = (new DateTime())->format('Y-m-d H:i:s');

        $queryData = [
            "enti_ID" => $this->data["entity"],
            "doce_name" => my_ucwords_ns($this->data["name"]),
            "doce_lastname" => my_ucwords_ns($this->data["lastname"]),
            "doce_dni" => $this->data["dni"],
            "doce_sexo" => (boolean)$this->data["gender"],
            "doce_specialty" => !empty($this->data["profession"]) ? my_ucfirst($this->data["profession"]) : null,
            "doce_email" => $this->data["email"],
            "doce_date_modified" => $date
        ];

        $this->db->where(['doceID' => $this->ID]);

        return $this->db->update("docente", $queryData);
    }

    public function delete($ID)
    {
        $date = (new DateTime())->format('Y-m-d H:i:s');
        $data = [
            "doce_status" => false,
            "doce_date_modified" => $date
        ];

        $this->db->where(['doceID' => $ID]);

        return $this->db->update('docente', $data);
    }

    public function exists_dni($dni = "")
    {
        $get_docente = $this->consult_db(['d.doce_dni' => $dni]);
        return (! empty($get_docente));
    }

    public function restore_password($ID)
    {
        $date = (new DateTime())->format('Y-m-d H:i:s');

        $newPass = random_string('alnum');

        $data = [
            "doce_password" => sha1($newPass),
            "doce_date_modified" => $date
        ];

        $this->db->where(["doceID" => $ID]);

        $confirm = $this->db->update('docente', $data);

        if ($confirm) {
            return $this->sendMail_restore($this->data_find[0]->email, [
                "fullname" => ($this->data_find[0]->name . " " . $this->data_find[0]->lastname),
                "username" => $this->data_find[0]->username,
                "password" => $newPass
            ]);
        } else {
            return false;
        }
    }

    public function locked_session($ID, $ACTION)
    {
        $date = (new DateTime())->format('Y-m-d H:i:s');

        $data = [
            "doce_locked" => (boolean)$ACTION,
            "doce_date_modified" => $date
        ];

        $this->db->where(["doceID" => $ID]);

        return $this->db->update('docente', $data);
    }

    /*
    CONSULTA GENERAL:
    - No fitral predeterminadamente por status porque se buscaran los dni de todos sin exclusion
     */
    private function consult_db($_where = [])
    {
        $strQuery = "d.doceID id, d.doce_name 'name', d.doce_lastname lastname, d.doce_dni dni, 
		d.doce_sexo gender, d.doce_username username, d.doce_active active, d.doce_locked locked, 
		d.doce_email email, d.doce_specialty profesion, et.entpID type_id, et.entp_name type_name, 
		es.ensuID subtype_id, es.ensu_name subtype_name, d.enti_ID entidad_id, e.enti_name entidad_name, 
		d.doce_date_modified last_modified";

        $this->db->select($strQuery)
            ->from('docente d')
            ->join('entidad e ', 'e.entiID = d.enti_ID', 'inner')
            ->join('entidad_subtipo es', 'es.ensuID = e.ensu_ID', 'inner')
            ->join('entidad_tipo et', 'et.entpID = es.entp_ID', 'inner')
            ->where($_where);

        return $this->db->get()->result();
    }

    public function sendMail_register($input)
    {
        $config = $this->config->item('email');

        $emails = explode(",", $input['email']);

        foreach ($emails as $key => $value) {
            $emails[$key] = trim($value);
        }

        $html = $this->load->view('emails/docente_reg_view', $input, true);
        
        $this->email->initialize($config);
        $this->email->from('capacita@treffpunkt-liebefeld.ch', "Docente :: Capacitaciones 2016");
        $this->email->to($emails);
        $this->email->subject("Verificación de Inscripción");
        $this->email->message($html);

        $confirm = $this->email->send();
        
        if (! $confirm) {
            return false;
        } else {
            return true;
        }
    }

    public function sendMail_restore($emails, $input)
    {
        $config = $this->config->item('email');

        $emails_arr = explode(",", $emails);

        foreach ($emails_arr as $key => $value) {
            $emails_arr[$key] = trim($value);
        }

        $html = $this->load->view('emails/restore_password_view', $input, true);
        
        $this->email->initialize($config);
        $this->email->from('capacita@treffpunkt-liebefeld.ch', "Docente :: Capacitaciones 2016");
        $this->email->to($emails_arr);
        $this->email->subject("Restauración de Acceso");
        $this->email->message($html);

        $confirm = $this->email->send();
        
        if (! $confirm) {
            return false;
        } else {
            return true;
        }
    }
}

/* End of file Coordinator_model.php */
/* Location: ./application/models/Coordinator_model.php */
