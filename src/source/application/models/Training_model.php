<?php if (! defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class Training_model extends CI_Model
{
    public $ID;
    public $data;
    public $prg_data;

    public function __construct()
    {
        parent::__construct();
        $this->load->library('email');
        date_default_timezone_set('America/Lima');
        setlocale(LC_ALL, array('es_PE.UTF-8','es_PE@amer','es_PE','peru'));
    }

    /**
    * METODOS PARA EL REGISTRO DE CAPACITACIONES
    */
    public function show_register()
    {
        $strQuery = "c.capaID id, ct.catiID type_id, ct.cati_name type_name, c.capa_name title,
		c.capa_description description, c.capa_date_modified last_modified";

        $this->db->select($strQuery)
            ->from('capacitacion c')
                ->join('capacitacion_tipo ct', 'ct.catiID = c.cati_ID AND ct.cati_status = 1', 'inner')
            ->where(['c.capa_status' => true]);

        return $this->db->get()->result();
    }

    public function find_register()
    {
        $strQuery = "c.capaID id, c.tema_ID theme_id, ct.catiID type_id, ct.cati_name type_name,
		c.capa_name title, c.capa_description description, c.capa_date_modified last_modified";

        $this->db->select($strQuery)
            ->from('capacitacion c')
                ->join('capacitacion_tipo ct', 'ct.catiID = c.cati_ID AND ct.cati_status = 1', 'inner')
            ->where(['c.capa_status' => true, 'c.capaID' => $this->ID]);

        $table = $this->db->get()->result();

        return empty($table) ? [] : $table[0];
    }

    public function insert_register()
    {
        $date = (new DateTime())->format('Y-m-d H:i:s');

        $param = [
            "cati_ID" => $this->data["type_training"],
            "tema_ID" => $this->data['theme'],
            "capa_name" => my_ucfirst($this->data["title"]),
            "capa_description" => empty($this->data["description"]) ? null : my_ucfirst($this->data["description"]),
            "capa_date_created" => $date,
            "capa_date_modified" => $date
        ];

        $this->db->insert('capacitacion', $param);
        $this->ID = $this->db->insert_id();

        if ($this->ID > 0) {
            return true;
        } else {
            throw new Exception("NO SE PUDO AGREGAR LA CAPACITACIÓN, VUELVE A INTENTARLO", 400);
        }
    }

    public function update_register($training_id)
    {
        $date = (new DateTime())->format('Y-m-d H:i:s');

        $param = [
            "cati_ID" => $this->data["type_training"],
            "tema_ID" => $this->data['theme'],
            "capa_name" => my_ucfirst($this->data["title"]),
            "capa_description" => empty($this->data["description"]) ? null : my_ucfirst($this->data["description"]),
            "capa_date_modified" => $date
        ];

        $this->db->where(['capaID' => $training_id]);

        if ($this->db->update('capacitacion', $param)) {
            return true;
        } else {
            throw new Exception("NO SE PUDO AGREGAR LA CAPACITACIÓN, VUELVE A INTENTARLO", 400);
        }
    }

    public function delete_register($training_id)
    {
        $date = (new DateTime())->format('Y-m-d H:i:s');

        $param = [
            "capa_status" => false,
            "capa_date_modified" => $date
        ];

        $this->db->where(['capaID' => $training_id]);

        $confirm = $this->db->update('capacitacion', $param);

        if ($confirm) {
            return true;
        } else {
            throw new Exception("NO SE PUDO AGREGAR LA CAPACITACIÓN, VUELVE A INTENTARLO", 400);
        }
    }

    public function find_theme_of_training($training_id)
    {
        $strQuery = "t.temaID id, t.tema_title title, t.tema_description description,
		st.suteID subtheme_id, st.sute_name subtheme_title, st.sute_description subtheme_description,
		t.tema_date_modified last_modified";

        $where = [
            't.tema_status' => true,
            'c.capaID' => $training_id
        ];

        $this->db->select($strQuery)
            ->from('tema t')
                ->join('sub_tema st', 'st.tema_ID = t.temaID', 'left')
                ->join('capacitacion c', 'c.tema_ID = t.temaID', 'inner')
            ->where($where);

        $rtable = $this->db->get()->result();
        $table = [];
        $subthemes_arr = [];
        $curr_id = 0;

        foreach ($rtable as $row => $cell) {
            $curr_id = $cell->id;

            $sthemes = new stdClass;

            if ($cell->subtheme_id != null) {
                $sthemes->id = (int) $cell->subtheme_id;
                $sthemes->title = $cell->subtheme_title;
                $sthemes->description = $cell->subtheme_description;

                array_push($subthemes_arr, $sthemes);
            }

            if (isset($rtable[$row + 1])) {
                if ($rtable[$row + 1]->id != $curr_id) {
                    array_push($table, (object)[
                        "id" => (int)$cell->id,
                        "title" => $cell->title,
                        "description" => $cell->description,
                        "last_modified" => $cell->last_modified,
                        "subthemes" => $subthemes_arr
                    ]);

                    $subthemes_arr = [];
                }
            } else {
                array_push($table, (object)[
                    "id" => $cell->id,
                    "title" => $cell->title,
                    "description" => $cell->description,
                    "last_modified" => $cell->last_modified,
                    "subthemes" => $subthemes_arr
                ]);
            }
        }

        return $table;
    }
//


    /**
    * METODOS PARA LA PROGRAMACION DE CAPACITACIONES
    */
    public function show_programing($where = [])
    {
        if (empty($where)) {
            if ($this->session->filter_prg == 2) {
                $where = [
                    'CAST(CONCAT(date_realization, " ", time_finish) AS DATETIME) >=' => 'NOW()',
                    'its_canceled' => 0
                ];
            } elseif ($this->session->filter_prg == 3) {
                $where = [
                    'CAST(CONCAT(date_realization, " ", time_finish) AS DATETIME) <' => 'NOW()',
                ];
            } elseif ($this->session->filter_prg == 4) {
                $where = [
                    'its_canceled' => 1
                ];
            } elseif ($this->session->filter_prg == 5) {
                $where = [
                    'its_customized' => 1
                ];
            } else {
                $where = [];
            }
        }

        $this->db->select('*')
            ->from('v_programming_min')
            ->where($where, [], false);

        $table = $this->db->get()->result();

        foreach ($table as $row => &$cell) {
            $cell->training_title = ucfirst(mb_strtolower($cell->training_title, 'UTF-8'));
        }

        return $table;
    }

    public function show_programing_no_personalize()
    {
        $this->db->select('*')
            ->from('v_programming_min')
            ->where([
                'its_customized' => 0,
                'its_canceled' => 0
            ]);

        $table = $this->db->get()->result();
        $table2 = [];

        foreach ($table as $row => &$cell) {
            $now = new DateTime();
            $date_realization = new DateTime($cell->date_realization . ' ' . $cell->time_finish);

            if ($date_realization >= $now) {
                array_push($table2, $cell);
            }
        }

        return $table2;
    }

    public function find_programming($any = false)
    {
        $where = [];

        if (!$any) {
            $where = [
                'CAST(CONCAT(date_realization, " ", time_finish) AS DATETIME) >=' => 'NOW()',
                'programming_id' => $this->ID,
                'its_canceled' => 0
            ];
        } else {
            $where = [
                'programming_id' => $this->ID,
                'its_canceled' => 0
            ];
        }

        $this->db->select('*')
            ->from('v_programming_min')
            ->where($where, [], false);

        $table = $this->db->get()->result();

        foreach ($table as $row => &$cell) {
            $cell->file_size 		= (float) $cell->file_size;
            $cell->its_canceled 	= (boolean) $cell->its_canceled;
            $cell->its_customized 	= (boolean) $cell->its_customized;
            $cell->its_finish 		= (boolean) $cell->its_finish;
            $cell->limit_max 		= (int) $cell->limit_max;
            $cell->nregisters 		= (int) $cell->nregisters;
            $cell->programming_id 	= (int) $cell->programming_id;
            $cell->training_id 		= (int) $cell->training_id;
            $cell->typetraining_id 	= (int) $cell->typetraining_id;
            $cell->departamento_id 	= (int) $cell->departamento_id;
            $cell->provincia_id 	= (int) $cell->provincia_id;
            $cell->distrito_id 		= (int) $cell->distrito_id;
            $cell->folder 			= base_url($cell->folder . "/");
        }

        return !empty($table) ? $table[0] : [];
    }

    public function insert_programming()
    {
        $date = (new DateTime())->format('Y-m-d H:i:s');

        $param = [
            "capa_ID" => $this->data['training'],
            "prog_date" => $this->data['date_realization'],
            "prog_time_start" => $this->data['time_start'],
            "prog_time_end" => $this->data['time_finish'],
            "prog_place" => my_ucwords_ns($this->data['place']),
            "prog_address" => my_ucwords_ns($this->data['address']),
            "dist_ID" => $this->data['distrito'],
            "prog_is_private" => (boolean)$this->data['personalize'],
            "prog_limit" => $this->data['limit'],
            "prog_date_created" => $date,
            "prog_date_modified" => $date,
        ];

        $this->db->insert('programa', $param);
        $this->ID = $this->ID = $this->db->insert_id();

        if ($this->ID > 0) {

            // ASIGNA DOCENTES A CADA TEMA
            $this->asign_docentes_programming();

            if (((boolean)$this->data['personalize'])) {
                $this->training_personalize();
            }

            return true;
        }

        return false;
    }

    public function update_programming()
    {
        $date = (new DateTime())->format('Y-m-d H:i:s');

        $param = [
            "capa_ID" => $this->data['training'],
            "prog_place" => my_ucwords_ns($this->data['place']),
            "prog_address" => my_ucwords_ns($this->data['address']),
            "dist_ID" => $this->data['distrito'],
            "prog_is_private" => (boolean)$this->data['personalize'],
            "prog_limit" => (int)$this->data['limit'],
            "prog_date_modified" => $date,
        ];

        $confirm = $this->db->update('programa', $param, ['progID' => $this->ID]);

        if ($confirm) {
            // ASIGNA DOCENTES A CADA TEMA
            $this->asign_docentes_programming(true);

            $this->db->delete('programa_private', ['prog_ID' => $this->ID]);

            if (((boolean)$this->data['personalize'])) {
                $this->training_personalize(true);
            }

            return true;
        }

        return false;
    }

    public function cancel_programming()
    {
        $date = (new DateTime())->format('Y-m-d H:i:s');

        $params = [
            'prog_is_canceled' => 1,
            'prog_reason_canceled' => (!empty($this->data['reason']) ? $this->data['reason'] : null),
            'prog_date_modified' => $date
        ];

        $this->db->where(['progID' => $this->ID]);

        return $this->db->update('programa', $params);
    }

    public function delete_programming()
    {
        $result = new stdClass;

        $where_number = [
            'i.insc_status' => true,
            'i.insc_email_confirmed' => true,
            'i.prog_ID' => $this->ID,
            'p.prog_is_canceled' => false
        ];

        $cant = $this->verify_number_registered($where_number);

        if ($cant > 0) {
            $result->error = true;
            $result->message = "Intente cancelar la programación primero antes de eliminarla.";
            $result->data = false;
        } else {
            $date = (new DateTime())->format('Y-m-d H:i:s');

            $params = [
                "prog_status" => 0,
                "prog_date_modified" => $date
            ];

            $this->db->where(['progID' => $this->ID]);
            $status = $this->db->update('programa', $params);

            $status_msg = ($status ? "La eliminación fue satisfactoria. Reiniciando..."
                            :"Hubo un error al intentar eliminar, vuelve a intentarlo");

            $result->error = (! (boolean)$status);
            $result->message = $status_msg;
            $result->data = $status;
        }

        return $result;
    }

    public function programming_change_schedule()
    {
        $date = (new DateTime())->format('Y-m-d H:i:s');

        $params = [
            "prog_date" => $this->data['date_realization'],
            "prog_time_start" => $this->data['time_start'],
            "prog_time_end" => $this->data['time_finish'],
            "prog_date_modified" => $date
        ];

        $this->db->where(['progID' => $this->ID]);
        $confirm = $this->db->update('programa', $params);

        if ($confirm) {
            $this->warn_schedule_programming();
        }

        return $confirm;
    }

    public function warn_schedule_programming()
    {
        $config = $this->config->item('email');
        $table = $this->get_emails_from_inscript();
        $count = -1;

        if (! empty($table)) {
            $count = 0;

            foreach ($table as $row => $cell) {
                $emails = explode(",", $cell->email);

                foreach ($emails as $key => $value) {
                    $emails[$key] = trim($value);
                }

                // Limpia los parametros incluye datos adjuntos
                $this->email->clear(true);

                $html = $this->load->view('emails/change_schedule_prog_view', $cell, true);

                $this->email->initialize($config);
                $this->email->from('capacita@treffpunkt-liebefeld.ch', "Capacitaciones SGP - 2016");
                $this->email->to($emails);
                $this->email->subject("Cambios en la Programación :: " . $cell->training_name);
                $this->email->message($html);

                $confirm = $this->email->send();
                
                if ($confirm) {
                    $count += 1;
                }
            }
        }

        return $count;
    }

    public function warn_cancel_programming()
    {
        $config = $this->config->item('email');
        $table = $this->get_emails_from_inscript();
        $count = -1;

        if (! empty($table)) {
            $count = 0;

            foreach ($table as $row => $cell) {
                $emails = explode(",", $cell->email);

                foreach ($emails as $key => $value) {
                    $emails[$key] = trim($value);
                }

                // Limpia los parametros incluye datos adjuntos
                $this->email->clear(true);

                $html = $this->load->view('emails/cancelation_prog_view', $cell, true);

                $this->email->initialize($config);
                $this->email->from('capacita@treffpunkt-liebefeld.ch', "Capacitaciones SGP - 2016");
                $this->email->to($emails);
                $this->email->subject("Capacitación cancelada :: " . $cell->training_name);
                $this->email->message($html);

                $confirm = $this->email->send();

                if ($confirm) {
                    $count += 1;
                }
            }
        }

        return $count;
    }

    public function get_emails_from_inscript()
    {
        $strQuery = "p.part_name 'name', p.part_lastname lastname, p.part_email email, pg.prog_date date_realization,
		pg.prog_time_start time_start, pg.prog_time_end time_finish, pg.prog_reason_canceled reason, c.capa_name training_name";

        $this->db->select($strQuery)
            ->from('inscripcion i')
                ->join('participantes p', 'p.partID = i.part_ID', 'inner')
                ->join('programa pg', 'pg.progID = i.prog_ID', 'join')
                ->join('capacitacion c', 'c.capaID = pg.capa_ID', 'join')
            ->where(['i.prog_ID' => $this->ID]);

        return $this->db->get()->result();
    }

    public function asign_docentes_programming($update = false)
    {
        $date = (new DateTime())->format('Y-m-d H:i:s');
        $params = [];

        if ($update) {
            $this->db->delete('programa_docente', ['prog_ID' => $this->ID]);
        }

        foreach ($this->data['docen_assign'] as $key => $value) {
            if ($value['profesor'] > 0) {
                array_push($params, [
                    'prog_ID' => $this->ID,
                    'doce_ID' => (int)$value['profesor'],
                    'sute_ID' => (int)$value['subtheme'],
                    'prdo_is_coordinator' => false,
                    'prdo_date_created' => $date,
                    'prdo_date_modified' => $date
                ]);
            }
        }

        return $this->db->insert_batch('programa_docente', $params);
    }

    public function training_personalize($update = false)
    {
        if ($update) {
            $this->db->delete('programa_private', ['prog_ID' => $this->ID]);
        }

        $param = [
            "prog_ID" => $this->ID,
            "enti_ID" => $this->data['entity']
        ];

        $this->db->insert('programa_private', $param);
    }
//


    public function verify_number_registered($where = [])
    {
        if (empty($where)) {
            $where = [
                'i.insc_status' => true,
                'i.insc_email_confirmed' => true,
                'i.prog_ID' => $this->ID
            ];
        }

        $this->db->select('COUNT(i.prog_ID) number')
            ->from('inscripcion i')
            ->join('programa p', 'p.progID = i.prog_ID', 'inner')
            ->where($where);

        $cantidad = $this->db->get()->result();

        if (! empty($cantidad)) {
            return (int)($cantidad[0]->number);
        } else {
            return 0;
        }
    }

    public function get_programming_themes()
    {
        $strQuery = 'pd.prdoID id, d.doceID docente_id, d.doce_name docente_name, d.doce_lastname docente_lastname,
		d.doce_specialty docente_specialty, st.suteID subtheme_id, st.sute_name subtheme_title, st.sute_description subtheme_description,
		t.temaID tema_id, t.tema_title tema_title, t.tema_description tema_description';

        $this->db->select($strQuery)
            ->from('programa_docente pd')
                ->join('docente d', 'd.doceID = pd.doce_ID AND d.doce_status = 1', 'inner')
                ->join('sub_tema st', 'st.suteID = pd.sute_ID AND st.sute_status = 1', 'inner')
                ->join('tema t', 't.temaID = st.tema_ID AND t.tema_status = 1', 'inner')
            ->where(['pd.prdo_status' => true, 'pd.prog_ID' => $this->ID])
            ->order_by('t.temaID', 'ASC');

        $table = $this->db->get()->result();
        $table2 = [];
        $curr_id = 0;
        $theme_arr = [];

        foreach ($table as $row => $cell) {
            if ($curr_id != $cell->tema_id) {
                $theme_arr = [
                    "theme_id" => (int) $cell->tema_id,
                    "title" => $cell->tema_title,
                    "description" => $cell->tema_description,
                    "subthemes" => []
                ];

                $curr_id = $cell->tema_id;
            }

            array_push($theme_arr['subthemes'], [
                'subtheme_id' => (int) $cell->subtheme_id,
                'title' => $cell->subtheme_title,
                'docente_id' => (int) $cell->docente_id,
                'docente' => $cell->docente_name . " " . $cell->docente_lastname,
                'description' => $cell->subtheme_description,
            ]);

            if (isset($table[($row + 1)])) {
                if ($table[($row + 1)]->tema_id != $curr_id) {
                    array_push($table2, $theme_arr);
                }
            } else {
                array_push($table2, $theme_arr);
            }
        }

        return $table2;
    }
}

/* End of file Training_model.php */
/* Location: ./application/models/Training_model.php */
