<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Workshops_model extends CI_Model {

	public $programID 		= 0;
	public $particiID 		= 0;
	public $inscripID 		= 0;
	public $data 			= [];
	public $data_program 	= [];
	public $nrequest 		= 0;

	public function __construct()
	{
		parent::__construct();
		$this->load->library('email');
		date_default_timezone_set('America/Lima');
		setlocale(LC_ALL, array('es_PE.UTF-8','es_PE@amer','es_PE','peru'));
	}

	public function insertInscription($sendMail = TRUE, $autoActive = FALSE, $autoAssist = FALSE)
	{
		$date = (new DateTime())->format('Y-m-d H:i:s');
		$time_assist = NULL;
		$this->nrequest = random_string("sha1");
		$program = $this->data_program;

		if( $autoAssist )
		{
			$time_finish = new DateTime($program->date_realization . " " . $program->time_finish);
			$time_start =  new DateTime($program->date_realization . " " . $program->time_start);
			$now = new DateTime();

			if( $now > $time_finish ) {
				$time_assist = $program->time_start;
			}
			else if( $now < $time_start ) {
				$time_assist = $program->time_start;
			}
			else
			{
				$time_assist = $now->format('H:i:s');
			}
		}

		$param = [
			'part_ID' => $this->particiID,
			'prog_ID' => $this->programID,
			'insc_registry_data' => '',
			'insc_request_number' => $this->nrequest,
			'insc_email_confirmed' => $autoActive,
			'insc_assistance' => $autoAssist,
			'insc_time_assistance' => $time_assist,
			'insc_date_created' => $date,
			'insc_date_modified' => $date
		];

		$this->db->insert('inscripcion', $param);
		$this->inscripID = $this->db->insert_id();

		if ( $sendMail ) {

			$name_arr = explode(' ', $this->data['name']);

			$param_send = [
				'request_number' => $this->nrequest,
				'program' => $this->programID,
				'participante' => $this->particiID,
				'inscriptcion' => $this->inscripID,
				'date_creation' => $date
			];

			$this->sendMail_register([
				'email' => $this->data['email'],
				'training_title' => $this->data_program->training_title,
				'training_type' => $this->data_program->type_training,
				'date_realization' => $this->data_program->date_realization,
				'time_start' => $this->data_program->time_start,
				'time_finish' => $this->data_program->time_finish,
				'names' => ($name_arr[0] . " " . $this->data['lastname']),
				'datacript' => $this->base64url_encode(json_encode($param_send))
			]);
		}
	}

	public function insertParticipant()
	{
		$date = (new DateTime())->format('Y-m-d H:i:s');
		$profession = ! empty($this->data['profesion']) ? my_ucfirst($this->data['profesion']) : NULL;

		$params = [
			"enti_ID" => $this->data['entity'],
			"dist_ID" => $this->data['distrito'],
			"part_name" => my_ucwords_ns($this->data['name']),
			"part_lastname" => my_ucwords_ns($this->data['lastname']),
			"part_dni" => $this->data['dni'],
			"part_gender" => ($this->data['gender'] == 0) ? FALSE : TRUE,
			"part_email" => $this->data['email'],
			"part_profesion" => my_ucfirst($profession),
			"part_cellphone" => (!empty($this->data['cellphone']) ? $this->data['cellphone'] : NULL),
			"part_phone" => (!empty($this->data['phone']) ? $this->data['phone'] : NULL),
			"part_annex" => (!empty($this->data['annex']) ? $this->data['annex'] : NULL),
			"part_position" => (!empty($this->data['position']) ? my_ucfirst($this->data['position']) : NULL),
			"part_dispacity" => ($this->data['dispacity'] == 0) ? FALSE : TRUE,
			"part_details_dispacity" => (!empty($this->data['details_dispacity']) ? my_ucfirst($this->data['details_dispacity']) : NULL),
			"part_date_created" => $date,
			"part_date_modified" => $date
		];

		$this->db->insert('participantes', $params);
		$this->particiID = $this->db->insert_id();
	}

	public function modifyParticipant()
	{
		$date = (new DateTime())->format('Y-m-d H:i:s');
		$profession = ! empty($this->data['profesion']) ? my_ucfirst($this->data['profesion']) : NULL;

		$params = [
			"enti_ID" => $this->data['entity'],
			"dist_ID" => $this->data['distrito'],
			"part_name" => my_ucwords_ns($this->data['name']),
			"part_lastname" => my_ucwords_ns($this->data['lastname']),
			"part_gender" => ($this->data['gender'] == 0) ? FALSE : TRUE,
			"part_email" => $this->data['email'],
			"part_profesion" => my_ucfirst($profession),
			"part_cellphone" => (!empty($this->data['cellphone']) ? $this->data['cellphone'] : NULL),
			"part_phone" => (!empty($this->data['phone']) ? $this->data['phone'] : NULL),
			"part_annex" => (!empty($this->data['annex']) ? $this->data['annex'] : NULL),
			"part_position" => (!empty($this->data['position']) ? my_ucfirst($this->data['position']) : NULL),
			"part_dispacity" => ($this->data['dispacity'] == 0) ? FALSE : TRUE,
			"part_details_dispacity" => (!empty($this->data['details_dispacity']) ? my_ucfirst($this->data['details_dispacity']) : NULL),
			"part_date_modified" => $date
		];

		$this->db->where(['partID' => $this->particiID, 'part_status' => TRUE]);
		return $this->db->update('participantes', $params);
	}

	public function sendMail_register($input)
	{
		$config = $this->config->item('email');

        $emails = explode(",", $input['email']);

        foreach ($emails as $key => $value) {
        	$emails[$key] = trim($value);
        }

        $html = $this->load->view('emails/participant_reg_view', $input, TRUE);

		$this->email->initialize($config);
		$this->email->from('capacita@treffpunkt-liebefeld.ch', "Capacitaciones SGP - 2016" );
		$this->email->to($emails);
		$this->email->subject("Verificación de Inscripción");
		$this->email->message($html);

		$confirm = $this->email->send();
		if ( ! $confirm )
			return FALSE;
		else
			return TRUE;
	}

	public function hasRegister()
	{
		$this->db->select('COUNT(i.inscID) has')
			->from('inscripcion i')
				->join('participantes p', 'p.partID = i.part_ID AND p.part_status = 1', 'inner')
			->where([
				'i.part_ID' => $this->particiID,
				'i.prog_ID' => $this->programID,
				'i.insc_status' => TRUE
			]);

		$result = $this->db->get()->result();

		return (int) $result[0]->has;
	}

	public function find_programming($programming_id)
	{
		$this->db->select('*')
			->from('v_programming_min')
			->where(['programming_id' => $programming_id]);

		return $this->db->get()->result();
	}

	function base64url_encode($data) {
		return rtrim(strtr(base64_encode($data), '+/', '-_'), '=');
	}

}

/* End of file Workshops_model.php */
/* Location: ./application/models/Workshops_model.php */
