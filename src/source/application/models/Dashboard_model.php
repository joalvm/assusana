<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Dashboard_model extends CI_Model {

	public $variable;

	public function __construct()
	{
		parent::__construct();
		date_default_timezone_set('America/Lima');
		setlocale(LC_ALL, array('es_PE.UTF-8','es_PE@amer','es_PE','peru'));
	}

	public function get_counts()
	{
		$this->db->select('get_NumberAllInscriptions(0) ninscriptions,
			get_NumberAllParticipants() nparticipants,
			get_NumberProgramming(1) nprogramfull,
			get_NumberProgramming(0) nprogram_nocancel');

		return $this->db->get()->result();
	}

	public function get_genders()
	{
		$this->db->select('get_NumberGender(0) hombres, get_NumberGender(1) mujeres');

		$table = $this->db->get()->result();
		$result = [];

		foreach ($table as $row => $cell) 
		{
			$result = [
				[
					"y" => (int) $cell->hombres,
					"indexLabel" => "Hombres"
				],
				[
					"y" => (int) $cell->mujeres,
					"indexLabel" => "Mujeres"
				]
			];
		}

		return $result;
	}

	public function get_timeline()
	{
		$this->db->select('COUNT(inscID) cant, i.insc_date_created date')
			->from('inscripcion i')
				->join('programa p', 'p.progID = i.prog_ID', 'inner')
			->where([
				'i.insc_status' => TRUE,
				'i.insc_email_confirmed' => TRUE,
				'p.prog_status' => TRUE,
				'p.prog_is_canceled' => FALSE
			])
			->group_by('MONTH(i.insc_date_created)')
			->order_by('i.insc_date_created ASC');
		$table = $this->db->get()->result();
		$result = [];

		foreach ($table as $row => $cell) 
		{
			$date = new DateTime($cell->date);
			
			array_push($result, [
				"x" => ($date->getTimestamp() * 1000),
				"y" => (int) $cell->cant
			]);
		}

		return $result;
	}

	public function get_invitados()
	{
		$strQuery = 'c.capa_name training,
			p.prog_date date_realization,
			p.prog_time_start time_start,
			p.prog_time_end time_finish,
			get_NumberInscriptions(i.prog_ID, 0) invitados,
			get_NumberInscriptions(i.prog_ID, 1) asistentes';

		$this->db->select($strQuery)
			->from('inscripcion i')
				->join('programa p', 'p.progID = i.prog_ID AND p.prog_status = 1 AND p.prog_is_canceled = 0', 'inner')
				->join('capacitacion c', 'c.capaID = p.capa_ID', 'inner')
			->group_by('i.prog_ID')
			->order_by('p.prog_date DESC');

		$table = $this->db->get()->result();
		$table2 = [];

		foreach ($table as $row => $cell) 
		{
			$now = new DateTime();
			$real = new DateTime($cell->date_realization . ' ' . $cell->time_finish);

			if( $real < $now ) 
			{
				array_push($table2, $cell);
			}
		}

		return $table2;
	}

	public function info_training()
	{
		$proxcap = [];
		$culmcap = [];

		$proxcap = $this->db->query('CALL sp_upcoming_trainings()');
		mysqli_next_result($this->db->conn_id);

		$culmcap = $this->db->query('CALL sp_finished_trainings()');
		mysqli_next_result($this->db->conn_id);

		return [
			'proxcap' => $proxcap->result(),
			'culmcap' => $culmcap->result()
		];
	}

	public function get_allparticipants() 
	{
		$strQuery = "p.partID participante_id,p.part_name 'name',p.part_lastname lastname,
		p.part_dispacity its_dispacity,p.part_gender gender,p.part_dni dni,
		i.insc_email_confirmed its_confirmed,i.insc_nota nota,i.insc_time_assistance time_assistance,
		i.insc_date_created date_created,c.capa_name training_name,pr.prog_date date_realization";

		$this->db->select($strQuery)
			->from('participantes p')
				->join('inscripcion i', 'i.part_ID = p.partID', 'inner')
				->join('programa pr', 'pr.progID = i.prog_ID', 'inner')
				->join('capacitacion c', 'c.capaID = pr.capa_ID', 'inner')
			->where(['i.insc_status' => TRUE, 'p.part_status' => TRUE]);

		$table = $this->db->get()->result();
		$mounts = [];
		$data = [];

		/*echo "<pre>";
		print_r($table);
		exit();*/

		foreach ($table as $row => &$cell) 
		{
			$cell->participante_id = (int) $cell->participante_id;
			$cell->its_dispacity = (boolean) $cell->its_dispacity;
			$cell->gender = (int) $cell->gender;
			$cell->its_confirmed = (boolean) $cell->its_confirmed;
			$cell->nota = (int) $cell->nota;


			$year = (new DateTime($cell->date_created))->format('Y');
			$mount = (new DateTime($cell->date_created))->format('m');

			$mounts[$mount][] = $cell;

			$data[$year] = $mounts;
		}

		return $data;

	}

	public function get_allAssistents()
	{
		$strQuery = "p.partID participante_id,p.part_name 'name',p.part_lastname lastname,
		p.part_dispacity its_dispacity,p.part_gender gender,p.part_dni dni,
		i.insc_email_confirmed its_confirmed,i.insc_nota nota,i.insc_time_assistance time_assistance,
		i.insc_date_created date_created,c.capa_name training_name,pr.prog_date date_realization";

		$this->db->select($strQuery)
			->from('participantes p')
				->join('inscripcion i', 'i.part_ID = p.partID', 'inner')
				->join('programa pr', 'pr.progID = i.prog_ID', 'inner')
				->join('capacitacion c', 'c.capaID = pr.capa_ID', 'inner')
			->where([
				'i.insc_status' => TRUE, 
				'p.part_status' => TRUE, 
				'i.insc_email_confirmed' => TRUE
			]);

		$table = $this->db->get()->result();
		$mounts = [];
		$data = [];

		/*echo "<pre>";
		print_r($table);
		exit();*/

		foreach ($table as $row => &$cell) 
		{
			$cell->participante_id = (int) $cell->participante_id;
			$cell->its_dispacity = (boolean) $cell->its_dispacity;
			$cell->gender = (int) $cell->gender;
			$cell->its_confirmed = (boolean) $cell->its_confirmed;
			$cell->nota = (int) $cell->nota;


			$year = (new DateTime($cell->date_created))->format('Y');
			$mount = (new DateTime($cell->date_created))->format('m');

			$mounts[$mount][] = $cell;

			$data[$year] = $mounts;
		}

		return $data;
	}

	public function finished_training()
	{
		$query = "";
		
		$this->db->close();

		return $query->result();
	}

}

/* End of file Dashboard_model.php */
/* Location: ./application/models/Dashboard_model.php */