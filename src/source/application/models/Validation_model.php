<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Validation_model extends CI_Model {

	public $data = [];

	public function __construct()
	{
		parent::__construct();
		date_default_timezone_set('America/Lima');
		setlocale(LC_ALL, array('es_PE.UTF-8','es_PE@amer','es_PE','peru'));
	}

	public function find_docente($data)
	{
		$strQuery = "CALL sp_valid_register_docente(?,?,?,?,?)";

		$parammeters = [
			'_username' => $data['username'],
			'_dni' => $data['DNI'],
			'_role' => $data['role'],
			'_request' => $data['request'],
			'_request_id' => $data['request_id']
		];

		$table = $this->db->query($strQuery, $parammeters)->result();

		if ( isset($table[0]->error) ) {
			return [
				"error" => TRUE,
				"message" => $table[0]->message
			];
		} else {
			return [
				"error" => FALSE,
				"data" => $table[0]
			];
		}
	}

	public function find_programming($data)
	{
		$strQuery = "CALL sp_valid_register_participante(?,?,?,?)";

		$parammeters = [
			'_request_number' => $data['request_number'],
			'_program_id' => $data['program'],
			'_participante_id' => $data['participante'],
			'_inscription_id' => $data['inscriptcion']
		];

		$table = $this->db->query($strQuery, $parammeters)->result();

		if ( isset($table[0]->error) ) {
			return [
				"error" => TRUE,
				"message" => $table[0]->message
			];
		} else {
			return [
				"error" => FALSE,
				"data" => $table[0]
			];
		}
	}

}

/* End of file Validation_model.php */
/* Location: ./application/models/Validation_model.php */