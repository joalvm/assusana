<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Maintenance_model extends CI_Model {

	public function __construct()
	{
		parent::__construct();
		date_default_timezone_set('America/Lima');
		setlocale(LC_ALL, array('es_PE.UTF-8','es_PE@amer','es_PE','peru'));
	}

// METODOS PARA EL MODULO MANTENIMIENTO::TIPO DE CAPACITACIONES
	public function list_type_training()
	{
		$strQuery = "ct.catiID id, ct.cati_name 'name',
		ct.cati_description description, ct.cati_date_modified last_modified";

		$this->db->select($strQuery)
			->from('capacitacion_tipo ct')
			->where(['ct.cati_status' => TRUE]);

		return $this->db->get()->result();
	}

	public function find_type_training($ID)
	{
		$strQuery = "ct.catiID id, ct.cati_name 'name',
		ct.cati_description description, ct.cati_date_modified last_modified";

		$this->db->select($strQuery)
			->from('capacitacion_tipo ct')
			->where(['ct.catiID' => $ID, 'ct.cati_status' => TRUE]);

		return $this->db->get()->result();
	}

	public function insert_type_training($post = [])
	{
		$date = (new DateTime())->format('Y-m-d H:i:s');

		$data = [
			"cati_name" => my_ucfirst($post['typeText']),
			"cati_description" => !empty($post['description']) ? my_ucfirst($post['description']) : NULL,
			"cati_date_created" => $date,
			"cati_date_modified" => $date
		];

		$this->db->insert('capacitacion_tipo', $data);

		return $this->db->insert_id();
	}

	public function update_type_training($post = [], $ID)
	{
		$date = (new DateTime())->format('Y-m-d H:i:s');

		$data = [
			"cati_name" => my_ucfirst($post['typeText']),
			"cati_description" => !empty($post['description']) ? my_ucfirst($post['description']) : NULL,
			"cati_date_modified" => $date
		];

		$this->db->where(["catiID" => $ID]);

		return $this->db->update('capacitacion_tipo', $data);
	}

	public function delete_type_training($ID)
	{
		$date = (new DateTime())->format('Y-m-d H:i:s');

		$data = [
			"cati_status" => FALSE,
			"cati_date_modified" => $date
		];

		$this->db->where(["catiID" => $ID]);

		return $this->db->update('capacitacion_tipo', $data);
	}
//


// METODOS PARA EL MODULO MATENIMIENTO::ENTIDADES
	public function insert_type($data)
	{
		$result = new stdClass;
		$date = (new DateTime())->format('Y-m-d H:i:s');

		if ( $data['type_id'] == 0 )
		{
			$params = [
				'entp_name' => my_ucfirst($data['type_name']),
				'entp_description' => NULL,
				'entp_date_created' => $date,
				'entp_date_modified' => $date
			];

			$this->db->insert('entidad_tipo', $params);

			$result->id = $this->db->insert_id();

			$result->message = ($result->id <= 0) ? "No se pudo registrar el tipo de la entidad" : '';
			$result->error = ($result->id > 0) ? FALSE : TRUE;
		}
		else
		{
			$result->id = $data['type_id'];
			$result->message = '';
			$result->error = FALSE;
		}

		return $result;
	}

	public function insert_subtype($data, $type_id)
	{
		$result = new stdClass;
		$date = (new DateTime())->format('Y-m-d H:i:s');

		if ( $data['subtype_id'] == 0 )
		{
			$params = [
				'entp_ID' => $type_id,
				'ensu_name' => my_ucfirst($data['subtype_name']),
				'ensu_description' => NULL,
				'ensu_date_created' => $date,
				'ensu_date_modified' => $date
			];

			$this->db->insert('entidad_subtipo', $params);

			$result->id = $this->db->insert_id();

			$result->message = ($result->id <= 0) ? "No se pudo registrar el subtipo de la entidad" : '';
			$result->error = ($result->id > 0) ? FALSE : TRUE;
		}
		else
		{
			$result->id = $data['subtype_id'];
			$result->message = '';
			$result->error = FALSE;
		}

		return $result;
	}

	public function insert_entity( $data, $subtype_id )
	{
		$result = new stdClass;
		$date = (new DateTime())->format('Y-m-d H:i:s');

		$params = [
			'ensu_ID' => $subtype_id,
			'enti_ubigeo' => '000000',
			'enti_cue' => NULL,
			'enti_name' => my_ucwords_ns($data['entity_name']),
			'enti_description' => NULL,
			'enti_date_created' => $date,
			'enti_date_modified' => $date
		];

		if ( $this->db->insert('entidad', $params) )
		{
			$result->error = FALSE;
			$result->message = "La entidad se guardo satisfactoriamente";
		}
		else
		{
			$result->error = TRUE;
			$result->message = "Error al intentar guardar la entidad, vuelva a intentarlo";
		}

		return $result;
	}
//


// METODOS PARA EL MODULO MATENIMIENTO::TEMAS
	public function insert_theme($theme_data)
	{
		$date = (new DateTime())->format('Y-m-d H:i:s');
		$return = new stdClass;

		$param = [
			"tema_title" => my_ucfirst($theme_data["title"]),
			"tema_description" => empty($theme_data["description"]) ? NULL : my_ucfirst($theme_data["description"]),
			"tema_date_created" => $date,
			"tema_date_modified" => $date
		];

		$this->db->insert('tema', $param);

		$ID = $this->db->insert_id();

		if( $ID > 0 ) {
			$return->error = FALSE;
			$return->message = "El tema ha sido agregado";
			$return->ID = $ID;
		}
		else
		{
			$return->error = TRUE;
			$return->message = "Error al momento de agregar el tema, vuelva a intentarlo";
			$return->ID = NULL;
		}

		return $return;
	}

	public function update_theme($theme_id, $theme_data)
	{
		$date = (new DateTime())->format('Y-m-d H:i:s');
		$return = new stdClass;

		$param = [
			"tema_title" => my_ucfirst($theme_data["title"]),
			"tema_description" => empty($theme_data["description"]) ? NULL : my_ucfirst($theme_data["description"]),
			"tema_date_modified" => $date
		];

		$this->db->where(['temaID' => $theme_id]);
		
		$result = $this->db->update('tema', $param);

		if( $result ) {
			$return->error = FALSE;
			$return->message = "El tema ha sido modificado";
			$return->result = $result;
		}
		else
		{
			$return->error = TRUE;
			$return->message = "Error al momento de modificar el tema, vuelva a intentarlo";
			$return->result = FALSE;
		}

		return $return;
	}

	public function delete_theme($theme_id)
	{
		$date = (new DateTime())->format('Y-m-d H:i:s');
		$result = new stdClass;

		$param = [
			"tema_status" => FALSE,
			"tema_date_modified" => $date
		];

		$this->db->where(['temaID' => $theme_id]);
		
		$confirm = $this->db->update('tema', $param);

		if( $confirm ) {
			$result->error = FALSE;
			$result->message = "El tema ha sido eliminado";
			$result->result = $confirm;
		}
		else
		{
			$result->error = TRUE;
			$result->message = "Error al momento de eliminar el tema, vuelva a intentarlo";
			$result->result = FALSE;
		}

		return $result;
	}

	public function find_theme($theme_id)
	{
		$this->db->select('temaID id, tema_title title, tema_description description')
			->from('tema')
			->where(['temaID' => $theme_id]);

		$table = $this->db->get()->result();

		if ( ! empty($table) )
		{
			foreach ($table as $row => &$cell )
			{
				$cell->id = (int) $cell->id;
			}

			return $table[0];
		}

		return [];
	}

	public function exists_theme($str_theme, $no_theme_id = 0)
	{
		$this->db->select('temaID, tema_title');

		if( $no_theme_id != 0 )
		{
			$this->db->where(['temaID !=' => $no_theme_id]);
		}

		$table = $this->db->get('tema')->result();
		$count = 0;

		foreach ($table as $row => $cell)
		{
			if( mb_strtolower($str_theme, 'UTF-8') == mb_strtolower($cell->tema_title, 'UTF-8') )
			{
				$count++;
			}
		}

		return $count;
	}

	public function insert_subtheme($themeID, $data)
	{
		$date = (new DateTime())->format('Y-m-d H:i:s');
		$return = new stdClass;

		$param = [
			"tema_ID" => $themeID,
			"sute_name" => my_ucfirst($data["title"]),
			"sute_description" => empty($data["description"]) ? NULL : my_ucfirst($data["description"]),
			"sute_date_created" => $date,
			"sute_date_modified" => $date
		];

		$this->db->insert('sub_tema', $param);

		$ID = $this->db->insert_id();

		if( $ID > 0 ) {
			$return->error = FALSE;
			$return->message = "El subtema ha sido agregado";
			$return->ID = $ID;
		}
		else
		{
			$return->error = TRUE;
			$return->message = "Error al momento de agregar el subtema, vuelva a intentarlo";
			$return->ID = NULL;
		}

		return $return;
	}

	public function update_subtheme( $theme_id, $subtheme_id, $data )
	{
		$date = (new DateTime())->format('Y-m-d H:i:s');
		$return = new stdClass;

		$param = [
			"sute_name" => my_ucfirst($data["title"]),
			"sute_description" => empty($data["description"]) ? NULL : my_ucfirst($data["description"]),
			"sute_date_modified" => $date
		];

		$this->db->where(["suteID" => $subtheme_id, "tema_ID" => $theme_id]);

		$result = $this->db->update('sub_tema', $param);

		if( $result ) {
			$return->error = FALSE;
			$return->message = "El subtema ha sido modificado";
			$return->result = $result;
		}
		else
		{
			$return->error = TRUE;
			$return->message = "Error al momento de modificar el subtema, vuelva a intentarlo";
			$return->result = FALSE;
		}

		return $return;
	}

	public function delete_subtheme( $theme_id, $subtheme_id )
	{
		$date = (new DateTime())->format('Y-m-d H:i:s');
		$return = new stdClass;

		$param = [
			"sute_status" => FALSE,
			"sute_date_modified" => $date
		];

		$this->db->where(["suteID" => $subtheme_id, "tema_ID" => $theme_id]);

		$result = $this->db->update('sub_tema', $param);

		if( $result ) {
			$return->error = FALSE;
			$return->message = "El subtema ha sido eliminado";
			$return->result = $result;
		}
		else
		{
			$return->error = TRUE;
			$return->message = "Error al momento de eliminar el subtema, vuelva a intentarlo";
			$return->result = FALSE;
		}

		return $return;
	}

	public function find_subtheme($subtheme_id)
	{
		$this->db->select('suteID id, sute_name title, sute_description description')
			->from('sub_tema')
			->where(['suteID' => $subtheme_id]);

		$table = $this->db->get()->result();

		if ( ! empty($table) )
		{
			foreach ($table as $row => &$cell )
			{
				$cell->id = (int) $cell->id;
			}

			return $table[0];
		}

		return [];
	}

	public function list_themes($find = "")
	{
		$strQuery = "t.temaID id, t.tema_title title, t.tema_description description,
		st.suteID subtheme_id, st.sute_name subtheme_title, st.sute_description subtheme_description,
		t.tema_date_modified last_modified";

		$where = [
			't.tema_status' => TRUE
		];

		if( !empty($find) ) {
			$where['t.temaID'] = $find;
		}


		$this->db->select($strQuery)
			->from('tema t')
				->join('sub_tema st', 'st.tema_ID = t.temaID AND st.sute_status = 1', 'left')
			->where($where);

		$rtable = $this->db->get()->result();
		$table = [];
		$subthemes_arr = [];
		$curr_id = 0;

		foreach  ($rtable as $row => $cell )
		{
			$curr_id = $cell->id;

			$sthemes = new stdClass;

			if( $cell->subtheme_id != NULL ) {
				$sthemes->id = (int) $cell->subtheme_id;
				$sthemes->title = $cell->subtheme_title;
				$sthemes->description = $cell->subtheme_description;

				array_push($subthemes_arr, $sthemes);
			}

			if ( isset($rtable[$row + 1]) )
			{
				if ( $rtable[$row + 1]->id != $curr_id )
				{
					array_push($table, (object)[
						"id" => (int)$cell->id,
						"title" => $cell->title,
						"description" => $cell->description,
						"last_modified" => $cell->last_modified,
						"subthemes" => $subthemes_arr
					]);

					$subthemes_arr = [];
				}
			} else {
				array_push($table, (object)[
					"id" => $cell->id,
					"title" => $cell->title,
					"description" => $cell->description,
					"last_modified" => $cell->last_modified,
					"subthemes" => $subthemes_arr
				]);
			}
		}

		return $table;
	}
//

}


/* End of file register_model.php */
/* Location: ./application/models/register_model.php */
