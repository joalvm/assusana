<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Certified_model extends CI_Model {

	public function __construct()
	{
		parent::__construct();
	}

	public function get_info_certified( $ID, $one = TRUE )
	{
		$where = [
			'i.insc_email_confirmed' => TRUE,
			'i.insc_status' => TRUE,
			'i.insc_assistance' => TRUE
		];

		if( $one ) {
			$where['i.inscID'] = $ID;
		} else {
			$where['i.prog_ID'] = $ID;
		}

		$strQuery = "c.capa_name training_name,
					t.tema_title tema,
					p.prog_date date_realization,
					get_Departamento(p.dist_ID) departamento,
					pr.part_name participant_name,
					pr.part_lastname participant_lastname";

		$this->db->select($strQuery)
			->from('inscripcion i')
				->join('programa p', 'p.progID = i.prog_ID', 'inner')
				->join('participantes pr', 'pr.partID = i.part_ID', 'inner')
				->join('capacitacion c', 'c.capaID = p.capa_ID', 'inner')
				->join('tema t', 't.temaID = c.tema_ID', 'inner')
			->where($where);

		$table = $this->db->get()->result();

		foreach ($table as $row => &$cell) {
			$cell->departamento = ucfirst(mb_strtolower($cell->departamento, 'UTF-8'));
		}

		return $table;
	}

}

/* End of file Certified_model.php */
/* Location: ./application/models/Certified_model.php */