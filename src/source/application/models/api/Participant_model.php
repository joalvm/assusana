<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Participant_model extends CI_Model {

	public $ID = 0;
	public $DNI = "";

	public function __construct()
	{
		parent::__construct();
		date_default_timezone_set('America/Lima');
		setlocale(LC_ALL, array('es_PE.UTF-8','es_PE@amer','es_PE','peru'));
	}

	public function get_data_from_dni()
	{
		$this->db->select('*')->from('v_participant')
			->where(['dni' => $this->DNI]);

		$table = $this->db->get()->result();

		foreach ($table as $row => &$cell ) 
		{
			$cell->id 					= (int) $cell->id;
			$cell->departamento_id 		= (int) $cell->departamento_id;
			$cell->dispacity 			= (boolean) $cell->dispacity;
			$cell->distrito_id 			= (int) $cell->distrito_id;
			$cell->entity_id 			= (int) $cell->entity_id;
			$cell->gender 				= (int) $cell->gender;
			$cell->provincia_id 		= (int) $cell->provincia_id;
			$cell->subtype_entity_id 	= (int) $cell->subtype_entity_id;
			$cell->type_entity_id 		= (int) $cell->type_entity_id;
		}

		return $table;
	}

}

/* End of file Participant_model.php */
/* Location: ./application/models/Participant_model.php */