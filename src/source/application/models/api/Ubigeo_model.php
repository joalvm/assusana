<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Ubigeo_model extends CI_Model {

	public $variable;

	public function __construct()
	{
		parent::__construct();
		date_default_timezone_set('America/Lima');
		setlocale(LC_ALL, array('es_PE.UTF-8','es_PE@amer','es_PE','peru'));
	}

	public function Listar()
	{
		$strQuery = "de.depaID id_departamento, de.depa_key key_departamento, 
		de.depa_name 'departamento', p.provID id_provincia, p.prov_key key_provincia,
		p.prov_name 'provincia', d.distID id_distrito, d.dist_key key_distrito, d.dist_name 'distrito'";
		
		$this->db->select($strQuery)
			->from('distrito d')
			->join('provincia p', 'p.provID = d.prov_ID', 'inner')
			->join('departamento de', 'de.depaID = p.depa_ID');

		$table = $this->db->get()->result();

		$data 		= [];

		$arrayDepa = [];
		$arrayProv = [];
		$arrayDist = [];

		$depabreak 	= 0;
		$provBreak 	= "";
		$distBreak 	= "";

		$countProv 	= 1;
		$countDist 	= 1;

		$addBranchDepa = FALSE;
		$addBranchProv = FALSE;

		foreach ($table as $key => $value) 
		{
			//al ver departamento repetidos en lo que nos trae de la BD debemos de agregar
			//solo el primero pero al ser diferentes es por que ha cambiado ya de departamento
			if ($depabreak != $value->id_departamento)
			{
				//al verificar que ha cambiado de departamento se verifica el permiso
				//para proceder a insertar todo lo recopilado del departamento anterior
				if ( $addBranchDepa ) 
				{
					//Se añade la ultima provincia que ha quedado rezagada ya que al llegar aqui,
					//se agregará toda la información que se ha estado recopilando de un departamento
					//y se lipiará todo para agregar un nuevo departamento.
					array_push( $arrayDepa["provincias"], $arrayProv );

					//Se agregan un nuevo departamento a la lista general
					array_push( $data, $arrayDepa );
					
					//se prohibe la agregacion de nuevas provincias porque al entrar a este bloque
					//el array de $arrayDepa, ya tiene un nuevo departamento.
					$addBranchProv = FALSE;
				}

				//actualizacion de la variable que confirma un nuevo departamento
				$depabreak = $value->id_departamento;

				//creacion del nuevo array para el nuevo departamento
				$arrayDepa = array(
					"id" 			=> (int)$value->id_departamento,
					"departamento" 	=> $value->departamento,
					"provincias" 	=> []
				);

				$countProv = 1;//reiniciando nuestro contador de provincias
				$provBreak = "";//limpiando la variable que verifica una nueva provincia
			}

			//$provBreak al ser limpiado en la sentencia anterior puede ingresar aqui por una ves
			//para insertar las provincias al nuevo array de departamento
			if ($provBreak != $value->provincia) 
			{
				//confirmacion que nos da el bloque de distritos para saber que existe almenos una
				//
				if ($addBranchProv) {
					array_push($arrayDepa["provincias"], $arrayProv);
				}

				$provBreak = $value->provincia;

				$arrayProv = array(
					"id" 		=> (int)$value->id_provincia,
					"key" 		=> $countProv,
					"provincia" => $value->provincia,
					"distritos" => []
				);

				$countProv++;
				$countDist = 1;
				$distBreak = "";
				$addBranchDepa = TRUE;
			}

			if ($distBreak != $value->distrito) 
			{
				$distBreak = $value->distrito;

				$arrayDist = array(
					"id" 		=> (int) $value->id_distrito,
					"key" 		=> $countDist,
					"distrito" 	=> $value->distrito
				);

				array_push($arrayProv["distritos"], $arrayDist);

				$addBranchProv = TRUE;
				$countDist++;
			}
		}


		array_push($arrayDepa["provincias"], $arrayProv);
		array_push($data, $arrayDepa);

		return $data;
	}

}

/* End of file Ubigeo_model.php */
/* Location: ./application/models/Ubigeo_model.php */