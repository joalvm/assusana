<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Entidad_model extends CI_Model {

	public $ID;

	private $strQuery = [
		"et.entpID type_id", 
		"et.entp_name type_name", 
		"es.ensuID subtype_id", 
		"es.ensu_name subtype_name", 
		"e.entiID entity_id", 
		"e.enti_cue entity_cue", 
		"e.enti_name entity_name"
	];

	public function __construct()
	{
		parent::__construct();
		date_default_timezone_set('America/Lima');
		setlocale(LC_ALL, array('es_PE.UTF-8','es_PE@amer','es_PE','peru'));
	}

	public function cascaded()
	{
		$this->db->select(implode(", ", $this->strQuery))
			->from('entidad e')
				->join('entidad_subtipo es', 'es.ensuID = e.ensu_ID AND es.ensu_status = 1', 'inner')
				->join('entidad_tipo et', 'et.entpID = es.entp_ID AND et.entp_status = 1', 'inner')
			->where(['e.enti_status' => TRUE])
			->order_by('et.entpID, es.ensuID, e.entiID');
		
		$table = $this->db->get()->result();
		$table_arrange = [];
		
		$next_is_diff = FALSE;
		
		$current_type_id = 0;
		$current_subtype_id = 0;
		$current_entity_id = 0;

		$arrType = new stdClass;

		foreach ( $table as $row => $cell ) 
		{
			if ( $current_type_id != $cell->type_id )
			{
				$arrType 			= new stdClass;
				$arrType->id 		= $cell->type_id;
				$arrType->type 		= my_ucwords_ns($cell->type_name);
				$arrType->sub_types = [];

				$current_type_id = $cell->type_id;
			}

			if ( $current_type_id == $cell->type_id && 
				$current_subtype_id != $cell->subtype_id )
			{
				$arrSubtype = new stdClass;
				$arrSubtype->id = $cell->subtype_id;
				$arrSubtype->subtype = my_ucwords_ns($cell->subtype_name);
				$arrSubtype->entities = [];

				array_push($arrType->sub_types, $arrSubtype);
				$current_subtype_id = $cell->subtype_id;
			}

			if ( $current_type_id == $cell->type_id && 
				$current_subtype_id == $cell->subtype_id && 
				$current_entity_id != $cell->entity_id )
			{
				$arrEntity = new stdClass;
				$arrEntity->id = $cell->entity_id;
				$arrEntity->cue = $cell->entity_cue;
				$arrEntity->entity = my_ucwords_ns($cell->entity_name);

				array_push($arrSubtype->entities, $arrEntity);
			}

			if ( isset($table[$row + 1]) )
				if ( $table[$row + 1]->type_id != $current_type_id )
					$next_is_diff = TRUE;

			if ( $next_is_diff )
			{
				array_push($table_arrange, $arrType);
				$next_is_diff = FALSE;
			}
		}

		array_push($table_arrange, $arrType);

		return $table_arrange;
	}

	public function expanded()
	{
		$this->db->select(implode(", ", $this->strQuery))
			->from('entidad e')
				->join('entidad_subtipo es', 'es.ensuID = e.ensu_ID AND es.ensu_status = 1', 'inner')
				->join('entidad_tipo et', 'et.entpID = es.entp_ID AND et.entp_status = 1', 'inner')
			->where(['e.enti_status' => TRUE])
			->order_by('et.entpID, es.ensuID, e.entiID');

		$table = $this->db->get()->result();

		$data = new stdClass;

		$data->entities = [];
		$data->type_entities = [];
		$data->subtype_entities = [];

		$next_is_diff = FALSE;
		
		$current_type_id = 0;
		$current_subtype_id = 0;

		$index_current_type = -1;
		$index_current_subtype = -1;

		foreach ($table as $row => $cell) {
			if ( $current_type_id != $cell->type_id )
			{
				$arrType 			= new stdClass;
				$arrType->id 		= (int)$cell->type_id;
				$arrType->name 		= my_ucwords_ns($cell->type_name);

				$current_type_id = $cell->type_id;

				// AL CONTAR LA CANTIDAD DE ELEMENTOS ME DA LA POSICION
				// DENTRO DEL ARRAY EN EL QUE SE ENCUENTRA
				$index_current_type = count($data->type_entities);

				array_push($data->type_entities, $arrType);
			}

			if ( $current_type_id == $cell->type_id && 
				$current_subtype_id != $cell->subtype_id )
			{
				$arrSubtype 			= new stdClass;
				$arrSubtype->id 		= (int) $cell->subtype_id;
				$arrSubtype->name 		= my_ucwords_ns($cell->subtype_name);
				$arrSubtype->type_id 	= (int) $current_type_id;
				$arrSubtype->type_ps 	= $index_current_type;

				$current_subtype_id = $cell->subtype_id;

				// AL CONTAR LA CANTIDAD DE ELEMENTOS ME DA LA POSICION
				// DENTRO DEL ARRAY EN EL QUE SE ENCUENTRA
				$index_current_subtype = count($data->subtype_entities);

				array_push($data->subtype_entities, $arrSubtype);
			}

			$arrEntity 				= new stdClass;
			$arrEntity->id 			= (int) $cell->entity_id;
			$arrEntity->cue 		= $cell->entity_cue;
			$arrEntity->entity 		= my_ucwords_ns($cell->entity_name);
			$arrEntity->stype_id 	= (int) $current_subtype_id;
			$arrEntity->stype_ps 	= $index_current_subtype;

			array_push($data->entities, $arrEntity);
		}

		return $data;
	}

}

/* End of file Entidad_model.php */
/* Location: ./application/models/Entidad_model.php */