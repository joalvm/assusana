<!DOCTYPE html>
<html lang="es-PE" data-module="{view}">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta class="csrf" name="csrf-param" content="<?= $this->security->get_csrf_token_name(); ?>">
    <meta class="csrf" name="csrf-token" content="<?= $this->security->get_csrf_hash(); ?>">

    <meta name="base-url" content="<?= base_url(); ?>">
    
    <title>{title_head}</title>

    <link async rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">
    
    <link rel="stylesheet" href="<?= base_url('/static/css/magnific-popup.css'); ?>">
    <link rel="stylesheet" href="<?= base_url('/static/css/workshops.min.css'); ?>">
    <style type="text/css">
        .material-icons {
            font-feature-settings: 'liga';
        }
    </style>
</head>
<body class="no-body-app {view}">
	<header>
        <div class="container">
            <div class="row">
                <div class="col s12">
                    <h1 class="header-page">{title_body}</h1>
                </div>
            </div>
        </div>
    </header>
    <main>
    	{body}
    </main>
    <div class="copy">
        <em>Copyright © <?= date('Y'); ?> Secretaría de Gestión Publica, Todos los derechos reservados.</em>
    </div>
    
</body>
</html>

<script type="text/javascript" src="<?= base_url('/static/js/jquery.min.js'); ?>"></script>
<script type="text/javascript" src="<?= base_url('/static/js/jquery.magnific-popup.min.js'); ?>"></script>
<script type="text/javascript" src="<?= base_url('/static/js/moment-with-locales.min.js'); ?>"></script>
<script type="text/javascript" src="<?= base_url('/static/js/workshops.min.js'); ?>"></script>
