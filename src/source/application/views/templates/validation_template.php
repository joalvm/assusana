<!DOCTYPE html>
<html lang="es-PE">
<head>
	<meta charset="UTF-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1.0"/>
    <meta name="robots" content="NOINDEX, NOFOLLOW" />

    <meta name="base-url" content="<?= base_url(); ?>">

	<title>Validación del registro del docente</title>
	
    <link async rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">
    
    <link rel="stylesheet" href="<?= base_url('/static/css/login.min.css'); ?>">
</head>
<body class="{body_class}">
    <main>
    	{body}
    </main>
</body>
</html>