<?php 
$rseg 		= $this->uri->rsegments;
$controller = $rseg[1];
$method 	= $rseg[2];
$menu 		= [];

if ( $this->session->role == 1 )
{
	$menu = [
		[
			"text" => "Tablero de control",
			"icon" => "&#xE871",
			"id" => "",
			"unique" => TRUE,
			"controller" => ["dashboard"],
			"method" => ["index"],
			"href" => base_url("app/dashboard.html")
		],
		[
			"text" => "Docentes",
			"icon" => "&#xE8D3;",
			"id" => "view_coordinador",
			"unique" => TRUE,
			"controller" => ["coordinator"],
			"method" => ["index", 'inscription_create'],
			"href" => base_url("app/coordinator.html")
		],
		[
			"text" => "Participantes",
			"icon" => "&#xE85E;",
			"id" => "view_coordinador",
			"unique" => TRUE,
			"controller" => ["inscription"],
			"method" => ["index"],
			"href" => base_url("app/inscription.html")
		],
		[
			"text" => "Capacitaciones",
			"icon" => "&#xE616;",
			"id" => "",
			"unique" => FALSE,
			"controller" => ["training"],
			"method" => ["index"],
			"sub_items" => [
				[
					"text" => "Registro",
					"method" => ["register", "register_create"],
					"id" => "",
					"href" => base_url("app/training/register.html")
				],
				[
					"text" => "Programación",
					"method" => ["programming", 'programming_create'],
					"id" => "",
					"href" => base_url("app/training/programming.html")
				]
			]
		],
		[
			"text" => "Certificados",
			"icon" => "&#xE431;",
			"id" => "",
			"unique" => TRUE,
			"controller" => ["certified"],
			"method" => ["index"],
			"href" => base_url("app/certified.html")
		],
		[
			"text" => "Reportes",
			"icon" => "&#xE85C;",
			"id" => "",
			"unique" => FALSE,
			"controller" => ["reports"],
			"method" => ["index"],
			"sub_items" => [
				[
					"text" => "General",
					"method" => ["report_general"],
					"id" => "",
					"href" => base_url("app/reports/report_general.html"),
					"target" => '_blank'
				],
				[
					"text" => "Asistencias",
					"method" => ["attendance"],
					"id" => "",
					"href" => base_url("app/reports/attendance.html")
				],
				[
					"text" => "Capacitaciones",
					"method" => ["report_training"],
					"id" => "",
					"href" => base_url("app/reports/report_training.html"),
					"target" => '_blank'
				],

				[
					"text" => "Por tipo de entidad",
					"method" => ["report_cppte"],
					"id" => "",
					"href" => base_url("app/reports/report_cppte.html"),
					"target" => '_blank'
				]
			]
		],
		[
			"text" => "Mantenimiento",
			"icon" => "&#xE85D",
			"id" => "",
			"unique" => FALSE,
			"controller" => ["maintenance"],
			"method" => ["index"],
			"sub_items" => [
				[
					"text" => "Tipo de capacitación",
					"method" => ["type_training"],
					"id" => "",
					"href" => base_url("app/maintenance/training/type.html")
				],
				[
					"text" => "Temas/Subtemas",
					"method" => ["themes"],
					"id" => "",
					"href" => base_url("app/maintenance/themes.html")
				],
				[
					"text" => "Entidades",
					"method" => ["entities"],
					"id" => "",
					"href" => base_url("app/maintenance/entities.html")
				]
			]
		],
		[
			"text" => "Cerrar Sesión",
			"icon" => "&#xE879",
			"id" => "logout-link",
			"unique" => TRUE,
			"controller" => ["login"],
			"method" => ["index"],
			"href" => base_url("login.html")
		]
	];
}
else if ( $this->session->role == 2 )
{
	$menu = [
		[
			"text" => "Tablero de control",
			"icon" => "&#xE871",
			"id" => "",
			"unique" => TRUE,
			"controller" => ["dashboard"],
			"method" => ["index"],
			"href" => base_url("app/dashboard.html")
		],
		[
			"text" => "Inscripciones",
			"icon" => "&#xE85E;",
			"id" => "",
			"unique" => TRUE,
			"controller" => ["inscription"],
			"method" => ["index"],
			"href" => base_url("docente/inscription.html")
		],
		[
			"text" => "Asistencias",
			"icon" => "&#xE862;",
			"id" => "",
			"unique" => TRUE,
			"controller" => ["attendance"],
			"method" => ["index"],
			"href" => base_url("docente/attendance.html")
		],
		[
			"text" => "Evaluaciones",
			"icon" => "&#xE873;",
			"id" => "",
			"unique" => TRUE,
			"controller" => ["evaluation"],
			"method" => ["index"],
			"href" => base_url("docente/evaluation.html")
		],
		[
			"text" => "Reportes",
			"icon" => "&#xE85C;",
			"id" => "",
			"unique" => FALSE,
			"controller" => ["reports"],
			"method" => ["index", "attendance"],
			"sub_items" => [
				[
					"text" => "Asistencias",
					"method" => ["attendance"],
					"id" => "",
					"href" => base_url("docente/reports/attendance.html")
				]
			]
		],
		[
			"text" => "Certificados",
			"icon" => "&#xE431;",
			"id" => "",
			"unique" => TRUE,
			"controller" => ["certified"],
			"method" => ["index"],
			"href" => base_url("docente/certified.html")
		],
		[
			"text" => "Cerrar Sesión",
			"icon" => "&#xE879",
			"id" => "logout-link",
			"unique" => TRUE,
			"controller" => ["login"],
			"method" => ["index"],
			"href" => base_url("login.html")
		]
	];

	if( $this->session->username != 'vHostosc' )
		array_splice($menu, 5, 1);
}

?>
<header>

	<nav class="navbar top-nav">
		<div class="container">
			<a class="page-title">
				<button type="button"
					data-activates="nav-mobile"
					class="open-menu button-collapse hide-on-large-only">
					<i class="material-icons">&#xE5D2;</i>
				</button>
				<span class="title"><?= ucfirst($page) ?> <small><?= $subpage ?></small></span>
			</a>
		</div>
	</nav>

	<div id="nav-mobile" class="side-nav fixed">
		<ul class="menu">
			<li class="menu-item logo">
				<a class="logo-content" href="<?= base_url('app/dashboard.html') ?>">
					<object id="front-page-logo" 
						class="logo" 
						type="image/svg+xml"
						 data="<?= base_url('static/img/SGPlogo.svg') ?>"></object>
					<!--<img src="" alt="SNP logo">-->
				</a>
			</li>
			<li class="menu-item user-info">
				
				<span class="icons"><?= strtoupper(substr($this->session->lastname, 0, 1)); ?></span>
				
				<?php
					$name_arr = explode(" ", $this->session->name);
					$lname_arr = explode(" ", $this->session->lastname);
				?>

				<p>
					<span class="name">
						
						<?= $name_arr[0] . " " . $this->session->lastname?>
						
						<span class="admin">
							<?= ($this->session->role == 1) ? 'Administrador': 'Coordinador'; ?>
						</span>
					</span>
				</p>

			</li>

			<?php foreach ($menu as $index => $item): ?>

					<?php $current = (in_array($controller, $item['controller']) ? 'current' : ''); ?>

				<?php if ( $item["unique"] ): ?>

						<li class="menu-item <?= $current; ?>">
							<a id="<?= $item['id'] ?>" href="<?= $item['href'] ?>">
								<i class="material-icons"><?= $item['icon'] ?></i>
								<span><?= $item['text'] ?></span>
							</a>
						</li>

				<?php else: ?>

						<?php $open_submenu = !empty($current) ? 'active' : ''; ?>
						
						<li class="menu-item no-pad  <?= $current; ?>">
							<ul class="collapsible collapsible-accordion">

								<li>

									<a id="<?= $item['id'] ?>" class="collapsible-header <?= $open_submenu; ?>">
										<i class="material-icons"><?= $item['icon'] ?></i>
										<span><?= $item['text'] ?></span>
									</a>

									<div class="collapsible-body">
										<ul class="submenu">

											<?php foreach ($item['sub_items'] as $index2 => $subitem ): ?>
												
												<?php 

													$mt_current = ((in_array($method, $subitem['method']) && 
																	in_array($controller, $item['controller'])) ? 'current' : '');

													$target 	= (isset($subitem['target'])) ? 'target="'.$subitem['target'].'"': '';
													$tooltip 	= '';

													if (isset($subitem['tooltip'])) 
														$tooltip = 'data-position="bottom" data-delay="50" data-tooltip="'.$subitem['tooltip'].'"';
												?>

													<li class="submenu-item <?= $mt_current ?>">
														<a id="<?= $subitem['id'] ?>" href="<?= $subitem['href']; ?>" <?= $target . ' ' . $tooltip ?> > 
															<?= $subitem["text"] ?>
														</a>
													</li>

											<?php endforeach; ?>

										</ul>
									</div>
								</li>
							</ul>
						</li>

				<?php endif; ?>

			<?php endforeach; ?>
		</ul>
	</div>

</header>
