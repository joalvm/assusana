<!DOCTYPE html>
<html lang="es-PE" data-section="{module}" id="{id}" data-module="{crud_page}">
<head>

	<meta charset="UTF-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1.0"/>
    <meta name="robots" content="NOINDEX, NOFOLLOW" />
    
	<meta class="csrf" name="csrf-param" content="<?= $this->security->get_csrf_token_name(); ?>">
	<meta class="csrf" name="csrf-token" content="<?= $this->security->get_csrf_hash(); ?>">
	
	<meta name="base-url" content="<?= base_url(); ?>">

	<title>{page} {subpage} - Modulo capacitaciones</title>

	<link async rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">

	<link rel="stylesheet" href="<?= base_url('/static/css/magnific-popup.css'); ?>">
    <link rel="stylesheet" href="<?= base_url('/static/css/application/{module}.min.css'); ?>">

</head>
<body>

	<?php $this->load->view('templates/header_view');  ?>

	<main>
		{body}
	</main>
	
</body>
</html>
<script type="text/javascript" src="<?= base_url('/static/js/jquery.min.js') ?>"></script>
<script type="text/javascript" src="<?= base_url('/static/js/jquery.magnific-popup.min.js') ?>"></script>
<script type="text/javascript" src="<?= base_url('/static/js/moment-with-locales.min.js') ?>"></script>

<?php 
	$rseg 		= $this->uri->rsegments;
	$controller = $rseg[1];
?>

<?php if ( $controller == 'dashboard' ): ?>
	
	<script type="text/javascript" src="<?= base_url('/static/js/canvasjs.min.js'); ?>"></script>

<?php endif; ?>

<script type="text/javascript" src="<?= base_url('/static/js/application/{module}.min.js'); ?>"></script>