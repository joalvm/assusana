<!DOCTYPE html>
<html lang="es-PE">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
    <meta name="robots" content="NOINDEX, NOFOLLOW" />

    <meta name="csrf-param" content="<?= $this->security->get_csrf_token_name(); ?>">
    <meta name="csrf-token" content="<?= $this->security->get_csrf_hash(); ?>">

    <meta name="base-url" content="<?= base_url(); ?>">
    
    <title>Acceso :: Modulo de Capacitaciones</title>

    <link async rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">

    <link rel="stylesheet" href="<?= base_url('/static/css/login.min.css'); ?>">

    <style type="text/css">
        .copy {
            max-width: 100%!important;
            margin-bottom: 5px;
            margin-top: 10px;
        }

        .copy em {
            color: #707070;
            font-family: Arial;
            font-size: 0.85rem;
            text-align: center;
            letter-spacing: -0.25px;
            display: block;
        }
    </style>
  </head>
  <body class="login">
    <header>
      <div class="container">
        <div class="row">
          <div class="col s12">
            <h1>LOGIN</h1>
          </div>
        </div>
      </div>
    </header>
    <main class="container">
      <div class="row">
        <div class="col s12">
          <div class="inner">
            <object id="front-page-logo" class="logo" type="image/svg+xml" data="<?= base_url('static/img/SGPlogo.svg') ?>"></object>
            <!--<img class="logo" src="" alt="SNP logo">-->
            <form id="frm_login" action="<?= base_url('/app/dashboard.html'); ?>" method="post">
              <div class="row no-margin-b">
                <div class="input-field col s12">
                  <i class="material-icons prefix">&#xE853;</i>
                  <input type="text" required id="txtuser" name="username" autofocus class="req">
                  <label for="txtuser">Usuario</label>
                </div>
              </div>
              <div class="row">
                <div class="input-field col s12">
                  <i class="material-icons prefix">&#xE85E;</i>
                  <select id="cbo_role" class="req" name="role" required>
                    <option value="1">Administrador</option>
                    <option value="2">Coordinador</option>
                  </select>
                  <label>Rol de usuario</label>
                </div>
              </div>
              <div class="row">
                <div class="input-field col s12">
                  <i class="material-icons prefix">&#xE0DA;</i>
                  <input id="txtpass" required name="password" type="password" class="req">
                  <label for="txtpass">Contraseña</label>
                </div>
              </div>
              <div class="row hide">
                <div class="col s12">
                  <div class="switch">
                    <label>Recuérdame
                      <input type="checkbox" name="record" id="chkrecord">
                      <span class="lever"></span>
                    </label>
                  </div>
                </div>
              </div>
              <br>
              <div class="row">
                <div class="col s12">
                  <button type="submit" 
                    class="btn btn-large waves-effect waves-light" 
                    name="action">ACCEDER</button>
                </div>
              </div>
            </form>
          </div>
        </div>
      </div>
    </main>
    <div class="copy">
      <em>Copyright © <?= date('Y'); ?> Secretaría de Gestión Publica, Todos los derechos reservados.</em>
    </div>
  </body>
</html>

<script type="text/javascript" src="<?= base_url('/static/js/jquery.min.js'); ?>"></script>
<script type="text/javascript" src="<?= base_url('/static/js/login.min.js'); ?>"></script>
