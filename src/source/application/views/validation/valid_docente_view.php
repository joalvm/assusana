<div class="container">
	<div class="row">
		<div class="col s12">
			<h1 class="title-page">Validación correcta</h1>
			<p class="description-page">Tu información registrada es la que se encuentra descrita, apartir de estos momentos
			tienes acceso a nuestro modulo de docentes.</p>
		</div>
	</div>
	<div class="row">
		<div class="col s12">
			<h4 class="title">
				<span>
					<i class="material-icons">&#xE7FD;</i>
					<?= ($items->name . " " . $items->lastname) ?>
				</span>
				<small class="profession">
					<i class="material-icons">&#xE80C;</i>
					<?= $items->profession ?>
				</small>
			</h4>
		</div>
	</div>
	<div class="row basic-info">
		<div class="col s4">
			<span class="descript center dni">
				<b>DNI<br></b>
				<?= $items->dni ?>
			</span>
		</div>
		<div class="col s4">
			<span class="descript center username">
				<b>USUARIO<br></b>
				<?= $items->username ?>
			</span>
		</div>
		<div class="col s4">
			<span class="descript center gender">
				<b>GENERO<br></b>
				<?= (($items->gender == 0) ? 'Masculino' : 'Femenino') ?>
			</span>
		</div>
	</div>
	<div class="row">
		<div class="col s12">
			<span class="descript email">
				<b>Email<small>(s)</small>: <br></b>
				<?= str_replace(",", "<br>", $items->email) ?>
			</span>
			
			<br>
			<div class="divider"></div>
			<br>

			<span class="descript type">
				<b>Typo de Entidad: <br></b>
				<?= $items->type_entity ?>
			</span>
			<span class="descript subtype">
				<b>Sub tipo de Entidad: <br></b>
				<?= $items->subtype_entity ?>
			</span>
			<span class="descript entity">
				<b>Nombre de Entidad: <br></b>
				<?= $items->entity ?>
			</span>

			<span class="data-created">
				<b>Fecha de Registro</b><br>
			<?= $items->date_created ?>
			</span>
		</div>
	</div>

	<div class="row">
		<div class="col s12">
			<a href="<?= base_url('login.html') ?>" class="btn btn-large center">Acceder al Sistema</a>
		</div>
	</div>
</div>