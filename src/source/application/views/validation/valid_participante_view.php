<div class="container" style="text-align: center;">
	<div class="row">
		<div class="col s12">
			<h4 class="title-page">Validación correcta</h4>
			<p class="description-page">
				Tu incripción ha sido validada, recuerda las fechas en las que se 
				celebrará esta capaciticación.
			</p>
		</div>
	</div>
	<div class="row">
		<div class="col s12">
			<h4 class="title">
				<span>
					<i class="material-icons">&#xE7FD;</i> <br>
					<?= ($items->name . " " . $items->last_name) ?>
				</span>
			</h4>
		</div>
	</div>
	<div class="row basic-info">
		<div class="col s12">
			<span class="descript center dni">
				<b>DNI: </b><?= $items->dni ?>
			</span>
		</div>
		<div class="col s12">
			<span class="descript center gender">
				<b>Codigo de Registro: </b> <?= $items->request_number ?>
			</span>
		</div>
	</div>
	<div class="row">
		<div class="col s12">
			<span class="descript email">
				<b>Email<small>(s)</small>: <br></b>
				<?= str_replace(",", "<br>", $items->email) ?>
			</span>
			
			<br>
			<div class="divider"></div>
			<br>

			<span class="descript type">
				<b>Fecha de Ralización: <br></b>
				<?= $items->date_realization ?><br>
			</span>
			<span class="descript subtype">
				<b>Hora de Inicio: <br></b>
				<?= $items->time_start ?><br>
			</span>
			<span class="descript entity">
				<b>Hora de Finalización: <br></b>
				<?= $items->time_end ?><br>
			</span>
		</div>
	</div>
</div>