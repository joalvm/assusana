<div class="container">
<div class="row">
<div class="col s12">
<div class="inner">

<div class="row">
	<div class="col s12 input-field">
		<input type="text" id="txt_programming" 
		data-programming="0" data-mfp-src="#md-list-programming" readonly="readonly">
		<label for="txt_programming">Lista de programaciones</label>
	</div>
</div>

<div class="row">
	<ul id="programming-data" style="display: none;" class="collection collection-table">
		<li class="collection-item row">
			<div class="col s12 m3 th right-align">Tipo de programación:</div>
			<div id="text_type_training" class="col s12 m9 td"></div>
		</li>
		<li class="collection-item row">
			<div class="col s12 m3 th right-align">Lugar de realización:</div>
			<div id="text_place" class="col s12 m9 td"></div>
		</li>
		<li class="collection-item row">
			<div class="col s12 m3 th right-align">Dirección:</div>
			<div id="text_address" class="col s12 m9 td"></div>
		</li>
		<li class="collection-item row">
			<div class="col s12 m3 th right-align">Entidad Personalizada:</div>
			<div id="text_entity_name" class="col s12 m9 td"></div>
		</li>
		<li class="collection-item row">
			<div class="col s12 m4 center-align">
				<span class="th">Departamento</span>
				<span id="text_departamento" class="td"></span>
			</div>
			<div class="col s12 m4 center-align">
				<span class="th">Provincia</span>
				<span id="text_provincia" class="td"></span>
			</div>
			<div class="col s12 m4 center-align">
				<span class="th">Distrito</span>
				<span id="text_distrito" class="td"></span>
			</div>
		</li>
		<li class="collection-item row">
			<div class="col s12 m4 center-align">
				<span class="th">Dia de realización</span>
				<span id="text_date_realization" class="td"></span>
			</div>
			<div class="col s12 m4 center-align">
				<span class="th">Hora de inicio</span>
				<span id="text_hour_start" class="td"></span>
			</div>
			<div class="col s12 m4 center-align">
				<span class="th">Hora de finalización</span>
				<span id="text_hour_finish" class="td"></span>
			</div>
		</li>
		<li class="collection-item row">
			<div class="col s12 m4 center-align">
				<span class="th">Max. Participantes</span>
				<span id="text_max_participant" class="td"></span>
			</div>
			<div class="col s12 m4 center-align">
				<span class="th">Personas Registradas</span>
				<span id="text_person_registered" class="td"></span>
			</div>
			<div class="col s12 m4 center-align">
				<span class="th">Prog. Personalizada</span>
				<span id="text_its_personalize" class="td"></span>
			</div>
		</li>
	</ul>
</div>

<div class="row">
	<div class="col s12 m8">
		<!--FILTRADO-->
		<div class="searcher">
			<input type="search" autofocus placeholder="Buscar DNI, Nombres completos" id="search_participant" autocomplete="off" spellcheck="false" >
			<i class="material-icons clear_search">&#xE5CD;</i>
			<label for="search">
				<i class="material-icons">&#xE8B6;</i>
			</label>
		</div>
	</div>
	<div class="col s12 m4">
		<p class="reverse-chk">
			<input type="checkbox" class="filled-in" id="chk_all" />
			<label for="chk_all">Todos</label>
		</p>
	</div>
	<div class="col s12">
		<ul id="list-participant" class="collection-main modal-list">
			<li class="empty-list">
				<h4>Seleccione una programación</h4>
			</li>
		</ul>

	</div>
</div>

</div>
</div>
</div>
</div>

<section id="md-list-programming" class="white-popup mfp-with-anim mfp-hide">
	<div class="popup-header">
		<h4 class="title">Capacitaciones</h4>
	</div>
	<div class="popup-body">

		<!--FILTRADO-->
		<div class="searcher in-modal">
			<input type="search" autofocus placeholder="Buscar" id="search" autocomplete="off" spellcheck="false" >
			<i class="material-icons clear_search">close</i>
			<label for="search"><i class="material-icons">search</i></label>
		</div>

		<ul id="list-programming" class="collection modal-list">
			<?php 
			foreach ( $programming as $row => $cell ) 
			{
			?>
				<li class="collection-item" data-search="<?= $cell->training_title ?>" data-id="<?= $cell->programming_id ?>">
					<p>
						<input class="with-gap" 
							type="radio" 
							id="programming<?= $cell->programming_id ?>" 
							name="programming"
							data-title="<?= $cell->training_title ?>"
							value="<?= $cell->programming_id ?>">
						<label for="programming<?= $cell->programming_id ?>">
							<span class="title entity truncate" title="<?= $cell->training_title ?>"><?= $cell->training_title ?></span>
							<span class="title2 type">
								<i class="material-icons">&#xE192;</i>&nbsp;
								<time class="humanize" 
									data-type="date" 
									data-datetime="<?= $cell->date_realization . " " . $cell->time_start ?>"></time>
							</span>
						</label>
					</p>
				</li>
			<?php
			}
			?>
		</ul>
	</div>
	<div class="popup-footer">
		<button type="button" id="btn-out-training" class="btn-flat waves waves-effect">Salir</button>
	</div>
</section>