<div class="container">
<div class="row">
<div class="col s12">
<div class="inner">

<div class="row">
	<div class="col s12 input-field">
		<input type="text" id="txt_programming" 
		data-programming="0" data-mfp-src="#md-list-programming" readonly="readonly">
		<label for="txt_programming">Lista de programaciones</label>
	</div>
</div>
<div class="row">
	<div class="col s12 file-field input-field">
		<a href="javascript:void(0)" target="_blank" class="hide" id="file_saved">
			<i class="fa" aria-hidden="true"></i>
			<span class="size"></span>
		</a>
		<div class="btn disabled">
			<span>Exámen</span>
			<input type="file" disabled="disabled" id="file_examen" name="examen" accept="application/pdf,application/msword,application/vnd.openxmlformats-officedocument.wordprocessingml.document">
		</div>
		<div class="file-path-wrapper">
			<input class="file-path validate" readonly="readonly" id="txt_namefile" type="text">
		</div>
	</div>
</div>
<div class="row">
	<div class="col s12">
		<!--FILTRADO-->
		<div class="searcher">
			<input type="search" autofocus placeholder="DNI, Nombres o Apellidos" id="search_participant" autocomplete="off" spellcheck="false" >
			<i class="material-icons clear_search">&#xE5CD;</i>
			<label for="search">
				<i class="material-icons">&#xE8B6;</i>
			</label>
		</div>
	</div>
</div>
<div class="row">
	<div class="col s12">
		<ul id="list-participant" class="collection-main">
			<li class="empty-list">
				<h4>Seleccione una programación</h4>
			</li>
		</ul>
	</div>
</div>

</div>
</div>
</div>
</div>

<section id="md-list-programming" class="white-popup mfp-with-anim mfp-hide">
	<div class="popup-header">
		<h4 class="title">Capacitaciones</h4>
	</div>
	<div class="popup-body">

		<!--FILTRADO-->
		<div class="searcher in-modal">
			<input type="search" autofocus placeholder="Buscar" id="search" autocomplete="off" spellcheck="false" >
			<i class="material-icons clear_search">close</i>
			<label for="search"><i class="material-icons">search</i></label>
		</div>

		<ul id="list-programming" class="collection modal-list">
			<?php 
			foreach ( $programming as $row => $cell ) 
			{
			?>
				<li class="collection-item" data-search="<?= $cell->training_title ?>" data-id="<?= $cell->programming_id ?>">
					<p>
						<input class="with-gap" 
							type="radio" 
							id="programming<?= $cell->programming_id ?>" 
							name="programming"
							data-title="<?= $cell->training_title ?>"
							value="<?= $cell->programming_id ?>">
						<label for="programming<?= $cell->programming_id ?>">
							<span class="title entity truncate" title="<?= $cell->training_title ?>"><?= $cell->training_title ?></span>
							<span class="title2 type">
								<i class="material-icons">&#xE192;</i>&nbsp;
								<time class="humanize" 
									data-type="date" 
									data-datetime="<?= $cell->date_realization . " " . $cell->time_start ?>"></time>
							</span>
						</label>
					</p>
				</li>
			<?php
			}
			?>
		</ul>
	</div>
	<div class="popup-footer">
		<button type="button" id="btn-out-training" class="btn-flat waves waves-effect">Salir</button>
	</div>
</section>