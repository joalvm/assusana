<div class="container">
<div class="row">
<div class="col s12">
<div class="inner">

<div class="row no-print">
	<div class="col s12 input-field">
		<input type="text" id="txt_programming" 
		data-programming="0" data-mfp-src="#md-list-programming" readonly="readonly">
		<label for="txt_programming">Lista de programaciones</label>
	</div>
</div>

<div class="row">
	<div class="col s12">
		<table class="table responsive-table" style="display: none;" id="programming-data">
			<caption>
				<h2 id="text_trining_title"></h2>
				<h4 id="text_type_training"></h4>
				<b>Lugar:&nbsp;</b><span id="text_place"></span><br>
				<b>Dirección:&nbsp;</b><span id="text_address"></span><br>
				<b>Entidad Personalizada:&nbsp;</b><span id="text_entity_name"></span><br>
				 <object id="front-page-logo" class="logo only-print" type="image/svg+xml" data="<?= base_url('static/img/SGPlogo.svg') ?>"></object>
			</caption>
			<tbody>
				<tr>
					<td valign="middle">
						<b>	Departamento</b>
						<span id="text_departamento" class="td"></span>
					</td>
					<td valign="middle">
						<b>	Provincia</b>
						<span id="text_provincia" class="td"></span>
					</td>
					<td valign="middle">
						<b>	Distrito</b>
						<span id="text_distrito" class="td"></span>
					</td>
				</tr>
				<tr>
					<td valign="middle">
						<b>	Dia de realización</b>
						<span id="text_date_realization" class="td"></span>
					</td>
					<td valign="middle">
						<b>	Hora de inicio</b>
						<span id="text_hour_start" class="td"></span>
					</td>
					<td valign="middle">
						<b>	Hora de finalización</b>
						<span id="text_hour_finish" class="td"></span>
					</td>
				</tr>
				<tr>
					<td valign="middle">
						<b>Max. Participantes</b>
						<span id="text_max_participant" class="td"></span>
					</td>
					<td valign="middle">
						<b>Personas Registradas</b>
						<span id="text_person_registered" class="td"></span>
					</td>
					<td valign="middle">
						<b>N° de Asistentes</b>
						<span id="text_cant_asistentes" class="td"></span>
					</td>
				</tr>
				<tr>
					<td valign="middle" colspan="3">
						<b>	Prog. Personalizada</b>
						<span id="text_its_personalize" class="td"></span>
					</td>
				</tr>
			</tbody>
		</table>
	</div>
	<div class="col s12">
		<table class="table table-themes responsive-table" id="programming-themes" style="display: none;">
			<thead>
				<tr><th>Temas</th><th>Subtemas</th></tr>
			</thead>
			<tbody></tbody>
		</table>
	</div>
</div>

<div class="row">
	<div class="col s12">
		<div class="divider-page">
			<h2>Personas registradas</h2>
		</div>
		<ul id="list-participant" class="collection-main">
			<li class="empty-list">
				<h4>Seleccione una programación</h4>
			</li>
		</ul>
		<form id="frm_certificate" action="<?= base_url('docente/certified/create'); ?>" method="post" target="_blank">
			
		</form>
	</div>
</div>

</div>
</div>
</div>
</div>

<section id="md-list-programming" class="white-popup mfp-with-anim mfp-hide">
	<div class="popup-header">
		<h4 class="title">Capacitaciones</h4>
	</div>
	<div class="popup-body">

		<!--FILTRADO-->
		<div class="searcher in-modal">
			<input type="search" autofocus placeholder="Buscar" id="search" autocomplete="off" spellcheck="false" >
			<i class="material-icons clear_search">close</i>
			<label for="search"><i class="material-icons">search</i></label>
		</div>

		<ul id="list-programming" class="collection modal-list">
			<?php 
			foreach ( $programming as $row => $cell ) 
			{
			?>
				<li class="collection-item" data-search="<?= $cell->training_title ?>" data-id="<?= $cell->programming_id ?>">
					<p>
						<input class="with-gap" 
							type="radio" 
							id="programming<?= $cell->programming_id ?>" 
							name="programming"
							data-title="<?= $cell->training_title ?>"
							value="<?= $cell->programming_id ?>">
						<label for="programming<?= $cell->programming_id ?>">
							<span class="title entity truncate" title="<?= $cell->training_title ?>"><?= $cell->training_title ?></span>
							<span class="title2 type">
								<i class="material-icons">&#xE192;</i>&nbsp;
								<time class="humanize" 
									data-type="date" 
									data-datetime="<?= $cell->date_realization . " " . $cell->time_start ?>"></time>
							</span>
						</label>
					</p>
				</li>
			<?php
			}
			?>
		</ul>
	</div>
	<div class="popup-footer">
		<button type="button" id="btn-out-training" class="btn-flat waves waves-effect">Salir</button>
	</div>
</section>