<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html;charset=utf-8">
		<style type="text/css">
			/*background: url('<?= $_SERVER['DOCUMENT_ROOT'] . '/static/img/fondo.jpg' ?>');*/
			html {
				font-size: 16px;
				font-family: Arial, Sans-Serif;
				height: 100%;
				width: 100%;
			}

			body {
				padding: 0px;
				margin: 0px;
				height: 100%;
				width: 100%;
			}

			@page pagina {
				size: A4;
			}

			.page-constan {
				page:pagina;
				position: relative;
				page-break-after: always;
				break-after: always;
				page-break-inner: always;
				break-inner: always;
				height: 100%;
				width: 100%;
			}

			h2 {
				position: relative;
				top: 40%;
				display: block;
				text-align: center;
				font-weight: bold;
				color: #222;
			}

			.dept {
				position: absolute;
				top: 60%;
				left: 10%;
				color: #222;
				font-weight: bolder;
				display: block;
			}

			.day {
				position: absolute;
				display: block;
				top: 60%;
				left: 27.5%;
				color: #222;
				font-weight: bolder;
			}

			.month {
				position: absolute;
				display: block;
				top: 60%;
				left: 42%;
				color: #222;
				font-weight: bolder;
			}

		</style>
	</head>
	<body>
	<!--<img width="100%" height="100%" src="" alt="">-->
	<?php 
	foreach ( $info as $row => $cell ) 
	{
		$name = ($cell->participant_name . ' ' . $cell->participant_lastname);
		$date = explode("-", $cell->date_realization);
		$months = [NULL, 'Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre'];
	?>
		<div class="page-constan">

			<h2><?= mb_strtoupper($name, 'UTF-8'); ?></h2>
			<span class="dept"><?= $cell->departamento; ?></span>
			<span class="day"> <?= $date[2]; ?></span> 
			<span class="month"> <?= $months[((int)$date[1])]; ?></span>
		</div>
	<?php
	}
	?>
	</body>
</html>