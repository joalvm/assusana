<div class="container">
<div class="row">
<div class="col s12">
<div class="inner">

<div class="row">
	<div class="col s12 input-field">
		<input type="text" id="txt_programming" 
		data-programming="0" data-mfp-src="#md-list-programming" readonly="readonly">
		<label for="txt_programming">Mis capacitaciones</label>
	</div>
</div>
<div class="row">
	<div class="col s12 m8">
		<!--FILTRADO-->
		<div class="searcher">
			<input type="search" autofocus placeholder="Buscar DNI, Nombres completos" id="search_participant" autocomplete="off" spellcheck="false" >
			<i class="material-icons clear_search">close</i>
			<label for="search"><i class="material-icons">search</i></label>
		</div>
	</div>

	<div class="col s12 m4">&nbsp;</div>

	<div class="col s12">
		<ul id="list-participant" class="collection-main">
			<li class="empty-list">
				<h4>Seleccione una programación</h4>
			</li>
		</ul>
	</div>
</div>

</div>
</div>
</div>
</div>

<section id="md-list-programming" class="white-popup mfp-with-anim mfp-hide">
	<div class="popup-header">
		<h4 class="title">Capacitaciones</h4>
	</div>
	<div class="popup-body">

		<!--FILTRADO-->
		<div class="searcher in-modal">
			<input type="search" autofocus placeholder="Buscar" id="search" autocomplete="off" spellcheck="false" >
			<i class="material-icons clear_search">close</i>
			<label for="search"><i class="material-icons">search</i></label>
		</div>

		<ul id="list-programming" class="collection modal-list">
			<?php 
			foreach ( $programming as $row => $cell ) 
			{
			?>
				<li class="collection-item" data-search="<?= $cell->training_title ?>" data-id="<?= $cell->programming_id ?>">
					<p>
						<input class="with-gap" 
							type="radio" 
							id="programming<?= $cell->programming_id ?>" 
							name="programming"
							data-title="<?= $cell->training_title ?>"
							value="<?= $cell->programming_id ?>">
						<label for="programming<?= $cell->programming_id ?>">
							<span class="title entity truncate" title="<?= $cell->training_title ?>"><?= $cell->training_title ?></span>
							<span class="title2 type">
								<i class="material-icons">&#xE192;</i>&nbsp;
								<time class="humanize" 
									data-type="date" 
									data-datetime="<?= $cell->date_realization . " " . $cell->time_start ?>"></time>
							</span>
						</label>
					</p>
				</li>
			<?php
			}
			?>
		</ul>
	</div>
	<div class="popup-footer">
		<button type="button" id="btn-out-training" class="btn-flat waves waves-effect">Salir</button>
	</div>
</section>

<section id="md-delete-inscription" class="white-popup mfp-with-anim mfp-hide">
	<div class="popup-header">
		<h4 class="title">¿Realmente deseas eliminar?</h4>
	</div>
	<div class="popup-body">
		<p class="message no-mar-bottom">
			<span class="text">
				Se eliminará de forma permanente esta inscripción.
			</span><br><br>
			Participante:<b class="participant"></b><br>
			F. de inscripción:<b class="date-inscription"></b>
		</p>
	</div>
	<div class="popup-footer">
		<button type="button" id="btn_continue" class="btn-flat waves waves-effect">Eliminar</button>
	</div>
</section>

<section id="md-participant" class="white-popup mfp-with-anim mfp-hide">
	<div class="popup-header">
		<h4 class="title">
			<span class="fullname"></span><br>
			<small class="grey-text text-darken-2">
				<b>DNI: </b>
				<span class="dni"></span>
			</small>
		</h4>
	</div>
	<div class="popup-body">
		<p class="item"><b>Genero: </b><span class="gender"></span></p>
		<p class="item"><b>Profesión: </b><span class="profesion"></span></p>
		<p class="item"><b>Cargo: </b><span class="cargo"></span></p>
		<p class="item"><b>Departamento: </b><span class="departamento"></span></p>
		<p class="item"><b>Provincia: </b><span class="provincia"></span></p>
		<p class="item"><b>Distrito: </b><span class="distrito"></span></p>

		<div class="divider"></div>

		<p class="item"><b>Celular: </b><span class="celular"></span></p>
		<p class="item"><b>Telefono: </b><span class="telefono"></span></p>
		<p class="item"><b>Anexo: </b><span class="anexo"></span></p>
		<p class="item"><b>Email: </b><span class="email"></span></p>

		<div class="divider"></div>
		<p class="item"><b>Entidad: </b><span class="entidad"></span></p>
		<p class="item"><b>Subtipo de entidad: </b><span class="subtipo"></span></p>
		<p class="item"><b>Tipo de entidad: </b><span class="tipo"></span></p>

		<div class="divider"></div>

		<p class="item"><b>Discapacidad: </b><span class="discapacidad"></span></p>
		<p class="item"><b>Detalle de discapacidad: </b><span class="detalle_discapacidad"></span></p>

		<br><div class="divider"></div>

		<small class="item"><b>Fecha de creación: </b><span class="creacion"></span></small>	
		
	</div>
	<div class="popup-footer"></div>
</section>