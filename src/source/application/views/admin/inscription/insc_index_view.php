<div class="container">
	<div class="row">
		<div class="col s12">
			<div class="inner">
				<div class="row">
					<div class="input-field col s12 m4">
						<select class="browser-default" name="filters" id="cbo_filters">
							<option value="1" <?= ($this->session->filter_prg == 1) ? 'selected' : '' ?>>Todos
							</option>
							<option value="2" <?= ($this->session->filter_prg == 2) ? 'selected' : '' ?>>Por
								realizarse</option>
							<option value="3" <?= ($this->session->filter_prg == 3) ? 'selected' : '' ?>>Concluidos
							</option>
							<option value="4" <?= ($this->session->filter_prg == 4) ? 'selected' : '' ?>>Cancelados
							</option>
							<option value="5" <?= ($this->session->filter_prg == 5) ? 'selected' : '' ?>>Personalizados
							</option>
						</select>
						<label for="cbofilter" class="active">Filtro de programaciones</label>
					</div>

					<div class="col s12 m8">
						<div class="input-field">
							<input type="text" id="txtprogramming" readonly="readonly"
								data-mfp-src="#md-list-programming">
							<label for="txtprogramming">Capacitaciones programadas</label>
						</div>
					</div>
				</div>
				<div class="row">
					<div class="col s12">
						<!--FILTRADO DE PERSONAS-->
						<div class="searcher">
							<input type="search" disabled="disabled" autofocus placeholder="Buscar Inscripción"
								id="search_inscription" autocomplete="off" spellcheck="false">
							<i class="material-icons clear_search">close</i>
							<label for="search"><i class="material-icons">search</i></label>
						</div>
					</div>
					<div class="col s12">
						<p class="legend">
							<label>
								<span class="icons-status amber darken-3" style="position:static"></span>
								Registro no activado
							</label>
							<label>
								<span class="icons-status green accent-4" style="position:static"></span>
								Registro activado
							</label>
						</p>
					</div>
				</div>
				<div class="row">
					<div class="col s12">
						<ul id="list-inscription" class="collection-main list-inscriptions">
							<li class="empty-list">
								<h4>ESCOJA UNA PROGRAMACIÓN 2</h4>
							</li>
						</ul>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

<section id="md-list-programming" class="white-popup mfp-with-anim mfp-hide">
	<div class="popup-header">
		<h4 class="title">Capacitaciones</h4>
	</div>
	<div class="popup-body">

		<!--FILTRADO DE PERSONAS-->
		<div class="searcher in-modal">
			<input type="search" autofocus placeholder="Buscar" id="search" autocomplete="off" spellcheck="false">
			<i class="material-icons clear_search">close</i>
			<label for="search"><i class="material-icons">search</i></label>
		</div>

		<ul id="list-programming" class="collection modal-list">
			<?php
            foreach ($programming as $row => $cell) {
                ?>
			<li class="collection-item"
				data-search="<?= $cell->training_title . " " . $cell->type_training ?>"
				data-id="<?= $cell->programming_id ?>">
				<p>
					<input class="with-gap" type="radio"
						id="programming<?= $cell->programming_id ?>"
						name="programming"
						data-title="<?= $cell->training_title ?>"
						value="<?= $cell->programming_id ?>">
					<label
						for="programming<?= $cell->programming_id ?>">
						<span class="title entity truncate"
							title="<?= $cell->training_title ?>"><?= $cell->training_title ?></span>
						<span class="title2 type">
							<i class="material-icons">&#xE192;</i>&nbsp;
							<time class="humanize" data-type="date"
								data-datetime="<?= $cell->date_realization . " " . $cell->time_start ?>"></time>
						</span>
						<span class="title2 type">
							<i class="material-icons">&#xE0AF;</i>&nbsp;
							<?= $cell->place; ?>
						</span>
						<small><b>Departamento:</b><?= $cell->departamento; ?></small><br>
						<small><b>Provincia:</b><?= $cell->provincia; ?></small><br>
						<small><b>Distrito:</b><?= $cell->distrito; ?></small>
					</label>
				</p>
				<?php
                    $inicial = $cell->its_customized ? "P" : "G";
                $color = $cell->its_canceled ? 'amber darken-3' : 'green accent-4';
                if ($cell->its_finish) {
                    $color = 'red';
                } ?>
				<span class="icons-status <?= $color ?>">
					<?= $inicial ?>
				</span>
			</li>
			<?php
            }
            ?>
		</ul>

	</div>
	<div class="popup-footer"> </div>

</section>

<section id="md-delete-inscription" class="white-popup mfp-with-anim mfp-hide">
	<div class="popup-header">
		<h4 class="title">¿Realmente deseas eliminar?</h4>
	</div>
	<div class="popup-body">
		<p class="message no-mar-bottom">
			<span class="text">
				Se eliminará de forma permanente esta inscripción.
			</span><br><br>
			Participante:<b class="participant"></b><br>
			F. de inscripción:<b class="date-inscription"></b>
		</p>
	</div>
	<div class="popup-footer">
		<button type="button" id="btn_exit" class="btn-flat waves waves-effect">Salir</button>
		<button type="button" id="btn_continue" class="btn-flat waves waves-effect">Eliminar</button>
	</div>
</section>

<section id="md-participant" class="white-popup mfp-with-anim mfp-hide">
	<div class="popup-header">
		<h4 class="title">
			<span class="fullname"></span><br>
			<small class="grey-text text-darken-2">
				<b>DNI: </b>
				<span class="dni"></span>
			</small>
		</h4>
	</div>
	<div class="popup-body">
		<div class="view">
			<p class="item"><b>Genero: </b><span class="gender"></span></p>
			<p class="item"><b>Profesión: </b><span class="profesion"></span></p>
			<p class="item"><b>Cargo: </b><span class="cargo"></span></p>
			<p class="item"><b>Departamento: </b><span class="departamento"></span></p>
			<p class="item"><b>Provincia: </b><span class="provincia"></span></p>
			<p class="item"><b>Distrito: </b><span class="distrito"></span></p>

			<div class="divider"></div>

			<p class="item"><b>Celular: </b><span class="celular"></span></p>
			<p class="item"><b>Telefono: </b><span class="telefono"></span></p>
			<p class="item"><b>Anexo: </b><span class="anexo"></span></p>
			<p class="item"><b>Email: </b><span class="email"></span></p>

			<div class="divider"></div>
			<p class="item"><b>Entidad: </b><span class="entidad"></span></p>
			<p class="item"><b>Subtipo de entidad: </b><span class="subtipo"></span></p>
			<p class="item"><b>Tipo de entidad: </b><span class="tipo"></span></p>

			<div class="divider"></div>

			<p class="item"><b>Discapacidad: </b><span class="discapacidad"></span></p>
			<p class="item"><b>Detalle de discapacidad: </b><span class="detalle_discapacidad"></span></p>

			<br>
			<div class="divider"></div>

			<small class="item"><b>Fecha de creación: </b><span class="creacion"></span></small>
		</div>
		<div class="edit" style="display:none;">
			<br>
			<form id="frm_edit_info" method="post" spellcheck="off" autocomplete="off">
				<div class="input-field">
					<input id="txtname" name="name" type="text">
					<label for="txtname">Nombres</label>
				</div>
				<div class="input-field">
					<input id="txtlastname" name="lastname" type="text">
					<label for="txtlastname">Apellidos</label>
				</div>
				<div class="input-field">
					<input id="txtdni" name="dni" minlength="8" maxlength="8" type="text">
					<label for="txtdni">DNI</label>
				</div>
			</form>
		</div>
	</div>
	<div class="popup-footer">
		<button type="button" id="btn_edit_info" class="btn-flat waves waves-effect">
			Editar datos generales
		</button>
	</div>
</section>