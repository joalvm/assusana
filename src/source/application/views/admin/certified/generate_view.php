<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html;charset=utf-8">
		<title>CERTIFICADOS :: <?= $info[0]->training_name ?></title>
		<style type="text/css" media="all">
			html {
				font-size: 16px;
				font-family: Arial, Sans-Serif;
			}

			body {
				padding: 0;
				margin: 0;
				height: 100%;
				width: 100%;
			}

			@page pagina {
				size: A4;
			}

			.page-constan {
				page:pagina;
				page-break-after: always;
				break-after: always;
				width: 100%;
				height: 100%;
			}

			.title-content {
				position: relative; 
				display: block;
				margin-top: 100px;
				margin-bottom: 100px;
			}

			.title-content h1 {
				display: block;
				text-align: center;
				margin-top: 70px;
				font-size: 40px;
				font-weight: bolder;
				letter-spacing: 2px;
				color: #444;
			}

			.title-content h2 {
				text-align: center;
				margin-top: 50px;
				font-size: 22px;
				text-transform: uppercase;
				padding-top: 40px;
				padding-bottom: 10px;
				border-bottom: 1px solid #aaa;
				color: #222;
				width: 85%;
				display: block;
				margin-left: auto;
				margin-right: auto;
			}

			.title-content p.body {
				margin-top:40px;
				color: #222;
				font-size: 18px;
				line-height: 1.6;
				width: 85%;
				margin-left: auto;
				margin-right: auto;
				text-align: justify;
			}

		</style>
	</head>
	<body>
	<?php  foreach ( $info as $row => $cell ): ?>
	<?php
		$date = explode("-", $cell->date_realization);
		$months = [
			NULL, 
			'Enero', 
			'Febrero', 
			'Marzo', 
			'Abril', 
			'Mayo', 
			'Junio', 
			'Julio', 
			'Agosto', 
			'Septiembre', 
			'Octubre', 
			'Noviembre', 
			'Diciembre'
		];
	?>
		<div class="page-constan">
			<div class="title-content">
				<h1 class="title"><b>CONSTANCIA</b></h1>
				<h2><?= mb_strtoupper(($cell->participant_lastname . ', ' . $cell->participant_name), 'UTF-8'); ?></h2>
				<p class="body">
					Por su asistencia al Taller de Capacitacion en&nbsp;<span><?= my_ucwords_ns($cell->tema); ?></span>:&nbsp;
					<span><?= my_ucwords_ns($cell->training_name); ?></span>, que se desarrolla en el marco de 
					la Política Nacional de Modernización de la Gestión Pública y que se realizó en&nbsp;<span><b><?= $cell->departamento; ?></b></span>&nbsp;el 
					<span>
						&nbsp;<b><?= $date[2] ?></b>
						&nbsp;de&nbsp;
						<b><?= $months[((int)$date[1])]; ?></b>
						&nbsp;de&nbsp;
						<b><?= $date[0] ?></b>.
					</span>
				</p>
			</div>
		</div>
	<?php endforeach; ?>
		
	</body>
</html>