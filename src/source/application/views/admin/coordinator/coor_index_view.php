<div class="container">
<div class="row">
<div class="col s12">
<div class="inner">

	<!--FILTRADO DE PERSONAS-->
	<div class="row search-coordinator">
		<div class="col s12">
			<div class="searcher">
				<input type="search" autofocus placeholder="Buscar" id="search" autocomplete="off" spellcheck="false" >
				<i class="material-icons clear_search">close</i>
				<label for="search"><i class="material-icons">search</i></label>
			</div>
		</div>
	</div>
	
	<div class="row">
		<div class="col s12" id="coor_index">
			<div class="inner no-pad-top">
				<ul class="collection collection-main" id="list-coordinator">

					<?php if ( ! empty($items) ): ?>
						<?php foreach ($items as $row => $cell): ?>
								
							<li data-search="<?=$cell->name . " " . $cell->lastname . " " . $cell->dni?>" class="collection-item" data-id="<?=$cell->id;?>">
									
									<a class="title truncate" data-mfp-src="#modal_fulldata" data-id="<?=$cell->id;?>">
										<?=$cell->name . " ". $cell->lastname;?>
									</a>

									<span class="description truncate"><?=$cell->email;?></span>

									<span class="last-modified">
										Modificado:&nbsp;
										<time class="humanize" data-type="datetime" data-datetime="<?=$cell->last_modified;?>"></time>

										<i class="material-icons tooltipped is_active <?=(!$cell->locked) ? 'active' : '';?>"
											title="<?=(!$cell->locked) ? 'Usuario Desbloqueado' : 'Usuario Bloqueado';?>"
											data-position="top" 
											data-delay="50" 
											data-tooltip="<?=(!$cell->locked) ? 'Usuario Desbloqueado' : 'Usuario Bloquedo';?>">&#xE061;</i>
									</span>
									
									<!--BOTON DE OPCIONES-->
									<button type="button" class="btn-options" data-activates='options_<?=$cell->id;?>'>
										<i class="material-icons">&#xE5D4;</i>
									</button>
									
									<!--MENU DE OPCIONES PARA CADA ITEM-->
									<ul id='options_<?=$cell->id;?>' class='dropdown-content'>
										<li>
											<button type="button" 
												id="restart_<?=$cell->id;?>" 
												class="btn-renove-pass" 
												data-id="<?= $cell->id ?>" 
												data-action="restore" 
												data-mfp-src="#popup-renove-pass" 
												<?=(!$cell->locked) ? '' : 'disabled';?>>
												<i class="material-icons">&#xE863;</i>
												Rest. Contraseña
											</button>
										</li>
										<li>
											<button type="button" 
												id="locked_<?=$cell->id;?>"
												class="btn-locked" 
												data-id="<?=$cell->id;?>" 
												data-action="<?= ($cell->locked) ? 'unlocked' : 'locked'; ?>" >
												<i class="material-icons"><?=(!$cell->locked) ? '&#xE899;' : '&#xE898;';?></i> 
												<?=(!$cell->locked) ? 'Bloq. Acceso' : 'Desbloq. Acceso';?>
											</button>
										</li>
										<li class="divider"></li>
										<li>
											<a href="<?=base_url('app/coordinator/'.$cell->id.'/update.html');?>"  
												data-id="<?=$cell->id;?>" 
												class="btn-modify" 
												data-action="modify" >
												<i class="material-icons">&#xE150;</i>
												Modificar
											</a>
										</li>
										<li>
											<button type="button"  
												id="delete_<?=$cell->id;?>"
												data-id="<?=$cell->id;?>" 
												class="btn-delete" 
												data-action="delete" 
												data-mfp-src="#popup-delete">
												<i class="material-icons">&#xE872;</i>
												Eliminar
											</button>
										</li>
									</ul>

							</li>

						<?php endforeach; ?>
					<?php else: ?>

							<li class="collection-item empty-list"><h4>No existen docentes registrados</h4></li>
					
					<?php endif; ?>

				</ul>
			</div>
		</div>
	</div>
	
</div>
</div>
</div>
</div>

<div class="fixed-action-btn active" style="bottom: 45px; right: 24px;">
	<a href="<?= base_url("app/coordinator/create.html"); ?>" 
		data-action="create" 
		data-mfp-src="#popup-manage" 
		class="btn-floating btn-large waves waves-effect">
		<i class="material-icons">&#xE145;</i>
	</a>
</div>

<!--
|	MODAL PARA ELIMINAR UN ELEMENTO DE LA LISTA
-->
<section id="popup-delete" class="white-popup mfp-with-anim mfp-hide">
	<form method="post" autocomplete="off">
		<div class="popup-header">
			<h4 class="title">¿Realmente deseas eliminar?</h4>
		</div>
		<div class="popup-body">
			<p class="message no-mar-bottom">
				El docente:&nbsp;<b>{name}</b>, será eliminado del sistema de forma permanente.
			</p>
		</div>
		<div class="popup-footer">
			<button type="submit" id="btn-save-del" class="btn-flat waves waves-effect">Eliminar</button>
		</div>
	</form>
</section>


<!--
| 	MODAL PARA MOSTRAR LOS INFORMACIÓN COMPLETA DE UN DOCENTE
-->
<section id="modal_fulldata" class="white-popup mfp-with-anim mfp-hide">
	<div class="popup-header">
		<h4 class="title">
			<span><i class="material-icons">&#xE7FD;</i></span>
			<br>
			<small class="profession"><i class="material-icons">&#xE80C;</i></small>
		</h4>
	</div>
	<div class="popup-body">
		<div class="row">
			<div class="col s4">
				<span class="descript center dni"><b>DNI<br></b></span>
			</div>
			<div class="col s4">
				<span class="descript center username"><b>USUARIO<br></b></span>
			</div>
			<div class="col s4">
				<span class="descript center gender"><b>GENERO<br></b></span>
			</div>
		</div>
		<br>
		<span class="descript email"><b>Email<small>(s)</small>: <br></b></span>
		<br>
		<div class="divider"></div><br>
		<span class="descript type"><b>Tipo de Entidad: <br></b></span>
		<span class="descript subtype"><b>Sub tipo de Entidad: <br></b></span>
		<span class="descript entity"><b>Nombre de Entidad: <br></b></span>

		<small class="last-modified"></small>
	</div>
	<div class="popup-footer"></div>
</section>


<!--
|	MODAL QUE SE MUESTRA PARA RENOVAR LA CONTRASEÑA DE UN DOCENTE
-->
<section id="popup-renove-pass" class="white-popup mfp-with-anim mfp-hide">
	<form method="post" autocomplete="off">
		<div class="popup-header">
			<h4 class="title">Restaurar contraseña</h4>
		</div>
		<div class="popup-body">
			<p class="message no-mar-bottom">
				Al cambiar el acceso al usuario se le notificará, via email, del cambio realizado y de su nueva contraseña, la cual sera generada de forma aleatoria.
			</p>
		</div>
		<div class="popup-footer">
			<button type="submit" id="btn-save-del" class="btn-flat waves waves-effect">Restaurar</button>
		</div>
	</form>
</section>