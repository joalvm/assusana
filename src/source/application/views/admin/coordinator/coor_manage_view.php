<?php 

	$name = ''; $lastname = ''; $dni = ''; $email = ''; $especial = ''; $entidad_name = ''; 
	$entidad_id = 'data-id="0"'; $subtype_name = ''; $type_name = ''; $username = '';
	$id_form = 0;

	if( $module == 'update' )
	{
		$doc 					= $docente[0];
		$name 				= $doc->name;
		$lastname 		= $doc->lastname;
		$dni 					= $doc->dni;
		$email 				= $doc->email;
		$especial 		= $doc->profesion;
		$entidad_name = $doc->entidad_name;
		$entidad_id 	= $entidad_id = 'data-id="'.$doc->entidad_id.'"';
		$subtype_name = $doc->subtype_name;
		$type_name 		= $doc->type_name;
		$username 		= $doc->username;
		$id_form 			= $doc->id;
	}

?>

<div class="container">
	<div class="row">
		<div class="col s12" id="coor_manage">
			<form id="frm_manage" action="./" method="post" data-id="<?= $id_form ?>" name="frm_manage" class="inner" autocomplete="off">
				<div class="row">
					<div class="input-field col s12 m4">
						<input id="txt_name" autofocus type="text" name="name" class="only-text datalist" required value="<?= $name ?>">
						<label for="txt_name">Nombres *</label>
					</div>
					<div class="input-field col s12 m4">
						<input id="txt_lastname" name="lastname" type="text" class="only-text datalist" required value="<?= $lastname ?>">
						<label for="txt_lastname">Apellidos *</label>
					</div>
					<div class="input-field col s12 m4">
						<input type="text" id="txt_dni" name="dni" maxlength="8" class="datalist" required value="<?= $dni ?>">
						<label for="txt_dni" data-error="DNI ya está registrado" data-success="OK">DNI *</label>
					</div>
				</div>
				<div class="row">
					<div class="input-field col s12 m4">
						<label for="cbo_gender" class="active">Sexo *</label>
						<select id="cbo_gender" class="browser-default datalist" name="gender" required>
						
						<?php if ($module == 'create'): ?>

							<option value="" disabled selected>Escoja su opción</option>
							<option value="0">Hombre</option>
							<option value="1">Mujer</option>

						<?php else: ?>

							<option value="" disabled>Escoja su opción</option>
							<option <?= ($doc->gender == '0') ? 'selected' : ''; ?> value="0">Hombre</option>
							<option <?= ($doc->gender == '1') ? 'selected' : ''; ?> value="1">Mujer</option>

						<?php endif; ?>
							
						</select>
					</div>

					<div class="input-field col s12 m4">
						<input id="txt_email" name="email" type="email" multiple class="datalist" value="<?= $email ?>">
						<label for="txt_email" data-error="Correo(s) invalido(s)" data-success="OK">
							Correo Electronico * 
							<i class="material-icons tooltipped" 
							data-position="top" 
							data-delay="50" 
							data-tooltip="Para mas de un correo puede separar usando una coma (,)">&#xE887;</i>
						</label>
					</div>

					<div class="input-field col s12 m4">
						<input id="txt_profession" name="profession" type="text" class="datalist" value="<?= $especial ?>">
						<label for="txt_profession">Especialidad</label>
					</div>
					
				</div>
				<div class="row">
					<div class="input-field col s12">
						<input id="txt_entity_display" type="text" <?= $entidad_id; ?> value="<?= $entidad_name; ?>" data-mfp-src="#popup-list-entities" readonly>
						<label for="txt_entity_display">Entidad *</label>
					</div>
				</div>
				<div class="row">
					<div class="input-field col s12 m4 method_search">
						<label for="txt_type_entity">Tipo de Entidades</label>
						<input id="txt_type_entity" type="text" readonly value="<?= $type_name ?>">
					</div>
					<div class="input-field col s12 m4">
						<label for="txt_subtype_entity">Subtipo de Entidades</label>
						<input id="txt_subtype_entity" type="text" readonly value="<?= $subtype_name ?>">
					</div>
					<div class="col s12 m4">

						<?php if( $module == 'create' ): ?>

							<p>
								<br>
								<input type="checkbox" name="send_email" class="filled-in datalist" id="chk_sendemail" />
								<label for="chk_sendemail">Enviar datos por Email</label>
							</p>

						<?php else: ?>

							<div class="input-field">
								<label for="txt_username">Nombre de usuario</label>
								<input id="txt_username" type="text" disabled readonly value="<?= $username ?>">
							</div>

						<?php endif; ?>
						
					</div>
				</div>
				<div class="row">
					<div class="col s12">

						<?php if( $module == 'update' ): ?>
							
							<p>
								<label>Ult. Modificación:&nbsp;
									<time class="humanize" data-type="datetime" data-datetime="<?= $doc->last_modified ?>"></time>
								</label>
							</p>
						
						<?php endif; ?>

						<p><label>(*) Dato obligatorio.</label></p>

					</div>
				</div>

			</form>
			
		</div>
	</div>
</div>

<div class="fixed-action-btn active" style="bottom: 45px; right: 24px;">
	<button type="submit" 
		form="frm_manage" 
		id="btn-submitdata" 
		data-action="<?= $module ?>" 
		class="btn-floating btn-large waves waves-effect">
		<i class="material-icons">&#xE161;</i>
	</button>
</div>

<section id="popup-list-entities" class="white-popup mfp-with-anim mfp-hide">
		<div class="popup-header">
			<h4 class="title">Seleccione una entidad</h4>
		</div>
		<div class="popup-body">
			<!--FILTRADO DE PERSONAS-->
			<div class="searcher in-modal">
				<input type="search" placeholder="Buscar" id="search" autocomplete="off" spellcheck="false" >
				<i class="material-icons clear_search">close</i>
				<label for="search"><i class="material-icons">search</i></label>
			</div>
			<ul id="list-entities" class="collection modal-list"></ul>
			<p class="items-count">
				<span class="curr">0</span> /
				<span class="total">0</span>
			</p>
		</div>
		<div class="popup-footer">
			<button type="button" disabled="disabled" id="btn-cancel" class="btn-flat waves waves-effect">Cancelar</button>
		</div>
</section>