<div class="container">

	<div class="row">
		<div class="col s12">
			<div class="inner registerMonths">
				<div class="card">
					<div class="card-content">
						<div id="chartInscriptions" style="max-height: 300px; height: 300px; width: 100%;">
					</div>
				</div>
			</div>
		</div>
	</div>
	
	<div class="row">
		<div class="col s12 mini-counts-container">
			<div class="col s12 m6 l3">
				<button type="button" id="btn-invitados" data-mfp-src="#md-participants" data-content="invi" class="card mini-counts blue hoverable">
					<div class="card-content white-text">
						<i class="material-icons">&#xE85E;</i>
						<h2>
							<small>Invitados</small>
							<?= $counts[0]->nparticipants ?>
						</h2>
					</div>
				</button>
			</div>
			<div class="col s12 m6 l3">
				<button type="button" id="btn-confirmados" data-mfp-src="#md-participants" data-content="asis" class="card mini-counts green hoverable">
					<div class="card-content white-text">
						<i class="material-icons">&#xE86E;</i>
						<h2>
							<small>Confirmados</small>
							<?= $counts[0]->ninscriptions ?>
						</h2>
					</div>
				</button>
			</div>
			<div class="col s12 m6 l3">
				<button type="button" id="btn-programa" data-content="programa" class="card mini-counts blue-grey hoverable">
					<div class="card-content white-text">
						<i class="material-icons">&#xE878;</i>
						<h2>
							<small>Programaciones</small>
							<?= $counts[0]->nprogramfull ?>
						</h2>
					</div>
				</button>
			</div>
			<div class="col s12 m6 l3">
				<button type="button" id="btn-programc" data-content="programac" class="card mini-counts yellow darken-3 hoverable">
					<div class="card-content">
						<i class="material-icons">&#xE614;</i>
						<h2>
							<small style="color: rgba(0, 0, 0, 0.5);">Prg. Realizadas</small>
							<?= $counts[0]->nprogram_nocancel ?>
						</h2>
					</div>
				</button>
			</div>
		</div>
	</div>

	<div class="row">
		<div class="col s12">
			<div class="col s12 m6">
				<div class="inner registerMonths">
					<div class="card">
						<div class="card-content">
							<div id="chartGenders" style="max-height: 300px; height: 300px; width: 100%;">
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="col s12 m6">
				<div class="inner registerMonths no-pad-left">
					<div class="card invitados">
						<div class="card-content header">
							<h4>Invitados y Asistentes</h4>
						</div>
						<ul class="collection card-content body">
						
						<?php if( ! empty($assist) ): ?>
							<?php foreach ($assist as $row => $cell ): ?>
								
								<li class="collection-item">
									<span class="title"><?= $cell->training; ?></span>
									<span class="time">
										<i class="material-icons">&#xE8B5;</i>
										<time class="humanize" 
											data-type="date" 
											data-datetime="<?= $cell->date_realization . ' ' . $cell->time_start ?>">
											<?= $cell->date_realization . ' ' . $cell->time_start ?>
										</time>
									</span>
									<div class="results">
										<span class="invi tooltipped" data-position="bottom" data-delay="50" data-tooltip="Personas que se registraron">
											<i class="material-icons">&#xE03B;</i>
											<?= $cell->invitados ?>
										</span>
										<span class="asis tooltipped" data-position="bottom" data-delay="50" data-tooltip="Personas que Asistieron">
											<i class="material-icons">&#xE065;</i>
											<?= $cell->asistentes ?>
										</span>
									</div>
								</li>

							<?php endforeach; ?>
						<?php else: ?>

							<li class="collection-item empty-list">
								<h4>No hay capacitaciones</h4>
							</li>

						<?php endif; ?>

						</ul>
					</div>
				</div>
			</div>
		</div>
	</div>

	<div class="row">
		<div class="col s12">
			<div class="col s12 m6">
				<div class="inner registerMonths">
					<div class="card invitados">

						<div class="card-content header">
							<h4>Proximas capacitaciones</h4>
						</div>

						<ul class="collection card-content body">

						<?php if( ! empty($proxcap) ): ?>
							<?php foreach ($proxcap as $row => $cell ):  ?>
								
								<li class="collection-item">
									<a href="#md-caprox" data-id="<?= $cell->id ?>" class="title title-caprox min-pad-right">
										<?= $cell->training_name; ?>
									</a>
									<span class="title2">
										<i class="material-icons">&#xE7EE;</i>
										<?= $cell->place ?>
									</span>
									<span class="time">
										<i class="material-icons">&#xE8B5;</i>
										<time class="humanize"
											data-type="date" 
											data-datetime="<?= $cell->date_realization . ' ' . $cell->time_start ?>" 
											>
											<?= $cell->date_realization . ' ' . $cell->time_start ?>
										</time>
									</span>
									<div class="chip">
										<i class="material-icons">&#xE7EF;</i>
										<b><?= $cell->number_reg ?></b>
									</div>
								</li>

							<?php endforeach; ?>
						<?php else: ?>

							<li class="collection-item empty-list">
								<h4>No hay capacitaciones</h4>
							</li>

						<?php endif; ?>

						</ul>

					</div>
				</div>
			</div>
			<div class="col s12 m6">
				<div class="inner registerMonths no-pad-left">
					<div class="card invitados">
						<div class="card-content header">
							<h4>Capacitaciones concluidas</h4>
						</div>
						<ul class="collection card-content body">

						<?php if( ! empty($culmcap) ):  ?>
							<?php foreach ($culmcap as $row => $cell ): ?> 
	
								<li class="collection-item">
									<a href="#!" data-id="<?= $cell->id ?>" class="title min-pad-right">
										<?= $cell->training_name; ?>
									</a>
									<span class="title2">
										<i class="material-icons">&#xE7EE;</i>
										<?= $cell->place ?>
									</span>
									<span class="time">
										<i class="material-icons">&#xE8B5;</i>
										<time data-type="date" data-datetime="<?= $cell->date_realization . ' ' . $cell->time_start ?>" class="humanize">
											<?= $cell->date_realization . ' ' . $cell->time_start ?>
										</time>
									</span>
									
									<div class="chip tooltipped" 
										data-position="bottom"
										data-delay="50" 
										data-tooltip="Cantidad de asistentes">
										<i class="material-icons">&#xE7EF;</i>
										<?= $cell->inscriptions ?>
									</div>

								</li>

							<?php endforeach; ?>
						<?php else: ?>

							<li class="collection-item empty-list">
								<h4>No hay capacitaciones</h4>
							</li>

						<?php endif; ?>
						</ul>
					</div>
				</div>
			</div>
		</div>
	</div>

</div>

<section id="md-caprox" class="white-popup mfp-with-anim mfp-hide">
	<div class="popup-header">
		<h4 class="title">Participantes</h4>
	</div>
	<div class="popup-body">
		<ul id="list-participants" class="collection collection-main modal-list"></ul>
	</div>
	<div class="popup-footer"></div>
</section>


<section id="md-participants" class="white-popup mfp-with-anim mfp-hide">
	<div class="popup-header">
		<h4 class="title">Participantes</h4>
	</div>
	<div class="popup-body">
		
	</div>
	<div class="popup-footer"></div>
</section>


<section id="md-programa" class="white-popup mfp-with-anim mfp-hide">
	<div class="popup-header">
		<h4 class="title">Programaciones</h4>
	</div>
	<div class="popup-body">
		<ul id="list-participants" class="collection collection-main modal-list"></ul>
	</div>
	<div class="popup-footer"></div>
</section>