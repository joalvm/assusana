<?php
$title = ''; $description = ''; $id = 'data-id="0"';
if($module == 'update')
{
	$tr = $training;
	$description = $tr->description;
	$title = $tr->title;
	$id = 'data-id="'.$tr->id.'"';
}
?>

<div class="container">
<div class="col s12">

<form id="frm_training" method="post" <?= $id ?> autocomplete="off" class="inner no-pad-top">
	<div class="row no-mar-bottom">
		<div class="input-field col s12 m7">
			<input id="txt_name" name="title" type="text" class="req dataset" value="<?= $title; ?>">
			<label for="txt_name">Titulo de la capacitación *</label>
		</div>
		<div class="input-field col s12 m5">
			<label for="cbo_type_training" class="active">Tipo de capacitación *</label>
			<select name="type_training"  class="browser-default req dataset" id="cbo_type_training">
			<?php
			if( $module == 'create' )
			{
			?>
				<option value="" disabled selected>Escoja su opción</option>
				<?php
					foreach ($types as $row => $cell)
					{
				?>
						<option value="<?= $cell->id ?>"><?= $cell->name; ?></option>
				<?php
					}
				?>
			<?php
			}
			else
			{
			?>
				<option value="" disabled>Escoja su opción</option>
				<?php
					foreach ($types as $row => $cell)
					{
				?>
						<option <?= ($cell->id == $tr->type_id) ? 'selected': '' ?> value="<?= $cell->id ?>"><?= $cell->name; ?></option>
				<?php
					}
				?>
			<?php
			}
			?>

			</select>
		</div>
	</div>


	<div class="row">
		<div class="input-field col s12">
			<textarea id="txt_description" name="description" class="materialize-textarea dataset"><?= trim($description) ?></textarea>
			<label for="txt_description">Descripción</label>
		</div>
	</div>

	<div class="row">
		<div class="col s12">
			<div class="divider_page">
				<h4>Temas</h4>
			</div>
			<ul id="list-themes" class="collection-main modal-list">
			<?php
				if( ! empty($themes) ):
					foreach ($themes as $row => $cell ): ?>
					<li class="collection-item" data-id="<?= $cell->id; ?>">
						<p>
							<input type="radio"
								class="with-gap dataset"
								id="theme_<?= $cell->id; ?>"
								<?= ($module == 'update' AND $cell->id == $tr->theme_id) ? 'checked="checked"' : '' ; ?>
								name="theme"
								value="<?= $cell->id; ?>">

							<label for="theme_<?= $cell->id; ?>">

								<span class="title" title=""><?= $cell->title ?></span>

								<?php if (!empty($cell->description)): ?>
									<span class="description"><?= $cell->description ?></span>
								<?php endif; ?>

								<?php if( ! empty($cell->subthemes) ): ?>
									<ul class="ulist">
										<?php foreach ($cell->subthemes as $index => $obj): ?>
											<li>
												<span><?= $obj->title ?></span>
												<p><?= $obj->description ?></p>
											</li>
										<?php endforeach; ?>
									</ul>
								<?php endif; ?>
							</label>
						</p>
					</li>
			<?php
					endforeach;
				endif;
			?>
			</ul>
		</div>
	</div>
</form>
</div>
</div>

<div class="fixed-action-btn active" style="bottom: 45px; right: 24px;">
	<button type="submit"
		form="frm_training"
		id="btn-submitdata"
		data-action="create"
		class="btn-floating btn-large waves waves-effect">
		<i class="material-icons">&#xE161;</i>
	</button>
</div>

<section id="md-delete" class="white-popup mfp-with-anim mfp-hide">
	<div class="popup-header">
		<h4 class="title">¿Realmente deseas eliminar?</h4>
	</div>
	<div class="popup-body">
		<p class="message no-mar-bottom">
			<span class="text">
				Se eliminará de forma permanente esta capacitación.
			</span>
		</p>
	</div>
	<div class="popup-footer">
		<button type="button" id="btn_exit" class="btn-flat waves waves-effect">Salir</button>
		<button type="button" id="btn_continue" class="btn-flat waves waves-effect">Eliminar</button>
	</div>
</section>
