<div class="container register-index">

<div class="row">
<div class="col s12" id="cap_register_index">
<div class="inner no-pad-top">
	<!--FILTRADO DE PERSONAS-->
	<div class="searcher">
		<input type="search" autofocus placeholder="Buscar" id="search" autocomplete="off" spellcheck="false" >
		<i class="material-icons clear_search">close</i>
		<label for="search"><i class="material-icons">search</i></label>
	</div>

	<ul class="collection collection-main" id="list-trainings">
	<?php
		if ( ! empty($items) )
		{
			foreach ($items as $row => $cell)
			{
				$search = mb_strtolower(($cell->title . ' ' . $cell->type_name), 'UTF-8');
				$title = ucfirst(mb_strtolower($cell->title, 'UTF-8'));
	?>
			<li class="collection-item"  data-search="<?= $search; ?>"  data-id="<?= $cell->id ?>">
				<ul class="collapsible" data-collapsible="accordion">
					<li>
						<button type="button"
							class="btn-options"
							data-activates="options<?= $cell->id; ?>">
							<i class="material-icons">&#xE5D4;</i>
						</button>

						<ul id="options<?= $cell->id ?>" class="dropdown-content">
							<li>
								<a href="<?= base_url('/app/training/register/'.$cell->id.'/update.html') ?>">
									<i class="material-icons">&#xE150;</i>
									Modificar
								</a>
							</li>
							<li>
								<button type="button"
									class="btn-delete"
									data-action="delete"
									data-mfp-src="#popup-delete"
									data-id="<?= $cell->id ?>">
									<i class="material-icons">&#xE872;</i>
									Eliminar
								</button>
							</li>
						</ul>

						<div class="collapsible-header" data-id="<?= $cell->id ?>">
							<span class="title" title="<?= $title; ?>">
								<?= $title; ?>
							</span>
							<span class="type"><?= $cell->type_name; ?></span>
							<?php
							if( !empty($cell->description) )
							{
							?>
								<p class="description"><?= $cell->description; ?></p>
							<?php
							}
							?>
							<small class="last-modified">
								Ult. Modificación:&nbsp;
								<time class="humanize" data-type="datetime" data-datetime="<?= $cell->last_modified; ?>"></time>
							</small>
						</div>
						<div class="collapsible-body">
							<div class="progress">
								<div class="indeterminate"></div>
							</div>
						</div>
					</li>
				</ul>
			</li>

	<?php
			}
		}
		else
		{
	?>
			<li class="collection-item empty"><p>No se registran datos.</p></li>
	<?php
		}
	?>
	</ul>
</div>
</div>
</div>
</div>

<div class="fixed-action-btn active" style="bottom: 45px; right: 24px;">
	<a href="<?= base_url("app/training/register/create.html") ?>" class="btn-floating btn-large waves waves-effect">
		<i class="material-icons">&#xE145;</i>
	</a>
</div>

<!--
|	MODAL PARA ELIMINAR UN ELEMENTO DE LA LISTA
-->
<section id="popup-delete" class="white-popup mfp-with-anim mfp-hide">
	<form method="post" autocomplete="off">
		<div class="popup-header">
			<h4 class="title">¿Realmente deseas eliminar?</h4>
		</div>
		<div class="popup-body">
			<p class="message no-mar-bottom">
				La Capacitación: <b>{name}</b>, será eliminado del sistema de forma permanente.
			</p>
		</div>
		<div class="popup-footer">
			<button type="button" id="btn-cancel" class="btn-flat waves waves-effect">Cancelar</button>
			<button type="submit" id="btn-save" class="btn-flat waves waves-effect">Eliminar</button>
		</div>
	</form>
</section>
