<div class="container program_index">
<div class="row">
<div class="col s12">
<div class="inner">
	<div class="row">
		<div class="input-field col s12 m4">
			<i class="material-icons prefix" title="Filtrar Lista">&#xE152;</i>
			<select class="browser-default" name="cbofilters" id="cbo_filters">
				<option value="1" <?= ($this->session->filter_prg == 1) ? 'selected' : '' ?>>Todos</option>
				<option value="2" <?= ($this->session->filter_prg == 2) ? 'selected' : '' ?>>Por realizarse</option>
				<option value="3" <?= ($this->session->filter_prg == 3) ? 'selected' : '' ?>>Concluidos</option>
				<option value="4" <?= ($this->session->filter_prg == 4) ? 'selected' : '' ?>>Cancelados</option>
				<option value="5" <?= ($this->session->filter_prg == 5) ? 'selected' : '' ?>>Personalizados</option>
			</select>
		</div>
		<div class="col s12 m8">
			<!--FILTRADO GENERAL-->
			<div class="searcher">
				<input type="search" autofocus placeholder="Buscar" id="search" autocomplete="off" spellcheck="false" >
				<i class="material-icons clear_search">close</i>
				<label for="search"><i class="material-icons">search</i></label>
			</div>
		</div>
	</div>
	<div class="row">
		<div class="col s12">
		<?php 
		if ( ! empty($items) ) 
		{ ?>
			<dl class="collection-sep">
				<?php 
				$month_arr = [NULL, "Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio", "Julio", "Agosto", "Septiembre", "Octubre", "Noviembre", "Diciembre"];
				$last_month = NULL;
				foreach ($items as $row => $cell) 
				{
					$time 	= strtotime($cell->date_realization);
					$month	= (int) date("n", $time);
					$year 	= date("Y", $time);

					if ( $last_month != $month )
					{
						echo '<dt class="sep-item">'. $month_arr[$month] . " " . $year.'</dt>';
						$last_month = $month;
					}
				?>
					<dd class="collection-item">

						<?php 
						$message = $cell->its_customized ? "Prog. personalizada" : "Prog. General";
						$msg_suffix = !$cell->its_canceled ? " :: Activa" : " :: Cancelada";
						$inicial = $cell->its_customized ? "P" : "G";
						$color = $cell->its_canceled ? 'amber darken-3' : 'green accent-4';
						
						if ( $cell->its_finish ) {
							$color = 'red';
							$msg_suffix = ' :: Concluida';
						}

						$message .= $msg_suffix;

						?>
						<span 
							data-position="left" 
							data-delay="50"
							data-tooltip="<?= $message; ?>"
							class="tooltipped icons-status <?= $color ?>">
							<?= $inicial ?>
						</span>

						<?php if( ! empty($cell->folder) ): ?>

							<a class="file_url hoverable tooltipped chip" 
								href="<?= base_url($cell->folder) . '/' . $cell->file_name ?>" 
								target="_blank" 
								data-position="left" 
								data-delay="50"
								data-tooltip="Examen de la capacitación"
								data-id="<?= $cell->programming_id ?>" >
								<i class="material-icons">&#xE226;</i>
							</a>

						<?php endif;?>

						<a class="number-reg hoverable tooltipped title-caprox" 
							href="#md-caprox" 
							data-position="left" 
							data-delay="50"
							data-tooltip="N° de Inscritos."
							data-id="<?= $cell->programming_id ?>" >
							<span><?= $cell->nregisters ?></span>
							<i class="material-icons">&#xE853;</i>
						</a>

						<button type="button" class="hoverable btn-options" data-activates='options_<?= $cell->programming_id ?>'>
							<i class="material-icons">&#xE5D4;</i>
						</button>

						<!--Menu de opciones para este item-->
						<ul id='options_<?= $cell->programming_id ?>' class='dropdown-content'>
							<?php if( $cell->its_customized && (! $cell->its_finish && !$cell->its_canceled) ) { ?>
								<li>
									<button type="button"
										class="get-url-prg" 
										data-id="<?= $cell->programming_id ?>"
										data-mfp-src="#md-get-url">
										<i class="material-icons">&#xE902;</i>
										Obtener URL
									</button>
								</li>
							<?php }

							if( ! $cell->its_canceled && ! $cell->its_finish ) {
							?>
								<li>
									<button type="button"
										class="change-schedule"
										id="btn-schedule-<?= $cell->programming_id ?>"
										data-id="<?= $cell->programming_id ?>"
										data-date="<?= $cell->date_realization; ?>"
										data-start="<?= $cell->time_start; ?>"
										data-finish="<?= $cell->time_finish; ?>">
										<i class="material-icons">&#xE923;</i>
										Cambiar Horario
									</button>
								</li>
							<?php
							}

							if ( ! $cell->its_finish && ! $cell->its_canceled )
							{
							?>
								<li>
									<button type="button" 
										id="btn-cancel-prg-<?= $cell->programming_id ?>" 
										class="cancel-prg" 
										data-id="<?= $cell->programming_id ?>"
										data-mfp-src="#md-cancel-programming">
										<i class="material-icons">&#xE5C9;</i>
										Cancelar
									</button>
								</li>
							<?php 
							}

							if (! $cell->its_finish && !$cell->its_canceled )
							{ 
							?>
								<li class="divider"></li>

								<li>
									<a href="<?= base_url('app/training/programming/'.$cell->programming_id.'/update.html') ?>">
										<i class="material-icons">&#xE150;</i>
										Modificar
									</a>
								</li>
							<?php 
							} 
							?>
							<li>
								<button type="button"
									id="btn-delete-prg-<?= $cell->programming_id ?>"
									data-mfp-src="#md-delete-programming"
									data-id="<?= $cell->programming_id ?>"
									class="delete-prg">
									<i class="material-icons">&#xE872;</i>
									Eliminar
								</button>
							</li>
						</ul>

						<h4 class="title">
							<?php if ( ! $cell->its_canceled && ! $cell->its_finish ) { ?>
								<a href="<?= base_url('workshops/' . $cell->programming_id . '/registration') ?>" target="_blank"><?= $cell->training_title; ?></a>
							<?php } else { ?>
								<a href="javascript:void(0)"><?= $cell->training_title; ?></a>
							<?php } ?>
						</h4>

						<span class="date"><i class="material-icons">&#xE878;</i> 
							<time class="humanize" data-type="date" data-datetime="<?= $cell->date_realization ?>"></time>
						</span>
						<span class="time"><i class="material-icons">&#xE192;</i> 
							<time class="humanize" data-type="time" data-datetime="<?= $cell->date_realization . " " . $cell->time_start ?>"></time> - 
							<time class="humanize" data-type="time" data-datetime="<?= $cell->date_realization . " " . $cell->time_finish ?>"></time>
						</span>
						<br>
						<span class="place"><i class="material-icons">&#xE0AF;</i><?= $cell->place; ?></span>
						<span class="address"><i class="material-icons">&#xE55F;</i><?= $cell->address; ?></span>
					</dd>
				<?php 
				} ?>
			</dl>
		<?php 
		} else { ?>
			<div class="empty-list">
				<h4>Lista Vacia</h4>
				<p>Es posible que no hayan capacitaciones programadas o que próximamante no 
				exista ninguna por celebrar.</p>
			</div>
		<?php 
		} ?>
		</div>
	</div>

</div>
</div>
</div>
	
</div>

<div 
	class="fixed-action-btn tooltipped"
	data-position="left" 
	data-delay="50"
	data-tooltip="Programar nueva capacitación"
	style="bottom: 45px; right: 24px;">
	<a href="<?= base_url("app/training/programming/create.html") ?>" class="btn-floating btn-large waves waves-effect">
		<i class="material-icons">&#xE145;</i>
	</a>
</div>

<section id="md-cancel-programming" class="white-popup mfp-with-anim mfp-hide">
	<div class="popup-header">
		<h4 class="title">¿Deseas cancelar la programación?</h4>
	</div>
	<div class="popup-body">
		<p class="message no-mar-bottom">
			Esta programación seguirá mostrandose y no será eliminada del sistema, tampoco
			se podrá regresar a su estado anterior.
		</p>
		<div class="input-field col s12">
          <textarea id="txt_reason" class="materialize-textarea"></textarea>
          <label for="txt_reason">Motivo de cancelación&nbsp;<small>(Opcional)</small></label>
        </div>
		<p>
			<input type="checkbox" class="filled-in" id="chk_sendmail" />
			<label for="chk_sendmail">Avisar a los que esten inscritos</label>
		</p>
	</div>
	<div class="popup-footer">
		<button type="button" id="btn_exit" class="btn-flat waves waves-effect">Salir</button>	
		<button type="button" id="btn_continue" class="btn-flat waves waves-effect">Continuar</button>
	</div>
</section>

<section id="md-delete-programming" class="white-popup mfp-with-anim mfp-hide">
	<div class="popup-header">
		<h4 class="title">¿Realmente deseas eliminar?</h4>
	</div>
	<div class="popup-body">
		<p class="message no-mar-bottom">
			<span class="text">
				Se eliminará de forma permanente la programación de la capacitación:&nbsp;<b>{CAPA}</b>
			</span>
			<br><br>
			<span class="note">
				<b>Nota:</b><br>
				Tenga en cuenta que solo se está permitido eliminar aquella 
				programación que no presenta participantes inscritos o que haya sido cancelada.
			</span>
		</p>
	</div>
	<div class="popup-footer">
		<button type="button" id="btn_exit" class="btn-flat waves waves-effect">Salir</button>	
		<button type="button" id="btn_continue" class="btn-flat waves waves-effect">Eliminar</button>
	</div>
</section>

<section id="md-get-url" class="white-popup mfp-with-anim mfp-hide">
	<div class="popup-header">
		<h4 class="title">URL de la capacitación</h4>
	</div>
	<div class="popup-body">
		<textarea id="txt_url" rows="1" readonly="readonly"></textarea>
	</div>
	<div class="popup-footer"></div>
</section>

<section id="md-caprox" class="white-popup mfp-with-anim mfp-hide">
	<div class="popup-header">
		<h4 class="title">Participantes</h4>
	</div>
	<div class="popup-body">

		<ul id="list-participants" class="collection collection-main modal-list">
			
		</ul>

	</div>
	<div class="popup-footer">
		
	</div>
</section>