<?php
	$place = ''; $address = ''; 
	$limit = 0; $customized = '';
	$dept = 0; $prov = 0; $dist = 0;
	$entity_id = 0; $entity_name = '';

	if( $module == 'update' )
	{
		$place = $programming->place;
		$address = $programming->address;
		$limit = $programming->limit_max;
		$customized = ($programming->its_customized) ? 'checked="checked"' : '';
		$dept = $programming->departamento_id;
		$prov = $programming->provincia_id;
		$dist = $programming->distrito_id;
		$entity_id = $programming->entity_id;
		$entity_name = $programming->entity_name;
	}

?>


<div class="container">

	<div class="row">
		<form id="frm_manage_program" 
			method="post" 
			<?= ( $module == 'update' ) ? 'data-programming="'.$programming->programming_id.'"':''; ?> 
			class="col s12 inner">
			<!--ELECCIÓN DE LA CAPACION QUE SERÁ PROGRAMADA-->
			<div class="row">
				<div class="input-field col s12 m7">
					<input id="txt_training" type="text" readonly data-mfp-src="#modal-list-training">
		          	<label for="txt_training">Capacitación</label>
				</div>
				<div class="input-field col s12 m5">
					<input id="txt_type_training" type="text" readonly>
		          	<label for="txt_type_training">Tipo de Capacitación</label>
				</div>
			</div>
			
			<!--UBICACIÓN EN EL PERU-->
			<div class="row">
				<div class="input-field col s12 m4">
					<select class="browser-default ubigeo" 
						disabled="disabled" 
						name="departamento" 
						id="cbo_departamento"
						data-selected="<?= $dept; ?>" >
						<option class="default" value="">Escoja su Opción</option>
					</select>
					<label for="cbo_departamento" class="active">Departamento</label>
				</div>
				<div class="input-field col s12 m4">
					<select class="browser-default ubigeo" 
						disabled="disabled" 
						name="provincia" 
						id="cbo_provincia"
						data-selected="<?= $prov; ?>">
						<option class="default" value="">Escoja su Opción</option>
					</select>
					<label for="cbo_provincia" class="active">Provincia</label>
				</div>
				<div class="input-field col s12 m4">
					<select class="browser-default ubigeo dataset" 
					disabled="disabled" 
					name="distrito" 
					id="cbo_distrito"
					data-selected="<?= $dist; ?>">
						<option class="default" value="">Escoja su Opción</option>
					</select>
					<label for="cbo_distrito" class="active">Distrito</label>
				</div>
			</div>
			
			<!--LUGAR Y DIRECCION DONDE SE CELEBRARÁ LA CAPACITACIÓN-->
			<div class="row">
				<div class="input-field col s12 m6">
					<i class="material-icons prefix">&#xE0AF;</i>
					<input id="txt_place" type="text" name="place" class="dataset" value="<?= $place; ?>">
	          		<label for="txt_place">Lugar de la Capacitación</label>
				</div>
				<div class="input-field col s12 m6">
					<i class="material-icons prefix">&#xE55F;</i>
					<input id="txt_address" name="address" class="dataset" type="text" value="<?= $address; ?>">
	          		<label for="txt_address">Dirección de la Capacitación</label>
				</div>
			</div>
			
			<!--DIA Y HORAS DE REALIZACIÓN-->
			<?php if ($module == 'create'): ?>

				<div class="row">
					<div class="input-field col s12 m4 l6">
						<i class="material-icons prefix">&#xE878;</i>
						<input type="text" 
							data-value="" 
							id="txt_date_realization" 
							class="dataset hidden" 
							name="date_realization" >
		          		<label for="txt_date_realization">Fecha de realización</label>
					</div>
					<div class="input-field col s12 m4 l3">
						<i class="material-icons prefix">&#xE192;</i>
						<input type="text" 
							data-value="" 
							id="txt_time_start" 
							name="time_start" 
							class="times dataset hidden">
		          		<label for="txt_time_start">Hora de inicio</label>
					</div>
					<div class="input-field col s12 m4 l3">
						<i class="material-icons prefix">&#xE192;</i>
						<input type="text" 
							data-value=""
							id="txt_time_finish" 
							disabled="disabled" 
							name="time_finish" 
							class="times dataset hidden">
		          		<label for="txt_time_finish">Hora de Final</label>
					</div>
				</div>
				
			<?php endif ?>

			<div class="row">
				<div class="input-field col s12 m4">
					<i class="material-icons prefix">&#xE25A;</i>
					<input type="number" 
						id="txt_limit" 
						name="limit" 
						class="dataset" 
						max="500" 
						maxlength="3" 
						value="<?= $limit; ?>">
	          		<label for="txt_limit">
	          			Limite de personas
	          			<small class="help tooltipped"
	          				data-position="top" 
	          				data-delay="50" 
	          				data-tooltip="0 = Sin limite en la cantidad de participantes">
	          				<i class="material-icons">&#xE887;</i>
	          			</small>
	          		</label>
				</div>
			</div>

			<!--VINCULACION DE DOCENTES CON LOS TEMAS-->
			<div class="row">
				<div class="col s12">

					<h4 class="header-title">
						Asignar Docentes
					</h4>

					<div id="list-themes-to-asign">
						
						<?php if ($module == 'update'): ?>
							<?php foreach ($themes as $row => $cell): ?>
								<?php foreach ($cell['subthemes'] as $row2 => $cell2): ?>
									
									<input type="hidden" 
										data-themeid="<?= $cell['theme_id'] ?>" 
										data-idsubtheme="<?= $cell2['subtheme_id'] ?>" 
										data-iddocente="<?= $cell2['docente_id'] ?>" 
										data-docentename="<?= $cell2['docente'] ?>" 
										id="stheme_<?= $cell2['subtheme_id'] ?>">

								<?php endforeach ?>
							<?php endforeach ?>
						<?php endif ?>

					</div>

				</div>
			</div>

			<!--Personalizar capacitación para un entidad-->
			<div class="row">

				<div class="col s12">
					<p class="header-title">
						<input type="checkbox" <?= $customized; ?> class="filled-in" id="chk_personalize" />
						<label class="" for="chk_personalize">
							Capacitación personalizada
							<small class="help tooltipped"
		          				data-position="top" 
		          				data-delay="50" 
		          				data-tooltip="Seleccione la entidad exclusiva de esta capacitación">
		          				<i class="material-icons">&#xE887;</i>
		          			</small>
						</label>
					</p>
				</div>
				
				<div class="input-field col s12">
					<input type="text" 
						id="txt_entity_display" 
						<?= empty($customized) ? 'disabled="disabled"' : ''; ?> 
						data-mfp-src="#popup-list-entities" 
						data-selected="<?= $entity_id ?>" 
						readonly="readonly"
						class="datalist"
						value="<?= $entity_name ?>">
					<label for="txt_entity_display">Entidad</label>
				</div>

			</div>

		</form>
	</div>
</div>

<div class="fixed-action-btn" style="bottom: 45px; right: 24px;">
	<button type="submit" 
		form="frm_manage_program" 
		id="btn-submitdata" 
		data-action="create" 
		class="btn-floating btn-large waves waves-effect">
		<i class="material-icons">&#xE161;</i>
	</button>
</div>

<section id="modal-list-training" class="white-popup mfp-with-anim mfp-hide">
	<div class="popup-header">
		<h4 class="title">Capacitaciones</h4>
	</div>
	<div class="popup-body">
		<!--FILTRADO DE PERSONAS-->
		<div class="searcher in-modal">
			<input type="search" autofocus placeholder="Buscar" id="search" autocomplete="off" spellcheck="false" >
			<i class="material-icons clear_search">close</i>
			<label for="search"><i class="material-icons">search</i></label>
		</div>
		<ul id="list-training" class="collection modal-list">
			<?php 
			foreach ( $training as $row => $cell ) 
			{
				$checked = '';

				if( $module == 'update' )
				{
					if( $cell->id == $programming->training_id )
					{
						$checked = 'checked="checked"';
					}
				}
			?>
				<li class="collection-item" data-search="<?= $cell->title . " " . $cell->type_name ?>" data-id="<?= $cell->id ?>">
					<p>
						<input class="with-gap" 
							type="radio" 
							id="training<?= $cell->id ?>" 
							<?= $checked ?> 
							name="training" 
							value="<?= $cell->id ?>" 
							data-title="<?= $cell->title ?>"
							data-type="<?= $cell->type_name ?>" >
						<label for="training<?= $cell->id ?>">
							<span class="title entity truncate" title="<?= $cell->title ?>"><?= $cell->title ?></span>
							<span class="title2 type">Tipo: <?= $cell->type_name ?></span>
						</label>
					</p>
				</li>
			<?php
			}
			?>
			
		</ul>
	</div>
	<div class="popup-footer">
		<button type="button" id="btn-out-training" class="btn-flat waves waves-effect">Salir</button>
	</div>
</section>

<!--MODAL PARA ASIGNAR LOS DOCENTES A LOS TEMAS-->
<section id="modal-asign-coordinator" class="white-popup mfp-with-anim mfp-hide">
	<div class="popup-header">
		<h4 class="title">Docentes registrados</h4>
	</div>
	<div class="popup-body">
		<ul id="list-coordinator" class="collection modal-list no-check">
			<?php 
			foreach ( $coordinator as $row => $cell ) 
			{
			?>
				
				<li id="coordinator_<?= $cell->id ?>" class="collection-item" 
					data-search="<?= $cell->name . " " . $cell->lastname ?>" 
					data-id="<?= $cell->id ?>">
					<a class="item" data-id="<?= $cell->id ?>"><?= $cell->name . " " . $cell->lastname ?></a>
				</li>
			<?php
			}
			?>
		</ul>
	</div>
	<div class="popup-footer"></div>
</section>

<!--MODAL PARA LISTAR LAS ENTIDADES-->
<section id="popup-list-entities" class="white-popup mfp-with-anim mfp-hide">
		<div class="popup-header">
			<h4 class="title">Seleccione una entidad</h4>
		</div>
		<div class="popup-body">
			<!--FILTRADO DE PERSONAS-->
			<div class="searcher in-modal">
				<input type="search" placeholder="Buscar" id="search" autocomplete="off" spellcheck="false" >
				<i class="material-icons clear_search">close</i>
				<label for="search"><i class="material-icons">search</i></label>
			</div>
			<ul id="list-entities" class="collection modal-list"></ul>

			<p class="items-count">
				<span class="curr">0</span> /
				<span class="total">0</span>
			</p>
		</div>
		<div class="popup-footer">
			<button type="button" id="btn-cancel" class="btn-flat waves waves-effect">Cancelar</button>
		</div>
</section>