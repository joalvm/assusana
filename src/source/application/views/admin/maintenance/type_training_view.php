<div class="container">
	<div class="row">
		<div class="col s12" id="typeWorkshops">
			<div class="inner">
				<ul class="collection collection-main" id="list-type">
					<?php
						if ( ! empty($items) )
						{
							foreach ($items as $row => $cell) 
							{
					?>
								<li data-search="<?= $cell->name ?>" class="collection-item">
									<a class="title name"><?= $cell->name ?></a>
									<span class="last-modified">
										Modificado: <time class="humanize" data-datetime="<?= $cell->last_modified ?>"></time>
									</span>
									<button type="button" class="btn-options" data-activates='options_<?= $cell->id ?>'>
										<i class="material-icons">&#xE5D4;</i>
									</button>
									<ul id='options_<?= $cell->id ?>' class='dropdown-content'>
										<li>
											<button type="button" data-id="<?= $cell->id ?>" class="btn-update" data-action="update" data-mfp-src="#popup-manage">
												<i class="material-icons">&#xE150;</i>Modificar
											</button>
										</li>
										<li>
											<button type="button"  data-id="<?= $cell->id ?>" class="btn-delete"  data-action="delete"  data-mfp-src="#popup-delete">
												<i class="material-icons">&#xE872;</i>Eliminar
											</button>
										</li>
									</ul>
								</li>
					<?php
							}
						}
						else
						{
					?>
							<li class="collection-item empty">
								<p>No se registran datos.</p>
							</li>
					<?php
						}
					?>
				</ul>
			</div>
		</div>
	</div>
</div>

<div class="fixed-action-btn active" style="bottom: 45px; right: 24px;">
	<button type="button" data-action="create" data-mfp-src="#popup-manage" class="btn-floating btn-large waves waves-effect">
		<i class="material-icons">&#xE145;</i>
	</button>
</div>

<section id="popup-manage" class="white-popup mfp-with-anim mfp-hide">
	<form method="post" autocomplete="off">
		<div class="popup-header">
			<h4 class="title">Nuevo</h4>
		</div>
		<div class="popup-body">
			<div class="row no-mar-bottom">
				<div class="input-field col s12">
					<input id="txt_type" type="text" name="typeText" required>
					<label data-error="Texto requerido" data-success="OK" for="txt_type">Tipo de Capacitación</label>
				</div>
				<div class="input-field col s12">
					<textarea id="txt_description" name="description" class="materialize-textarea"></textarea>
					<label for="txt_description">Descripción <small>(Opcional)</small></label>
				</div>
			</div>
		</div>
		<div class="popup-footer">
			<button type="submit" id="btn-save" class="btn-flat waves waves-effect">Guardar</button>
		</div>
	</form>
</section>

<section id="popup-delete" class="white-popup mfp-with-anim mfp-hide">
	<form method="post" autocomplete="off">
		<div class="popup-header">
			<h4 class="title">¿Realmente deseas eliminar?</h4>
		</div>
		<div class="popup-body">
			<p class="message no-mar-bottom">El tipo de capacitación: <b>{TYPE}</b>, será eliminado de forma permanentemente del sistema</p>
		</div>
		<div class="popup-footer">
			<button type="button" id="btn-cancel-del" class="btn-flat waves waves-effect">Cancelar</button>	
			<button type="submit" id="btn-save-del" class="btn-flat waves waves-effect">Eliminar</button>
		</div>
	</form>
</section>