<div class="container">
<div class="row">
<div class="col s12">
<div class="inner">

<div class="row">
	<div class="input-field col s12 m6">
		<select class="browser-default" name="types" id="cbotypes">
			<option value="0">Todo</option>
		</select>
	</div>
	<div class="input-field col s12 m6">
		<select class="browser-default" name="subtypes" id="cbosubtypes">
			<option value="0">Todo</option>
		</select>
	</div>
	<div class="col s12">
		<!--FILTRADO DE PERSONAS-->
		<div class="searcher">
			<input type="search" autofocus placeholder="Buscar Inscripción" id="search_entities" autocomplete="off" spellcheck="false" >
			<i class="material-icons clear_search">close</i>
			<label for="search"><i class="material-icons">search</i></label>
		</div>
	</div>
</div>

<div class="row">
	<div class="col s12" id="cnt-entities">
		<ul id="list-entities" class="list-entities collection-main"></ul>
	</div>
</div>

</div>
</div>
</div>
</div>

<div class="fixed-action-btn active" style="bottom: 45px; right: 24px;">
	<button type="button" id="btn-add" data-action="create" data-mfp-src="#popup-manage" class="btn-floating btn-large waves waves-effect">
		<i class="material-icons">&#xE145;</i>
	</button>
</div>

<section id="popup-manage" class="white-popup mfp-with-anim mfp-hide">
	<div class="popup-header">
		<h4 class="title">Nuevo</h4>
	</div>
	<div class="popup-body">
		<div class="row">
			<div class="col s12 m6">
				<p>
					<input type="checkbox" class="filled-in" id="chk_newtype" />
					<label for="chk_newtype">Nuevo tipo entidad</label>
				</p>
			</div>
			<div class="col s12 m6">
				<p>
					<input type="checkbox" class="filled-in" id="chk_newsubtype" />
					<label for="chk_newsubtype">Nuevo Subtipo entidad</label>
				</p>
			</div>
		</div>
		<div class="row" style="margin-bottom: 0;">
			<div id="cnt-types" class="input-field col s12">
				<select class="browser-default" name="type" id="cbo_type_create">
					<option value="">Escoja su opción</option>
				</select>
				<label for="cbo_type_create" class="active">Tipo de entidad</label>
			</div>
			<div id="cnt-subtypes" class="input-field col s12">
				<select class="browser-default" name="subtype" disabled="disabled" id="cbo_subtype_create">
					<option value="">Escoja su opción</option>
				</select>
				<label for="cbo_subtype_create" class="active">Subtipo de entidad</label>
			</div>
			<div class="input-field col s12">
				<input type="text" name="entity" id="txt_entity">
				<label for="txt_entity">Nombre entidad</label>
			</div>
		</div>
	</div>
	<div class="popup-footer">
		<button type="button" id="btn-save" class="btn-flat waves waves-effect">Guardar</button>
	</div>
</section>