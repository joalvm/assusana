<div class="container">
<div class="col s12">
<div class="col s12">
<div class="inner">

<ul class="collection collection-main" id="list-themes">

<?php if ( ! empty($themes) ): ?>
	<?php foreach ($themes as $row => $cell): ?>

		<li class="collection-item"  id="theme_<?= $cell->id ?>">

			<!--BOTON DE OPCIONES PARA EL TEMA-->
			<button type="button" class="btn-options" data-activates="options_<?= $cell->id ?>">
				<i class="material-icons">&#xE5D4;</i>
			</button>

			<!--MENU DE OPCIONES PARA EL TEMA-->
			<ul class="dropdown-content" id="options_<?= $cell->id ?>">
				<li>
					<button type="button"
						class="mng-subtheme"
						data-action="create"
						data-theme="<?= $cell->id ?>"
						data-mfp-src="#popup-subtheme">
						<i class="material-icons">&#xE145;</i>Subtema
					</button>
				</li>
				<li class="divider"></li>
				<li>
					<button type="button"
						class="mng-theme"
						data-id="<?= $cell->id ?>"
						data-action="update"
						data-mfp-src="#popup-theme">
						<i class="material-icons">&#xE150;</i>Modificar
					</button>
				</li>
				<li>
					<button type="button"
						class="btn-delete"
						data-id="<?= $cell->id ?>"
						data-action="delete" 
						data-hierarchy="theme" 
						data-title="<?= $cell->title ?>"
						data-mfp-src="#popup-delete">
						<i class="material-icons">&#xE872;</i>Eliminar
					</button>
				</li>
			</ul>

			<!--TITULO DEL TEMA-->
			<a class="title name"><?= $cell->title ?></a>

			<?php if( ! is_null($cell->description) ): ?>
				<p><?= $cell->description ?></p>
			<?php endif; ?>

			<!--SUBTEMAS PARA EL TEMA ACTUAL-->
			<span class="headertext-stheme">Subtemas</span>

			<!--LISTA DE SUBTEMAS-->
			<ul class="list-subthemes collection collection-main">

			<?php if ( ! empty($cell->subthemes)): ?>
				<?php foreach ( $cell->subthemes as $index => $obj ): ?>

						<li class="collection-item" id="subtheme_<?= $obj->id ?>">

							<!--BOTON DE OPCIONES PARA EL SUBTEMA-->
							<button type="button"
								class="btn-options"
								data-activates="options_subtheme_<?= $obj->id ?>">
								<i class="material-icons">&#xE5D4;</i>
							</button>

							<!--MENU DE OPCIONES PARA EL SUBTEMA-->
							<ul class="dropdown-content" id="options_subtheme_<?= $obj->id ?>">
								<li>
									<button type="button"
										class="mng-subtheme"
										data-theme="<?= $cell->id ?>"
										data-id="<?= $obj->id ?>"
										data-action="update"
										data-mfp-src="#popup-subtheme" >
										<i class="material-icons">&#xE150;</i>Modificar
									</button>
								</li>
								<li>
									<button type="button"
										class="btn-delete"
										data-theme="<?= $cell->id ?>"
										data-id="<?= $obj->id ?>"
										data-title="<?= $obj->title ?>"
										data-hierarchy="subtheme" 
										data-mfp-src="#popup-delete">
										<i class="material-icons">&#xE872;</i>Eliminar
									</button>
								</li>
							</ul>

							<h5 class="title"><?= $obj->title ?></h5>
							
							<?php if( $obj->description != null ): ?>
								
								<p class="description">
									<?= nl2br($obj->description) ?>
								</p>

							<?php endif; ?>
						</li>

				<?php endforeach; ?>
			<?php else: ?>

				<li class="empty">
					<p>No se han incluido subtemas</p>
				</li>

			<?php endif; ?>

			</ul>

			<span class="last-modified">
				Modificado:&nbsp;
				<time class="humanize" data-type="datetime" data-datetime="<?= $cell->last_modified ?>"></time>
			</span>

		</li>

	<?php endforeach; ?>
<?php else: ?>

	<li class="collection-item empty-list">
		<h4>No se registran datos</h4>
	</li>

<?php endif; ?>

</ul>

</div>
</div>
</div>
</div>

<div class="fixed-action-btn active" style="bottom: 45px; right: 24px;">
	<button type="button"
		id="mng-theme"
		class="btn-floating btn-large waves waves-effect"
		data-action="create"
		data-mfp-src="#popup-theme">
		<i class="material-icons">&#xE145;</i>
	</button>
</div>


<section id="popup-theme" class="white-popup mfp-with-anim mfp-hide">
	<form method="post" data-action="create" autocomplete="off">
		<div class="popup-header">
			<h4 class="title">Nuevo Tema</h4>
		</div>
		<div class="popup-body">
			<div class="input-field col s12">
				<input type="text" name="title" id="txt_title">
				<label for="txt_title">Titulo</label>
			</div>
			<div class="input-field col s12">
				<textarea name="description" rows="2" id="txt_description" class="materialize-textarea"></textarea>
				<label for="txt_description">Descripción</label>
			</div>
		</div>
		<div class="popup-footer">
			<button type="submit" id="btn-save" class="btn-flat waves waves-effect">Guardar</button>
		</div>
	</form>
</section>


<section id="popup-subtheme" class="white-popup mfp-with-anim mfp-hide">
	<form method="post" data-action="create" autocomplete="off">
		<div class="popup-header">
			<h4 class="title">Nuevo Subtema</h4>
		</div>
		<div class="popup-body">
			<div class="input-field col s12">
				<input type="text" name="title" id="txt_title">
				<label for="txt_title">Titulo</label>
			</div>
			<div class="input-field col s12">
				<textarea name="description" rows="2" id="txt_description" class="materialize-textarea"></textarea>
				<label for="txt_description">Descripción</label>
			</div>
		</div>
		<div class="popup-footer">
			<button type="submit" id="btn-save" class="btn-flat waves waves-effect">Guardar</button>
		</div>
	</form>
</section>


<section id="popup-delete" class="white-popup mfp-with-anim mfp-hide">
	<form method="post" autocomplete="off">
		<div class="popup-header">
			<h4 class="title">¿Realmente deseas eliminar?</h4>
		</div>
		<div class="popup-body">
			<p class="message no-mar-bottom"></p>
		</div>
		<div class="popup-footer">
			<button type="submit" id="btn-save" class="btn-flat waves waves-effect">Eliminar</button>
		</div>
	</form>
</section>

