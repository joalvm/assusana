<!DOCTYPE html>
<html lang="es-PE">
<head>
	<meta charset="UTF-8">
	<title></title>
</head>
<body>
<table style="border: none; border-collapse: collapse; max-width: 350px; margin: 20px auto; background: #f0f0f0;" cellpadding="0" cellspacing="0" border="0">
	<tr>
		<td valign="middle" align="center" style="padding: 20px; border-bottom: 1px solid #e0e0e0;">
			<h2 style="text-align: center; color: #333333; text-transform: uppercase; font-family: Segoe, 'Segoe UI', 'Helvetica Neue', Arial, sans-serif; font-weight: bolder;">Hola, <?= $fullname ?>.</h2>
		</td>
	</tr>
	<tr>
		<td  style="padding: 20px;">
			<p style="font-family: Segoe, 'Segoe UI', 'Helvetica Neue', Arial, sans-serif; font-size: 14px; line-height: 1.4; color: #444;">
				Tu solicitud de restauración de contraseña ha sido aceptada, tus nuevos datos son los que se describen a continuación:
			</p>
			<h4 style="margin: 0; color: #444444; font-family: Segoe, 'Segoe UI', 'Helvetica Neue', Arial, sans-serif;">Usuario: <?= $username ?></h4>
			<h4 style="margin: 0; color: #444444; font-family: Segoe, 'Segoe UI', 'Helvetica Neue', Arial, sans-serif;">Nueva Contraseña: <?= $password ?></h4>
		</td>
	</tr>
	<tr>
		<td >
			<a href="<?= base_url('login.html') ?>" style="border: none;
			    border-radius: 2px;
			    display: inline-block;
			    height: 56px;
			    line-height: 56px;
			    outline: 0;
			    text-transform: uppercase;
			    vertical-align: middle;
			    -webkit-tap-highlight-color: transparent;
			    background-color: #0D47A1;
			    width: 100%;
			    margin-top: 1rem;
			    text-align: center;
			    color: white;
			    font-weight: bold;
			    font-family: Segoe, 'Segoe UI', 'Helvetica Neue', Arial, sans-serif;
			    text-decoration: none;" title="Validar mi Usuario">Acceder</a>
		</td>
	</tr>
</table>
	
</body>
</html>