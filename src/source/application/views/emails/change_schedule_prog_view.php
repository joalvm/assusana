<!DOCTYPE html>
<html lang="es-PE">
<head>
	<meta charset="UTF-8">
	<title></title>
</head>
<body style="-webkit-text-size-adjust: 100%;-ms-text-size-adjust: 100%;margin: 0;padding: 0;background: #EEEEEE;height: 100% !important;width: 100% !important;">        
<table border="0" cellpadding="0" cellspacing="0" height="100%" width="100%" id="bodyTable" style="-webkit-text-size-adjust: 100%;-ms-text-size-adjust: 100%;mso-table-lspace: 0pt;mso-table-rspace: 0pt;margin: 0;padding: 0;font-family: Helvetica Neue, Helvetica, Arial, sans-serif;min-height: 100%;background: #EEEEEE;color: #444444;border-collapse: collapse !important;height: 100% !important;width: 100% !important;">
	<tbody>
		<tr>
			<td align="center" valign="top" style="-webkit-text-size-adjust: 100%;-ms-text-size-adjust: 100%;mso-table-lspace: 0pt;mso-table-rspace: 0pt;">
				<table align="center" border="0" cellspacing="0" width="600" id="emailContainer" style="width: 600px!important;min-width: 600px!important;-webkit-text-size-adjust: 100%;-ms-text-size-adjust: 100%;mso-table-lspace: 0pt;mso-table-rspace: 0pt;border-collapse: collapse !important;">
					<tbody>
						<tr>
							<td align="center" valign="top" style="-webkit-text-size-adjust: 100%;-ms-text-size-adjust: 100%;mso-table-lspace: 0pt;mso-table-rspace: 0pt;">
								<table border="0" cellpadding="0" cellspacing="0" width="100%" id="templatePreheader" style="margin: 15px 0 10px;-webkit-text-size-adjust: 100%;-ms-text-size-adjust: 100%;mso-table-lspace: 0pt;mso-table-rspace: 0pt;border-collapse: collapse !important;">
									<tbody>
										<tr>
										     <td id="logoWrap" align="bottom" style="text-align: center!important;background-image: url('');background-repeat: no-repeat;background-size: 600px 50px;background-position: center center;-webkit-text-size-adjust: 100%;-ms-text-size-adjust: 100%;mso-table-lspace: 0pt;mso-table-rspace: 0pt;background: url() center center / 600px 50px no-repeat!important;">
												<a href="http://sgp.pcm.gob.pe" style="-webkit-text-size-adjust: 100%; -ms-text-size-adjust: 100%; text-decoration: none; display: block;">
													<!--
													| LOGO DE LA INSTITUCIÓN
													-->
													<img id="logo" src="<?= base_url('/static/img/SGPlogo.png'); ?>" width="280px" height="125px" 
														style="margin: 10px auto 20px;-ms-interpolation-mode: bicubic;height: auto;line-height: 100%;outline: none;text-decoration: none;max-width: 100%;border: 0!important;">
												</a>
										     </td>
										</tr>
										<tr>
										    <td align="center" valign="top" style="-webkit-text-size-adjust: 100%;-ms-text-size-adjust: 100%;mso-table-lspace: 0pt;mso-table-rspace: 0pt;">
										        <table class="sectionWrap" border="0" cellpadding="0" cellspacing="0" width="600" style="background: #FFFFFF;overflow: hidden;border-radius: 4px;box-shadow: 0px 3px 0px #DDDDDD;max-width: 600px!important;min-width: 600px!important;-webkit-text-size-adjust: 100%;-ms-text-size-adjust: 100%;mso-table-lspace: 0pt;mso-table-rspace: 0pt;text-align: center;border-collapse: collapse !important;box-shadow: 0 2px 5px 0 rgba(0,0,0,0.16),0 2px 10px 0 rgba(0,0,0,0.12);" id="content">
										            <tbody>
										                <tr>
															<td id="header" valign="top" bgcolor="#E53935" style="padding: 20px 0 0;max-width: 100%!important;background: url('') #E53935 top center / 600px 300px repeat;-webkit-text-size-adjust: 100%;-ms-text-size-adjust: 100%;mso-table-lspace: 0pt;mso-table-rspace: 0pt;">
																<h1 style="font-family: Helvetica;font-size: 36px;font-style: normal;font-weight: bold;text-align: center;margin: 120px auto 50px;padding: 0 30px;line-height: 46px!important;">
																	<!--
																	| NOMBRE DE LA CAPCITACIÓN
																	-->
																	<font color="#ffffff">
																		<?= $training_name ?>
																	</font>
																</h1>
															</td>
										                </tr>
										           
										                <tr>
										                    <td id="content-block" style="-webkit-text-size-adjust: 100%;-ms-text-size-adjust: 100%;mso-table-lspace: 0pt;mso-table-rspace: 0pt;text-align: center;padding: 50px 30px;">
																<!--
																| TEXTO INTRUCTORIO
																-->
																<p style="margin: 0 auto 20px;text-align: center;-webkit-text-size-adjust: 100%;-ms-text-size-adjust: 100%;font-size: 20px;line-height: 24px;">
																	<?php
																	$names = explode(" ", $name);
																	$months = ["Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio", "Julio", "Agosto", "Septiembre", "Octubre", "Noviembre", "Diciembre"];
																	$date_real_arr = explode("-", $date_realization);
																	$date_real_str = $date_real_arr[2] . " " . $months[(((int)$date_real_arr[1]) - 1)];

																	$time_start_arr = explode(":", $time_start);
																	$time_start_str = $time_start_arr[0] . ":" . $time_start_arr[1];

																	$time_finish_arr = explode(":", $time_finish);
																	$time_finish_str = $time_finish_arr[0] . ":" . $time_finish_arr[1];
																	?>
																	<font color="#4d4d4d">
																		<b>Hola, <?= $names[0]; ?>.</b>
																		<br>
																		<br> 
																		Te informamos que la capacitación&nbsp;<b><?= $training_name ?></b>, 
																		ha tenido cambios en su programación, los cambios son:<br><br>
																		<span>Fecha</span><br>
																		<b><?= $date_real_str; ?></b><br>
																		<span>Hora de inicio</span><br>
																		<b><?= $time_start_str; ?></b><br>
																		<span>Hora de Finalización</span><br>
																		<b><?= $time_finish_str ?></b><br>
																	</font>
																</p>
																<br>
										                    </td>
										                </tr>
										        	</tbody>
										        </table>
										    </td>
										</tr>
										<tr>
										    <td align="center" valign="top" style="-webkit-text-size-adjust: 100%;-ms-text-size-adjust: 100%;mso-table-lspace: 0pt;mso-table-rspace: 0pt;">
										        <table border="0" align="center" cellpadding="10" cellspacing="0" width="600" id="emailFooter" style="-webkit-text-size-adjust: 100%;-ms-text-size-adjust: 100%;mso-table-lspace: 0pt;mso-table-rspace: 0pt;border-collapse: collapse !important;max-width: 100%!important;">
										            <tbody>
										                <tr>
										                    <td valign="top" class="footerContent" style="-webkit-text-size-adjust: 100%;-ms-text-size-adjust: 100%;mso-table-lspace: 0pt;mso-table-rspace: 0pt;">
										                        <table border="0" cellpadding="10" cellspacing="0" width="100%" style="-webkit-text-size-adjust: 100%;-ms-text-size-adjust: 100%;mso-table-lspace: 0pt;mso-table-rspace: 0pt;border-collapse: collapse !important;">
										                            <tbody>
										                            	<tr>
										                                    <td valign="top" style="-webkit-text-size-adjust: 100%;-ms-text-size-adjust: 100%;mso-table-lspace: 0pt;mso-table-rspace: 0pt;">
										                                        <br>
										                                        <div style="color: #707070;font-family: Arial;font-size: 12px;line-height: 125%;text-align: center;max-width: 100%!important;">
										                                            <em>Copyright © 2016 Secretaría de Gestión Publica, Todos los derechos reservados.</em>
										                                            <br>
										                                        </div>
										                                        <br>
										                                    </td>
										                            	</tr>
										                        	</tbody>
										                        </table>
										                    </td>
										                </tr>
										        	</tbody>
										        </table>
										    </td>
										</tr>
									</tbody>
								</table>
							</td>
						</tr>
					</tbody>
				</table>
			</td>
		</tr>
	</tbody>
</table>
</body>
</html>