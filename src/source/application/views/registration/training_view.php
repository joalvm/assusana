<div class="container">
	<div class="row">
		<div class="col s12">
			<div class="nav-options">
				<div class="nav-wrapper">
					<!--BUSCADOR DE CAPACITACIONES-->
					<div class="search-widget">
						<button type="button" class="search-button waves waves-effect">
							<i class="material-icons">&#xE8B6;</i>
						</button>
						<div class="input-wrapper">
							<input name="q" type="search" placeholder="Buscar Capacitación" id="search-input" autocomplete="off">
							<button type="button" class="close-button">
								<i class="material-icons">&#xE5CD;</i>
							</button>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>

	<div class="row">
		<div class="col s12">
			<dl class="collection-sep">
				<?php 
				$month_arr = [NULL, "Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio", "Julio", "Agosto", "Septiembre", "Octubre", "Noviembre", "Diciembre"];
				$last_month = NULL;

				if ( ! empty($training) ) 
				{
					foreach ($training as $row => $cell) 
					{
						$time 	= strtotime($cell->date_realization);
						$month	= (int) date("n", $time);
						$year 	= date("Y", $time);

						if ( $last_month != $month )
						{
							echo '<dt class="sep-item">'. $month_arr[$month] . " " . $year.'</dt>';
							$last_month = $month;
						}

						$dept = ucfirst(mb_strtolower($cell->departamento, 'UTF-8'));
						$prov = ucfirst(mb_strtolower($cell->provincia, 'UTF-8'));
						$dist = ucfirst(mb_strtolower($cell->distrito, 'UTF-8'));
					?>
						<dd class="collection-item" data-search="<?= $cell->training_title; ?>">
							<h4 class="title">
								<a href="<?= base_url('workshops/'.$cell->programming_id.'/registration') ?>"><?= $cell->training_title; ?></a>
							</h4>
							<?php
							if( !empty($cell->training_description) ) 
							{
							?>
								<p class="description">
									<?= $cell->training_description; ?>
								</p>
							<?php
							} 
							?>
							<span class="date">
								<i class="material-icons">&#xE878;</i>
								<time class="humanize" data-type="date" data-datetime="<?= $cell->date_realization ?>"></time>
							</span>

							<b class="hide-on-small-and-down">|&nbsp;&nbsp;&nbsp;</b>

							<span class="time"><i class="material-icons">&#xE192;</i>
								<time class="humanize" data-type="time" data-datetime="<?= $cell->date_realization . " " . $cell->time_start ?>"></time> - 
								<time class="humanize" data-type="time" data-datetime="<?= $cell->date_realization . " " . $cell->time_finish ?>"></time>
							</span>

							<span class="place">
								<i class="material-icons">&#xE0AF;</i>
								<?= $cell->place; ?>
							</span>
							<span class="address">
								<i class="material-icons">&#xE55F;</i>
								<?= $cell->address. ' - '. $dist . ', '. $prov. ', ', $dept; ?>
							</span>
						</dd>
					<?php
					}
				}
				else
				{
				?>
					<dd class="empty-list">
						<h4>No hay capacitaciones programadas</h4>
					</dd>
				<?php
				}
				?>
			</dl>
		</div>
	</div>

	<div class="row">
		<div class="col s12 center">
			<button type="button" class="hide btn waves waves-effect">Más capacitaciones</button>
		</div>
	</div>

</div>
