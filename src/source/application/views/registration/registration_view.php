<div class="container register">
	<section class="row">
		<div class="col s12 m4 push-m8">
			<object id="logo-reg" class="logo" type="image/svg+xml" data="<?= base_url('static/img/SGPlogo.svg') ?>"></object>
		</div>
		<div class="col s12 m8 pull-m4">
			<div class="header-section">
				<h1 class="title"><?= $programming->training_title ?></h1>
				<h2 class="subtitle"><?= $programming->type_training ?></h2>
				<p class="description"><?= $programming->training_description ?></p>	
			</div>
		</div>
	</section>
	<section class="row">
		<div class="col s12">
			
			<div class="body-section">
				<span class="date"><i class="material-icons">&#xE878;</i><time class="humanize" data-type="date" data-datetime="<?= $programming->date_realization ?>"></time></span>
				<span class="time"><i class="material-icons">&#xE192;</i>
					<time class="humanize" 
						data-type="time" 
						data-datetime="<?= $programming->date_realization . " " . $programming->time_start ?>">
					</time>&nbsp;-&nbsp;
					<time class="humanize" 
						data-type="time" 
						data-datetime="<?= $programming->date_realization . " " . $programming->time_finish ?>">
					</time>
				</span>

				<span class="place">
					<i class="material-icons">&#xE0AF;</i>
					<?= $programming->place; ?>
				</span>
				<span class="address">
					<i class="material-icons">&#xE55F;</i>
					<?= $programming->address; ?>
				</span>

				<span class="ubigeo">
					<span class="head">Departamento&nbsp;&nbsp;:</span>
					<span class="value"><?= $programming->departamento; ?></span>
				</span>
				<span class="ubigeo">
					<span class="head">Provincia&nbsp;&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;&nbsp;:</span>
					<span class="value"><?= $programming->provincia; ?></span>
				</span>
				<span class="ubigeo">
					<span class="head">Distrito &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;&nbsp;  &nbsp;&nbsp;:</span>
					<span class="value"><?= $programming->distrito; ?></span>
				</span>

				<h3 class="header-theme">Temas</h3>
				<blockquote class="blocktheme">
					<ul class="collapsible popout" data-collapsible="accordion">
				<?php 
					foreach ($themes as $row => $cell ) 
					{
				?>
					
					<li>
						<div class="collapsible-header tooltipped" data-position="top" data-delay="50" data-tooltip="Pulsa para saber más" title="pulsa para saber más">
							<span class="title" title="<?= $cell['title'] ?>">
								<i class="material-icons left">&#xE264;</i>
								<?= $cell['title'] ?>
								<i title="Mas información" class="material-icons right">&#xE313;</i>
							</span>
						</div>
						<p class="description"><?= $cell['description'] ?></p>
						<div class="collapsible-body">
							<span class="header-stheme">Subtemas</span>
							<?php 
							foreach ($cell['subthemes'] as $row2 => $cell2 ) 
							{
							?>
								<blockquote class="blocksubtheme">
									<span class="stitle"><?= $cell2['title']; ?></span>
									
									<span class="docente"><?= $cell2['docente']; ?></span>
									<?php 
									if ( ! empty($cell2['description']) ) { 
									?>
										<p class="sdescription"><?= $cell2['description']; ?></p>
									<?php 
									}
									?>
								</blockquote>
							<?php
							}
							?>
						</div>
					</li>
				<?php
					}
				?>
					</ul>
				</blockquote>

				<!--
				| 	DESCRIPCIÓN PARA CAPACITACIONES PERSONALIZADAS
				-->
				<?php 
				if ( $programming->its_customized )
				{
				?>
					<p class="personalize">
						Capacitación programada exclusivamente para la entidad:<br />
						<b><?= $programming->entity_name ?></b>
					</p>
				<?php 
				}
				?>
			</div>
		</div>
	</section>

	<section class="row">
	<?php if ( ($number_reg < $programming->limit_max OR $programming->limit_max == 0) && $programming->its_canceled == 0 && $programming->its_finish == 0 ): ?>
		<form id="frm_manage" 
			autocomplete="off" 
			data-personalize="<?= $programming->its_customized ?>" 
			data-programming="<?= $programming->programming_id; ?>"
			data-entity="<?= (!empty($programming->entity_id)) ? $programming->entity_id : 0 ?>" 
			action="/workshop/<?= $programming->programming_id; ?>/registration" 
			method="post">
			
			<h2 class="header-section">
				Formulario de inscripción
				<div class="plazas tooltipped" data-position="button" data-delay="50" data-tooltip="<?= 'Catidad/Limite de personas inscritas' ?>">
					<?php if ( $programming->limit_max > 0 ): ?>
						<span class="count">
							<?= $number_reg ?>/<?= $programming->limit_max ?>
						</span>
						<i class="material-icons">&#xE7FD;</i>
					<?php endif; ?>
				</div>
			</h2>
			<p>
				<li>En caso de haberse registrado en una anterior capacitación, ingrese su&nbsp;<strong>DNI</strong>&nbsp;y los datos se
				cargarán automaticamente</li>
				<li>Asegúrese de colocar sus datos generales de&nbsp;<b>forma completa</b>, para que no presenten inconvenientes en el proceso de certificados</li>
			</p>
			<div class="row">
				<div class="col s12">
					<h4 class="title-page">Datos Generales:</h4>
				</div>
			</div>

			<div class="row">
				<div class="input-field col s12 m4">
					<input id="txt_dni" type="text" name="dni" maxlength="8" class="only-number dataset" required>
					<label for="txt_dni" data-success="DNI permitido">DNI  *</label>
				</div>
			</div>
			<div class="row">
				<div class="input-field col s12 m6">
					<input id="txt_name" type="text" name="name" class="cleanable dataset" required>
					<label for="txt_name">Nombres *</label>
				</div>
				<div class="input-field col s12 m6">
					<input id="txt_lastname" type="text" name="lastname" class="cleanable dataset" required>
					<label for="txt_lastname">Apellidos  *</label>
				</div>
			</div>
			<div class="row">
				<div class="input-field col s12 m4">
					<select name="gender" id="cbo_gender" class="cleanable dataset">
						<option value="" class="default" disabled selected>Escoja su opción</option>
						<option value="0">Masculino</option>
						<option value="1">Femenino</option>
					</select>
					<label for="cbo_gender">Genero *</label>
				</div>
				<div class="input-field col s12 m4">
					<input type="text" id="txt_profesion" name="profesion" class="cleanable dataset">
					<label for="txt_profesion">Especialidad</label>
				</div>
				<div class="input-field col s12 m4">
					<input id="txt_email" name="email" multiple="multiple" type="email" class="validate cleanable dataset" required>
					<label data-error="Email(s) invalido(s)" data-success="OK" for="txt_email">
						Email *
						<sub style="font-size: 14px; margin-left: 5px; color: #222;cursor: default;" 
							class="tooltipped" 
							data-position="top" 
							data-delay="50" 
							data-tooltip="Use comas(,) para separar multiples correos electronicos">
							<i style="font-size: 14px;font-weight: 600;" class="material-icons">&#xE8FD;</i>
						</sub>
					</label>
				</div>
			</div>

			<div class="row">
				<div class="input-field col s12 m4">
					<input id="txt_cellphone" type="text" name="cellphone" maxlength="9" class="only-number cleanable dataset">
					<label for="txt_cellphone">Celular</label>
				</div>
				<div class="input-field col s12 m4">
					<input id="txt_phone" type="text" name="phone" maxlength="10" class="only-number cleanable dataset">
					<label for="txt_phone">Telefono</label>
				</div>
				<div class="input-field col s12 m4">
					<input id="txt_annex" type="text" name="annex" class="cleanable dataset">
					<label for="txt_annex">Anexo</label>
				</div>
			</div>

			<!--UBICACIÓN EN EL PERU-->
			<div class="row">
				<div class="col s12">
					<h4 class="title-page">Lugar de residencia:</h4>
				</div>
			</div>
			<div class="row">
				<div class="input-field col s12 m4">
					<select class="browser-default ubigeo cleanable" disabled name="departamento" id="cbo_departamento">
						<option value="0" disabled selected class="default">Escoja su Opción</option>
					</select>
					<label for="cbo_departamento" class="active">Departamento</label>
				</div>
				<div class="input-field col s12 m4">
					<select class="browser-default ubigeo cleanable" disabled name="provincia" id="cbo_provincia">
						<option value="0" class="default" disabled selected>Escoja su Opción</option>
					</select>
					<label for="cbo_provincia" class="active">Provincia</label>
				</div>
				<div class="input-field col s12 m4">
					<select id="cbo_distrito" name="distrito" class="browser-default ubigeo cleanable dataset" disabled>
						<option value="0" class="default" disabled selected>Escoja su Opción</option>
					</select>
					<label for="cbo_distrito" class="active">Distrito *</label>
				</div>
			</div>
			<div class="row">
				<div class="col s12">
					<h4 class="title-page">Datos laborales:</h4>
				</div>
			</div>
			<!--
			| 	Para escojer entidad no debe de ser un
			|	una capacitación personalizada
			-->
			<?php if ( ! $programming->its_customized ): ?>
				<div class="row">
					<div class="input-field col s12">
						<label for="txt_entity_display">Entidad *</label>
						<input id="txt_entity_display" type="text" data-mfp-src="#popup-list-entities" readonly class="cleanable">
					</div>
				</div>
				<div class="row">
					<div class="input-field col s12 m4 method_search">
						<label for="txt_type_entity">Tipo de Entidades</label>
						<input id="txt_type_entity" type="text" readonly class="cleanable">
					</div>
					<div class="input-field col s12 m4">
						<label for="txt_subtype_entity">Subtipo de Entidades</label>
						<input id="txt_subtype_entity" type="text" readonly class="cleanable">
					</div>
					<div class="input-field col s12 m4">
						<input id="txt_position" name="position" type="text" class="cleanable dataset">
						<label for="txt_position">Cargo</label>
					</div>
				</div>
			<?php else: ?>
				<div class="row">
					<div class="input-field col s12">
						<input id="txt_position" name="position" type="text" class="cleanable dataset">
						<label for="txt_position">Cargo</label>
					</div>
				</div>
			<?php endif; ?>
			
			<div class="row">
				<div class="col s12">
					<h4 class="title-page">Para tomar en cuenta:</h4>
				</div>
			</div>
			<div class="row">
				<div class="col s12">
					<p>
						<input type="checkbox" name="dispacity" class="filled-in cleanable dataset" id="chk_is_dispacity" />
						<label for="chk_is_dispacity">¿Presenta alguna discapacidad?</label>
					</p>
				</div>
				<div class="input-field col s12">
					<textarea 
						id="txt_dispacity_details" 
						name="details_dispacity" 
						disabled 
						class="materialize-textarea cleanable dataset"></textarea>
					<label for="txt_dispacity_details">Detalle de la discapacidad</label>
				</div>
			</div>
			
			<div class="fixed-action-btn active" style="bottom: 45px; right: 24px;">
				<button type="submit" class="btn-floating btn-large">
					<i class="material-icons">&#xE161;</i>
				</button>
			</div>

			<br>
			
			<a href="/workshops" 
				data-position="top" 
				data-delay="50" 
				data-tooltip="Volver a la lista de capacitaciones programadas"
				class="btn btn-block tooltipped waves waves-effect">
				<i class="material-icons">&#xE5DC;</i>
			</a>
		</form>
	<?php elseif( $programming->its_canceled ): ?>
		<div class="col s12">
			<div class="empty-list">
				<h4>Capacitación cancelada</h4>
			</div>
		</div>
	<?php elseif( $programming->its_finish ): ?>
		<div class="col s12">
			<div class="empty-list">
				<h4>Capacitación finalizada</h4>
			</div>
		</div>
	<?php else: ?>
		<div class="col s12">
			<div class="empty-list">
				<h4>Plazas cubiertas</h4>
				<p>El limite de participantes para esta capacitación ha sido alcanzado</p>
			</div>
		</div>
	<?php endif; ?>
	</section>
</div>

<!--
|	MODAL LISTA DE ENTIDADES
-->
<section id="popup-list-entities" class="white-popup mfp-with-anim mfp-hide">
		<div class="popup-header">
			<h4 class="title">Seleccione una entidad</h4>
		</div>
		<div class="popup-body">
			<!--FILTRADO DE PERSONAS-->
			<div class="searcher in-modal">
				<input type="search" placeholder="Buscar Tipo, Subtipo o Entidad" id="search" autocomplete="off" spellcheck="false" >
				<i class="material-icons clear_search">close</i>
				<label for="search"><i class="material-icons">search</i></label>
			</div>
			<ul id="list-entities" class="collection modal-list"></ul>
			<p class="items-count">
				<span class="curr">0</span> / <span class="total">0</span>
			</p>
		</div>
		<div class="popup-footer">
			<button type="button" id="btn-cancel" class="btn-flat waves waves-effect">Salir</button>
		</div>
</section>

<!--
| MODAL DE PREGUNTA ANTES DE GUARDAR LOS DATOS
-->
<section id="md-before-submit" class="white-popup mfp-with-anim mfp-hide">
	<div class="popup-header">
		<h4 class="title">Lee atentamente</h4>
	</div>
	<div class="popup-body">
		<p class="message no-mar-bottom">
			Este es el primer paso de la inscripción, en los próximos minutos
			te enviaremos un correo electrónico con tus datos y un link, 
			a las direcciones de correo que has proporcionado, para que
			puedas activar tu inscripción.<br><br>
			La inscripción será válida solo después de que accedas al link que te enviaremos.
		</p>
		<br>
		<p class="message no-mar-bottom">
			<b>Antes de proceder con la inscripción, asegura que tus datos están correctos.</b>
		</p>
	</div>
	<div class="popup-footer">
		<button type="button" id="btn-cancel" class="btn-flat waves waves-effect">Cancelar</button>	
		<button type="button" id="btn-save" class="btn-flat waves waves-effect">Inscribirme</button>
	</div>
</section>