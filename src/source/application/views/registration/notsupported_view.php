<!DOCTYPE html>
<html lang="es-PE">
<head>
	<meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
	<title>SGP :: Capacitaciones <?= date('Y'); ?></title>
	<style type="text/css" rel="stylesheet">
		*, html {
			font-family: Arial, monospace;
			font-size: 16px;
			margin: 0;
			padding: 0;
		}

		body {
			background-color: #f0f0f0;
		}

		.not-supported {
			margin-top: 32px;
			text-align: center;
		}

		.not-supported p {
			color: #444;
			margin: 16px 0;
		}

		.not-supported h3 {
			font-size: 24px;
			margin: 16px 0;
			color: #444;
		}

		.copy {
			max-width: 100% !important;
    		margin-bottom: 5px;
    		margin-top: 16px;
		}

		.copy em {
		    color: #707070;
		    font-family: Arial;
		    font-size: 12px;
		    text-align: center;
		    letter-spacing: -0.25px;
		    display: block;
		}
	</style>
</head>
<body>
	<div class="not-supported">
		<p>
			La Web esta optimizada para las siguiente versiones de navegador:&nbsp;
			<br><br><b>Chrome 35+, Firefox 31+, Safari 7+, IE 10+, Opera 35+</b>
		</p>
		<h3>Si estas viendo esto, se debe a que tu navegador está desactualizado</h3>
	</div>
	<div class="copy">
        <em>Copyright © <?= date('Y'); ?> Secretaría de Gestión Publica, Todos los derechos reservados.</em>
    </div>
</body>
</html>