<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Login extends CI_Controller
{
    private $users = [];

    public function __construct()
    {
        parent::__construct();
        $this->load->library('user_agent');
        $this->load->model('login_model');
        $this->users = [
            [
                "username" => "joalvm",
                "password" => sha1("t5r4e3w2q1"),
                "name" => "Alejandro",
                "lastname" => "Vilchez"
            ],
            [
                "username" => "victorfranco",
                "password" => sha1("victorfranco123"),
                "name" => "Víctor Franco",
                "lastname" => "Chinchay Alburqueque"
            ]
        ];
    }

    public function index()
    {
        $this->_check_browser_supported();
        $this->load->view("login_view");
    }

    public function authenticate()
    {
        $data = $this->decodeLoginData();
        $result = new stdClass;

        try {
            if (!$this->input->is_ajax_request()) {
                throw new Exception("Es recurso no es accesible mediante este metodo", 405);
            }

            if (empty($data)) {
                throw new Exception("No hay datos que manipular, la acción será cancelada", 400);
            }

            if ($data->role == 2) {
                $response = $this->login_model->verify_users($data->username, $data->password, $data->role);

                if ($response["error"]) {
                    throw new Exception($response["message"], 404);
                }

                $result->error = $response["error"];
                $result->error_code = 200;
                $result->message = $response["message"];
                $result->href = $response["url"];
            } else {
                $found = [];

                foreach ($this->users as $index => $arr) {
                    if ($arr['username'] == $data->username) {
                        if ($arr['password'] == sha1($data->password)) {
                            $found = $arr;
                            break;
                        }
                    }
                }

                if (empty($found)) {
                    throw new Exception("Usuario y/o Contraseña erroneos, verifique sus datos.", 404);
                }

                $_array = [
                    'name' => $found['name'],
                    'lastname' => $found['lastname'],
                    'username' => $found['username'],
                    'role' => 1,
                    'logged_in' => true
                ];

                $this->session->set_userdata($_array);

                $result->error = false;
                $result->error_code = 200;
                $result->message = "Usuario autentificado... Redireccionando.";
                $result->href = base_url("/app/dashboard.html");
            }
        } catch (Exception $ex) {
            $result->error = true;
            $result->error_code = $ex->getCode();
            $result->message = $ex->getMessage();
        }

        $this->output
            ->set_content_type('Application/json', 'utf-8')
            ->set_output(json_encode($result));
    }

    public function logout()
    {
        $this->output->set_status_header(200);
        $this->output
            ->set_content_type('application/json')
            ->set_output(json_encode($this->session->unset_userdata('logged_in')));
    }

    private function decodeLoginData()
    {
        $n = $this->login_model->decripter_length;

        $cut_left 	= substr($this->input->post('data'), ($n / 2));
        $cut_right 	= substr($cut_left, 0, (strlen($cut_left) - $n));

        return json_decode(base64_decode($cut_right));
    }

    private function _check_browser_supported()
    {
        $version = (int)$this->agent->version();
        $browser = $this->agent->browser();

        if (($browser == 'Internet Explorer' && $version < 10) ||
             ($browser == 'Chrome' && $version < 35) ||
             ($browser == 'Opera' && $version < 35) ||
             ($browser == 'Mozilla' && $version < 31) ||
             ($browser == 'Safari' && $version < 7) ||
             ($browser == 'Firefox' && $version < 31)) {
            echo $this->load->view('registration/notsupported_view', [], true);
            exit();
        }
    }
}

/* End of file Login.php */
/* Location: ./application/controllers/Login.php */
