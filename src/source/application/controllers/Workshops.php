<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Workshops extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->library('user_agent');

		$this->load->model('training_model');
		$this->load->model('maintenance_model');
		$this->load->model('workshops_model');
	}

	public function index()
	{
		$this->_check_browser_supported();

		$dataView["training"] = $this->training_model->show_programing_no_personalize();
		$dataView["type_training"] = $this->maintenance_model->list_type_training();

		$strView = $this->load->view('registration/training_view', $dataView, TRUE);

		$dataTemplate = [
			"title_head" => "Capacitaciones :: Lista",
			"title_body" => "Capacitaciones",
			"view" => "training",
			"body" => $strView
		];

		$this->parser->parse('templates/reg_process_template', $dataTemplate);
	}

	public function registration($programming_id)
	{
		$this->_check_browser_supported();

		$this->training_model->ID = $programming_id;

		$program = $this->workshops_model->find_programming($programming_id);

		if ( ! empty($program) )
		{
			$dataView['programming'] = $program[0];
			$dataView['number_reg'] = $this->training_model->verify_number_registered();
			$dataView['themes'] = $this->training_model->get_programming_themes();

			$strView = $this->load->view('registration/registration_view', $dataView, TRUE);

			$dataTemplate = [
				"title_head" => "Capacitaciones :: Registro",
				"title_body" => "Registro",
				"view" => "registration",
				"body" => $strView
			];

			$this->parser->parse('templates/reg_process_template', $dataTemplate);

		} else {
			show_404();
			exit();
		}
	}

	public function inscription($programming)
	{
		$data_post = $this->input->post();
		$result = new stdClass;

		try
		{
			if ( !$this->input->is_ajax_request() )
				throw new Exception("Es recurso no es accesible mediante este metodo", 405);

			if ( empty($data_post) )
				throw new Exception("No hay datos que manipular, la acción será cancelada", 400);

			$this->workshops_model->programID = $programming;
			$this->training_model->ID = $programming;
			$this->workshops_model->data_program = $this->training_model->find_programming($programming);

			if ( empty($this->workshops_model->data_program) )
				throw new Exception("La capacitación a la que intentas registrarte ya ha sido eliminada", 404);

			$this->workshops_model->data = $data_post;

			if ( $data_post['id'] != 0 )
			{
				$this->workshops_model->particiID = $data_post['id'];

				if ( $this->workshops_model->hasRegister() )
					throw new Exception("Usted ya esta registrado en este taller", 400);

				$this->workshops_model->modifyParticipant();
				$this->workshops_model->insertInscription();
			}
			else
			{
				$this->workshops_model->insertParticipant();
				$this->workshops_model->insertInscription();
			}

			$result->error = FALSE;
			$result->code = 200;
			$result->message = "Inscripción aceptada. Verifique el correo electronico que hemos enviado, reiniciando el formulario...";
			$result->data = [];

		}
		catch(Exception $ex)
		{
			$result->error = TRUE;
			$result->message = $ex->getMessage();
			$result->code = $ex->getCode();
		}

		$this->output
			->set_status_header($result->code)
	        ->set_content_type('application/json', 'utf-8')
	        ->set_output(json_encode($result));
	}

	private function _check_browser_supported()
	{
		$version = (int)$this->agent->version();
		$browser = $this->agent->browser();

		if ( ($browser == 'Internet Explorer' && $version < 10) ||
			 ($browser == 'Chrome' && $version < 35) ||
			 ($browser == 'Opera' && $version < 35) ||
			 ($browser == 'Mozilla' && $version < 31) ||
			 ($browser == 'Safari' && $version < 7) ||
			 ($browser == 'Firefox' && $version < 31) )
		{
			echo $this->load->view('registration/notsupported_view', [], TRUE);
			exit();
		}
	}

}

/* End of file Workshops.php */
/* Location: ./application/controllers/Workshops.php */
