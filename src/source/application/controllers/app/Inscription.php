<?php if (! defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class Inscription extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();

        $this->load->model('training_model');
        $this->load->model('inscription_model');
        $this->load->model('workshops_model');
        $this->load->model('docente/attendance_model');

        if (! $this->session->has_userdata('filter_prg')) {
            $this->session->set_userdata('filter_prg', 2);
        }
    }

    public function index()
    {
        $dataView['programming'] = $this->training_model->show_programing();
        $dataView['inscription'] = $this->inscription_model->get_ALLInscriptions();

        $strView = $this->load->view('admin/inscription/insc_index_view', $dataView, true);

        $dataTemplate = [
            "page" => "Participantes",
            "is_crud" => false,
            "subpage" => "",
            "crud_page" => "index",
            "module" => "inscription",
            "id" => "inscription_index",
            "body" => $strView
        ];

        $this->parser->parse('templates/application_template', $dataTemplate);
    }

    public function create($programming_id)
    {
        $this->training_model->ID = $programming_id;
        $program = $this->training_model->find_programming(true);

        $dataView['programming'] = $program;

        $strView = $this->load->view('admin/inscription/insc_manage_view', $dataView, true);

        $dataTemplate = [
            "page" => "Inscripciones",
            "is_crud" => true,
            "subpage" => "(Nuevo)",
            "crud_page" => "create",
            "module" => "inscription",
            "id" => "inscription_manage",
            "body" => $strView
        ];

        $this->parser->parse('templates/application_template', $dataTemplate);
    }

    public function insert($programming_id)
    {
        $data_post = $this->input->post();
        $result = new stdClass;

        try {
            if (!$this->input->is_ajax_request()) {
                throw new Exception("Es recurso no es accesible mediante este metodo", 405);
            }

            if (empty($data_post)) {
                throw new Exception("No hay datos que manipular, la acción será cancelada", 400);
            }

            $this->workshops_model->programID = $programming_id;
            $this->training_model->ID = $programming_id;
            $this->workshops_model->data_program = $this->training_model->find_programming($programming_id);
            $this->workshops_model->data = $data_post;

            if ($data_post['id'] != 0) {
                $this->workshops_model->particiID = $data_post['id'];

                if ($this->workshops_model->hasRegister()) {
                    throw new Exception("Usted ya esta registrado en este taller", 400);
                }

                $this->workshops_model->modifyParticipant();
            } else {
                $this->workshops_model->insertParticipant();
            }

            if (isset($data_post['attendance'])) {
                $this->workshops_model->insertInscription(false, true, ((boolean)$data_post['attendance']));
            } else {
                $this->workshops_model->insertInscription(false, true);
            }

            $result->error = false;
            $result->code = 200;
            $result->message = "Participante registrado, reiniciando...";
            $result->data = [];
        } catch (Exception $ex) {
            $result->error = true;
            $result->message = $ex->getMessage();
            $result->code = $ex->getCode();
        }

        $this->output
            ->set_status_header($result->code)
            ->set_content_type('application/json', 'utf-8')
            ->set_output(json_encode($result));
    }

    public function programming($programming_id)
    {
        $result = new stdClass;

        try {
            if (! $this->input->is_ajax_request()) {
                throw new Exception("Es recurso no es accesible mediante este metodo", 405);
            }

            $this->inscription_model->PGID = $programming_id;

            $table = $this->inscription_model->get_inscription_from_programming();

            $result->error = false;
            $result->code = 200;
            $result->data = $table;
            $result->its_canceled = $this->inscription_model->check_ifs_canceled_programming();
            $result->url = base_url("/app/inscription/programming/" . $programming_id . "/create.html");
        } catch (Exception $e) {
            $result->error = true;
            $result->code = $e->getCode();
            $result->message = $e->getMessage();
        }

        $this->output
            ->set_status_header($result->code)
            ->set_content_type('application/json', 'utf-8')
            ->set_output(json_encode($result));
    }

    public function active($inscription_id)
    {
        $result = new stdClass;

        try {
            if (! $this->input->is_ajax_request()) {
                throw new Exception("Es recurso no es accesible mediante este metodo", 405);
            }

            $this->inscription_model->ID = $inscription_id;

            $confirm = $this->inscription_model->active();

            $result->error = (! $confirm);
            $result->code = $confirm ? 200 : 400;
            $result->message = $confirm ? "Se activó correctamente": "Error al momento de activar, vuelva a intentarlo";
            $result->data = $confirm;
        } catch (Exception $e) {
            $result->error = true;
            $result->code = $e->getCode();
            $result->message = $e->getMessage();
        }

        $this->output
            ->set_status_header($result->code)
            ->set_content_type('application/json', 'utf-8')
            ->set_output(json_encode($result));
    }

    public function delete($inscription_id)
    {
        $result = new stdClass;

        try {
            if (! $this->input->is_ajax_request()) {
                throw new Exception("Es recurso no es accesible mediante este metodo", 405);
            }

            $this->inscription_model->ID = $inscription_id;

            $confirm = $this->inscription_model->delete();

            $result->error = (! $confirm);
            $result->code = $confirm ? 200 : 400;
            $result->message = $confirm ? "Se elimino correctamente": "Error al momento de eliminar, vuelva a intentarlo";
            $result->data = $confirm;
        } catch (Exception $e) {
            $result->error = true;
            $result->code = $e->getCode();
            $result->message = $e->getMessage();
        }

        $this->output
            ->set_status_header($result->code)
            ->set_content_type('application/json', 'utf-8')
            ->set_output(json_encode($result));
    }

    public function find_participant($programming_id)
    {
        $result = new stdClass;

        try {
            if (! $this->input->is_ajax_request()) {
                throw new Exception("Es recurso no es accesible mediante este metodo", 405);
            }

            $result->error = false;
            $result->code = 200;
            $result->data['participants'] = $this->attendance_model->get_participants($programming_id);
            $this->training_model->ID = $programming_id;
            $result->data['programming'] = $this->training_model->find_programming(true);
            $result->data['themes'] = $this->training_model->get_programming_themes();
        } catch (Exception $e) {
            $result->error = true;
            $result->code = $e->getCode();
            $result->message = $e->getMessage();
        }

        $this->output
            ->set_status_header($result->code)
            ->set_content_type('application/json', 'utf-8')
            ->set_output(json_encode($result));
    }

    public function update_info($participant_id)
    {
        $post = $this->input->post();
        $result = new stdClass;

        try {
            if (!$this->input->is_ajax_request()) {
                throw new Exception("Es recurso no es accesible mediante este metodo", 405);
            }

            if (empty($post)) {
                throw new Exception("No hay datos que manipular, la acción será cancelada", 400);
            }

            $this->inscription_model->data = $post;

            $confirm = $this->inscription_model->update_info_participant($participant_id);

            $result->error = $confirm->error;
            $result->code = (!$confirm->error) ? 200 : 400;
            $result->message = $confirm->message;
        } catch (Exception $ex) {
            $result->error = true;
            $result->message = $ex->getMessage();
            $result->code = $ex->getCode();
        }

        $this->output
            ->set_status_header($result->code)
            ->set_content_type('application/json', 'utf-8')
            ->set_output(json_encode($result));
    }
}

/* End of file Participant.php */
/* Location: ./application/controllers/Participant.php */
