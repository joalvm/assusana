<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Training extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->model('training_model');
		$this->load->model('maintenance_model');
		$this->load->model('coordinator_model');
	}

	public function register()
	{
		$dataView['items'] = $this->training_model->show_register();

		$strView = $this->load->view('admin/training/register/index_view', $dataView, TRUE);

		$dataTemplate = [
			"page" => "Capacitación",
			"subpage" => "(Lista General)",
			"is_crud" => FALSE,
			"crud_page" => "index",
			"module" => "training_register",
			"id" => "tr_register_index",
			"body" => $strView
		];

		$this->parser->parse('templates/application_template', $dataTemplate);
	}

	public function register_create()
	{
		$dataView['types'] = $this->maintenance_model->list_type_training();
		$dataView['themes'] = $this->maintenance_model->list_themes();
		$dataView['module'] = 'create';

		$strView = $this->load->view('admin/training/register/manage_view', $dataView, TRUE);

		$dataTemplate = [
			"page" => "Capacitación",
			"subpage" => "(Nuevo)",
			"is_crud" => TRUE,
			"crud_page" => "create",
			"module" => "training_register",
			"id" => "tr_register_manage",
			"body" => $strView
		];

		$this->parser->parse('templates/application_template', $dataTemplate);
	}

	public function register_update($training_id)
	{
		$this->training_model->ID = $training_id;

		$dataView['training'] = $this->training_model->find_register();

		if( !empty($dataView['training']) )
		{
			$dataView['themes'] = $this->maintenance_model->list_themes();
			$dataView['types'] = $this->maintenance_model->list_type_training();

			$dataView['module'] = 'update';

			$strView = $this->load->view('admin/training/register/manage_view', $dataView, TRUE);

			$dataTemplate = [
				"page" => "Capacitación",
				"subpage" => "(Modificar)",
				"is_crud" => TRUE,
				"crud_page" => "update",
				"module" => "training_register",
				"id" => "tr_register_manage",
				"body" => $strView
			];

			$this->parser->parse('templates/application_template', $dataTemplate);
		}
		else {
			show_404();
		}
		
	}

	public function register_insert()
	{
		$post_data = $this->input->post();
		$result = new stdClass;
		try
		{
			if ( ! $this->input->is_ajax_request() )
				throw new Exception("Es recurso no es accesible mediante este metodo", 405);

			if ( empty($post_data) )
				throw new Exception("No hay datos que manipular, la acción será cancelada", 400);

			$this->training_model->data = $post_data;

			$this->training_model->insert_register();

			$result->error = FALSE;
			$result->response_code = 200;
			$result->message = "La Capacitación se registro correctamente.";
		}
		catch (Exception $e) {
			$result->error = TRUE;
			$result->response_code = $e->getCode();
			$result->message = $e->getMessage();
		}

		$this->output
			->set_status_header($result->response_code)
	        ->set_content_type('application/json', 'utf-8')
	        ->set_output(json_encode($result));
	}

	public function register_modify($training_id)
	{
		$post_data = $this->input->post();
		$result = new stdClass;
		try
		{
			if ( ! $this->input->is_ajax_request() )
				throw new Exception("Es recurso no es accesible mediante este metodo", 405);

			if ( empty($post_data) )
				throw new Exception("No hay datos que manipular, la acción será cancelada", 400);

			$this->training_model->data = $post_data;

			$this->training_model->update_register($training_id);

			$result->error = FALSE;
			$result->response_code = 200;
			$result->message = "La Capacitación se modificó correctamente.";
		}
		catch (Exception $e) {
			$result->error = TRUE;
			$result->response_code = $e->getCode();
			$result->message = $e->getMessage();
		}

		$this->output
			->set_status_header($result->response_code)
	        ->set_content_type('application/json', 'utf-8')
	        ->set_output(json_encode($result));
	}

	public function register_delete($training_id)
	{
		$result = new stdClass;

		try
		{
			if ( ! $this->input->is_ajax_request() )
				throw new Exception("Es recurso no es accesible mediante este metodo", 405);

			$this->training_model->delete_register($training_id);

			$result->error = FALSE;
			$result->response_code = 200;
			$result->message = "La Capacitación se eliminó correctamente.";
		}
		catch (Exception $e) {
			$result->error = TRUE;
			$result->response_code = $e->getCode();
			$result->message = $e->getMessage();
		}

		$this->output
			->set_status_header($result->response_code)
	        ->set_content_type('application/json', 'utf-8')
	        ->set_output(json_encode($result));
	}

	public function themes_find($training_ID)
	{
		$result = new stdClass;

		try
		{
			if ( ! $this->input->is_ajax_request() )
				throw new Exception("Es recurso no es accesible mediante este metodo", 405);

			$this->training_model->ID = $training_ID;
			$table = $this->training_model->find_theme_of_training($training_ID);

			$result->error = FALSE;
			$result->response_code = 200;
			$result->data = $table;
		}
		catch(Exception $ex)
		{
			$result->error = TRUE;
			$result->response_code = $e->getCode();
			$result->message = $e->getMessage();
		}

		$this->output
			->set_status_header($result->response_code)
	        ->set_content_type('application/json', 'utf-8')
	        ->set_output(json_encode($result));
	}

	public function programming()
	{
		if ( ! $this->session->has_userdata('filter_prg') ) {
			$this->session->set_userdata('filter_prg', 2);
		}

		$dataView['items'] = $this->training_model->show_programing();

		$strView = $this->load->view('admin/training/program/index_view', $dataView, TRUE);

		$dataTemplate = [
			"page" => "Programación",
			"subpage" => "(Lista General)",
			"is_crud" => FALSE,
			"crud_page" => "index",
			"module" => "training_program",
			"id" => "tr_program_index",
			"body" => $strView
		];

		$this->parser->parse('templates/application_template', $dataTemplate);
	}

	public function programming_create()
	{
		$dataView['coordinator'] = $this->coordinator_model->show();
		$dataView['training'] = $this->training_model->show_register();

		$dataView['module'] = 'create';

		$strView = $this->load->view('admin/training/program/manage_view', $dataView, TRUE);

		$dataTemplate = [
			"page" => "Programación",
			"subpage" => "(Nuevo)",
			"is_crud" => TRUE,
			"crud_page" => "create",
			"module" => "training_program",
			"id" => "tr_program_manage",
			"body" => $strView
		];

		$this->parser->parse('templates/application_template', $dataTemplate);
	}

	public function programming_update($programming_id)
	{
		$this->training_model->ID = $programming_id;
		$dataView['programming'] = $this->training_model->find_programming(TRUE);

		if( ! empty($dataView['programming']) )
		{
			$dataView['themes'] = $this->training_model->get_programming_themes();
			$dataView['coordinator'] = $this->coordinator_model->show();
			$dataView['training'] = $this->training_model->show_register();

			$dataView['module'] = 'update';

			$strView = $this->load->view('admin/training/program/manage_view', $dataView, TRUE);

			$dataTemplate = [
				"page" => "Programación",
				"subpage" => "(Modificar)",
				"is_crud" => TRUE,
				"crud_page" => "update",
				"module" => "training_program",
				"id" => "tr_program_manage",
				"body" => $strView
			];

			$this->parser->parse('templates/application_template', $dataTemplate);
		} 
		else{
			show_404();
		}
		
	}

	public function programming_modify($programming_id)
	{
		$post_data = $this->input->post();
		$result = new stdClass;

		try
		{
			if ( ! $this->input->is_ajax_request() )
				throw new Exception("Es recurso no es accesible mediante este metodo", 405);

			if ( empty($post_data) )
				throw new Exception("No hay datos que manipular, la acción será cancelada", 400);

			$this->training_model->ID = $programming_id;
			$this->training_model->prg_data = $this->training_model->find_programming(TRUE);

			if( empty($this->training_model->prg_data) )
				throw new Exception("La programación ya no existe, es posible que haya sido eliminada", 400);

			$this->training_model->data = $post_data;

			$confirm = $this->training_model->update_programming();

			$msg = $confirm
				? "La programación de la capacitación ha sido modificada correctamente."
				: "Hubo un error al momento de modificar la programación, porfavor vuelve a intentarlo.";

			$result->error = !$confirm;
			$result->response_code = ($confirm) ? 200 : 400;
			$result->message = $msg;
			$result->data = $confirm;
		}
		catch (Exception $e) {
			$result->error = TRUE;
			$result->response_code = $e->getCode();
			$result->message = $e->getMessage();
		}

		$this->output
			->set_status_header($result->response_code)
	        ->set_content_type('application/json', 'utf-8')
	        ->set_output(json_encode($result));
	}

	public function programming_insert()
	{
		$post_data = $this->input->post();
		$result = new stdClass;

		try
		{
			if ( ! $this->input->is_ajax_request() )
				throw new Exception("Es recurso no es accesible mediante este metodo", 405);

			if ( empty($post_data) )
				throw new Exception("No hay datos que manipular, la acción será cancelada", 400);

			$this->training_model->data = $post_data;

			$confirm = $this->training_model->insert_programming();

			$msg = $confirm
				? "La capacitación ha sido programada correctamente."
				: "Hubo un error al momento de generar la programación, porfavor vuelve a intentarlo.";

			$result->error = !$confirm;
			$result->response_code = ($confirm) ? 200 : 400;
			$result->message = $msg;
			$result->data = $confirm;
		}
		catch (Exception $e) {
			$result->error = TRUE;
			$result->response_code = $e->getCode();
			$result->message = $e->getMessage();
		}

		$this->output
			->set_status_header($result->response_code)
	        ->set_content_type('application/json', 'utf-8')
	        ->set_output(json_encode($result));
	}

	public function programming_cancel($programming_id)
	{
		$post_data = $this->input->post();
		$result = new stdClass;

		try
		{
			if ( ! $this->input->is_ajax_request() )
				throw new Exception("Es recurso no es accesible mediante este metodo", 405);

			if ( empty($post_data) )
				throw new Exception("No hay datos que manipular, la acción será cancelada", 400);

			$this->training_model->ID = $programming_id;
			$this->training_model->data = $post_data;

			$confirm = $this->training_model->cancel_programming();

			if ( $post_data['send_mail'] == 1 AND $confirm ) {
				$length_emails_send = $this->training_model->warn_cancel_programming();
			} else {
				$length_emails_send = 0;
			}

			$msg = $confirm
				? "La programación ha sido cancelada correctamente."
				: "Hubo un error al momento de cancelar la programación, porfavor vuelve a intentarlo.";

			if ( $length_emails_send > 0 )
				$msg .= " - " . $length_emails_send . " Mensajes enviados.";
			else if( $length_emails_send == 0 )
				$msg .= " - No se envio ningún mensaje";
			else
				$msg .= " - No hay personas registradas";

			$result->error = !$confirm;
			$result->response_code = ($confirm) ? 200 : 400;
			$result->message = $msg;
			$result->data = $confirm;
		}
		catch (Exception $e) {
			$result->error = TRUE;
			$result->response_code = $e->getCode();
			$result->message = $e->getMessage();
		}

		$this->output
			->set_status_header($result->response_code)
	        ->set_content_type('application/json', 'utf-8')
	        ->set_output(json_encode($result));
	}

	public function programming_delete($programming_id)
	{
		$post_data = $this->input->post();
		$result = new stdClass;

		try
		{
			if ( ! $this->input->is_ajax_request() )
				throw new Exception("Es recurso no es accesible mediante este metodo", 405);

			$this->training_model->ID = $programming_id;

			$res = $this->training_model->delete_programming();

			$result->error = $res->error;
			$result->code = ($res->error) ? 400 : 200;
			$result->message = $res->message;
			$result->data = $res->data;
		}
		catch (Exception $e) {
			$result->error = TRUE;
			$result->code = $e->getCode();
			$result->message = $e->getMessage();
		}

		$this->output
			->set_status_header($result->code)
	        ->set_content_type('application/json', 'utf-8')
	        ->set_output(json_encode($result));
	}

	public function change_filter_programming()
	{
		$post_data = $this->input->post();
		$result = new stdClass;

		try
		{
			if ( ! $this->input->is_ajax_request() )
				throw new Exception("Es recurso no es accesible mediante este metodo", 405);

			if ( empty($post_data) )
				throw new Exception("No hay datos que manipular, la acción será cancelada", 400);

			$number = $post_data['filter_level'];

			if ( ! is_numeric($number) )
				$number = 2;

			if ( ! in_array($number, [1, 2, 3, 4, 5]) )
				$number = 2;

			$this->session->set_userdata('filter_prg', $number);

			$result->error = FALSE;
			$result->response_code = 200;
			$result->message = "Filtro cambiado. Reiniciando...";
			$result->data = $number;
		}
		catch (Exception $e) {
			$result->error = TRUE;
			$result->response_code = $e->getCode();
			$result->message = $e->getMessage();
		}

		$this->output
			->set_status_header($result->response_code)
	        ->set_content_type('application/json', 'utf-8')
	        ->set_output(json_encode($result));
	}

	public function programming_change_schedule($programming_id)
	{
		$post_data = $this->input->post();
		$result = new stdClass;

		try
		{
			if ( ! $this->input->is_ajax_request() )
				throw new Exception("Es recurso no es accesible mediante este metodo", 405);

			if ( empty($post_data) )
				throw new Exception("No hay datos que manipular, la acción será cancelada", 400);

			$this->training_model->data = $post_data;
			$this->training_model->ID = $programming_id;

			$confirm = $this->training_model->programming_change_schedule();

			$msg = ($confirm
						? "Horario ha sido modificado, se ha enviado un email a cada participante inscrito"
						: "No pudo ser modificado, por favor vuelve a intentarlo");

			$result->error = !$confirm;
			$result->code = $confirm ? 200 : 400;
			$result->message = $msg;
			$result->data = $confirm;
		}
		catch (Exception $e) {
			$result->error = TRUE;
			$result->code = $e->getCode();
			$result->message = $e->getMessage();
		}

		$this->output
			->set_status_header($result->code)
	        ->set_content_type('application/json', 'utf-8')
	        ->set_output(json_encode($result));
	}

	public function get_url_programming($programming_id)
	{
		$url = base_url('workshops/' . $programming_id . '/registration' );

		$this->output
			->set_status_header(200)
	        ->set_content_type('application/json', 'utf-8')
	        ->set_output(json_encode($url));
	}
}

/* End of file Training.php */
/* Location: ./application/controllers/Training.php */
