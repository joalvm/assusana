<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Dashboard extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->model('dashboard_model');
	}

	public function index()
	{
		$dataView['counts'] = $this->dashboard_model->get_counts();
		$dataView['assist'] = $this->dashboard_model->get_invitados();
		$infoTraining = $this->dashboard_model->info_training();
		$dataView['proxcap'] = $infoTraining['proxcap'];
		$dataView['culmcap'] = $infoTraining['culmcap'];

		$strView = $this->load->view('admin/dashboard/dashboard_view', $dataView, TRUE);

		$dataTemplate = [
			"page" => "Tablero de control",
			"is_crud" => FALSE,
			"subpage" => "",
			"crud_page" => "index",
			"module" => "dashboard",
			"body" => $strView
		];

		$this->parser->parse('templates/application_template', $dataTemplate);
	}

	public function genders()
	{
		$this->output
			->set_status_header(200)
	        ->set_content_type('application/json', 'utf-8')
	        ->set_output(json_encode($this->dashboard_model->get_genders()));
	}

	public function timeline()
	{
		$this->output
			->set_status_header(200)
	        ->set_content_type('application/json', 'utf-8')
	        ->set_output(json_encode($this->dashboard_model->get_timeline()));
	}

	public function counts($key) 
	{
		$keys = ['invi', 'asis', 'prog', 'real'];
		$result = new stdClass;

		try {

			if ( !$this->input->is_ajax_request() )
				throw new Exception("Es recurso no es accesible mediante este metodo", 405);

			if ( ! in_array($key, $keys) )
				throw new Exception("El valor de busqueda no esta aceptado", 400);

			$info = [];

			switch ($key) 
			{
				case 'invi': $info = $this->dashboard_model->get_allparticipants(); break;
				case 'asis': $info = $this->dashboard_model->get_allAssistents(); break;
			}

			$result->error = FALSE;
			$result->code = 200;
			$result->data = $info;
						
		} catch (Exception $e) {
			$result->error = TRUE;
			$result->message = $e->getMessage();
			$result->code = $e->getCode();
		}

		$this->output
			->set_status_header($result->code)
	        ->set_content_type('application/json', 'utf-8')
	        ->set_output(json_encode($result));
	}

}

/* End of file Dashboard.php */
/* Location: ./application/controllers/Dashboard.php */