<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Certified extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->model('training_model');
		$this->load->model('certified_model');
	}

	public function index()
	{
		$dataView['programming'] = $this->training_model->show_programing();
		$strView = $this->load->view('admin/certified/index_view', $dataView, TRUE);

		$dataTemplate = [
			"page" => "Certificados",
			"is_crud" => FALSE,
			"subpage" => "",
			"crud_page" => "certified",
			"module" => "reports",
			"id" => "certified",
			"body" => $strView
		];

		$this->parser->parse('templates/application_template', $dataTemplate);
	}

	public function generate()
	{
		$post_data = $this->input->post();

		$info = [];

		if( $post_data['type'] == 'one' )
			$info = $this->certified_model->get_info_certified($post_data['inscription']);
		else {
			$info = $this->certified_model->get_info_certified($post_data['programming'], FALSE);
		}

		$data['title'] = "Certificaciones";
		$data['info'] = $info;

		//$this->load->view('admin/reports/certified_view', $data);

		$html = $this->load->view('admin/certified/generate_view', $data, TRUE);

		$this->pdf_create($html, 'certificados');
	}

	private function pdf_create($html, $filename='', $stream=TRUE)
	{
	    include APPPATH . 'third_party/dompdf/dompdf_config.inc.php';

	    $dompdf = new DOMPDF();

	    $dompdf->set_paper('A4', 'landscape');
	    $dompdf->set_option('enable_html5_parser', true);

	    $dompdf->load_html($html);
	    $dompdf->render();
	    
	    if ($stream) {
	        $dompdf->stream($filename.".pdf", array("Attachment" => false));
	    } else {
	        return $dompdf->output();
	    }
	}

}

/* End of file Certified.php */
/* Location: ./application/controllers/Certified.php */