<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Coordinator extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->model('coordinator_model');
	}

	public function index()
	{
		$dataView['items'] = $this->coordinator_model->show();

		$strView = $this->load->view('admin/coordinator/coor_index_view', $dataView, TRUE);

		$dataTemplate = [
			"page" => "Docentes",
			"subpage" => "(Lista General)",
			"is_crud" => FALSE,
			"crud_page" => "index",
			"module" => "coordinator",
			"id" => "coordinatorIndex",
			"body" => $strView
		];

		$this->parser->parse('templates/application_template', $dataTemplate);
	}

	public function find($ID) 
	{
		$result = new stdClass;
		try {
			if ( !$this->input->is_ajax_request() )
				throw new Exception("Es recurso no es accesible mediante este metodo", 405);

		$docente = $this->coordinator_model->find($ID);

		$result->error = empty($docente);
		$result->error_code = (empty($docente) ? 404 : 200);
		$result->message = (empty($docente) ? "Es posible que este docente haya sido eliminado desde otra sesión." : NULL);
		$result->data = empty($docente) ? [] : $docente[0];
		
		} catch (Exception $e) {
			$result->error = TRUE;
			$result->error_code = $e->getCode();
			$result->message = $e->getMessage();
		}

		$this->output
			->set_status_header($result->error_code)
	        ->set_content_type('application/json', 'utf-8')
	        ->set_output(json_encode($result));
	}

	public function create()
	{
		$dataView['module'] = 'create';

		$strView = $this->load->view('admin/coordinator/coor_manage_view', $dataView, TRUE);

		$dataTemplate = [
			"page" => "Docentes",
			"subpage" => "(Nuevo)",
			"is_crud" => TRUE,
			"crud_page" => "create",
			"module" => "coordinator",
			"id" => "coordinatorCreate",
			"body" => $strView
		];

		$this->parser->parse('templates/application_template', $dataTemplate);
	}

	public function update($docente_id)
	{
		$dataView['docente'] = $this->coordinator_model->find($docente_id);
		$dataView['module'] = 'update';

		$strView = $this->load->view('admin/coordinator/coor_manage_view', $dataView, TRUE);

		$dataTemplate = [
			"page" => "Docentes",
			"subpage" => "(Modificar)",
			"is_crud" => TRUE,
			"crud_page" => "update",
			"module" => "coordinator",
			"id" => "coordinatorUpdate",
			"body" => $strView
		];

		$this->parser->parse('templates/application_template', $dataTemplate);
	}

	public function insert()
	{
		$post = $this->input->post();
		$result = new stdClass;

		try {
			if ( !$this->input->is_ajax_request() )
				throw new Exception("Es recurso no es accesible mediante este metodo", 405);

			if ( empty($post) )
				throw new Exception("No hay datos que manipular, la acción será cancelada", 400);

			if ( $this->coordinator_model->exists_dni($post['dni']) )
				throw new Exception("El DNI proporcionado ya se encuentra registrado y es un dato unico", 400);

			$resp = $this->coordinator_model->insert($post);

			if ( $resp > 0 && ((boolean)$post["send_email"]) )
			{
				$this->coordinator_model->sendMail_register([
					"email" => $post["email"],
					"fullname" => $post["name"] . " " . $post["lastname"],
					"username" => $this->coordinator_model->usuario,
					"password" => '123456',
					"datacript" => $this->base64url_encode(json_encode([
						"username" => $this->coordinator_model->usuario,
						"email" => $post["email"],
						"DNI" => $post["dni"],
						"role" => 2,
						"request" => $this->coordinator_model->nrequest,
						"request_id" => $resp
					]))
				]);
			}

			$result->error = FALSE;
			$result->message = "El Coordinador se registro satisfactoriamente";
			$result->error_code = 200;
						
		} catch (Exception $e) {
			$result->error = TRUE;
			$result->message = $e->getMessage();
			$result->error_code = $e->getCode();
		}

		$this->output
			->set_status_header($result->error_code)
	        ->set_content_type('application/json', 'utf-8')
	        ->set_output(json_encode($result));
	}

	public function modify($docente_id)
	{
		$post_data = $this->input->post();
		$result = new stdClass;

		try 
		{
			if ( !$this->input->is_ajax_request() )
				throw new Exception("Es recurso no es accesible mediante este metodo", 405);

			$find_coord = $this->coordinator_model->find($docente_id);

			if ( empty($find_coord) )
				throw new Exception("El usuario ya no esta registrado, es posible que haya sido eliminado", 404);

			$this->coordinator_model->ID = $docente_id;
			$this->coordinator_model->data = $post_data;

			$confirm = $this->coordinator_model->update();

			$result->error = (!$confirm);
			$result->code = (!$confirm) ? 400 : 200;
			$result->message = ($confirm) ? 'Se modificó satisfactoriamente' : 'Error al momento de modificar, vuelva a intentarlo';
			$result->data = $confirm;
		} 
		catch (Exception $ex) 
		{
			$result->error = TRUE;
			$result->code = $ex->getCode();
			$result->message = $ex->getMessage();
		}

		$this->output
			->set_status_header($result->code)
	        ->set_content_type('application/json', 'utf-8')
	        ->set_output(json_encode($result));

	}

	public function delete($ID) 
	{
		$result = new stdClass;

		try {

			if ( !$this->input->is_ajax_request() )
				throw new Exception("Es recurso no es accesible mediante este metodo", 405);

			$find_coord = $this->coordinator_model->find($ID);

			if ( empty($find_coord) )
				throw new Exception("Es posible que este docente haya sido eliminado desde otra sesión.", 400);

			$confirm = $this->coordinator_model->delete($ID);

			$result->error = !$confirm;
			$result->error_code = ((!$confirm) ? 400 : 200);
			$result->message = (($confirm) 
				? "El docente ha sido eliminado satisfactoriamente." 
				: "Hubo un error al intentar eliminar al docente, vuelva a intentarlo."
			);
			$result->data = $confirm ? TRUE : FALSE;
		
		} catch (Exception $e) {
			$result->error = TRUE;
			$result->error_code = $e->getCode();
			$result->message = $e->getMessage();
		}

		$this->output
			->set_status_header($result->error_code)
	        ->set_content_type('application/json', 'utf-8')
	        ->set_output(json_encode($result));
	}

	public function restore($ID)
	{
		$result = new stdClass;
		try {
			if ( !$this->input->is_ajax_request() )
				throw new Exception("Es recurso no es accesible mediante este metodo", 405);

			$this->coordinator_model->data_find = $this->coordinator_model->find($ID);

			if ( empty($this->coordinator_model->data_find) )
				throw new Exception("Es posible que este docente haya sido eliminado desde otra sesión.", 400);

			$confirm = $this->coordinator_model->restore_password($ID);

			$result->error = !$confirm;
			$result->error_code = ((!$confirm) ? 400 : 200);
			$result->message = (($confirm) 
				? "Los accesos para usuario, an sido restaurados datisfactoriamente" 
				: "Hubo un error al intentar modificar los accesos al docente, vuelva a intentarlo."
			);
			$result->data = $confirm ? TRUE : FALSE;
		
		} catch (Exception $e) {
			$result->error = TRUE;
			$result->error_code = $e->getCode();
			$result->message = $e->getMessage();
		}

		$this->output
			->set_status_header($result->error_code)
	        ->set_content_type('application/json', 'utf-8')
	        ->set_output(json_encode($result));
	}

	public function locked($ID, $action)
	{
		$result = new stdClass;
		try {
			if ( !$this->input->is_ajax_request() )
				throw new Exception("Es recurso no es accesible mediante este metodo", 405);

			$this->coordinator_model->data_find = $this->coordinator_model->find($ID);

			if ( empty($this->coordinator_model->data_find) )
				throw new Exception("Es posible que este docente haya sido eliminado desde otra sesión.", 400);

			$confirm = $this->coordinator_model->locked_session($ID, $action);

			$message = (($action == 1) 
				? "Sesión de usuario bloqueda, Cualquier acción que intente apartir de ahora será negada."
				: "Sesión de usuario reestablecida, Cualquier acción que intente apartir de ahora será permitido."
			);

			$result->error = !$confirm;
			$result->error_code = ((!$confirm) ? 400 : 200);
			$result->message = (($confirm) 
				? $message
				: "Hubo un error al intentar modificar los accesos al docente, vuelva a intentarlo."
			);
			$result->data = $confirm ? TRUE : FALSE;
		
		} catch (Exception $e) {
			$result->error = TRUE;
			$result->error_code = $e->getCode();
			$result->message = $e->getMessage();
		}

		$this->output
			->set_status_header($result->error_code)
	        ->set_content_type('application/json', 'utf-8')
	        ->set_output(json_encode($result));
	}

	function base64url_encode($data) { 
		return rtrim(strtr(base64_encode($data), '+/', '-_'), '='); 
	}

}

/* End of file Coordinator.php */
/* Location: ./application/controllers/Coordinator.php */