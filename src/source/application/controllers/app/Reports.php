<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Reports extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->model('training_model');
		$this->load->model('reports_model');
		$this->load->model('docente/attendance_model');

		if ( ! $this->session->has_userdata('filter_prg') )
			$this->session->set_userdata('filter_prg', 2);
	}

	public function attendance()
	{
		$dataView['programming'] = $this->training_model->show_programing();
		$strView = $this->load->view('admin/reports/attendance_view', $dataView, TRUE);

		$dataTemplate = [
			"page" => "Reportes",
			"is_crud" => FALSE,
			"subpage" => " - Asistencias a capacitaciones",
			"crud_page" => "attendance",
			"module" => "reports",
			"id" => "reports",
			"body" => $strView
		];

		$this->parser->parse('templates/application_template', $dataTemplate);
	}

	public function attendance_excel($programming_id)
	{
		$part = $this->attendance_model->get_participants($programming_id);

		$this->training_model->ID = $programming_id;

		$prg = $this->training_model->find_programming(TRUE);
		$tm = $this->training_model->get_programming_themes();

		$this->load->library('excel');
		$this->excel->setActiveSheetIndex(0);
		$asheet = $this->excel->getActiveSheet();
		$asheet->setTitle('ASISTENCIAS');
		PHPExcel_Shared_Font::setAutoSizeMethod(PHPExcel_Shared_Font::AUTOSIZE_METHOD_EXACT);

		$asheet->setCellValue('A1', $prg->training_title);
		$asheet->getStyle('A1')->getFont()->setSize(20);
		$asheet->getStyle('A1')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
		$asheet->mergeCells('A1:J1');

		$asheet->setCellValue('A2', $prg->type_training);
		$asheet->getStyle('A2')->getFont()->setSize(12);
		$asheet->getStyle('A2')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
		$asheet->mergeCells('A2:J2');

		$asheet->setCellValue('A4', "FECHA: " . $prg->date_realization);
		$asheet->mergeCells('A4:E4');
		$asheet->setCellValue('F4', "LUGAR DE REALIZACIÓN: " . $prg->place);
		$asheet->mergeCells('F4:J4');

		$asheet->setCellValue('A5', "HORA DE INICIO: " . $prg->time_start);
		$asheet->mergeCells('A5:E5');
		$asheet->setCellValue('F5', "DIRECCIÓN: " . $prg->address);
		$asheet->mergeCells('F5:J5');

		$asheet->setCellValue('A6', "HORA DE FINALIZACIÓN: " . $prg->time_finish);
		$asheet->mergeCells('A6:E6');

		$asheet->setCellValue('A7', "DOCENTE: " . $tm[0]['subthemes'][0]['docente']);
		$asheet->mergeCells('A7:E8');

		$asheet->setCellValue('F6', "DISTRITO: " . $prg->distrito);
		$asheet->mergeCells('F6:J6');
		$asheet->setCellValue('F7', "PROVINCIA: " . $prg->provincia);
		$asheet->mergeCells('F7:J7');
		$asheet->setCellValue('F8', "DEPARTAMENTO: " . $prg->departamento);
		$asheet->mergeCells('F8:J8');

		$part_header = ['N°', 'NOMBRES', 'GENERO', 'DNI', 'EMAIL', 'ENTIDAD', 'ASISTIO?', 'H. ASISTENCIA', 'NOTA', 'DISPACIDAD?'];

		$part_table = [];

		array_push($part_table, $part_header);

		foreach ($part as $row => $cell) {
			array_push($part_table, [
				($row + 1),
				($cell->last_name . " " . $cell->name),
				(($cell->gender) ? 'Mujer': 'Hombre'),
				$cell->dni,
				$cell->email,
				$cell->entity_name,
				(($cell->attendance) ? 'SI': 'NO'),
				$cell->time_attendance,
				$cell->nota,
				(($cell->dispacity) ? 'SI': 'NO')
			]);
		}

		$asheet->fromArray( $part_table, NULL, 'A12' );
		$asheet->getColumnDimension('A')->setAutoSize(true);
		$asheet->getColumnDimension('B')->setAutoSize(true);
		$asheet->getColumnDimension('C')->setAutoSize(true);
		$asheet->getColumnDimension('D')->setAutoSize(true);
		$asheet->getColumnDimension('E')->setAutoSize(true);
		$asheet->getColumnDimension('F')->setAutoSize(true);
		$asheet->getColumnDimension('G')->setAutoSize(true);
		$asheet->getColumnDimension('H')->setAutoSize(true);
		$asheet->getColumnDimension('I')->setAutoSize(true);
		$asheet->getColumnDimension('J')->setAutoSize(true);

		$title = remove_accents($prg->training_title);
		$date = str_replace('/', '-', $prg->date_realization);

		$filename = 'Reporte  - ' . $date . ' - Capacitacion - ' . substr($title, 0, 50) . '...-.xlsx';

		header('Content-Type: application/vnd.ms-excel'); //mime type
		header('Content-Disposition: attachment;filename="' . $filename . '"'); //tell browser what's the file name
		header('Cache-Control: max-age=0'); //no cache

		$objWriter = PHPExcel_IOFactory::createWriter($this->excel, 'Excel2007');

		$objWriter->save('php://output');
	}

	public function report_cppte() {
		$data = $this->reports_model->get_cant_participants_for_type_entity();

		$date = (new DateTime())->format('d-m-Y');
		$filename = 'Reporte - '.$date.' - Por Tipo de Entidad cantidad de capacitados.xlsx';

		$this->load->library('excel');
		$this->excel->setActiveSheetIndex(0);
		$asheet = $this->excel->getActiveSheet();
		$asheet->setTitle('PARTICIPANTES');

		$asheet->setCellValue('A1', 'CANTIDAD DE PARTICIPANTES POR TIPO DE ENTIDAD');
		$asheet->getStyle('A1')->getFont()->setSize(20);
		$asheet->getStyle('A1')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
		$asheet->mergeCells('A1:C1');

		// CABECERA DE LA TABLA
		$_header = ['N°', 'TIPO DE ENTIDAD', 'C. PARTICIPANTES'];

		$_table = [];

		array_push($_table, $_header);

		foreach ($data as $row => $cell) {
			array_push($_table,[
				($row + 1),
				$cell->name,
				$cell->cant
			]);
		}

		$asheet->fromArray( $_table, NULL, 'A4' );

		$asheet->getColumnDimension('A')->setAutoSize(true);
		$asheet->getColumnDimension('B')->setAutoSize(true);
		$asheet->getColumnDimension('C')->setAutoSize(true);

		header('Content-Type: application/vnd.ms-excel'); //mime type
		header('Content-Disposition: attachment;filename="' . $filename . '"'); //tell browser what's the file name
		header('Cache-Control: max-age=0'); //no cache

		$objWriter = PHPExcel_IOFactory::createWriter($this->excel, 'Excel2007');

		$objWriter->save('php://output');
	}

	public function report_general() 
	{
		$data = $this->reports_model->get_report_general();

		$date = (new DateTime())->format('d-m-Y');
		$filename = 'Reporte General - '.$date.' - Fortalecimiento de capacidades registro de participantes.xlsx';

		$this->load->library('excel');
		$this->excel->setActiveSheetIndex(0);
		$asheet = $this->excel->getActiveSheet();
		$asheet->setTitle('PARTICIPANTES');

		$asheet->setCellValue('A1', 'Fortalecimiento de capacidades registro de participantes');
		$asheet->getStyle('A1')->getFont()->setSize(20);
		$asheet->getStyle('A1')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
		$asheet->mergeCells('A1:N1');

		// CABECERA DE LA TABLA
		$_header = [
			'N°', 
			'F. CAPACITACIÓN', 
			'H. INICIO',
			'H. FINALIZACIÓN', 
			'DEPARTAMENTO',
			'PROVINCIA',
			'DISTRITO',
			'TIPO DE CAPACITACIÓN', 
			'TEMA', 
			'NOMBRE DE CAPACITACIÓN',
			'SEXO',
			'ASISTIO',
			'NOMBRE DE PARTICIPANTES',
			'CARGO'
		];

		$_table = [];
		$count = 5;
		$total = 0;

		array_push($_table, $_header);

		foreach ($data as $row => $cell) {

			array_push($_table,[
				($row + 1),
				$cell->date_realization,
				$cell->time_start,
				$cell->time_finish,
				$cell->departamento,
				$cell->provincia,
				$cell->distrito,
				$cell->type_training,
				$cell->theme,
				$cell->training_name,
				$cell->gender,
				$cell->assistance,
				$cell->names,
				$cell->cargo
			]);

			$total += 1;
		}

		$asheet->fromArray( $_table, NULL, 'A4' );

		$asheet->getStyle('B5:B'.($total + 4)) ->getNumberFormat()->setFormatCode('dd/mm/yyyy');

		$asheet->getColumnDimension('A')->setAutoSize(true);
		$asheet->getColumnDimension('B')->setAutoSize(true);
		$asheet->getColumnDimension('C')->setAutoSize(true);
		$asheet->getColumnDimension('D')->setAutoSize(true);
		$asheet->getColumnDimension('E')->setAutoSize(true);
		$asheet->getColumnDimension('F')->setAutoSize(true);
		$asheet->getColumnDimension('G')->setAutoSize(true);
		$asheet->getColumnDimension('H')->setAutoSize(true);
		$asheet->getColumnDimension('I')->setAutoSize(true);
		$asheet->getColumnDimension('J')->setAutoSize(true);
		$asheet->getColumnDimension('K')->setAutoSize(true);
		$asheet->getColumnDimension('L')->setAutoSize(true);
		$asheet->getColumnDimension('M')->setAutoSize(true);
		$asheet->getColumnDimension('N')->setAutoSize(true);

		header('Content-Type: application/vnd.ms-excel'); //mime type
		header('Content-Disposition: attachment;filename="' . $filename . '"'); //tell browser what's the file name
		header('Cache-Control: max-age=0'); //no cache

		$objWriter = PHPExcel_IOFactory::createWriter($this->excel, 'Excel2007');

		$objWriter->save('php://output');
	}

	public function report_training()
	{
		$data = $this->reports_model->get_report_training();

		$date = (new DateTime())->format('d-m-Y');
		$filename = 'Reporte general de Capacitaciones - '.$date.'.xlsx';

		$this->load->library('excel');
		$this->excel->setActiveSheetIndex(0);
		$asheet = $this->excel->getActiveSheet();
		$asheet->setTitle('CAPACITACIONES');

		$asheet->setCellValue('A1', 'REPORTE GENERAL DE CAPACITACIONES');
		$asheet->getStyle('A1')->getFont()->setSize(20);
		$asheet->getStyle('A1')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
		$asheet->mergeCells('A1:N1');

		// CABECERA DE LA TABLA
		$_header = [
			'N°', /*A*/
			'F. CAPACITACIÓN', /*B*/
			'H. INICIO',/*C*/
			'H. FINALIZACIÓN', /*D*/
			'LUGAR DE REALIZACIÓN',/*E*/
			'DIRECCIÓN', /*F*/
			'DEPARTAMENTO',/*G*/
			'PROVINCIA',/*H*/
			'DISTRITO',/*I*/
			'NOMBRE DE CAPACITACIÓN',/*J*/
			'TIPO DE CAPACITACIÓN', /*K*/
			'COORDINADOR(ES)',/*L*/
			'N° DE INSCRITOS',/*M*/
			'N° DE PARTICIPANTES'/*N*/
		];

		$_table = [];
		$count = 5;
		$total = 0;

		array_push($_table, $_header);

		foreach ($data as $row => $cell) {

			array_push($_table,[
				($row + 1),
				$cell->date_realization,
				$cell->time_start,
				$cell->time_finish,
				$cell->place,
				$cell->address,
				$cell->departamento,
				$cell->provincia,
				$cell->distrito,
				$cell->training_name,
				$cell->type_training,
				$cell->coordinador,
				$cell->inscriptions,
				$cell->attendance
			]);

			$total += 1;
		}

		$asheet->getStyle('B5:B'.($total + 4)) ->getNumberFormat()->setFormatCode('dd/mm/yyyy');

		$asheet->fromArray( $_table, NULL, 'A4' );

		$asheet->getColumnDimension('A')->setAutoSize(true);
		$asheet->getColumnDimension('B')->setAutoSize(true);
		$asheet->getColumnDimension('C')->setAutoSize(true);
		$asheet->getColumnDimension('D')->setAutoSize(true);
		$asheet->getColumnDimension('E')->setAutoSize(true);
		$asheet->getColumnDimension('F')->setAutoSize(true);
		$asheet->getColumnDimension('G')->setAutoSize(true);
		$asheet->getColumnDimension('H')->setAutoSize(true);
		$asheet->getColumnDimension('I')->setAutoSize(true);
		$asheet->getColumnDimension('J')->setAutoSize(true);
		$asheet->getColumnDimension('K')->setAutoSize(true);
		$asheet->getColumnDimension('L')->setAutoSize(true);
		$asheet->getColumnDimension('M')->setAutoSize(true);
		$asheet->getColumnDimension('N')->setAutoSize(true);

		header('Content-Type: application/vnd.ms-excel'); //mime type
		header('Content-Disposition: attachment;filename="' . $filename . '"'); //tell browser what's the file name
		header('Cache-Control: max-age=0'); //no cache

		$objWriter = PHPExcel_IOFactory::createWriter($this->excel, 'Excel2007');

		$objWriter->save('php://output');
	}

}

/* End of file Reports.php */
/* Location: ./application/controllers/Reports.php */
