<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Maintenance extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->model('maintenance_model');
	}

/**
 *	METODOS PARA EL MODULO MANTENIMIENTO::TIPO DE CAPACITACIONES
 */
	public function type_training()
	{
		$dataView['items'] = $this->maintenance_model->list_type_training();

		$strView = $this->load->view('admin/maintenance/type_training_view', $dataView, TRUE);

		$dataTemplate = [
			"page" => "Tipo de capacitación",
			"is_crud" => FALSE,
			"subpage" => "",
			"crud_page" => "index",
			"module" => "type_training",
			"id" => "typeTraining",
			"body" => $strView
		];

		$this->parser->parse('templates/application_template', $dataTemplate);
	}

	public function type_training_find( $ID )
	{
		$result = new stdClass;

		try
		{
			if ( !$this->input->is_ajax_request() )
				throw new Exception("Este recurso no es accesible mediente este metodo", 405);

			$table = $this->maintenance_model->find_type_training($ID);

			$result->error 		= FALSE;
			$result->code 		= 200;
			$result->data 		= (!empty($table)) ? $table[0] : [];
		}
		catch(Exception $ex)
		{
			$result->error 		= TRUE;
			$result->message 	= $ex->getMessage();
			$result->code 		= $ex->getCode();
		}

		$this->output
			->set_status_header($result->code)
	        ->set_content_type('application/json', 'utf-8')
	        ->set_output(json_encode($result));
	}

	public function type_training_create()
	{
		$data = $this->input->post();
		$result = new stdClass;

		try
		{
			if ( !$this->input->is_ajax_request() )
				throw new Exception("Este recurso no es accesible mediente este metodo", 405);

			if ( empty($data) )
				throw new Exception("No hay datos que manipular, la acción será cancelada", 400);

			$id = $this->maintenance_model->insert_type_training($data);

			$result->error 		= FALSE;
			$result->code 		= 200;
			$result->message 	= "Dato agregado satisfactoriamente.";
			$result->data 		= $id;
		}
		catch(Exception $ex)
		{
			$result->error 		= TRUE;
			$result->message 	= $ex->getMessage();
			$result->code 		= $ex->getCode();
		}

		$this->output
			->set_status_header($result->code)
	        ->set_content_type('application/json', 'utf-8')
	        ->set_output(json_encode($result));
	}

	public function type_training_update($ID)
	{
		$data = $this->input->post();
		$result = new stdClass;

		try
		{
			if ( !$this->input->is_ajax_request() )
				throw new Exception("Este recurso no es accesible mediente este metodo", 405);

			if ( empty($data) )
				throw new Exception("No hay datos que manipular, la acción será cancelada", 400);

			// BUSCANDO EL TIPO PARA VALIDAR SU EXISTENCIA
			$findTT = $this->maintenance_model->find_type_training($ID);

			if ( empty($findTT) )
				throw new Exception("El recurso no ha sido encontrado es posible que ya esté eliminado", 404);

			// MODIFICAR LOS DATOS
			$confirm = $this->maintenance_model->update_type_training($data, $ID);

			$result->error 		= !$confirm;
			$result->code 		= ($confirm) ? 200 : 400;
			$result->message 	= ($confirm) ? "Datos Modificados satisfactoriamente." : json_encode($this->db->error());
			$result->data 		= $confirm;
		}
		catch(Exception $ex)
		{
			$result->error 		= TRUE;
			$result->message 	= $ex->getMessage();
			$result->code 		= $ex->getCode();
		}

		$this->output
			->set_status_header($result->code)
	        ->set_content_type('application/json', 'utf-8')
	        ->set_output(json_encode($result));
	}

	public function type_training_delete($ID)
	{
		$result = new stdClass;

		try
		{
			if ( !$this->input->is_ajax_request() )
				throw new Exception("Este recurso no es accesible mediente este metodo", 405);

			// BUSCANDO EL TIPO PARA VALIDAR SU EXISTENCIA
			$findTT = $this->maintenance_model->find_type_training($ID);

			if ( empty($findTT) )
				throw new Exception("El recurso no ha sido encontrado es posible que ya esté eliminado", 404);

			// ELIMINANDO LOS DATOS
			$confirm = $this->maintenance_model->delete_type_training($ID);

			$result->error 		= !$confirm;
			$result->code 		= ($confirm) ? 200 : 400;
			$result->message 	= ($confirm) ? "Dato eliminado satisfactoriamente." : json_encode($this->db->error());
			$result->data 		= $confirm;
		}
		catch(Exception $ex)
		{
			$result->error 		= TRUE;
			$result->message 	= $ex->getMessage();
			$result->code 		= $ex->getCode();
		}

		$this->output
			->set_status_header($result->code)
	        ->set_content_type('application/json', 'utf-8')
	        ->set_output(json_encode($result));
	}
//=================================================================================

	public function themes()
	{
		$dataView['themes'] = $this->maintenance_model->list_themes();

		$strView = $this->load->view('admin/maintenance/themes_view', $dataView, TRUE);

		$dataTemplate = [
			"page" => "Temas & Subtemas",
			"is_crud" => FALSE,
			"subpage" => "",
			"crud_page" => "index",
			"module" => "themes",
			"id" => "themes",
			"body" => $strView
		];

		$this->parser->parse('templates/application_template', $dataTemplate);
	}

	public function themes_find($theme_id)
	{
		$result = new stdClass;

		try
		{
			if ( ! $this->input->is_ajax_request() )
				throw new Exception("Este recurso no es accesible mediente este metodo", 405);

			$req = $this->maintenance_model->find_theme($theme_id);

			$result->error 		= FALSE;
			$result->code 		= 200;
			$result->data 		= $req;
		}
		catch(Exception $ex)
		{
			$result->error 		= TRUE;
			$result->message 	= $ex->getMessage();
			$result->code 		= $ex->getCode();
		}

		$this->output
			->set_status_header($result->code)
	        ->set_content_type('application/json', 'utf-8')
	        ->set_output(json_encode($result));
	}

	public function themes_create()
	{
		$post_data = $this->input->post();
		$result = new stdClass;

		try
		{
			if ( ! $this->input->is_ajax_request() )
				throw new Exception("Este recurso no es accesible mediente este metodo", 405);

			if( empty($post_data) )
				throw new Exception("No hay datos que manipular, la operación será cancelada", 400);

			$exists_theme = $this->maintenance_model->exists_theme($post_data['title']);

			if( $exists_theme > 0 )
				throw new Exception("El nombre del tema ya esta registrado", 400);

			$req = $this->maintenance_model->insert_theme($post_data);

			$result->error 		= $req->error;
			$result->code 		= (!$req->error) ? 200 : 400;
			$result->message 	= $req->message;
			$result->ID 		= $req->ID;
		}
		catch(Exception $ex)
		{
			$result->error 		= TRUE;
			$result->message 	= $ex->getMessage();
			$result->code 		= $ex->getCode();
		}

		$this->output
			->set_status_header($result->code)
	        ->set_content_type('application/json', 'utf-8')
	        ->set_output(json_encode($result));
	}

	public function themes_update($theme_id)
	{
		$post_data = $this->input->post();
		$result = new stdClass;

		try
		{
			if ( !$this->input->is_ajax_request() )
				throw new Exception("Este recurso no es accesible mediente este metodo", 405);

			if( empty($post_data) )
				throw new Exception("No hay datos que manipular, la operación será cancelada", 400);

			$exists_theme = $this->maintenance_model->exists_theme($post_data['title'], $theme_id);
			
			if( $exists_theme > 0 )
				throw new Exception("El nombre del tema ya esta registrado", 400);

			$req = $this->maintenance_model->update_theme($theme_id, $post_data);

			$result->error 		= $req->error;
			$result->code 		= (!$req->error) ? 200 : 400;
			$result->message 	= $req->message;
			$result->data 		= $req->result;
		}
		catch(Exception $ex)
		{
			$result->error 		= TRUE;
			$result->message 	= $ex->getMessage();
			$result->code 		= $ex->getCode();
		}

		$this->output
			->set_status_header($result->code)
	        ->set_content_type('application/json', 'utf-8')
	        ->set_output(json_encode($result));
	}

	public function themes_delete($theme_id)
	{
		$data = $this->input->post();
		$result = new stdClass;

		try
		{
			if ( !$this->input->is_ajax_request() )
				throw new Exception("Este recurso no es accesible mediente este metodo", 405);

			if ( empty($data) )
				throw new Exception("No hay datos que manipular, la acción será cancelada", 400);

			$req = $this->maintenance_model->delete_theme($data['id']);

			$result->error 		= $req->error;
			$result->code 		= (!$req->error) ? 200 : 400;
			$result->message 	= $req->message;
			$result->data 		= $req->result;
		}
		catch(Exception $ex)
		{
			$result->error 		= TRUE;
			$result->message 	= $ex->getMessage();
			$result->code 		= $ex->getCode();
		}

		$this->output
			->set_status_header($result->code)
	        ->set_content_type('application/json', 'utf-8')
	        ->set_output(json_encode($result));
	}

	public function subthemes_create($theme_id)
	{
		$post_data = $this->input->post();
		$result = new stdClass;

		try
		{
			if ( !$this->input->is_ajax_request() )
				throw new Exception("Este recurso no es accesible mediente este metodo", 405);

			if( empty($post_data) )
				throw new Exception("No hay datos que manipular, la operación será cancelada", 400);

			$req = $this->maintenance_model->insert_subtheme($theme_id, $post_data);

			$result->error 		= $req->error;
			$result->code 		= (!$req->error) ? 200 : 400;
			$result->message 	= $req->message;
			$result->ID 		= $req->ID;
		}
		catch(Exception $ex)
		{
			$result->error 		= TRUE;
			$result->message 	= $ex->getMessage();
			$result->code 		= $ex->getCode();
		}

		$this->output
			->set_status_header($result->code)
	        ->set_content_type('application/json', 'utf-8')
	        ->set_output(json_encode($result));
	}

	public function subthemes_update($theme_id, $subtheme_id)
	{
		$post_data = $this->input->post();
		$result = new stdClass;

		try
		{
			if ( !$this->input->is_ajax_request() )
				throw new Exception("Este recurso no es accesible mediente este metodo", 405);

			if( empty($post_data) )
				throw new Exception("No hay datos que manipular, la operación será cancelada", 400);

			$req = $this->maintenance_model->update_subtheme($theme_id, $subtheme_id, $post_data);

			$result->error 		= $req->error;
			$result->code 		= (!$req->error) ? 200 : 400;
			$result->message 	= $req->message;
			$result->data 		= $req->result;
		}
		catch(Exception $ex)
		{
			$result->error 		= TRUE;
			$result->message 	= $ex->getMessage();
			$result->code 		= $ex->getCode();
		}

		$this->output
			->set_status_header($result->code)
	        ->set_content_type('application/json', 'utf-8')
	        ->set_output(json_encode($result));
	}

	public function subthemes_delete($theme_id, $subtheme_id)
	{
		$data = $this->input->post();
		$result = new stdClass;

		try
		{
			if ( !$this->input->is_ajax_request() )
				throw new Exception("Este recurso no es accesible mediente este metodo", 405);

			if ( empty($data) )
				throw new Exception("No hay datos que manipular, la acción será cancelada", 400);

			$req = $this->maintenance_model->delete_subtheme($data['theme'], $data['id']);

			$result->error 		= $req->error;
			$result->code 		= (!$req->error) ? 200 : 400;
			$result->message 	= $req->message;
			$result->data 		= $req->result;
		}
		catch(Exception $ex)
		{
			$result->error 		= TRUE;
			$result->message 	= $ex->getMessage();
			$result->code 		= $ex->getCode();
		}

		$this->output
			->set_status_header($result->code)
	        ->set_content_type('application/json', 'utf-8')
	        ->set_output(json_encode($result));
	}

	public function subthemes_find($theme_id, $subtheme_id)
	{
		$result = new stdClass;

		try
		{
			if ( !$this->input->is_ajax_request() )
				throw new Exception("Este recurso no es accesible mediente este metodo", 405);

			$req = $this->maintenance_model->find_subtheme($subtheme_id);

			$result->error 		= FALSE;
			$result->code 		= 200;
			$result->data 		= $req;
		}
		catch(Exception $ex)
		{
			$result->error 		= TRUE;
			$result->message 	= $ex->getMessage();
			$result->code 		= $ex->getCode();
		}

		$this->output
			->set_status_header($result->code)
	        ->set_content_type('application/json', 'utf-8')
	        ->set_output(json_encode($result));
	}

/**
* 	METODOS PARA EL MODULO MATENIMIENTO::ENTIDADES
*/
	public function entities()
	{
		$dataView['item_menu'] = "hola mundo!";

		$strView = $this->load->view('admin/maintenance/entities_view', $dataView, TRUE);

		$dataTemplate = [
			"page" => "Entidades",
			"is_crud" => FALSE,
			"subpage" => "",
			"crud_page" => "index",
			"module" => "entities",
			"id" => "entities",
			"body" => $strView
		];

		$this->parser->parse('templates/application_template', $dataTemplate);
	}

	public function entities_create()
	{
		$post_data = $this->input->post();

		$result = new stdClass;

		try
		{
			if ( !$this->input->is_ajax_request() )
				throw new Exception("Este recurso no es accesible mediente este metodo", 405);

			$type = $this->maintenance_model->insert_type($post_data);

			if ( $type->error )
				throw new Exception($type->message, 400);

			$subtype = $this->maintenance_model->insert_subtype($post_data, $type->id);

			if ( $subtype->error )
				throw new Exception($subtype->message, 400);

			$entity = $this->maintenance_model->insert_entity($post_data, $subtype->id);

			$result->error 		= $entity->error;
			$result->code 		= (!$entity->error) ? 200 : 400;
			$result->message 	= $entity->message;
		}
		catch(Exception $ex)
		{
			$result->error 		= TRUE;
			$result->message 	= $ex->getMessage();
			$result->code 		= $ex->getCode();
		}

		$this->output
			->set_status_header($result->code)
	        ->set_content_type('application/json', 'utf-8')
	        ->set_output(json_encode($result));
	}
//=================================================================================



}

/* End of file Register.php */
/* Location: ./application/controllers/Register.php */
