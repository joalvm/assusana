<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

require REST_PATH;

class Entidad extends REST_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->model('api/entidad_model');
	}

	public function cascaded_get()
	{
		$result = $this->entidad_model->cascaded();
		$this->response($result, REST_Controller::HTTP_OK);
	}

	public function expanded_get()
	{
		$result = $this->entidad_model->expanded();
		$this->response($result, REST_Controller::HTTP_OK);
	}

}

/* End of file Entidades.php */
/* Location: ./application/controllers/Entidades.php */