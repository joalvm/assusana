<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

require REST_PATH;

class Ubigeo extends REST_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->model('api/ubigeo_model');
	}

	public function index_get()
	{
		$this->response($this->ubigeo_model->Listar(), 200);
	}

}

/* End of file Ubigeo.php */
/* Location: ./application/controllers/Ubigeo.php */