<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

require REST_PATH;

class Participant extends REST_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->model("api/participant_model");
	}

	public function get_data_of_dni_get($dni)
	{
		$result = new stdClass;

		try
		{
			if ( !$this->input->is_ajax_request() )
				throw new Exception("Este recurso no es accesible mediente este metodo", 405);
			
			$this->participant_model->DNI = $dni;
			$participant = $this->participant_model->get_data_from_dni();

			$result->error = FALSE;
			$result->code = (!empty($participant) ? 200 : 404 );
			$result->message = "OK";
			$result->data = (!empty($participant) ? $participant[0] : []);
		}
		catch(Exception $ex)
		{
			$result->error = TRUE;
			$result->code = $ex->getCode();
			$result->message = $ex->getMessage();
			$result->data = [];
		}
		
		$this->response($result, ((!$result->error) ? 200 : $result->code ));
	}
}

/* End of file Participant.php */
/* Location: ./application/controllers/Participant.php */