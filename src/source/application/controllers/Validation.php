<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Validation extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->library('form_validation');
		$this->load->model('validation_model');
	}

	public function docente($strB64 = "")
	{
		try
		{
			// TIENE QUE ENVIARTE UN STRING BASE64 EN LA URL
			if ( strlen($strB64) == 0 )
				throw new Exception("SE NECESITA MAS INFORMACIÓN", 1);

			// DECODIFICAMOS Y LO CONVERTIMOS EN UN ARRAY Y NO EN UN OBJETO
			$b64_data = base64_decode($strB64, TRUE);

			// EN CASO DE QUE LA DECODIFICACION DE LA BASE64 NO HAYA RESULTADO PRODUCTO
			// DE UNA MANIPULACION EN EL STRING
			if ( !$b64_data )
				throw new Exception("TU INFORMACIÓN HA SIDO MANIPULADA", 1);

			$decode_data = json_decode($b64_data, TRUE);

			if ( is_null($decode_data) )
				throw new Exception("TU INFORMACIÓN HA SIDO MANIPULADA", 1);

			// VERIFICAR QUE EL ARRAY EXISTAN LAS CLAVES PERMITIDAS NI UNA MAS NI UNA MENOS
			if ( !array_key_exists('username', $decode_data) OR
				!array_key_exists('email', $decode_data) OR 
				!array_key_exists('DNI', $decode_data) OR 
				!array_key_exists('role', $decode_data) OR 
				!array_key_exists('request', $decode_data) OR 
				!array_key_exists('request_id', $decode_data) )
			{
				throw new Exception("Los datos proporcionados no son los requeridos para procesar la información, Acaso intentas aprovecharte de nuestra nobleza?", 1);
			}

			// VALIDAD QUE TODOS LOS DATOS CUMPLAN LOS REQUISITOS EN UN POSIBLE CASO DE QUE SE INTENTE EENVIAR 
			// DATOS QUE NO CORRESPONDAN
			if ( ! $this->isValidData($decode_data) )
				throw new Exception("Tus datos no cumplen con nuestros parametros, Acaso intentas algo?", 1);

			$data = $this->validation_model->find_docente($decode_data);

			if ( $data["error"] )
			{
				throw new Exception($data["message"], 1);
			}
			else
			{
				$dataView['items'] = $data['data'];

				$strView = $this->load->view('validation/valid_docente_view', $dataView, TRUE);

				$dataTemplate = [
					"body_class" => "valid_docente",
					"body" => $strView
				];

				$this->parser->parse('templates/validation_template', $dataTemplate);
			}
		}
		catch(Exception $ex)
		{
			echo "<h3>"; echo $ex->getMessage(); echo "</h3>";
			echo '<a href="'.base_url('login.html').'">PAGINA CORRECTA!</a>';
		}
		
	}

	public function participante($strB64 = "") 
	{
		try
		{
			// TIENE QUE ENVIARTE UN STRING BASE64 EN LA URL
			if ( strlen($strB64) == 0 )
				throw new Exception("SE NECESITA MAS INFORMACIÓN", 1);

			// DECODIFICAMOS Y LO CONVERTIMOS EN UN ARRAY Y NO EN UN OBJETO
			$b64_data = base64_decode($strB64, TRUE);

			// EN CASO DE QUE LA DECODIFICACION DE LA BASE64 NO HAYA RESULTADO PRODUCTO
			// DE UNA MANIPULACION EN EL STRING
			if ( !$b64_data )
				throw new Exception("TU INFORMACIÓN HA SIDO MANIPULADA", 1);

			$decode_data = json_decode($b64_data, TRUE);

			if ( is_null($decode_data) )
				throw new Exception("TU INFORMACIÓN HA SIDO MANIPULADA", 1);

			// VERIFICAR QUE EL ARRAY EXISTAN LAS CLAVES PERMITIDAS NI UNA MAS NI UNA MENOS
			if ( !array_key_exists('request_number', $decode_data) OR
				!array_key_exists('program', $decode_data) OR 
				!array_key_exists('participante', $decode_data) OR 
				!array_key_exists('inscriptcion', $decode_data) OR 
				!array_key_exists('date_creation', $decode_data))
			{
				throw new Exception("Los datos proporcionados no son los requeridos para procesar la información, Acaso intentas aprovecharte de nuestra nobleza?", 1);
			}

			// VALIDAD QUE TODOS LOS DATOS CUMPLAN LOS REQUISITOS EN UN POSIBLE CASO DE QUE SE INTENTE EENVIAR 
			// DATOS QUE NO CORRESPONDAN
			if ( ! $this->isValidInscription($decode_data) )
				throw new Exception("Tus datos no cumplen con nuestros parametros, Acaso intentas algo?", 1);

			$data = $this->validation_model->find_programming($decode_data);

			if ( $data["error"] )
			{
				throw new Exception($data["message"], 1);
			}
			else
			{
				$dataView['items'] = $data['data'];

				$strView = $this->load->view('validation/valid_participante_view', $dataView, TRUE);

				$dataTemplate = [
					"body_class" => "valid_participant",
					"body" => $strView
				];

				$this->parser->parse('templates/validation_template', $dataTemplate);
			}
		}
		catch(Exception $ex)
		{
			echo "<h3>"; echo $ex->getMessage(); echo "</h3>";
			echo '<a href="'.base_url('login.html').'">PAGINA CORRECTA!</a>';
		}
	}

	public function isValidData($data = [])
	{
		if ( empty($data) )
			return FALSE;

		$this->form_validation->set_data($data);

		$this->form_validation->set_rules('username', 'username', 'trim|required|alpha_numeric');
		$this->form_validation->set_rules('email', 'email', 'trim|required|valid_emails');
		$this->form_validation->set_rules('DNI', 'DNI', 'required|exact_length[8]|numeric');
		$this->form_validation->set_rules('role', 'role', 'required|is_natural_no_zero');
		$this->form_validation->set_rules('request', 'request', 'required|alpha_numeric|regex_match[/^[0-9a-f]{40}$/i]');
		$this->form_validation->set_rules('request_id', 'request_id', 'required|is_natural_no_zero');

		return $this->form_validation->run();
	}

	public function isValidInscription($data = [])
	{
		if ( empty($data) )
			return FALSE;

		$this->form_validation->set_data($data);

		$this->form_validation->set_rules('request_number', 'request_number', 'required|alpha_numeric|regex_match[/^[0-9a-f]{40}$/i]');
		$this->form_validation->set_rules('program', 'program', 'required|is_natural_no_zero');
		$this->form_validation->set_rules('participante', 'participante', 'required|is_natural_no_zero');
		$this->form_validation->set_rules('inscriptcion', 'inscriptcion', 'required|is_natural_no_zero');

		return $this->form_validation->run();
	}

}

/* End of file Validation.php */
/* Location: ./application/controllers/Validation.php */