<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Certified extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->model('docente/attendance_model');
		$this->load->model('reports_model');
	}

	public function index()
	{
		$dataView['programming'] = $this->attendance_model->get_programming();

		$strView = $this->load->view('docente/certified/cert_index_view', $dataView, TRUE);

		$dataTemplate = [
			"page" => "Certificado",
			"is_crud" => FALSE,
			"subpage" => " - Asistentes",
			"crud_page" => "index",
			"module" => "certified",
			"external" => "doc_certified",
			"body" => $strView
		];

		$this->parser->parse('templates/docente_template', $dataTemplate);
	}

	public function create() {
		$post_data = $this->input->post();

		$info = [];

		if( $post_data['type'] == 'one' )
			$info = $this->reports_model->get_info_certified($post_data['inscription']);
		else {
			$info = $this->reports_model->get_info_certified($post_data['programming'], FALSE);
		}

		$data['title'] = "Certificaciones";
		$data['info'] = $info;

		$html = $this->load->view('docente/certified/certified_view', $data, TRUE);

		$this->pdf_create($html, 'certificados');
	}

	private function pdf_create($html, $filename='', $stream=TRUE) 
	{
	    include APPPATH . 'third_party/dompdf/dompdf_config.inc.php';

	    $dompdf = new DOMPDF();

	    $dompdf->set_paper('A4', 'landscape');
	    $dompdf->set_option('enable_html5_parser', true);

	    $dompdf->load_html($html);
	    $dompdf->render();

	    if ($stream) {
	        $dompdf->stream($filename.".pdf", array("Attachment" => false));
	    } else {
	        return $dompdf->output();
	    }
	}

}

/* End of file Certified.php */
/* Location: ./application/controllers/Certified.php */