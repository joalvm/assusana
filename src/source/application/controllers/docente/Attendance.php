<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Attendance extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->model('docente/attendance_model');
		$this->load->model('training_model');
	}

	public function index()
	{
		$dataView['programming'] = $this->attendance_model->get_programming();

		$strView = $this->load->view('docente/attendance/att_index_view', $dataView, TRUE);

		$dataTemplate = [
			"page" => "Asistencias",
			"is_crud" => FALSE,
			"subpage" => "",
			"crud_page" => "index",
			"module" => "attendance",
			"external" => "attendance",
			"body" => $strView
		];

		$this->parser->parse('templates/docente_template', $dataTemplate);
	}

	public function insert($programming_id) {

		$data_post = $this->input->post('data');
		$result = new stdClass;

		try
		{
			if ( !$this->input->is_ajax_request() )
				throw new Exception("Es recurso no es accesible mediante este metodo", 405);

			if ( empty($data_post) )
				throw new Exception("No hay datos que manipular, la acción será cancelada", 400);

			$this->training_model->ID = $programming_id;
			$this->attendance_model->data_program = $this->training_model->find_programming(TRUE);
			$this->attendance_model->insert($data_post, $programming_id);

			$result->error = FALSE;
			$result->code = 200;
			$result->message = "OK";
			$result->data = [];
			
		}
		catch(Exception $ex) 
		{
			$result->error = TRUE;
			$result->message = $ex->getMessage();
			$result->code = $ex->getCode();
		}

		$this->output
			->set_status_header($result->code)
	        ->set_content_type('application/json', 'utf-8')
	        ->set_output(json_encode($result));
	}

	public function find_participant($programming_id)
	{
		$result = new stdClass;

		try 
		{
			if ( ! $this->input->is_ajax_request() )
				throw new Exception("Es recurso no es accesible mediante este metodo", 405);

			$result->error = FALSE;
			$result->code = 200;
			$result->data['participants'] = $this->attendance_model->get_participants($programming_id);

			$this->training_model->ID = $programming_id;
			$result->data['programming'] = $this->training_model->find_programming(TRUE);
			$result->data['themes'] = $this->training_model->get_programming_themes();
		} 
		catch (Exception $e) 
		{
			$result->error = TRUE;
			$result->code = $e->getCode();
			$result->message = $e->getMessage();
		}

		$this->output
			->set_status_header($result->code)
	        ->set_content_type('application/json', 'utf-8')
	        ->set_output(json_encode($result));
	}

}

/* End of file Attendance.php */
/* Location: ./application/controllers/Attendance.php */