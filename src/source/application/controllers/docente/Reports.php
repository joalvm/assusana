<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Reports extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->model('docente/attendance_model');
	}

	public function attendance()
	{
		$dataView['programming'] = $this->attendance_model->get_programming();

		$strView = $this->load->view('docente/reports/rep_attendance_view', $dataView, TRUE);

		$dataTemplate = [
			"page" => "Reportes",
			"is_crud" => FALSE,
			"subpage" => " - Asistentes a capacitación",
			"crud_page" => "attendance",
			"module" => "reports_assist",
			"external" => "doc_reports",
			"body" => $strView
		];

		$this->parser->parse('templates/docente_template', $dataTemplate);
	}

}

/* End of file Reports.php */
/* Location: ./application/controllers/Reports.php */