<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Dashboard extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
	}

	public function index()
	{
		$dataView = [];

		$strView = $this->load->view('docente/dashboard/dashboard_view', $dataView, TRUE);

		$dataTemplate = [
			"page" => "Tablero de control",
			"is_crud" => FALSE,
			"subpage" => "",
			"crud_page" => "index",
			"module" => "dashboard",
			"external" => "doc_dashboard",
			"body" => $strView
		];

		$this->parser->parse('templates/docente_template', $dataTemplate);
	}

}

/* End of file Dashboard.php */
/* Location: ./application/controllers/Dashboard.php */