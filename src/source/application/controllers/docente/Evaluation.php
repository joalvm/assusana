<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Evaluation extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->model('docente/attendance_model');
		$this->load->model('docente/evaluation_model');
		$this->load->model('training_model');
	}

	public function index()
	{
		$dataView['programming'] = $this->attendance_model->get_programming();

		$strView = $this->load->view('docente/evaluation/eval_index_view', $dataView, TRUE);

		$dataTemplate = [
			"page" => "Evaluaciones",
			"is_crud" => FALSE,
			"subpage" => "",
			"crud_page" => "index",
			"module" => "evaluation",
			"external" => "doc_evaluation",
			"body" => $strView
		];

		$this->parser->parse('templates/docente_template', $dataTemplate);
	}

	public function insert($programming_id)
	{
		$post_data = $this->input->post();
		$result = new stdClass;

		try
		{
			if ( ! $this->input->is_ajax_request() )
				throw new Exception("Es recurso no es accesible mediante este metodo", 405);

			if ( empty($post_data) && empty($_FILES) )
				throw new Exception("No hay datos que manipular, la acción será cancelada", 400);

			$this->training_model->ID = $programming_id;
			$programming = $this->training_model->find_programming(TRUE);

			if( empty($programming) )
				throw new Exception("El programa ya no esta registrado, es posible que haya sido eliminado, actualice la página", 400);
			
			$date_arr = explode("-", $programming->date_realization);
			$file_res = FALSE;
			$data_res = FALSE;

			if ( isset($_FILES['examen']) ) 
			{
				$folder = 'evaluaciones/'.$date_arr[0]. '/'.$date_arr[1];
				$info = $this->evaluation_model->save_file('examen', $folder);
				$file_res = $this->evaluation_model->insert_file(
					$programming_id, $info['folder'], 
					$info['data']['file_name'], 
					$info['data']['file_size']
				);
			}

			if ( ! empty( $post_data ) )
			{
				$data_res = $this->evaluation_model->insert_evaluation($post_data);
			}

			$message = "Notas guardadas: " . (($data_res) ? 'OK' : 'NO') . " - ";
			$message .= "Archivo registrado: " . (($file_res) ? 'OK' : 'NO');

			$result->error = FALSE;
			$result->code = 200;
			$result->message = $message;
		}
		catch (Exception $e) {
			$result->error = TRUE;
			$result->code = $e->getCode();
			$result->message = $e->getMessage();
		}

		$this->output
			->set_status_header($result->code)
	        ->set_content_type('application/json', 'utf-8')
	        ->set_output(json_encode($result));
	}

}

/* End of file Evaluation.php */
/* Location: ./application/controllers/Evaluation.php */