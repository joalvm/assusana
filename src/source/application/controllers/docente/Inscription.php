<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Inscription extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->model('docente/attendance_model');
		$this->load->model('training_model');
		$this->load->model('workshops_model');
		$this->load->model('inscription_model');
	}

	public function index()
	{
		$dataView['programming'] = $this->attendance_model->get_programming();

		$strView = $this->load->view('docente/inscription/ins_index_view', $dataView, TRUE);

		$dataTemplate = [
			"page" => "Inscripciones",
			"is_crud" => FALSE,
			"subpage" => "",
			"crud_page" => "index",
			"module" => "inscription",
			"external" => "doc_inscription",
			"body" => $strView
		];

		$this->parser->parse('templates/docente_template', $dataTemplate);
	}

	public function create($programming_id)
	{
		$this->training_model->ID = $programming_id;
		$program = $this->training_model->find_programming(TRUE);

		$dataView['programming'] = $program;

		$strView = $this->load->view('docente/inscription/ins_manage_view', $dataView, TRUE);

		$dataTemplate = [
			"page" => "Inscripciones",
			"is_crud" => TRUE,
			"subpage" => "(Nuevo)",
			"crud_page" => "create",
			"module" => "inscription",
			"external" => "doc_inscription",
			"body" => $strView
		];

		$this->parser->parse('templates/docente_template', $dataTemplate);
	}

	public function insert($programming_id)
	{
		$data_post = $this->input->post();
		$result = new stdClass;

		try
		{
			if ( !$this->input->is_ajax_request() )
				throw new Exception("Es recurso no es accesible mediante este metodo", 405);

			if ( empty($data_post) )
				throw new Exception("No hay datos que manipular, la acción será cancelada", 400);

			$this->workshops_model->programID = $programming_id;
			$this->training_model->ID = $programming_id;
			$this->workshops_model->data_program = $this->training_model->find_programming($programming_id);
			$this->workshops_model->data = $data_post;
			
			if ( $data_post['id'] != 0 ) 
			{
				$this->workshops_model->particiID = $data_post['id'];

				$hasRegister = $this->workshops_model->hasRegister();
				
				if ( $hasRegister )
					throw new Exception("Usted ya esta registrado en este taller", 400);

				$this->workshops_model->modifyParticipant();
			} 
			else 
			{
				$this->workshops_model->insertParticipant();
			}

			if( isset($data_post['attendance']) ) {
				$this->workshops_model->insertInscription(FALSE, TRUE, ((boolean)$data_post['attendance']));
			} else {
				$this->workshops_model->insertInscription(FALSE, TRUE);
			}

			$result->error = FALSE;
			$result->code = 200;
			$result->message = "Participante registrado, reiniciando...";
			$result->data = [];
			
		}
		catch(Exception $ex) 
		{
			$result->error = TRUE;
			$result->message = $ex->getMessage();
			$result->code = $ex->getCode();
		}

		$this->output
			->set_status_header($result->code)
	        ->set_content_type('application/json', 'utf-8')
	        ->set_output(json_encode($result));
	}

	public function programming($programming_id)
	{
		$result = new stdClass;

		try 
		{
			if ( ! $this->input->is_ajax_request() )
				throw new Exception("Es recurso no es accesible mediante este metodo", 405);

			$this->inscription_model->PGID = $programming_id;

			$table = $this->inscription_model->get_inscription_from_programming();

			$result->error = FALSE;
			$result->code = 200;
			$result->data = $table;
			$result->its_canceled = $this->inscription_model->check_ifs_canceled_programming();
			$result->url = base_url("/docente/programming/" . $programming_id . "/inscription.html");
		} 
		catch (Exception $e) {
			$result->error = TRUE;
			$result->code = $e->getCode();
			$result->message = $e->getMessage();
		}

		$this->output
			->set_status_header($result->code)
	        ->set_content_type('application/json', 'utf-8')
	        ->set_output(json_encode($result));
	}

}

/* End of file Inscription.php */
/* Location: ./application/controllers/Inscription.php */