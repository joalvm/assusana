<?php
defined('BASEPATH') OR exit('No direct script access allowed');

$route['default_controller'] = 'login';
$route['404_override'] = '';
$route['translate_uri_dashes'] = TRUE;

$route['login/authenticate']['post'] = "login/authenticate";
$route['logout']['post'] = "login/logout";

$route['workshops/(:num)/registration']['get'] = "workshops/registration/$1";
$route['workshops/(:num)/registration']['post'] = "workshops/inscription/$1";

/////////////////////////////
// RUTAS DEL SISTEMA
////////////////////////////
$route['app/maintenance/training/type']['get'] 				= "app/maintenance/type_training";
$route['app/maintenance/training/type/(:num)/find(\.)([a-zA-Z0-9_-]+)(.*)']['get'] = "app/maintenance/type_training_find/$1/format/$3$4";
$route['app/maintenance/training/type/create']['post'] 		= "app/maintenance/type_training_create";
$route['app/maintenance/training/type/(:num)/update']['post'] 	= "app/maintenance/type_training_update/$1";
$route['app/maintenance/training/type/(:num)/delete']['post'] 	= "app/maintenance/type_training_delete/$1";

$route['app/maintenance/themes/create']['post']             = "app/maintenance/themes_create";
$route['app/maintenance/themes/(:num)/update']['post']      = "app/maintenance/themes_update/$1";
$route['app/maintenance/themes/(:num)/delete']['post']      = "app/maintenance/themes_delete/$1";
$route['app/maintenance/themes/(:num)/find.json']['get']    = "app/maintenance/themes_find/$1/format/json";

$route['app/maintenance/themes/(:num)/subtheme/create']['post'] = "app/maintenance/subthemes_create/$1";
$route['app/maintenance/themes/(:num)/subtheme/(:num)/update']['post'] = "app/maintenance/subthemes_update/$1/$2";
$route['app/maintenance/themes/(:num)/subtheme/(:num)/delete']['post'] = "app/maintenance/subthemes_delete/$1/$2";
$route['app/maintenance/themes/(:num)/subtheme/(:num)/find.json']['get'] = "app/maintenance/subthemes_find/$1/$2/format/json";

$route['app/maintenance/entities/create']['post'] 			= "app/maintenance/entities_create";

$route['app/coordinator/(:num)/find(\.)([a-zA-Z0-9_-]+)(.*)']['get'] = "app/coordinator/find/$1/format/$3$4";
$route['app/coordinator/create']['get'] 				= "app/coordinator/create";
$route['app/coordinator/create']['post'] 				= "app/coordinator/insert";
$route['app/coordinator/(:num)/update']['get'] 			= "app/coordinator/update/$1";
$route['app/coordinator/(:num)/update']['post'] 		= "app/coordinator/modify/$1";
$route['app/coordinator/(:num)/delete']['post'] 		= "app/coordinator/delete/$1";
$route['app/coordinator/(:num)/restore']['post'] 		= "app/coordinator/restore/$1";
$route['app/coordinator/(:num)/locked']['post'] 		= "app/coordinator/locked/$1/1";
$route['app/coordinator/(:num)/unlocked']['post'] 		= "app/coordinator/locked/$1/0";

$route['validation/docente/(:any)']['get'] 				= "validation/docente/$1";

////////////////////////////////////////////
// RUTAS PARA EL MANEJO DE CAPACITACIONES
///////////////////////////////////////////

$route['app/training/register/create']['get'] 			= "app/training/register_create";
$route['app/training/register/create']['post'] 			= "app/training/register_insert";
$route['app/training/register/(:num)/update']['get'] 	= "app/training/register_update/$1";
$route['app/training/register/(:num)/update']['post'] 	= "app/training/register_modify/$1";
$route['app/training/register/(:num)/delete']['post'] 	= "app/training/register_delete/$1";
$route['app/training/(:num)/themes(\.)([a-zA-Z0-9_-]+)(.*)']['get'] = "app/training/themes_find/$1/format/$3$4";

$route['app/training/programming/(:num)/url']['get'] = "app/training/get_url_programming/$1";
$route['app/training/programming/filter']['post'] = "app/training/change_filter_programming";
$route['app/training/programming/create']['get'] = "app/training/programming_create";
$route['app/training/programming/create']['post'] = "app/training/programming_insert";
$route['app/training/programming/(:num)/update']['get'] 	= "app/training/programming_update/$1";
$route['app/training/programming/(:num)/update']['post'] 	= "app/training/programming_modify/$1";
$route['app/training/programming/(:num)/cancel']['post'] = "app/training/programming_cancel/$1";
$route['app/training/programming/(:num)/delete']['post'] = "app/training/programming_delete/$1";
$route['app/training/programming/(:num)/schedule/change']['post'] = "app/training/programming_change_schedule/$1";


$route['app/inscription/programming/(:num)/create']['get'] = "app/inscription/create/$1";
$route['app/inscription/programming/(:num)/create']['post'] = "app/inscription/insert/$1";
$route['app/inscription/participant/(:num)/update']['post'] = "app/inscription/update_info/$1";
$route['app/inscription/(:num)/active']['post'] = "app/inscription/active/$1";
$route['app/inscription/(:num)/delete']['post'] = "app/inscription/delete/$1";
$route['app/inscription/programming/(:num)/participants(\.)([a-zA-Z0-9_-]+)(.*)']['get'] = "app/inscription/find_participant/$1/format/$3$4";

$route['app/reports/attendance/(:num)/excel']['get'] = "app/reports/attendance_excel/$1";

////////////////////////////
// RUTAS DE LOS DOCENTES
////////////////////////////

$route['docente/attendance/programming/(:num)/participants(\.)([a-zA-Z0-9_-]+)(.*)']['get'] = "docente/attendance/find_participant/$1/format/$3$4";
$route['docente/attendance/programming/(:num)/register']['post'] = "docente/attendance/insert/$1";

$route['docente/programming/(:num)/inscription']['get'] = "docente/inscription/create/$1";
$route['docente/programming/(:num)/inscription']['post'] = "docente/inscription/insert/$1";

$route['docente/evaluation/programming/(:num)']['post'] = "docente/evaluation/insert/$1";

////////////////////////////
// RUTAS DE LA API REST
////////////////////////////
$route['api/ubigeo(\.)([a-zA-Z0-9_-]+)(.*)']['get'] = "api/ubigeo/index/format/$2$3";
$route['api/entities/cascaded(\.)([a-zA-Z0-9_-]+)(.*)']['get'] = "api/entidad/cascaded/format/$2$3";
$route['api/entities/expanded(\.)([a-zA-Z0-9_-]+)(.*)']['get'] = "api/entidad/expanded/format/$2$3";
$route['api/participant/(:num)(\.)([a-zA-Z0-9_-]+)(.*)']['get'] = "api/participant/get_data_of_dni/$1/format/$3$4";
// $route['api/example/users/(:num)(\.)([a-zA-Z0-9_-]+)(.*)'] = 'api/example/users/id/$1/format/$3$4';
