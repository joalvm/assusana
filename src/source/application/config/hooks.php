<?php
defined('BASEPATH') or exit('No direct script access allowed');

/*
| -------------------------------------------------------------------------
| Hooks
| -------------------------------------------------------------------------
| This file lets you define "hooks" to extend CI without hacking the core
| files.  Please see the user guide for info:
|
|	https://codeigniter.com/user_guide/general/hooks.html
|
*/
// Validar retorno al login despues de haberse autentificado
$hook['post_controller_constructor'][] = function () {
    $CI 		=& get_instance();

    $segm 		= $CI->uri->segments;
    $rsegm 		= $CI->uri->rsegments;

    $_custom_base_url = (TYPE_SERVER == 'VHOST') ? VIRTUAL_HOST_URL : EXT_DIRECTORY_URL;


    $uriExclude = ["api", "workshops", "validation", "ubigeo", "entidad", "participant"];

    if ((! empty($rsegm)) && (! in_array(strtolower($rsegm[1]), $uriExclude))) {
        if ($CI->session->has_userdata('logged_in')) {
            if ((! $CI->session->logged_in) && strcasecmp($rsegm[1], 'login') !== 0) {
                header("location: ". $_custom_base_url . 'login.html', 'refresh');
            } elseif ($CI->session->logged_in) {
                if (strcasecmp($rsegm[1], 'login') === 0) {
                    // PARA LA ADMINISTRACION GENERAL
                    if ($CI->session->role == 1) {
                        header("location: ". $_custom_base_url . 'app/dashboard.html');
                    } else { // PARA EL SISTEMA DE DOCENTES
                        header("location: ". $_custom_base_url . 'docente/dashboard.html');
                    }
                } else {
                    // PARA LA ADMINISTRACION GENERAL
                    if ($CI->session->role == 1 && strcasecmp($rsegm[1], 'docente') === 0) {
                        header("location: ". $_custom_base_url . 'app/dashboard.html');
                    } elseif ($CI->session->role == 2 && strcasecmp($rsegm[1], 'app') === 0) {// PARA EL SISTEMA DE DOCENTES
                        header("location: ". $_custom_base_url . 'docente/dashboard.html');
                    }
                }
            }
        } else { // En caso que no exista la variable logged_in
            $CI->session->set_userdata(["logged_in" => false]);

            if (strcasecmp($rsegm[1], 'login') !== 0) {
                header("location: ". $_custom_base_url . 'login.html');
            }
        }
    }

    $CI->db->query('SET SESSION sql_mode =
                  REPLACE(REPLACE(REPLACE(
                  @@sql_mode,
                  "ONLY_FULL_GROUP_BY,", ""),
                  ",ONLY_FULL_GROUP_BY", ""),
                  "ONLY_FULL_GROUP_BY", "")');
};
