app.docente = {};

app.docente.certified = (function(_window, $){

	'use strict';

	var opt = $.extend(true, {}, options),
		hlp = $.extend(true, {}, helper),
		data_send = {};

	var mfp_programming = $.extend(true, {}, opt.magnificPopup);

	var items = [],
		timeOutID = 0;

	var $_input_programming = $('input[type=text]#txt_programming'),
		$_list_participant = $('ul#list-participant'),
		$_md_programming = $('section#md-list-programming'),
		$_radio_programming = $_md_programming.find('input[type=radio]'),
		$_form 				= $('form#frm_certificate');

	var _bind = function() {

		_humanize_data();

		$_input_programming.magnificPopup(mfp_programming);

		$_radio_programming.on('change.app.docente.certified.module', _selected_programming);
	},

	_selected_programming = function() {
		var $this = $(this),
			id = $this.val(),
			resource = '/docente/attendance/programming/' + id + '/participants.json';

		$_list_participant.children('li').remove();
		$('#button_new').remove();
		app.progress.show();

		$_input_programming.val($this.data('title'));

		$.getJSON(resource, function(json, textStatus) {
			if ( json ) {
				if ( ! json.error ) {

					json.data.participants.forEach( function(obj, index) {
						if( obj.attendance ) {
							var item = _get_item(obj)
							$_list_participant.append(item);
							item.find('button.btn-options').dropdown(opt.dropdown);
							item.find('button.gen_certified')
								.on('click.app.reports.certified.module', _assign_to_form);
						}
					});

					if( json.data.participants.length == 0 ) {
						$_list_participant.append('<li class="empty-list"><h4>Sin participantes</h4><p>Aun no se han registrado participantes a esta capacitación</p></li>');
					} else {
						$('main').append(_get_button_save(json.data.programming.programming_id));
					}
					_fill_programming_data(json.data.programming, $('#programming-data'));
					_fill_programming_theme(json.data.themes, $('#programming-themes'));
				}
			} else {
				Materialize.toast('Tu sesión a terminado, reiniciando...', 2000);
			}
		}).always(function() {
			app.progress.close();
			$.magnificPopup.close();
		});
	},

	_assign_to_form = function() {
		var $this = $(this);
		
		$_form.find('input').remove();

		if( $this.data('option') == 'all' ) {

			var prog = $('<input />', {
					type:'hidden',
					name:'programming',
					value: $this.data('id')
				}),
				 type = $('<input />', {
					type:'hidden',
					name:'type',
					value: 'all'
				});

			$_form.append([prog, type]);

		} else if( $this.data('option') == 'one' ) {

			var insc = $('<input />', {
					type:'hidden',
					name:'inscription',
					value: $this.data('id')
				})
				type = $('<input />', {
					type:'hidden',
					name:'type',
					value: 'one'
				});

			$_form.append([insc, type]);
		}

		$_form.append($('<input />', {
			type:'hidden',
			name:csrf.name,
			value: csrf.value
		}));

		$_form.submit();
	},

	_get_item = function(DATA) {

		var jli = $('<li />', {class:'collection-item'}),
			jdiv = $('<div />'),
			jname = $('<span />', {
					class:'title',
				}).text((DATA.last_name + ", " + DATA.name)),

			jprof = $('<span />', {class:'title3'}).text(DATA.entity_name),
			jdni = $('<span />', {class:'title2'}).html('<b>DNI: </b>'+ DATA.dni ),
			jemail = $('<span />', {class:'title2'}).html('<b>Email: </b>'+ DATA.email ),
			joptions = $('<button />', {
				class:'btn-options', 
				type:'button',
				'data-activates': 'options_' + DATA.inscription_id
			}).html('<i class="material-icons">&#xE5D4;</i>'),
			joption_cnt = _button_options(DATA.inscription_id);

		jdiv.append([jdni, jemail, jprof]);

		if ( DATA.attendance ) {
			jname.append('<i class="material-icons si">&#xE834;</i>');
			jname.append($('<span />', {
				class:'chip'
			}).html( moment(DATA.time_attendance).format('h:mma')));
		} else {
			jname.append('<i class="material-icons no">&#xE909;</i>');
		}

		jli.append([jname, jdiv, joptions, joption_cnt]);

		return jli;
	},

	_button_options = function(KEY) {
		var jul = $('<ul />', {id:'options_' + KEY, class:'dropdown-content'}),
			jlidelete = $('<li />'),
			jbutton_delete = $('<button />', {
				type:'button', 
				class:'gen_certified',
				'data-option': 'one',
				'data-id': KEY
			}).html('Generar certificado');

		jlidelete.append(jbutton_delete);

		jul.append([jlidelete]);

		return jul;
	},

	_get_button_save = function(KEY) {
		var jdiv_cnt = $('<div />', {
				id:'button_new',
				class:'fixed-action-btn tooltipped',
				'data-delay': 50,
				'data-position':"left",
				'data-tooltip': "Generar Certificados",
				style: "bottom: 45px; right: 24px;"
			}),
			jbutton = $('<button />', {
				type: 'button',
				'data-option': 'all',
				'data-id': KEY,
				class: "gen_certified btn-floating btn-large waves waves-effect"
			}),
			jicon = $('<i />', {class:'material-icons'}).html('&#xE415;');

		$('div#button_new').remove();

		jbutton.on('click.docente.certified.module', _assign_to_form);

		jbutton.append(jicon);
		jdiv_cnt.append(jbutton);

		return jdiv_cnt;
	},

	_fill_programming_data = function(DATA, ELEM) {
		ELEM.find('#text_trining_title').text(DATA.training_title);
		ELEM.find('#text_type_training').text(DATA.type_training);
		ELEM.find('#text_place').text(DATA.place);
		ELEM.find('#text_address').text(DATA.address);
		ELEM.find('#text_departamento').text(DATA.departamento);
		ELEM.find('#text_provincia').text(DATA.provincia);
		ELEM.find('#text_distrito').text(DATA.distrito);
		ELEM.find('#text_date_realization').text(moment(DATA.date_realization).format('ddd DD MMMM YYYY'));
		ELEM.find('#text_hour_start').text(moment(DATA.date_realization + " " + DATA.time_start).format('h:mma'));
		ELEM.find('#text_hour_finish').text(moment(DATA.date_realization + " " + DATA.time_finish).format('h:mma'));
		ELEM.find('#text_max_participant').text((DATA.limit_max == 0) ? "Sin limite" : DATA.limit_max);
		ELEM.find('#text_person_registered').text(DATA.nregisters);
		ELEM.find('#text_cant_asistentes').text(DATA.nassistents);
		ELEM.find('#text_its_personalize').html(DATA.its_customized ? '<i class="material-icons">&#xE876;</i>' : '<i class="material-icons">&#xE14C;</i>');
		ELEM.find('#text_entity_name').text((DATA.entity_name == null) ? "No personalizado" : DATA.entity_name);
		ELEM.fadeIn(400);
	},

	_fill_programming_theme = function(DATA, ELEM) {
		DATA.forEach( function(obj, index) {
			var tr 				= $('<tr />'),
				td_theme 		= $('<td />', {valign:'top'}),
				td_subtheme 	= $('<td />'),
				h_theme_title	= $('<h4 />', {class:'title'}).text(obj.title),
				p_theme_descr 	= $('<p> /', {class:'description'}).text(hlp.nl2br(obj.description));

			td_theme.append([h_theme_title, p_theme_descr]);

			obj.subthemes.forEach( function(obj2, index2) {
				var divisor = $('<div />', {class:'divisor'}),
					h_stheme_title = $('<h5 />', {class:'title2'}).text(obj2.title),
					span_docente = $('<span />', {class:'docente'}).text(obj2.docente),
					p_stheme_descr = $('<p />', {class:'description'}).text(hlp.nl2br(obj2.description));

				divisor.append([h_stheme_title, span_docente, p_stheme_descr]);

				td_subtheme.append([divisor]);
			});

			tr.append([td_theme, td_subtheme]);

			ELEM.find('tbody').append(tr);
			ELEM.fadeIn(400);
		});
	},

	_humanize_data = function() {

		moment.locale("es");

		$('time.humanize').each(function(index, el) {
			var datetime 	= el.getAttributeNode("data-datetime").textContent,
				type 		= el.getAttributeNode("data-type").textContent;

			if ( type == 'date' ) {
				el.textContent = moment(datetime).format('ddd DD MMMM YYYY h:mma');
			} else {
				el.textContent = moment(datetime).format('h:mma');
			}
		});
	};

	return {
		init: _bind
	}

}(window, window.jQuery));

$(function() {
	if( $('html').data('module') == 'index' )
		app.docente.certified.init();
});