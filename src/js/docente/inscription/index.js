app.docente = {};

app.docente.inscription = (function(_window, $){
	'use strict';

	var opt = $.extend(true, {}, options),
		hlp = $.extend(true, {}, helper);

	var mfp_programming = $.extend(true, {}, opt.magnificPopup),
		mfp_delete = $.extend(true, {}, opt.magnificPopup),
		mfp_participant = $.extend(true, {}, opt.magnificPopup);

	var elements_list 	= [],
		elements_list_filter = [],
		entities 		= [],
		timeOutID 		= 0;

	var $_input_programming = $('input[type=text]#txt_programming'),
		$_search_participant = $('input[type=search]#search_participant'),
		$_list_participant = $('ul#list-participant'),

		$_md_programming = $('section#md-list-programming'),
		$_radio_programming = $_md_programming.find('input[type=radio]'),

		$_md_deletion 			= $('section#md-delete-inscription'),
		$_btn_continue_del 		= $_md_deletion.find('button#btn_continue'),
		$_element_to_delete 	= {},

		$_md_participant 		= $('section#md-participant'),

		$_container 	= $(document),
		$_body 			= $(document.body);

	var _bind = function() {

		_humanize_data();

		$_input_programming.magnificPopup(mfp_programming);

		mfp_delete.callbacks.beforeOpen = _beforeOpen_md_delete;
		mfp_delete.callbacks.afterClose = _afterClose_md_delete;

		mfp_participant.callbacks.beforeOpen = _beforeOpen_md_participant;

		$_container.on('scroll.app.docente.inscription.module', _detected_botton);
		$_radio_programming.on('change.app.docente.inscription.module', _selected_programming);
		$_search_participant.on('keyup.app.docente.inscription.module', _filter_participant);
	},

	_selected_programming = function() {
		var $this = $(this),
			id = $this.val(),
			resource = '/docente/inscription/programming/' + id;

		elements_list 	= [];
		elements_list_filter = [];

		$_list_participant.children('li').remove();
		app.progress.show();

		$_input_programming.val($this.data('title'));

		$.getJSON(resource, function(json, textStatus) {
			if ( json ) {
				if ( ! json.error ) {

					json.data.forEach( function(obj, index) {

						var fullname = (obj.participant_name + " " + obj.participant_lastname).toLowerCase(),
							item = _get_item(obj);

						elements_list.push({
							textSearch: (hlp.remove_accent(fullname) + " " + obj.participant_dni),
							elementList: item
						});

						if ( index < 30 ) {
							$_list_participant.append(item);
							item.find('button.btn-options').dropdown(opt.dropdown);
							item.find('button.active-inscription').on('click.app.docente.inscription.module', _active_inscription);
							item.find('button.delete-inscription').magnificPopup(mfp_delete);
						}
					});

					// Copiar la data inicial a la data de filtros
					elements_list_filter = elements_list;

					if( json.data.length == 0 ) {
						$_list_participant.append('<li class="empty-list"><h4>Sin participantes</h4><p>Aun no se han registrado participantes a esta capacitación</p></li>');
					}

					$('main').append(_button_create(json.url));
				}
			}
		}).always(function() {
			app.progress.close();
			$.magnificPopup.close();
		});
	},

	_beforeOpen_md_delete = function() {
		var btn_active = $.magnificPopup.instance.st.el;

		$_btn_continue_del.data({'id':btn_active.data('id')});
		$_element_to_delete = btn_active.parents('li.collection-item');

		$_md_deletion.find('b.participant').text($_element_to_delete.find('.title').text());
		$_md_deletion.find('b.date-inscription').text($_element_to_delete.find('time').text());

		$_btn_continue_del.on('click.app.docente.inscription.module', _delete_inscription);
	},

	_beforeOpen_md_participant = function() {
		var btn_active = $.magnificPopup.instance.st.el,
			resource = '/api/participant/'+btn_active.data('dni')+'.json';

		$_md_participant.find('.fullname').text("");
		$_md_participant.find('.dni').text("");

		$_md_participant.append('<div class="progress"><div class="indeterminate"></div></div>');
		$_md_participant.find('.popup-body').slideUp(250);

		$.getJSON(resource, function(json, textStatus) {
			var item = json.data;
			$_md_participant.find('.fullname').text(item.name + " " + item.lastname);
			$_md_participant.find('.dni').text(item.dni);
			$_md_participant.find('.gender').text((item.gender == 1) ? "Femenino" : "Masculino");
			$_md_participant.find('.profesion').text((item.profesion == null) ? "No Especifico": item.profesion );
			$_md_participant.find('.cargo').text((item.position == null) ? "No Especifico": item.position);
			$_md_participant.find('.departamento').text(item.departamento_name);
			$_md_participant.find('.provincia').text(item.provincia_name);
			$_md_participant.find('.distrito').text(item.distrito_name);
			$_md_participant.find('.celular').text((item.cellphone == null) ? "No Especifico": item.cellphone);
			$_md_participant.find('.telefono').text((item.phone == null) ? "No Especifico": item.phone);
			$_md_participant.find('.anexo').text((item.annex == null) ? "No Especifico": item.annex);
			$_md_participant.find('.email').text(item.email);
			$_md_participant.find('.entidad').text(item.entity_name);
			$_md_participant.find('.subtipo').text(item.subtype_entity_name);
			$_md_participant.find('.tipo').text(item.type_entity_name);
			$_md_participant.find('.discapacidad').text(item.dispacity ? "SI" : "NO");
			$_md_participant.find('.detalle_discapacidad').text(hlp.nl2br(item.details_dispacity));
			$_md_participant.find('.creacion').text(moment(item.date_created).format('ddd DD MMMM YYYY h:mma'));

		}).always(function(){
			$_md_participant.find('.progress').remove();
			$_md_participant.find('.popup-body').slideDown(250);
		});
	},

	_afterClose_md_delete = function() {
		$_btn_continue_del.data({'id':0});
		$_btn_continue_del.off('click.app.docente.inscription.module');
	},

	_delete_inscription = function() {
		var $this = $(this),
			id = $this.data('id'),
			resource = '/app/inscription/' + id + '/delete';

		var data_request = {};
		data_request[csrf.name] = csrf.value;

		$this.prop({disabled:true});

		$.post(resource, data_request, function(data, textStatus, xhr) {
			if ( data )
			{
				if ( ! data.error ) {
					Materialize.toast(data.message, 3000);

					$_element_to_delete.slideUp(300, function(){

						$_element_to_delete.remove();

						if ( $_list_participant.children('li').length == 0 ) {
							$_list_participant.append('<li class="empty-list"><h4>Sin participantes</h4></li>');
						}
					});

					$this.prop({disabled:false});
				} else {
					Materialize.toast(data.message, 3000);
					$this.prop({disabled:false});
				}
			} else {
				Materialize.toast('Tu sessión ha terminado, Reiniciando...', 4000, '', function(){
					location.reload(true);
				});
			}

			app.progress.close();

		}).fail(function(objResponse, textStatus, responseText){
			
			if ( objResponse.readyState == 4 ) {

				if ( typeof objResponse.responseJSON != 'undefined' ) {
					Materialize.toast(objResponse.responseJSON.message, 3000);
				} else {
					Materialize.toast('Ha ocurrido un error en el servidor, vuelve a intentarlo', 3000);
				}

				$this.prop({disabled:false});

			} else {
				Materialize.toast('Ha ocurrido un error en el servidor, intentalo mas tarde', 4000, '', function(){
					location.reload(true);
				});
			}
		}).always(function() {
			app.progress.close();
			$.magnificPopup.close();
		});
	},

	_active_inscription = function() {
		var $this 			= $(this),
			parent 			= $this.parents('li.collection-item'),
			id 				= $this.data('id'),
			resource 		= '/app/inscription/'+id+'/active',
			data_request 	= {};

		$this.prop({disabled:true});
		app.progress.show();

		data_request[csrf.name] = csrf.value;

		$.post(resource, data_request, function(data, textStatus, xhr) {
			if ( data )
			{
				if ( ! data.error ) {
					Materialize.toast(data.message, 2500);

					parent.find('.icons-status')
						.removeClass('amber darken-3')
						.addClass('green accent-4');

					$this.parent('li').remove();
				} else {
					Materialize.toast(data.message, 3000);
					$this.prop({disabled:false});
				}
			} else {
				Materialize.toast('Tu sessión ha terminado, Reiniciando...', 4000, '', function(){
					location.reload(true);
				});
			}

			app.progress.close();
		}).fail(function(objResponse, textStatus, responseText){
			
			if ( objResponse.readyState == 4 ) {

				if ( typeof objResponse.responseJSON != 'undefined' ) {
					Materialize.toast(objResponse.responseJSON.message, 3000);
				} else {
					Materialize.toast('Ha ocurrido un error en el servidor, vuelve a intentarlo', 3000);
				}

				$this.prop({disabled:false});

			} else {
				Materialize.toast('Ha ocurrido un error en el servidor, intentalo mas tarde', 4000, '', function(){
					location.reload(true);
				});
			}
		}).always(function() {
			app.progress.close();
		});
	},

	_filter_participant = function() {
		var texto = this.value;

		if ( timeOutID != 0) clearTimeout(timeOutID);

		elements_list_filter = [];
		$_list_participant.children('li').remove();

		timeOutID = _window.setTimeout(function() {

			var count = 0;

			elements_list.forEach(function(obj, index) {

				if ( obj.textSearch.indexOf(texto) != -1 ) {
					
					elements_list_filter.push(obj);

					if ( count < 30 ) {
						$_list_participant.append(obj.elementList);
						obj.elementList.find('button.btn-options').dropdown(opt.dropdown);
						obj.elementList.find('button.active-inscription').on('click.app.docente.inscription.module', _active_inscription);
						obj.elementList.find('button.delete-inscription').magnificPopup(mfp_delete);
						obj.elementList.find('.title').magnificPopup(mfp_participant);
					}

					++count;
				}

			})
			
		}, 300);
	},

	_get_item = function(DATA) {
		var fullname = DATA.participant_lastname + " " + DATA.participant_name,
			date_real = moment(DATA.date_inscription).format('ddd DD MMMM YYYY h:mma');

		var jli = $('<li />', {
				class: 'collection-item',
				id: 'item_' + DATA.inscription_id,
				'data-id': DATA.inscription_id,
				'data-search': fullname + " " + DATA.participant_dni
			}),
			jtitle = $('<span />', {
				class:'title',
				'data-dni': DATA.participant_dni,
				'data-mfp-src':'#md-participant'
			}).text(fullname),
			jprofe = $('<span />', {class:'profesion title2'}).text(DATA.profesion),
			jdni = $('<span />', {class:'dni title2'}).html('<b>DNI: </b>' + DATA.participant_dni),
			jemails = $('<span />', {class:'email title2'}).html('<b>Correo Electronico: </b>' + DATA.participant_emails),
			jdate = $('<span />').html('<b>Fecha de inscripción: </b><time>' + date_real+'</time>'),
			joptions = $('<button />', {
				class:'btn-options', 
				type:'button',
				'data-activates': 'options_' + DATA.inscription_id
			}).html('<i class="material-icons">&#xE5D4;</i>'),
			joption_cnt = _button_options(DATA.inscription_id, DATA.email_confirmed),
			jicons_status = $('<span />', {
				class:'icons-status ' + ((DATA.email_confirmed == 1) ? 'green accent-4' : 'amber darken-3')
			});

		if( DATA.dispacity )
			jtitle.append('<i class="material-icons dispacity" title="Presenta discapacidad">&#xE914;</i>');

		jli.append([joptions, joption_cnt, jicons_status, jtitle, jprofe, jdni, jemails, jdate]);

		jtitle.magnificPopup(mfp_participant);

		return jli;
	},

	_button_options = function(KEY, ACTIVATED) {
		var jul = $('<ul />', {id:'options_' + KEY, class:'dropdown-content'}),
			jliactive = $('<li />'),
			jbutton_active = $('<button />', {
				type:'button', 
				class:'active-inscription',
				'data-id': KEY
			}).html('<i class="material-icons">&#xE877;</i>Activar'),
			jlidelete = $('<li />'),
			jbutton_delete = $('<button />', {
				type:'button', 
				class:'delete-inscription',
				'data-mfp-src':"#md-delete-inscription",
				'data-id': KEY
			}).html('<i class="material-icons">&#xE872;</i>Eliminar');

		jbutton_delete.magnificPopup(mfp_delete);
		jbutton_active.on('click.app.docente.inscription', _active_inscription);

		jliactive.append(jbutton_active);
		jlidelete.append(jbutton_delete);

		if ( ACTIVATED == 0 ) {
			
			jul.append([jliactive, jlidelete]);
		} else 
			jul.append([jlidelete]);

		return jul;
	},

	_button_create = function(URL) {
		var jdiv_cnt = $('<div />', {
				id:'button_new',
				class:'fixed-action-btn tooltipped',
				'data-delay': 50,
				'data-position':"left",
				'data-tooltip': "Programar nueva capacitación",
				style: "bottom: 45px; right: 24px;"
			}),
			jlink = $('<a />', {
				href: URL,
				class: "btn-floating btn-large waves waves-effect"
			}),
			jicon = $('<i />', {class:'material-icons'}).html('&#xE145;');

		$('div#button_new').remove();

		jlink.append(jicon);
		jdiv_cnt.append(jlink);

		return jdiv_cnt;
	},

	// Scroll infinito, muestra mas datos cuando se aproxima al final
	_detected_botton = function(e) {

		if ( $_body.scrollTop() > (($_body.height() * 40) / 100) ) {

			var lengthItems = $_list_participant.children('li').length;
			
			if( lengthItems < elements_list.length ) {
				for( var i = lengthItems; i < (lengthItems + 30); i++ ) {
					if( typeof(elements_list_filter[i]) != 'undefined' ) {

						var item = elements_list_filter[i].elementList;
						$_list_participant.append(item);
						item.find('button.btn-options').dropdown(opt.dropdown);
						item.find('button.active-inscription').on('click.app.docente.inscription.module', _active_inscription);
						item.find('button.delete-inscription').magnificPopup(mfp_delete);
						item.find('.title').magnificPopup(mfp_participant);
					}
				}
			}
		}
	},

	_humanize_data = function() {

		moment.locale("es");

		$('time.humanize').each(function(index, el) {
			var datetime 	= el.getAttributeNode("data-datetime").textContent,
				type 		= el.getAttributeNode("data-type").textContent;

			if ( type == 'date' ) {
				el.textContent = moment(datetime).format('ddd DD MMMM YYYY h:mma');
			} else {
				el.textContent = moment(datetime).format('h:mma');
			}
		});
	};

	return {
		init: _bind
	}

}(window, window.jQuery));

$(function() {
	if( $('html').data('module') == 'index' )
		app.docente.inscription.init();
});