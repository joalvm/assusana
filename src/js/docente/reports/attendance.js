app.docente = {};
app.docente.report = {};

app.docente.report.attendance = (function(_window, $){

	'use strict';

	var opt = $.extend(true, {}, options),
		hlp = $.extend(true, {}, helper),
		data_send = {};

	var mfp_programming = $.extend(true, {}, opt.magnificPopup);

	var items = [],
		timeOutID = 0;

	var $_input_programming = $('input[type=text]#txt_programming'),
		$_list_participant = $('ul#list-participant'),
		$_md_programming = $('section#md-list-programming'),
		$_radio_programming = $_md_programming.find('input[type=radio]');

	var _bind = function() {

		_humanize_data();

		$_input_programming.magnificPopup(mfp_programming);

		$_radio_programming.on('change.app.docente.attendance.module', _selected_programming);
	},

	_selected_programming = function() {
		var $this = $(this),
			id = $this.val(),
			resource = '/docente/attendance/programming/' + id + '/participants.json';

		$_list_participant.children('li').remove();
		$('#button_new').remove();
		app.progress.show();

		$_input_programming.val($this.data('title'));

		$.getJSON(resource, function(json, textStatus) {
			if ( json ) {
				if ( ! json.error ) {

					json.data.participants.forEach( function(obj, index) {
						var item = _get_item(obj)
						$_list_participant.append(item);
					});

					if( json.data.participants.length == 0 ) {
						$_list_participant.append('<li class="empty-list"><h4>Sin participantes</h4><p>Aun no se han registrado participantes a esta capacitación</p></li>');
					} else {
						$('main').append(_get_button_save());
					}
					_fill_programming_data(json.data.programming, $('#programming-data'));
				}
			} else {
				Materialize.toast('Tu sesión a terminado, reiniciando...', 2000);
			}
		}).always(function() {
			app.progress.close();
			$.magnificPopup.close();
		});
	},

	_get_item = function(DATA) {

		var jli = $('<li />', {class:'collection-item'}),
			jdiv = $('<div />'),
			jname = $('<span />', {
					class:'title',
				}).text((DATA.last_name + ", " + DATA.name)),

			jprof = $('<span />', {class:'title3'}).text(DATA.entity_name),
			jdni = $('<span />', {class:'title2'}).html('<b>DNI: </b>'+ DATA.dni ),
			jemail = $('<span />', {class:'title2'}).html('<b>Email: </b>'+ DATA.email ),
			jrubic = $('<div />', {class:'rubic'}).text('Firma')

		jdiv.append([jdni, jemail, jprof]);

		if ( DATA.attendance ) {
			jname.append('<i class="material-icons si">&#xE834;</i>');
			jname.append($('<span />', {
				class:'chip'
			}).html( moment(DATA.time_attendance).format('DD MMMM YYYY h:mma')));
			jdiv.append(jrubic);
		} else {
			jname.append('<i class="material-icons no">&#xE909;</i>');
		}

		jli.append([jname, jdiv]);

		return jli;
	},

	_get_button_save = function() {
		var jdiv_cnt = $('<div />', {
				id:'button_new',
				class:'fixed-action-btn tooltipped',
				'data-delay': 50,
				'data-position':"left",
				'data-tooltip': "Programar nueva capacitación",
				style: "bottom: 45px; right: 24px;"
			}),
			jbutton = $('<button />', {
				type: 'button',
				class: "btn-floating btn-large waves waves-effect"
			}),
			jicon = $('<i />', {class:'material-icons'}).html('&#xE8AD;');

		$('div#button_new').remove();

		jbutton.on('click.app.docente.attendance.module', function(){
			_window.print();
		});

		jbutton.append(jicon);
		jdiv_cnt.append(jbutton);

		return jdiv_cnt;
	},

	_fill_programming_data = function(DATA, ELEM) {
		ELEM.find('#text_trining_title').text(DATA.training_title);
		ELEM.find('#text_type_training').text(DATA.type_training);
		ELEM.find('#text_place').text(DATA.place);
		ELEM.find('#text_address').text(DATA.address);
		ELEM.find('#text_departamento').text(DATA.departamento);
		ELEM.find('#text_provincia').text(DATA.provincia);
		ELEM.find('#text_distrito').text(DATA.distrito);
		ELEM.find('#text_date_realization').text(moment(DATA.date_realization).format('ddd DD MMMM YYYY'));
		ELEM.find('#text_hour_start').text(moment(DATA.date_realization + " " + DATA.time_start).format('h:mma'));
		ELEM.find('#text_hour_finish').text(moment(DATA.date_realization + " " + DATA.time_finish).format('h:mma'));
		ELEM.find('#text_max_participant').text((DATA.limit_max == 0) ? "Sin limite" : DATA.limit_max);
		ELEM.find('#text_person_registered').text(DATA.nregisters);
		ELEM.find('#text_its_personalize').html(DATA.its_customized ? '<i class="material-icons">&#xE876;</i>' : '<i class="material-icons">&#xE14C;</i>');
		ELEM.find('#text_entity_name').text((DATA.entity_name == null) ? "No personalizado" : DATA.entity_name);

		ELEM.fadeIn(400);
	},

	_humanize_data = function() {

		moment.locale("es");

		$('time.humanize').each(function(index, el) {
			var datetime 	= el.getAttributeNode("data-datetime").textContent,
				type 		= el.getAttributeNode("data-type").textContent;

			if ( type == 'date' ) {
				el.textContent = moment(datetime).format('ddd DD MMMM YYYY h:mma');
			} else {
				el.textContent = moment(datetime).format('h:mma');
			}
		});
	};

	return {
		init: _bind
	}

}(window, window.jQuery));

$(function() {
	if( $('html').data('module') == 'attendance' )
		app.docente.report.attendance.init();
});