app.docente = {};

app.docente.attendance = (function(_window, $){
	'use strict';

	var opt = $.extend(true, {}, options),
		hlp = $.extend(true, {}, helper),
		data_send = {};

	var mfp_programming = $.extend(true, {}, opt.magnificPopup);

	var items = [],
		timeOutID = 0;

	var $_input_programming = $('input[type=text]#txt_programming'),
		$_search_participant = $('input[type=search]#search_participant'),
		$_list_participant = $('ul#list-participant'),
		$_md_programming = $('section#md-list-programming'),
		$_checked_all 	= $('input[type=checkbox]#chk_all'),
		$_radio_programming = $_md_programming.find('input[type=radio]');

	var _bind = function() {
		_humanize_data();
		$_input_programming.magnificPopup(mfp_programming);

		$_search_participant.on('keyup.app.docente.attendance.module', _search_in_list);
		$_radio_programming.on('change.app.docente.attendance.module', _selected_programming);
		$_checked_all.on('change.app.docente.attendance.module', _checked_all_participant);
	},

	_selected_programming = function() {
		var $this = $(this),
			id = $this.val(),
			resource = '/docente/attendance/programming/' + id + '/participants.json';

		$_list_participant.children('li').remove();
		$('#button_new').remove();
		app.progress.show();

		$_input_programming.val($this.data('title'));

		$.getJSON(resource, function(json, textStatus) {
			if ( json ) {
				if ( ! json.error ) {
					json.data.participants.forEach( function(obj, index) {
						$_list_participant.append(_get_item(obj));
					});

					if( json.data.participants.length == 0 ) {
						$_list_participant.append('<li class="empty-list"><h4>Sin participantes</h4><p>Aun no se han registrado participantes a esta capacitación</p></li>');
					} else {
						$('main').append(_get_button_save(parseInt($this.val())));
					}

					_fill_programming_data(json.data.programming, $('ul#programming-data'));
				}
			} else {
				Materialize.toast('Tu sesión a terminado, reiniciando...', 2000);
			}
		}).always(function() {
			app.progress.close();
			$.magnificPopup.close();
		});
	},

	_checked_all_participant = function() {
		
		var check = false;

		if ( $_checked_all.is(':checked') ) {
			check = true;
		} else {
			check = false;
		}

		$.each(items, function(index, el) {
			$(el).prop({checked:check}).change();
		});
	},

	_search_in_list = function() {
		var texto = this.value;

		if ( timeOutID != 0) clearTimeout(timeOutID);

		timeOutID = _window.setTimeout(function() {
			$.each( $_list_participant.children('li'), function(index, elem) {
				var newStr = hlp.remove_accent(elem.getAttributeNode('data-search').value);
				
				if ( (newStr.toLowerCase()).indexOf(texto) != -1 ) {
					$(elem).fadeIn(200);
				} else {
					$(elem).fadeOut(200);
				}
			})
		}, 300);
	},

	_checked_participant = function() {
		var $this = $(this);

		if ( $this.is(':checked') ) {
			data_send[$this.attr('id')] = {
				'insc': parseInt($this.val()),
				'time': $this.data('time'),
				'assi': 1
			};
		} else {
			data_send[$this.attr('id')] = {
				'insc': parseInt($this.val()),
				'time': '',
				'assi': 0
			};;
		}
	},

	_save_attendance = function() {
		var $this = $(this),
			id = $this.data('programming'),
			data_request = {'data': data_send},
			resource = "/docente/attendance/programming/"+id+"/register";

		data_request[csrf.name] = csrf.value;

		app.progress.show();
		$this.prop({disabled:true});

		$.post(resource, data_request, function(data, textStatus, xhr) {

			if ( data )
			{
				if ( ! data.error ) {
					Materialize.toast(data.message, 1500, '', function(){
						location.reload(true);
					});
				} else {
					Materialize.toast(data.message, 3000);
					$this.prop({disabled:false});
				}
			} else {
				Materialize.toast('Tu sessión ha terminado, Reiniciando...', 4000, '', function(){
					location.reload(true);
				});
			}

			app.progress.close();

		}).fail(function(objResponse, textStatus, responseText){
		
			if ( objResponse.readyState == 4 ) {

				if ( typeof objResponse.responseJSON != 'undefined' ) {
					Materialize.toast(objResponse.responseJSON.message, 3000);
				} else {
					Materialize.toast('Ha ocurrido un error en el servidor, vuelve a intentarlo', 3000);
				}

				$this.prop({disabled:false});

			} else {
				Materialize.toast('Ha ocurrido un error en el servidor, intentalo mas tarde', 4000, '', function(){
					location.reload(true);
				});
			}
		}).always(function() {
			app.progress.close();
		});
	},

	_get_item = function(DATA) {
		var searh = DATA.last_name + " " + DATA.name + " " + DATA.dni;

		var jli = $('<li />', {class:'collection-item', 'data-id':DATA.inscription_id, 'data-search': searh }),
			jp = $('<p />'),
			jcheck = $('<input />', {
					type:'checkbox', 
					class:'filled-in', 
					name:'participant',
					id: 'part_' + DATA.participant_id,
					'data-time': DATA.time_attendance,
					'data-pos': -1,
					'data-id':DATA.participant_id
				}).val(DATA.inscription_id),

			jlabel = $('<label />', {for:'part_' + DATA.participant_id}),

			jname = $('<span />', {
					class:'title', 
					title:(DATA.last_name + ", " + DATA.name)
				}).text((DATA.last_name + ", " + DATA.name)),

			jprof = $('<span />', {class:'title3'}).text(DATA.entity_name),
			jdni = $('<span />', {class:'title2'}).html('<b>DNI: </b>'+ DATA.dni );

		jcheck.on('change.app.docente.attendance.module', _checked_participant);

		if ( DATA.attendance ) {
			jcheck.prop({checked:true});
		}

		if ( DATA.time_attendance != null ) {

			var jtime = $('<span />', {
				class:'time-assi',
				title: 'Hora de ingreso/registro'
			}).html(moment(DATA.time_attendance).format('h:mma'));

			jli.append(jtime);
		}

		items.push(jcheck);

		if( DATA.dispacity )
			jdni.append('<i class="material-icons dispacity" title="Presenta discapacidad">&#xE914;</i>');

		jlabel.append([jname, jdni, jprof]);

		jp.append([jcheck, jlabel]);

		jli.append([jp]);

		return jli;
	},

	_get_button_save = function(ID) {
		var jdiv_cnt = $('<div />', {
				id:'button_new',
				class:'fixed-action-btn tooltipped',
				'data-delay': 50,
				'data-position':"left",
				'data-tooltip': "Programar nueva capacitación",
				style: "bottom: 45px; right: 24px;"
			}),
			jbutton = $('<button />', {
				type: 'button',
				'data-programming': ID,
				class: "btn-floating btn-large waves waves-effect"
			}),
			jicon = $('<i />', {class:'material-icons'}).html('&#xE161;');

		$('div#button_new').remove();

		jbutton.on('click.app.docente.attendance.module', _save_attendance);

		jbutton.append(jicon);
		jdiv_cnt.append(jbutton);

		return jdiv_cnt;
	},

	_fill_programming_data = function(DATA, ELEM) {
		ELEM.find('#text_type_training').text(DATA.type_training);
		ELEM.find('#text_place').text(DATA.place);
		ELEM.find('#text_address').text(DATA.address);
		ELEM.find('#text_departamento').text(DATA.departamento);
		ELEM.find('#text_provincia').text(DATA.provincia);
		ELEM.find('#text_distrito').text(DATA.distrito);
		ELEM.find('#text_date_realization').text(moment(DATA.date_realization).format('ddd DD MMMM YYYY'));
		ELEM.find('#text_hour_start').text(moment(DATA.date_realization + " " + DATA.time_start).format('h:mma'));
		ELEM.find('#text_hour_finish').text(moment(DATA.date_realization + " " + DATA.time_finish).format('h:mma'));
		ELEM.find('#text_max_participant').text((DATA.limit_max == 0) ? "Sin limite" : DATA.limit_max);
		ELEM.find('#text_person_registered').text(DATA.nregisters);
		ELEM.find('#text_its_personalize').html(DATA.its_customized ? '<i class="material-icons">&#xE876;</i>' : '<i class="material-icons">&#xE14C;</i>');
		ELEM.find('#text_entity_name').text(DATA.entity_name);

		ELEM.fadeIn(400);
	},

	_humanize_data = function() {

		moment.locale("es");

		$('time.humanize').each(function(index, el) {
			var datetime 	= el.getAttributeNode("data-datetime").textContent,
				type 		= el.getAttributeNode("data-type").textContent;

			if ( type == 'date' ) {
				el.textContent = moment(datetime).format('ddd DD MMMM YYYY h:mma');
			} else {
				el.textContent = moment(datetime).format('h:mma');
			}
		});
	};

	return {
		init: _bind
	} 

}(window, window.jQuery));

$(function() {
	if ( $('html').data('module') == 'index' )
		app.docente.attendance.init();
});