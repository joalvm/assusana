app.docente = {};

app.docente.evaluation = (function(_window, $){
	'use strict';

	var opt = $.extend(true, {}, options),
		hlp = $.extend(true, {}, helper),
		data_send = {};

	var mfp_programming = $.extend(true, {}, opt.magnificPopup);

	var items = [],
		timeOutID = 0;

	var $_input_programming = $('input[type=text]#txt_programming'),
		$_input_file 		= $('input[type=file]#file_examen'),
		$_input_name_file 	= $('input[type=text]#txt_namefile'),
		$_search_participant = $('input[type=search]#search_participant'),
		$_list_participant 	= $('ul#list-participant'),
		$_md_programming 	= $('section#md-list-programming'),
		$_checked_all 		= $('input[type=checkbox]#chk_all'),
		$_radio_programming = $_md_programming.find('input[type=radio]'),
		$_file_saved 		= $('a#file_saved');

	var _bind = function() {

		_humanize_data();

		$_input_programming.magnificPopup(mfp_programming);

		$_search_participant.on('keyup.app.docente.attendance.module, change.app.docente.attendance.module', _search_in_list);
		$_radio_programming.on('change.app.docente.evaluation.module', _selected_programming);
		$_input_file.on('change.app.docente.evaluation.module', _catch_file);

	},

	_catch_file = function(e){
		var $this 	= $(this),
			name 	= $this.attr('name'),
			file 	= e.target.files; 

		if( $.isEmptyObject(file) || 
			( file[0].type !== 'application/vnd.openxmlformats-officedocument.wordprocessingml.document' && 
				file[0].type !== 'application/pdf' && 
				file[0].type !== 'application/msword' ) ) {
			Materialize.toast('Archivo no permitido, solo pdf o word', 2000);
			return false;
		}

		var size_file = (Math.round( ((file[0].size / 1024) / 1024) * 100 ) / 100).toFixed(1);

		if ( size_file > 6 ) {
			Materialize.toast('El tamaño del archivo a excedido los 6MB', 2000);
			return false;
		}

		data_send[name] = file[0];

		delete data_send.remove_image;
	},

	_save_attendance = function() {
		var $this = $(this),
			id = $this.data('programming'),
			data_request = {'data': data_send},
			resource = "/docente/evaluation/programming/" + id;

		var frm_data = new FormData();

		frm_data.append(csrf.name, csrf.value);
		
		$.each(data_send, function(key, val) {
			if (typeof(val) == 'object' && key != 'examen') {
				frm_data.append(val.insc, val.nota);
			} else {
				frm_data.append(key, val);
			}
		});

		app.progress.show();
		$this.prop({disabled:true});

		$.ajax({
			url: resource,
			type: 'POST',
			dataType: 'json',
			data: frm_data,
			processData: false,
			contentType: false
		}).done(function(data){
			if ( data )
			{
				if ( ! data.error ) {
					Materialize.toast(data.message, 1500, '', function(){
						location.reload(true);
					});
				} else {
					Materialize.toast(data.message, 3000);
					$this.prop({disabled:false});
				}
			} else {
				Materialize.toast('Tu sessión ha terminado, Reiniciando...', 4000, '', function(){
					location.reload(true);
				});
			}

		}).fail(function(objResponse, textStatus, responseText) {
			if ( objResponse.readyState == 4 ) {

				if ( typeof objResponse.responseJSON != 'undefined' ) {
					Materialize.toast(objResponse.responseJSON.message, 3000);
				} else {
					Materialize.toast('Ha ocurrido un error en el servidor, vuelve a intentarlo', 3000);
				}

				$this.prop({disabled:false});

			} else {
				Materialize.toast('Ha ocurrido un error en el servidor, intentalo mas tarde', 4000, '', function(){
					location.reload(true);
				});
			}
		}).always(function() {
			app.progress.close();
		});
	},

	_selected_programming = function() {
		var $this = $(this),
			id = $this.val(),
			resource = '/docente/attendance/programming/' + id + '/participants.json';

		$_list_participant.children('li').remove();
		$('#button_new').remove();
		app.progress.show();

		$_input_programming.val($this.data('title'));

		$.getJSON(resource, function(json, textStatus) {

			if ( json ) {
				if ( ! json.error ) {

					var program = json.data.programming,
						partici = json.data.participants;

					partici.forEach( function(obj, index) {
						$_list_participant.append(_get_item(obj));
					});

					if( partici.length == 0 ) {
						$_list_participant.append('<li class="empty-list"><h4>Sin participantes</h4><p>Aun no se han registrado participantes a esta capacitación</p></li>');
						$_input_file.prop({disabled:true}).parent('.btn').addClass('disabled');
						$_search_participant.prop({disabled:true});
					} else {
						$('main').append(_get_button_save(parseInt($this.val())));
						$_input_file.prop({disabled:false}).parent('.btn').removeClass('disabled');
						$_search_participant.prop({disabled:false});
					}

					if( program.file_name != null ) {

						$_file_saved.attr({href: (program.folder + "/" + program.file_name) });
						$_input_name_file.val(program.folder + "/" + program.file_name);
						$_file_saved.removeClass('hide');
						$_file_saved.children('.size').text((program.file_size / 1024).toFixed(2) + "MB");

						var ext = program.file_name.split(".");

						if( ext[1] == 'pdf' ) {
							$_file_saved.children('.fa')
								.removeClass('fa-file-word-o').addClass('fa-file-pdf-o');
							$_file_saved.removeClass('word').addClass('pdf');
						} else {
							$_file_saved.children('.fa')
								.removeClass('fa-file-pdf-o').addClass('fa-file-word-o');
							$_file_saved.removeClass('pdf').addClass('word');
						}
					} else {
						$_file_saved.addClass('hide');
					}
				}
			} else {
				Materialize.toast('Tu sesión a terminado, reiniciando...', 2000);
			}
		}).always(function() {
			app.progress.close();
			$.magnificPopup.close();
		});
	},

	_assign_record = function() {
		var $this = $(this),
			value = $.trim($this.val()),
			record = '';

		if ( value.length > 0 ) {
			
			if ( $.isNumeric(value) ) {
				if( parseInt(value) > 20 ) {
					record = 20;
				} else if(parseInt(value) < 0) {
					record = 0;
				} else {
					record = value;
				}
			} else {
				record = '';
			}

			$this.val(record);

		}else {
			$this.val('');
		}

		if( record != '' ) {
			data_send[$this.attr('id')] = {
				'insc': parseInt($this.data('id')),
				'nota': parseInt(record)
			};
		} else {
			data_send[$this.attr('id')] = {
				'insc': parseInt($this.data('id')),
				'nota': ''
			};
		}
	},

	_search_in_list = function() {
		var texto = this.value.toLowerCase();

		if ( timeOutID != 0) clearTimeout(timeOutID);

		timeOutID = _window.setTimeout(function() {

			$.each( $_list_participant.children('li'), function(index, elem) {

				var newStr = hlp.remove_accent(elem.getAttributeNode('data-search').value);
				
				if ( (newStr.toLowerCase()).indexOf(texto) != -1 ) {
					$(elem).fadeIn(200);
				} else {
					$(elem).fadeOut(200);
				}
			})

		}, 300);
	},

	_get_item = function(DATA) {
		var searh = DATA.last_name + " " + DATA.name + " " + DATA.dni;

		var jli = $('<li />', {class:'collection-item', 'data-id':DATA.inscription_id, 'data-search': searh }),
			jdiv = $('<div />'),
			jinput = $('<input />', {
					type:'text', 
					placeholder:'NOTA',
					maxlength: 2, 
					id: 'part_' + DATA.participant_id,
					'data-id':DATA.inscription_id
				}),
			jname = $('<span />', {
					class:'title', 
					title:(DATA.last_name + ", " + DATA.name)
				}).text((DATA.last_name + ", " + DATA.name)),

			jprof = $('<span />', {class:'title3'}).text(DATA.entity_name),
			jdni = $('<span />', {class:'title2'}).html('<b>DNI: </b>'+ DATA.dni );

		jdiv.append([jdni, jprof]);

		if ( DATA.attendance ) {
			jname.append('<i class="material-icons si">&#xE834;</i>');
			jname.append($('<span />', {class:'time-assist'}).text(moment(DATA.time_attendance).format('h:mma')));

			jinput.on('keydown.app.docente.evaluation.module', hlp.keyup_Numeric);
			jinput.on('change.app.docente.evaluation.module', _assign_record);

			if( DATA.nota != null ) {
				jinput.val(DATA.nota);
			}

			jdiv.append(jinput);

		} else {
			jname.append('<i class="material-icons no">&#xE909;</i>');
		}

		jli.append([jname, jdiv]);

		return jli;
	},

	_get_button_save = function(ID) {
		var jdiv_cnt = $('<div />', {
				id:'button_new',
				class:'fixed-action-btn tooltipped',
				'data-delay': 50,
				'data-position':"left",
				'data-tooltip': "Programar nueva capacitación",
				style: "bottom: 45px; right: 24px;"
			}),
			jbutton = $('<button />', {
				type: 'button',
				'data-programming': ID,
				class: "btn-floating btn-large waves waves-effect"
			}),
			jicon = $('<i />', {class:'material-icons'}).html('&#xE161;');

		$('div#button_new').remove();

		jbutton.on('click.app.docente.attendance.module', _save_attendance);

		jbutton.append(jicon);
		jdiv_cnt.append(jbutton);

		return jdiv_cnt;
	},

	_humanize_data = function() {

		moment.locale("es");

		$('time.humanize').each(function(index, el) {
			var datetime 	= el.getAttributeNode("data-datetime").textContent,
				type 		= el.getAttributeNode("data-type").textContent;

			if ( type == 'date' ) {
				el.textContent = moment(datetime).format('ddd DD MMMM YYYY h:mma');
			} else {
				el.textContent = moment(datetime).format('h:mma');
			}
		});
	};

	return {
		init: _bind
	}

}(window, window.jQuery));

$(function() {
	
	if( $('html').data('module') == 'index' ) {
		app.docente.evaluation.init();
	}

});

$(window).load(function() {
	$('header').append('<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.6.1/css/font-awesome.min.css">');
});