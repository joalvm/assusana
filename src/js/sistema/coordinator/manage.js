app.coordinator.manage = ( function( _window, $ ) {

	'use strict';

	var config = {
			url : {
				create : "/app/coordinator/create",
				update : "/app/coordinator/{ID}/update"
			}
		},

		data_default = {
			"name" : "",
			"lastname" : "",
			"gender" : -1,
			"dni" : "",
			"email" : "",
			"profession" : "",
			"entity" : 0,
			"send_email" : 0
		},

		data_send = {};

	var entities_data 					= [],
			entities_filter 				= [],
			module 									= $('html').data('module'),
			opt 										= $.extend(true, {}, options),
			hlp 										= $.extend(true, {}, helper),
			timeOutID 							= 0;

	var $_form 									= $('form#frm_manage'),
			$_btn_submit 						= $('button#btn-submitdata'),
			$_modal_entities 				= $('section#popup-list-entities'),
			$_txt_dni 							= $_form.find('input#txt_dni'),
			$_txt_only_text 				= $_form.find('input.only-text'),
			$_txt_entities 					= $_form.find('input#txt_entity_display'),
			$_cnt_entities_search 	= $_form.find('.method_search'),
			$_modal_list_entities 	= $_modal_entities.find('ul#list-entities'),
			$_searcher 							= $_modal_entities.find('input[type=search]'),
			$_btn_close_modal 			= $_modal_entities.find('#btn-cancel'),
			$_item_counts 					= $_modal_entities.find('.items-count'),

			ID 											= $_form.data('id'),

			$dataChanges 						= $_form.find('.datalist'),
			$_item_checked 					= {};

	var _bind = function() {

		$_txt_dni.on('keydown.app.coordinator.manage.module', hlp.keyup_Numeric);

		opt.magnificPopup.callbacks.beforeOpen = _beforeOpen_modal;
		opt.magnificPopup.focus = "input[type=search]";
		opt.magnificPopup.modal = true;
		$_txt_entities.magnificPopup(opt.magnificPopup);

		$_searcher.on('keyup.app.coordinator.manage.module', _filter_entities);
		$_btn_close_modal.on('click.app.coordinator.manage.module', _close_modal_entities);

		$dataChanges.on('change.app.coordinator.manage.module', _watch_changes_data);

		$_form.on('submit.app.coordinate.manage.module', _submit_form);
		$_modal_list_entities.on('scroll.app.coordinate.manage.module', _detected_botton);

		hlp.humanize_DateTime();

	},

 	_init_update = function( e ) {

 		$dataChanges.change();

 		data_send.entity = parseInt($_txt_entities.data('id'));
 		
 		var url = (config.url[module]).replace(/{ID}/gi, ID);
 		
 		$_form.attr({action:url});

 	},

/*
| ENVIANDO LA INFORMACIÓN
 */

	_submit_form = function( e ) {

		var resource = config.url[module];

		data_send = $.extend({}, data_default, data_send);

		if ( ! _valid_data_before_send() ) {
			e.preventDefault();
			return false;
		}

		if( module == 'update' ) resource = resource.replace(/{ID}/gi, ID);

		$_btn_submit.prop({ disabled: true });

		$.post(resource, data_send, function(response, textStatus, xhr) {

			if ( response ) {

				if ( ! response.error ) {

					Materialize.toast( response.message, 2500, '', function() {

						if( module == 'update' ) 
							location.href = app.base_url + 'app/coordinator.html';
						else
							location.reload(true);

					});

				} else {

					Materialize.toast(response.message, 2500);
					$_btn_submit.prop({ disabled: false });

				}

			} else {

				Materialize.toast("Tu sesión a caducado, se reiniciara el sistema", 2000, '', function() {
					location.reload(true);
				});

			}

		}).fail(function( objResponse, textStatus, responseText ) {

			if ( objResponse.readyState == 4 ) {
				$_btn_submit.prop({ disabled: false });
			}

		});

		e.preventDefault();

	},

	_watch_changes_data = function () {

		var $this = $(this),
				name 	= $this.attr('name'),
				type 	= $this.attr('type'),
				tag 	= $this.prop("tagName").toLowerCase();

		if ( tag == 'input' && type != 'checkbox' ) {

			switch (type) {
				case 'text': 

					data_send[name] = $.trim($this.val()); 

					break;
				case 'email':

					if ( $this.val().length > 0 ) {

						if ( ! _check_emails($this.val()) ) {

							$this.removeClass('valid').addClass('invalid');
							data_send[name] = "";

						} else {

							$this.removeClass('invalid').addClass('valid');
							data_send[name] = $.trim($this.val()).replace(/, /gi, ',');

						}

					} else {

						$this.removeClass('valid invalid');
						data_send[name] = "";

					}
					break;
			}

		} else if ( tag == 'select' ) {

			data_send[name] = parseInt($this.children('option:selected').val());

		} else if ( tag == 'input' && type == "checkbox") {

			data_send[name] = $this.is(':checked') ? 1 : 0;

		}

	},

	_check_emails = function( EMAILS ) {

		var emails_arr 		= $.trim(EMAILS).split(','),
				errors_counts = 0;

		if ( emails_arr.length > 0 ) {

			emails_arr.forEach( function( element, index ) {

				if ( $.trim(element).length > 0 ) {

					if ( ! hlp.isValidEmail($.trim(element)) ) 
						errors_counts += 1;

				} else {

					errors_counts += 1;

				}

			});

		} else {

			errors_counts += 1;

		}

		return (errors_counts == 0);

	},

	_valid_data_before_send = function() {

		var error_arr = [];

		$.each(data_send, function( key, value ) {

			var elem = $_form.find('input[name="' + key + '"]');

			switch (key) {
				case 'name':
				case 'lastname':

					if ( $.trim(value).length == 0 ) {
						error_arr.push({
							elem: elem,
							message: "Complete este campo requerido"
						});
					}

					if( ! hlp.is_alpha(value) ) {
						error_arr.push({
							elem: elem,
							message: "Este campo solo tolera carácteres alfabeticos."
						});
					}

					break;
				case 'dni':

					if ( $.trim(value).length != 8 ) {
						error_arr.push({
							elem: elem,
							message: "Este campo acepta un dato exacto de 8 digitos numericos."
						});
					}

					if ( ! $.isNumeric($.trim(value)) ) {
						error_arr.push({
							elem: elem,
							message: "Todos los digitos de este dato tienen que ser numericos."
						});
					}

					break;
				case 'gender': 

					if ( value == (-1) ) {
						error_arr.push({
							elem: elem,
							message: "Seleccione un elemento de esta lista."
						});
					}

					break;
				case 'email': 

					if ( $.trim(value).length == 0 ) {
						error_arr.push({
							elem: elem,
							message: "El email es obligatorio."
						});
					}

					break;
				case 'entity':

					if ( value == 0 ) {
						error_arr.push({
							elem: elem,
							message: "No puedes continuar mientras no hayas elegido una entidad."
						});
					}

					break;
			}

		});

		if ( error_arr.length > 0 ) {

			Materialize.toast( error_arr[0].message, 2500 );
			error_arr[0].elem.focus();

			return false;

		}

		return true;

	},

//

	_beforeOpen_modal = function() {

		var btn_active 		= $.magnificPopup.instance.st.el,
			id_selected 	= btn_active.data('id'),
			resource 		= '/api/entities/expanded.json';

		if ( entities_filter.length > 0 ) return false;

		entities_data 		= [];
		entities_filter 	= [];

		$_modal_list_entities.children('li').remove();

		$.getJSON(resource, function(json, textStatus) {

			json.entities.forEach( function( obj, index ) {

				var stype 		= json.subtype_entities[obj.stype_ps],
						type 			= json.type_entities[stype.type_ps],
						litem 		= _get_item_list(),
						strSearch = (obj.entity + " " + type.name + " " + stype.name).toLowerCase();

				litem.attr({ 'data-id' : obj.id });

				litem.find('input[type=radio]').attr({
					id: "entity" + obj.id,
					'data-id' : "entity" + obj.id,
					'data-entity' : obj.entity,
					'data-type' : type.name,
					'data-subtype' : stype.name
				}).val(obj.id);

				if( obj.id == id_selected )
					litem.find('input[type=radio]').prop({checked:true});

				litem.find('label').attr({ 
					for:('entity' + obj.id) 
				});

				litem.find('span.entity').attr({
					title: obj.entity
				}).html(obj.entity);

				litem.find('span.subtype').html('Subtipo : ' + stype.name);
				litem.find('span.type').html('Tipo &nbsp;&nbsp;&nbsp; : ' + type.name);
				
				entities_data.push({
					strSearch : hlp.remove_accent(strSearch),
					element : litem
				});

				if ( index < 50 ) {
					litem.find('input[type=radio]').on('change.app.coordinator.manage.module', _choise_item);
					$_modal_list_entities.append(litem);
				}

			});

			// Copiar la data inicial a la data de filtros
			entities_filter = entities_data;

			$_item_counts.find('.curr').text(50);
			$_item_counts.find('.total').text(entities_data.length);

			if ( entities_filter.length > 0 ) 
				$_modal_entities.find('button#btn-cancel').prop({disabled:false});

		});

	},

	_choise_item = function() {

		var $this 				= $(this),
				entity 				= $this.data('entity'),
				type_entity 	= $this.data('type'),
				stype_entity 	= $this.data('subtype'),
				id 						= $this.val();

		$_item_checked = $this;

		$_txt_entities
			.attr({title: entity})
			.val(entity);

		$_form
			.find('input#txt_type_entity').val(type_entity)
				.siblings('label').addClass('active');

		$_form
			.find('input#txt_subtype_entity').val(stype_entity)
				.siblings('label').addClass('active');

		data_send.entity = parseInt(id);

		$.magnificPopup.close();

	},

	_filter_entities = function() {

		var texto = hlp.remove_accent( this.value.toLowerCase() );

		if ( timeOutID != 0 ) _window.clearTimeout(timeOutID);

		entities_filter = [];

		$_modal_list_entities.children('li').remove();

		timeOutID = _window.setTimeout(function() {

			var count = 0;

			entities_data.forEach(function(obj, index) {

				if ( obj.strSearch.indexOf(texto) != -1 ) {
					
					entities_filter.push(obj);

					if ( count < 50 ) {

						obj.element
							.find('input[type=radio]').on('change.app.coordinator.manage.module', _choise_item);

						$_modal_list_entities.append(obj.element);

					}

					++count;
				}

			})

			$_item_counts.find('.curr').text($_modal_list_entities.children('li').length);
			$_item_counts.find('.total').text(entities_filter.length);
			
		}, 300);

	},

	_get_item_list = function() {

		return $('<li class="collection-item" data-search="" data-id="">' +
							'<p>' +
								'<input class="with-gap" type="radio" id="" name="entities" data-name="" value="">' +
								'<label for="">' +
									'<span class="title entity truncate" title=""></span>' +
									'<span class="title2 type"></span>' +
									'<span class="title3 subtype"></span>' +
								'</label>' +
							'</p>' +
						'</li>');

	},

	_close_modal_entities = function() { $.magnificPopup.close(); },


	_detected_botton = function( e ) {

		if ( parseInt($_modal_list_entities.scrollTop() + $_modal_list_entities.height()) > 
			parseInt($_modal_list_entities[0].scrollHeight -  $_modal_list_entities.height()) ) {

			var lengthItems = $_modal_list_entities.children('li').length;

			for ( var i = lengthItems; i < (lengthItems + 50); i++ ) {

				var obj = entities_filter[i];
				
				if ( typeof(obj) != 'undefined' ) {

					obj.element.find('input[type=radio]').on('change.app.coordinator.manage.module', _choise_item);
					$_modal_list_entities.append(obj.element);

				}

			}

			$_item_counts.find('.curr').text($_modal_list_entities.children('li').length);

		}

	};



	return {
		create : _bind,
		update : function() {
			_bind();
			_init_update();
		}
	}

}( window, window.jQuery ) );

// DOCUMENT READY
$(function() {

	if( $('html').data('module') == 'create' )
		app.coordinator.manage.create();
	else if( $('html').data('module') == 'update' )
		app.coordinator.manage.update();

});