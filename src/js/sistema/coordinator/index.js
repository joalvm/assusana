app.coordinator = ( function( _window, $ ) {

	'use strict';

	var opt 			= $.extend(true, {}, options),
		hlp 				= $.extend(true, {}, helper),
		timeOutID 	= 0,
		config 			= {
			url : {
				create : "/app/coordinator",
				find : "/app/coordinator/{ID}/find.json",
				delete : "/app/coordinator/{ID}/delete",
				restore: "/app/coordinator/{ID}/restore",
				locked: "/app/coordinator/{ID}/{ACTION}"
			}
		};

	var $_modal_del 					= $("section#popup-delete"),
		$_modal_fullinfo 				= $("section#modal_fulldata"),
		$_modal_restore 				= $("section#popup-renove-pass"),
		$_form_del 							= $_modal_del.find('form'),
		$_form_restore 					= $_modal_restore.find('form'),
		$_btn_close_fullinfo 		= $_modal_fullinfo.find('button#closeModal'),
		$_btn_delete 						= $("button.btn-delete"),
		$_btn_reset_pass 				= $("button.btn-renove-pass:not(:disabled)"),
		$_btn_locked 						= $("button.btn-locked"),
		$_list_main 						= $('ul#list-coordinator'),
		$_open_fullinfo 				= $_list_main.find('a.title'),
		$_search								= $('input[type=search]#search');

	var $_li_current 					= {};

	var _bind = function() {

		var opt_del = $.extend(true, {}, opt.magnificPopup);
		opt_del.callbacks.beforeOpen = _beforeOpen_delete;
		opt_del.callbacks.afterClose = _afterClose_delete;
		$_btn_delete.magnificPopup(opt_del);

		var opt_fullinfo = $.extend(true, {}, opt.magnificPopup);
		opt_fullinfo.callbacks.beforeOpen = _get_docente;
		opt_fullinfo.callbacks.afterClose = _clear_modal_fullinfo;
		$_open_fullinfo.magnificPopup(opt_fullinfo);

		var opt_resetpass = $.extend(true, {}, opt.magnificPopup);
		opt_resetpass.callbacks.beforeOpen = _beforeOpen_restore;
		$_btn_reset_pass.magnificPopup(opt_resetpass);


		$_form_del.on('submit.app.coordinator.module', _submit_del);
		$_form_restore.on('submit.app.coordinator.module', _submit_restore);
		$_btn_locked.on('click.app.coordinator.module', _locked_session);
		$_search.on('keyup.app.coordinator.module', _search_in_list);

		$("button.btn-options").dropdown(opt.dropdown);
		
		hlp.humanize_DateTime();
	},

	_beforeOpen_delete = function() {

		var btn_active 	= $.magnificPopup.instance.st.el,
			resource_id 	= btn_active.data('id');

		$_form_del.attr({
			action: config.url.delete.replace(/{ID}/g, resource_id)
		});

		$_li_current = btn_active.parents('li.collection-item');

		var old_text 		= $.trim($_modal_del.find('p.message').html()),
			text_replace 	= $.trim($_li_current.find('a.title').text());

		$_modal_del.find('p.message').html( old_text.replace(/{name}/gi,text_replace) );

	},

	_afterClose_delete = function() {
		$_modal_del.find('.message b').text('{name}');
	},

	_submit_del = function( e ) {

		var resource_data 	= {},
				resource_id 		= $_li_current.data('id'),
				resource 				= config.url.delete.replace(/{ID}/g, resource_id);

		$_form_del.find('button[type=submit]').prop({disabled:true});

		$.post( resource, resource_data, function(data, textStatus, xhr) {

			if ( data ) {

				if ( ! data.error ) {
					
					Materialize.toast(data.message, 3000);

					$_li_current.hide(500, function() {

						$_li_current.remove();

						if ( $_list_main.find('li.collection-item').length == 0 )
							$_list_main.append('<li class="collection-item empty"><p>No se registran datos.</p></li>');
					
					});

					$.magnificPopup.close();

					$_form_del.removeAttr('action');

					$_form_del.find('button[type=submit]').prop({disabled:false});

				} else {
					Materialize.toast(data.message, 3000);
					$_form_del.find('button[type=submit]').prop({disabled:false});
				}

			} else {
				Materialize.toast('Tu sessión ha caducado, reiniciando...', 3000, function(){
					location.reload(true);
				});
			}

		}).fail( function( objResponse, textStatus, responseText ) {

			if ( objResponse.readyState == 4 ) {
				$_form_del.find('button[type=submit]').prop({
					disabled: false
				});
			}

		});

		e.preventDefault();

	},

	_beforeOpen_restore = function() {

		var btn_active 	= $.magnificPopup.instance.st.el,
			resource_id 	= btn_active.data('id');

		$_form_restore.attr({
			action: config.url.restore.replace(/{ID}/g, resource_id)
		});

		$_li_current = btn_active.parents('li.collection-item');

	},

	_submit_restore = function( e ) {

		var resource_data = {},
			resource_id 		= $_li_current.data('id'),
			resource 				= config.url.restore.replace(/{ID}/g, resource_id);

		$_form_restore.find('button[type=submit]').prop({disabled:true});

		$.post(resource, resource_data, function(data, textStatus, xhr) {

			if ( data ) {

				if ( ! data.error ) {
					
					Materialize.toast(data.message, 2500);

					$.magnificPopup.close();
					$_form_restore.removeAttr('action');
					$_form_restore.find('button[type=submit]').prop({ disabled: false });

				} else {

					Materialize.toast(data.message, 2500);
					$_form_restore.find('button[type=submit]').prop({ disabled: false });

				}

			} else {

				Materialize.toast( data.message, 2500, '', function() {
					location.reload(true);
				});

			}

		}).fail(function( objResponse, textStatus, responseText ){
			if ( objResponse.readyState == 4 )
				$_form_restore.find('button[type=submit]').prop({disabled:false});
		});

		e.preventDefault();

	},

/*
| Metodos para la visualización de la informacion total de un docente
*/
	/**
	 * Consulta recurso toda la informacion de un docente seleccionado
	 * @return {void}
	 */
	_get_docente = function() {

		var btn_active 	= $.magnificPopup.instance.st.el,
			resource_id 	= btn_active.data('id'),
			resource 			= config.url.find.replace(/{ID}/gi, resource_id);

		$.getJSON(resource, function(json, textStatus) {

			if ( json ) {

				if ( ! json.error ) {

					_fill_fullinfo(json.data);

				} else {

					$.magnificPopup.close();
					Materialize.toast(json.message, 2500);

				}

			} else {

				Materialize.toast("Tu sesión a caducado, Reiniciando...", 2500, '', function(){
					location.reload(true);

				});

			}

		});

	},

	_fill_fullinfo = function(DATA) {

		$_modal_fullinfo.find('.title span').append((DATA.name + " " + DATA.lastname));
		$_modal_fullinfo.find('.title .profession').append(DATA.profesion);
		$_modal_fullinfo.find('.dni').append(DATA.dni);
		$_modal_fullinfo.find('.gender').append((parseInt(DATA.gender) == 1) ? "Femenino" : "Masculino");
		$_modal_fullinfo.find('.username').append(DATA.username);
		$_modal_fullinfo.find('.email').append(DATA.email.replace(/,/gi, '<br>'));
		$_modal_fullinfo.find('.type').append(DATA.type_name);
		$_modal_fullinfo.find('.subtype').append(DATA.subtype_name);
		$_modal_fullinfo.find('.entity').append(DATA.entidad_name);

		$_modal_fullinfo.find('.last-modified').html('Ult. Modificación: ' + moment(DATA.last_modified).calendar());

	},

	_clear_modal_fullinfo = function() {

		$_modal_fullinfo.find('.title span').html('<i class="material-icons">&#xE7FD;</i>');
		$_modal_fullinfo.find('.title .profession').html('<i class="material-icons">&#xE80C;</i>');
		$_modal_fullinfo.find('.dni').html('<b>DNI<br></b>');
		$_modal_fullinfo.find('.gender').html('<b>GENERO<br></b>');
		$_modal_fullinfo.find('.username').html('<b>USUARIO: <br></b>');
		$_modal_fullinfo.find('.email').html('<b>Email<small>(s)</small>: <br></b>');
		$_modal_fullinfo.find('.type').html('<b>Typo de Entidad: <br></b>');
		$_modal_fullinfo.find('.subtype').html('<b>Sub tipo de Entidad: <br></b>');
		$_modal_fullinfo.find('.entity').html('<b>Nombre de Entidad: <br></b>');

	},

/*  */

	_locked_session = function() {

		var $this 			= $(this),
			resource_data = {},
			resource_id 	= $this.data('id'),
			action 				= $this.data('action'),
			resource 			= config.url.locked.replace(/{ACTION}/gi, action);

		resource 				= resource.replace(/{ID}/gi, resource_id);

		$this.prop({disabled:true});

		$.post( resource, resource_data, function( data, textStatus, xhr ) {

			if ( data ) {

				if ( ! data.error ) {

					Materialize.toast(data.message, 3000);

					$this
						.data({action: ((action == 'locked') ? 'unlocked' : 'locked')})
						.html((action == 'locked') 
							? '<i class="material-icons">&#xE898;</i> Desbloq. Acceso' 
							: '<i class="material-icons">&#xE899;</i> Bloq. Acceso'
						);

					var visor = $this.parents('li.collection-item').find('i.is_active');
					
					if ( action == 'locked' ) {

						$this.parents('ul.dropdown-content')
								.find('button.btn-renove-pass').prop({disabled:true});

						visor.removeClass('active');

					} else {

						$this.parents('ul.dropdown-content')
							.find('button.btn-renove-pass').prop({disabled:false});

						visor.addClass('active');

					}

				} else {
					Materialize.toast(data.message, 3000);
				}

				$this.prop({ disabled:false });
				
			} else {

				Materialize.toast(data.message, 3000, function(){
					location.reload(true);
				});

			}

		}).fail( function( objResponse, textStatus, responseText ) {
			if ( objResponse.readyState == 4 )
				$this.prop({ disabled:false });
		});

	},

	_search_in_list = function() {

		var $this 	= $(this),
				texto 	= hlp.remove_accent($.trim($this.val()).toLowerCase());

		if ( timeOutID != 0 ) clearTimeout(timeOutID);

		timeOutID = _window.setTimeout(function(){

			$_list_main.find('li.collection-item').each( function(index, elem) {

				var nombre = hlp.remove_accent((elem.getAttributeNode('data-search').textContent).toLowerCase());

				if ( nombre.indexOf(texto) != -1 )
					$(elem).fadeIn(300);
				else
					$(elem).fadeOut(300);

			});

		}, 200);

	};

	return {
		init : _bind
	}

}(window, window.jQuery));

$(function() {
	if( $('html').data('module') == 'index' )
		app.coordinator.init();
});