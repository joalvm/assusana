app.reports.attendance = (function(_window, $){

	'use strict';

	var opt = $.extend(true, {}, options),
		hlp = $.extend(true, {}, helper);

	var mfp_program = $.extend(true, {}, opt.magnificPopup);

	var $_input_programming 		= $('input[type=text]#txtprogramming'),
		$_cbo_filter 				= $('select#cbo_filters'),
		$_list_participant 			= $('ul#list-participant'),
		$_md_programming 			= $('section#md-list-programming'),
		$_radio_programming 		= $_md_programming.find('input[type=radio]');

	var _bind = function() {
		_humanize_data();

		$_input_programming.magnificPopup(mfp_program);

		$_radio_programming.on('change.app.inscription.module', _selected_programming);
		$_cbo_filter.on('change.app.inscription.module', _change_filter);
	},

	_selected_programming = function() {
		var $this = $(this),
			id = $this.val(),
			resource = '/app/inscription/programming/' + id + '/participants.json';

		$_list_participant.children('li').remove();
		$('#button_new').remove();
		app.progress.show();

		$_input_programming.val($this.data('title'));

		$.getJSON(resource, function(json, textStatus) {
			if ( json ) {
				if ( ! json.error ) {

					json.data.participants.forEach( function(obj, index) {
						var item = _get_item(obj)
						$_list_participant.append(item);
					});

					if( json.data.participants.length == 0 ) {
						$_list_participant.append('<li class="empty-list"><h4>Sin participantes</h4><p>Aun no se han registrado participantes a esta capacitación</p></li>');
					} else {
						$('main').append(_get_button_save(json.data.programming.programming_id));
					}
					_fill_programming_data(json.data.programming, $('#programming-data'));
					_fill_programming_theme(json.data.themes, $('#programming-themes'));
				}
			} else {
				Materialize.toast('Tu sesión a terminado, reiniciando...', 2000);
			}
		}).always(function() {
			app.progress.close();
			$.magnificPopup.close();
		});
	},

	_change_filter = function() {
		var $this = $(this),
			valor = parseInt($this.children('option:selected').val());

		var data_request 	= { filter_level: valor },
			resource 		= '/app/training/programming/filter';

		data_request[csrf.name] = csrf.value;

		$this.prop({disabled:true});
		app.progress.show();

		$.post(resource, data_request, function(data, textStatus, xhr) {
			if ( data ) {
				location.reload(true);
			} else {
				Materialize.toast('Tu sessión ha terminado, Reiniciando...', 4000, '', function(){
					location.reload(true);
				});
			}

			app.progress.close();

		}).fail(function(objResponse, textStatus, responseText){

			if ( objResponse.readyState == 4 ) {

				if ( typeof objResponse.responseJSON != 'undefined' ) {
					Materialize.toast(objResponse.responseJSON.message + ". Reiniciando...", 3000);
				} else {
					Materialize.toast('Ha ocurrido un error en el servidor, vuelve a intentarlo', 3000);
				}

				$this.prop({disabled:false});

			} else {
				Materialize.toast('Ha ocurrido un error en el servidor, intentalo mas tarde', 4000, '', function(){
					location.reload(true);
				});
			}

		}).always(function() {
			app.progress.close();
		});
	},

	_get_item = function(DATA) {

		var jli = $('<li />', {class:'collection-item'}),
			jdiv = $('<div />'),
			jname = $('<span />', {
					class:'title',
				}).text((DATA.last_name + ", " + DATA.name)),

			jprof = $('<span />', {class:'title3'}).text(DATA.entity_name),
			jdni = $('<span />', {class:'title2'}).html('<b>DNI: </b>'+ DATA.dni ),
			jemail = $('<span />', {class:'title2'}).html('<b>Email: </b>'+ DATA.email );

		jdiv.append([jdni, jemail, jprof]);

		if ( DATA.attendance ) {
			jname.append('<i class="material-icons si">&#xE834;</i>');
			jname.append($('<span />', {
				class:'chip'
			}).html( moment(DATA.time_attendance).format('h:mma')));
			jname.append($('<span />', {class:'nota chip'}).text(DATA.nota));
		} else {
			jname.append('<i class="material-icons no">&#xE909;</i>');
		}

		jli.append([jname, jdiv]);

		return jli;
	},

	_get_button_save = function(ID) {
		var jdiv_cnt = $('<div />', {
				id:'button_new',
				class:'fixed-action-btn tooltipped',
				'data-delay': 50,
				'data-position':"left",
				'data-tooltip': "Programar nueva capacitación",
				style: "bottom: 45px; right: 24px;"
			}),
			jbutton = $('<a />', {
				class: "btn-floating btn-large"
			}).html('<i class="material-icons">&#xE5D4;</i>'),
			jul 	= $('<ul />'),
			jli_pdf = $('<li />'),
			jli_pdf_button = $('<a />', {
				class:'btn-floating red'
			}).html('<i class="material-icons">&#xE8AD;</i>'),
			jli_excel = $('<li />'),
			jli_excel_button = $('<a />', {
				href: hlp.base_url + 'app/reports/attendance/' + ID + '/excel',
				target:'_blank',
				class: 'btn-floating green'
			}).html('xls');

		$('div#button_new').remove();

		jli_pdf_button.on('click.app.docente.attendance.module', function(){
			_window.print();
		});

		jli_pdf.append([jli_pdf_button]);
		jli_excel.append([jli_excel_button]);

		jul.append([jli_pdf, jli_excel]);

		jdiv_cnt.append(jbutton, jul);

		return jdiv_cnt;
	},

	_fill_programming_data = function(DATA, ELEM) {
		ELEM.find('#text_trining_title').text(DATA.training_title);
		ELEM.find('#text_type_training').text(DATA.type_training);
		ELEM.find('#text_place').text(DATA.place);
		ELEM.find('#text_address').text(DATA.address);
		ELEM.find('#text_departamento').text(DATA.departamento);
		ELEM.find('#text_provincia').text(DATA.provincia);
		ELEM.find('#text_distrito').text(DATA.distrito);
		ELEM.find('#text_date_realization').text(moment(DATA.date_realization).format('ddd DD MMMM YYYY'));
		ELEM.find('#text_hour_start').text(moment(DATA.date_realization + " " + DATA.time_start).format('h:mma'));
		ELEM.find('#text_hour_finish').text(moment(DATA.date_realization + " " + DATA.time_finish).format('h:mma'));
		ELEM.find('#text_max_participant').text((DATA.limit_max == 0) ? "Sin limite" : DATA.limit_max);
		ELEM.find('#text_person_registered').text(DATA.nregisters);
		ELEM.find('#text_cant_asistentes').text(DATA.nassistents);
		ELEM.find('#text_its_personalize').html(DATA.its_customized ? '<i class="material-icons">&#xE876;</i>' : '<i class="material-icons">&#xE14C;</i>');
		ELEM.find('#text_entity_name').text((DATA.entity_name == null) ? "No personalizado" : DATA.entity_name);
		ELEM.fadeIn(400);
	},

	_fill_programming_theme = function(DATA, ELEM) {
		DATA.forEach( function(obj, index) {
			var tr 				= $('<tr />'),
				td_theme 		= $('<td />', {valign:'top'}),
				td_subtheme 	= $('<td />'),
				h_theme_title	= $('<h4 />', {class:'title'}).text(obj.title),
				p_theme_descr 	= $('<p> /', {class:'description'}).text(hlp.nl2br(obj.description));

			td_theme.append([h_theme_title, p_theme_descr]);

			obj.subthemes.forEach( function(obj2, index2) {
				var divisor = $('<div />', {class:'divisor'}),
					h_stheme_title = $('<h5 />', {class:'title2'}).text(obj2.title),
					span_docente = $('<span />', {class:'docente'}).text(obj2.docente),
					p_stheme_descr = $('<p />', {class:'description'}).text(hlp.nl2br(obj2.description));

				divisor.append([h_stheme_title, span_docente, p_stheme_descr]);

				td_subtheme.append([divisor]);
			});

			tr.append([td_theme, td_subtheme]);

			ELEM.find('tbody').append(tr);
			ELEM.fadeIn(400);
		});
	},

	_humanize_data = function() {

		moment.locale("es");

		$('time.humanize').each(function(index, el) {
			var datetime 	= el.getAttributeNode("data-datetime").textContent,
				type 		= el.getAttributeNode("data-type").textContent;

			if ( type == 'date' ) {
				el.textContent = moment(datetime).format('ddd DD MMMM YYYY h:mma');
			} else {
				el.textContent = moment(datetime).format('h:mma');
			}
		});
	};

	return {
		init: _bind
	};

}(window, window.jQuery));

$(function() {
	switch ($('html').data('module')) {
		case 'attendance': app.reports.attendance.init(); break;
	}
});
