app.inscription.manage = (function(_window, $) {
	'use strict';

	var config = {
		url : {
			participant: '/api/participant/{DNI}.json',
			inscription: '/app/inscription/programming/{ID}/create'
		},
		allowEntity : 0,
		is_customized: 0,
		auto_loaded: false
	};

	var data_default = {
			id: 0,
			programming: '',
			is_customized: 0, //bool
			name: '',
			lastname: '',
			dni: '',
			gender: -1,
			profesion: '',
			email: '',
			cellphone: '',
			attendance: 0,
			phone: '',
			annex: '',
			distrito: 0,
			entity: 0,
			details_dispacity: '',
			dispacity: 0, //bool
			position: ''
		},
		data_send = {};

	var entities_data 				= [],
		entities_filter 			= [],
		ubigeo_data 				= [],
		opt 						= $.extend(true, {}, options),
		hlp 						= $.extend(true, {}, helper),
		mfp_entities 				= $.extend(true, {}, options.magnificPopup),
		mfp_before_submit 			= $.extend(true, {}, options.magnificPopup),
		timeOutID 					= 0,
		last_dni_searched 			= 0;

	var $_form 						= $('form#frm_manage'),
		$_btn_submit 				= $_form.find('button[type=submit]'),
		$_filter_select 			= $('select:not(.browser-default)'),
		$_cbo_ubigeos 				= $_form.find('select.ubigeo'),
		$_txt_only_number 			= $_form.find('input[type=text].only-number'),
		$_elements_cleanables 		= $_form.find('.cleanable'),
		$_inputs_watches 			= $_form.find('.dataset'),
		$_txt_dni 					= $_form.find('input[type=text]#txt_dni'),
		$_chk_is_discapacty 		= $_form.find('#chk_is_dispacity'),

		$_txt_entities 				= $_form.find('input#txt_entity_display'),
		$_cnt_entities_search 		= $_form.find('.method_search'),
		$_modal_entities 			= $('section#popup-list-entities'),
		$_modal_list_entities 		= $_modal_entities.find('ul#list-entities'),
		$_searcher 					= $_modal_entities.find('input[type=search]'),
		$_btn_close_modal 			= $_modal_entities.find('#btn-cancel'),
		$_item_counts 				= $_modal_entities.find('.items-count'),
		$_item_checked 				= {},

		$_modal_before_submit 		= $('section#md-before-submit'),
		$_btn_continue 				= $_modal_before_submit.find('button#btn-save'),
		$_btn_cancel 				= $_modal_before_submit.find('button#btn-cancel'),
		continue_submit 			= false;

	var _bind = function() {
		data_default.programming 	= parseInt($_form.data('programming'));
		data_default.is_customized 	= parseInt($_form.data('personalize'));
		data_default.entity 		= parseInt($_form.data('entity'));
		config.is_customized 		= parseInt($_form.data('personalize'));
		config.allowEntity 			= parseInt($_form.data('entity'));

		$_filter_select.material_select();

		mfp_entities.callbacks.beforeOpen = _beforeOpen_modal;
		mfp_entities.focus = "input[type=search]";
		mfp_entities.modal = false;
		$_txt_entities.magnificPopup(mfp_entities);

		mfp_before_submit.modal = true;
		mfp_before_submit.callbacks.afterClose = _afterClose_modal_submit;
		mfp_before_submit['items'] = { src: $_modal_before_submit };
		mfp_before_submit.type = 'inline';

		$_searcher.on('keyup.workshops.module', _filter_entities);
		$_btn_close_modal.on('click.workshops.module', _close_modal_entities);
		$_modal_list_entities.on('scroll.workshops.module', _detected_botton);
		$_txt_only_number.on('keydown.workshops.module', hlp.keyup_Numeric);
		$_txt_dni.on('keyup.workshops.module', _verify_dni);
		$_chk_is_discapacty.on('change.workshops.module', _enable_details);
		$_form.on('submit.workshops.module', _submit_data_send);
		$_inputs_watches.on('change.workshops.module', _watching_changes_data);

		$_cbo_ubigeos.on('change.workshop.module', _changing_ubigeo_cascaded);

		_humanize_data();
		_getUbigeo();
	},

/**
| PROCESANDO EL REGISTRO
*/
	_submit_data_send = function(e) {

		data_send = $.extend({}, data_default, data_send);

		if ( ! _is_valid_data_before_send() ) {
			e.preventDefault();
			return false;
		}

		$_btn_submit.prop({ disabled: true });

		if ( continue_submit ) {

			var resource = config.url.inscription.replace(/{ID}/gi, data_default.programming);

			app.progress.show();

			data_send[csrf.name] = csrf.value;

			$.post(resource, data_send, function(data, textStatus, xhr) {
				
				if ( data )
				{
					if ( ! data.error ) {
						Materialize.toast(data.message, 2000, '', function(){
							location.reload(true);
						});
					} else {
						Materialize.toast(data.message, 3000);
						$_btn_submit.prop({disabled:false});
					}
				} else {
					Materialize.toast('Tu sessión ha terminado, Reiniciando...', 3000, '', function(){
						location.reload(true);
					});
				}

			}).fail(function(objResponse, textStatus, responseText) {
			
				if ( objResponse.readyState == 4 ) {

					if ( typeof objResponse.responseJSON != 'undefined' ) {
						Materialize.toast(objResponse.responseJSON.message, 3000);
					} else {
						Materialize.toast('Ha ocurrido un error en el servidor, vuelve a intentarlo', 4000);
					}

					$_btn_submit.prop({disabled:false});

				} else {
					Materialize.toast('Ha ocurrido un error en el servidor, intentalo mas tarde', 4000, '', function(){
						location.reload(true);
					});
				}
				
			}).always(function(){
				app.progress.close();
			});
		} else {
			continue_submit = false;
			$_btn_continue.on('click.workshops.module', _continue_inscription);
			$_btn_cancel.on('click.workshops.module', _cancel_inscription);
			$.magnificPopup.open(mfp_before_submit, 0);
			return false;
		}

		e.preventDefault();
	},

	_afterClose_modal_submit = function() {
		$_btn_continue.off('click.workshops.module');
		$_btn_cancel.off('click.workshops.module');
	},

	_continue_inscription = function() {
		continue_submit = true;
		$.magnificPopup.close();
		$_form.submit();
	},

	_cancel_inscription = function() {
		continue_submit = false;
		$_btn_submit.prop({ disabled: false });
		$.magnificPopup.close();
	},

	_is_valid_data_before_send = function() {
		var errors = [],
			msgs = {
				empty: "Este campo es obligatorio.",
				lengthDNI: "El DNI debe tener 8 digitos.",
				allNumeric: "Todos los digitos tienen que ser numericos.",
				choiseOpt: "Debe escoger una opción",
				choiseDist: "Debe seleccionar un distrito",
				choiseGender: "Debes escojer tu genero",
				onlyString: "Este campo debe tener solo caracteres alfabeticos."
			};

		$.each(data_send, function(key, value) {

			var elem = $_inputs_watches.filter('[name="' + key + '"]');

			switch (key) {
				case 'name':
				case 'lastname':
					if ( $.trim(value).length == 0 ) {
						errors.push({ elem : elem, message: msgs.empty });
					} else if( ! hlp.is_alpha(value) ) {
						errors.push({ elem : elem, message: msgs.onlyString });
					}
					break;
				case 'dni': 
					if ( $.trim(value).length == 0 ) {
						errors.push({ elem : elem, message: msgs.empty });
					} else if ( ! $.isNumeric($.trim(value)) ) {
						error_arr.push({ elem: elem, message: msgs.allNumeric });
					} else if( $.trim(value).length != 8 ) {
						errors.push({ elem : elem, message: msgs.lengthDNI });
					}
					break;
				case 'gender':
					if ( value < 0 ) {
						errors.push({ elem : elem, message: msgs.choiseGender });
					}
					break;
				case 'email':
					if ( $.trim(value).length == 0 )
						errors.push({ elem : elem, message: msgs.empty });
					break;
				case 'distrito':
					if ( value <= 0 )
						errors.push({ elem : elem, message: msgs.choiseDist });
					break;
				case 'entity':
					if ( value <= 0 )
						errors.push({ elem : elem, message: msgs.choiseOpt });
					break;
			}
		});

		if ( errors.length > 0 )
		{
			Materialize.toast(errors[0].message, 3000);
			errors[0].elem.focus();
			return false;
		} else {
			return true;
		}
	},

	_watching_changes_data = function() {
		var $this = $(this),
			name = $this.attr('name'),
			type = $this.attr('type'),
			tag = $this.prop("tagName").toLowerCase();

		if ( tag == 'input' ) {
			switch (type) {
				case 'text': 
					if ( name != 'dni' ) {
						data_send[name] = $.trim($this.val()); 
					} else { 
						// para el que se le ocurra cambiar los attributos en el estado desabilitado
						if ( config.auto_loaded == false )
							data_send[name] = $.trim($this.val());
					}
					break;
				case 'email':
					if ( $this.val().length > 0 ) {
						if ( ! _check_emails($this.val()) ) {
							$this.removeClass('valid').addClass('invalid');
							data_send[name] = "";
						} else {
							$this.removeClass('invalid').addClass('valid');
							data_send[name] = $.trim($this.val()).replace(/, /gi, ',');
						}
					} else {
						$this.removeClass('valid invalid');
						data_send[name] = "";
					}
				break;
				case 'checkbox':
					data_send[name] = $this.is(':checked') ? 1 : 0;
				break;
			}
		} else if ( tag == 'select' ) {
			data_send[name] = parseInt($this.children('option:selected').val());
		} else if ( tag == 'textarea' ) {
			data_send[name] = $.trim($this.val());
		}
	},

	_check_emails = function( EMAILS ) {
		var emails_arr = $.trim(EMAILS).split(','),
			errors_counts = 0;
		if ( emails_arr.length > 0 ) {
			emails_arr.forEach( function(element, index) {
				if ( $.trim(element).length > 0 ) {
					if ( ! hlp.isValidEmail($.trim(element)) ) 
						errors_counts += 1;
				} else {
					errors_counts += 1;
				}
			});
		} else {
			errors_counts += 1;
		}

		return (errors_counts == 0);
	},
//=================================================================================================================



/**
| PROCESO DE BUSQUEDA Y SELECCION DE LA ENTIDAD DEL PARTICIPANTE
*/
	_detected_botton = function(e) {

		//var scrollCount = $_modal_list_entities.scrollTop() + $_modal_list_entities.height(),
		//	initCallback = $_modal_list_entities[0].scrollHeight -  $_modal_list_entities.height();

		if ( parseInt($_modal_list_entities.scrollTop() + $_modal_list_entities.height()) > 
			parseInt($_modal_list_entities[0].scrollHeight -  $_modal_list_entities.height()) ) {

			var lengthItems = $_modal_list_entities.children('li').length;

			for( var i = lengthItems; i < (lengthItems + 50); i++ ) {
				var elem = entities_filter[i];
				elem.find('input[type=radio]').on('change.workshop.module', _choise_item);
				$_modal_list_entities.append(elem);
			}

			$_item_counts.find('.curr').text($_modal_list_entities.children('li').length);
		}
	},

	_beforeOpen_modal = function() {

		var _opener = $.magnificPopup.instance.st.el,
			data_id = _opener.data('id') || 0;

		if ( entities_filter.length > 0 )
			return false;

		$_modal_entities.find('.popup-header')
			.append('<div class="progress"><div class="indeterminate"></div></div>');

		entities_data = [];
		entities_filter = [];
		$_modal_list_entities.children('li').remove();

		$.getJSON('/api/entities/expanded.json', function(json, textStatus) {

			json.entities.forEach( function(obj, index) {
				var stype 	= json.subtype_entities[obj.stype_ps],
					type 	= json.type_entities[stype.type_ps],
					litem 	= _get_item_list();

				litem.attr({
					'data-search' : (obj.entity + " " + obj.cue + " " + type.name + " " + stype.name),
					'data-id' : obj.id
				});

				litem.find('input[type=radio]').attr({
					'data-id' : "entity" + obj.id,
					'data-entity' : obj.entity,
					'data-type' : type.name,
					'data-subtype' : stype.name,
					id: "entity" + obj.id
				}).val(obj.id);

				if ( data_id == obj.id)
					litem.find('input[type=radio]').prop({checked:true});

				litem.find('label').attr({ for:('entity' + obj.id) });
				litem.find('span.entity').attr({
					title: obj.entity
				}).html(obj.entity);

				litem.find('span.subtype').html('Subtipo : ' + stype.name);
				litem.find('span.type').html('Tipo &nbsp;&nbsp;&nbsp; : ' + type.name);

				litem.find('input[type=radio]').on('change.workshop.module', _choise_item);

				entities_data.push(litem);
				if ( index < 50 )
					$_modal_list_entities.append(litem);
			});

			// Copiar la data inicial a la data de filtros
			entities_filter = entities_data;

			$_item_counts.find('.curr').text(50);
			$_item_counts.find('.total').text(entities_data.length);

			$_modal_entities.find('.popup-header .progress').remove();
		});
	},

	_close_modal_entities = function() {
		$.magnificPopup.close();
	},

	_choise_item = function() {
		var $this = $(this);
		var entity = $this.data('entity'),
			type_entity = $this.data('type'),
			stype_entity = $this.data('subtype'),
			id = $this.val();

		$_item_checked = $this;

		$_txt_entities.attr({title: entity }).val(entity);
		$_form.find('input#txt_type_entity').val(type_entity).siblings('label').addClass('active');
		$_form.find('input#txt_subtype_entity').val(stype_entity).siblings('label').addClass('active');
		//$value.val(id);

		data_send.entity = parseInt(id);

		$.magnificPopup.close();
	},

	_filter_entities = function() {
		var texto = this.value;

		if ( timeOutID != 0) clearTimeout(timeOutID);

		entities_filter = [];
		$_modal_list_entities.children('li').remove();

		timeOutID = _window.setTimeout(function() {

			var count = 0;
			entities_data.forEach(function(elem, index) {

				if ( (elem.data('search').toLowerCase()).indexOf(texto) != -1 ) {
					
					entities_filter.push(elem);

					if ( count < 50 ) {
						elem.find('input[type=radio]')
							.on('change.workshop.module', _choise_item);
						$_modal_list_entities.append(elem);
					}

					++count;
				}

			})

			$_item_counts.find('.curr').text($_modal_list_entities.children('li').length);
			$_item_counts.find('.total').text(entities_filter.length);
			
		}, 300);
	},

	_get_item_list = function() {
		return $('<li class="collection-item" data-search="" data-id="">'+
			'<p>'+
				'<input class="with-gap" type="radio" id="" name="entities" data-name="" value="">'+
				'<label for="">'+
					'<span class="title entity truncate" title=""></span>'+
					'<span class="title2 type"></span>' +
					'<span class="title3 subtype"></span>'+
				'</label>'+
			'</p>'+
		'</li>');
	},
//=================================================================================================================



/**
| METODOS PARA LA CARGA Y SELECCION DEL UBIGEO DE UBICACIÓN DE
| LA CAPACITACIÓN
*/
	_getUbigeo = function() {
		var depart = $_cbo_ubigeos.filter('#cbo_departamento');

		$.getJSON('/api/ubigeo.json', function(json, textStatus) {
			
			json.forEach( function(element, index) {
				depart.append($('<option />', {
					value: element.id,
					'data-index': index,
				}).html(element.departamento));
			});

			ubigeo_data = json;
			depart.prop({disabled:false});
		});
	},

	_changing_ubigeo_cascaded = function() {
		var $this = $(this),
			name = $this.attr('name'),
			position = parseInt($this.children('option:selected').data('index'));

		if ( $this.children('option:selected').val() != '0' ) {
			switch (name) {
				case 'departamento':
					$_cbo_ubigeos
						.filter('#cbo_distrito')
						.children('option:not(.default)').remove();
					_load_provincia(position);
					$_cbo_ubigeos
						.filter('#cbo_provincia')
							.children('option.default').prop({selected:true});
					break;
				case 'provincia':
					_load_distrito(position);
					break;
			}
		} else {
			$_cbo_ubigeos.filter('#cbo_provincia').children('option:not(.default)').remove();
			$_cbo_ubigeos.filter('#cbo_distrito').children('option:not(.default)').remove();
		}
	},

	_load_provincia = function( POSITION ) {
		var prov = $_cbo_ubigeos.filter('#cbo_provincia');

		prov.children('option:not(.default)').remove();

		ubigeo_data[POSITION].provincias.forEach( function(element, index) {
			prov.append($('<option />', {
				value: element.id,
				'data-index': index,
			}).html(element.provincia));
		});

		prov.prop({disabled:false});
	},

	_load_distrito = function( POSITION ){
		var dist = $_cbo_ubigeos.filter('#cbo_distrito'),
			pos_depart = $_cbo_ubigeos.filter('#cbo_departamento').children('option:selected').data('index');

		dist.children('option:not(.default)').remove();

		ubigeo_data[parseInt(pos_depart)].provincias[POSITION].distritos.forEach( function(element, index) {
			dist.append($('<option />', {
				value: element.id,
				'data-index': index,
			}).html(element.distrito));
		});

		dist.prop({disabled:false});
	},
//=================================================================================================================



/**
| 	METODOS PARA LA CARGA AUTOMATICA DE LA INFORMACION
| 	DEL PARTICIPANTE EN CASO DE ESTAR PREVIAMENTE REGISTRADO
*/
	_verify_dni = function() {

		var $this = $(this),
			number = $.trim($this.val()),
			parent = $this.parent('.input-field');

		if ( number.length == 8 && last_dni_searched != number ) {

			if ( ! $.isNumeric( number ) ) {
				Materialize.toast("El DNI solo debe tener digitos numericos", 2000);
				$this.val('').focus();
				return false;
			}

			var resource = config.url.participant.replace(/{DNI}/gi, number);
			
			$this.prop({disabled:true});
			parent.append('<div class="progress"><div class="indeterminate"></div></div>');
			
			// Evitar juego de repitición automatico
			// E.G EL JUEGO SE TRATA CAUNDO SE PULSA RAPIDAMENTE
			// AL LLEGAR AL ULTIMO CARACTER, EVITAMOS QUE VUELVA A CONSULTAR
			// SI ES EL MISMO NUMERO QUE ANTERIORMENTE SE DIGITO
			last_dni_searched = number;

			$.getJSON(resource, function(json, textStatus) {

				var prefix_button = $('<i />', {
					class:'material-icons prefix',
					style:'cursor:pointer;'
				}).html('&#xE15D;');

				if ( ! json.error ) {
					if ( json.code == 200 ) {

						// limpio todos los elemento
						_clean_inputs();

						if ( (json.data.entity_id === config.allowEntity && config.is_customized == 1) || 
								config.is_customized == 0 ) {

							config.auto_loaded = true;
							data_send['dni'] = json.data.dni;
							$_txt_dni.off('keyup.workshops.module');

							// lleno todos los elementos
							_fill_inputs(json.data);

							prefix_button.on('click.workshops.module', function() {
								// limpio el dni y lo desabilito
								$this.val('').prop({disabled:false});
								// quito el boton clear
								parent.children('.prefix').remove();
								// limpio el ultimo dni ingresado
								last_dni_searched = 0;
								// limpio todos los elemento
								_clean_inputs();

								// avisando que se ha limpiado la bs
								config.auto_loaded = false;

								// retornando el evento de busqueda
								$_txt_dni.on('keyup.workshops.module', _verify_dni);

								// regreso el foco a dni
								$this.focus();
							});

							// agrego el boton
							parent.prepend(prefix_button);

							Materialize.toast('Usted ya esta registrado, sus datos se han cargado automaticamente', 4000);
						
						} else {

							$this.prop({disabled:false}).val('').focus();

							// limpio el ultimo dni ingresado
							last_dni_searched = 0;

							Materialize.toast("Usted ya esta registrado, pero la entidad a la que pertenece no esta permitida para esta capacitación", 5000);
						}

					} else {
						$this.addClass('validate valid');
						$this.prop({disabled:false}).focus();
					}
				}
				
			}).always(function(){
				parent.children('.progress').remove();
			});
		}
	},

	_clean_inputs = function() {

		$_elements_cleanables.each(function(index, el) {
			var $el = $(el),
				elTag = el.nodeName,
				elType = el.getAttributeNode('type') || undefined;

			if ( elTag == 'INPUT' ) {
				if ( elType.value == 'text' || elType.value == 'email' ) {
					$el.val('').focusout().change();
				} else {
					$el.prop({checked:false}).change();
				}
			} else if( elTag == 'TEXTAREA' ) {
				$el.val('').focusout().change();
			}
		});

		_select_dropdwon($_elements_cleanables.filter('select#cbo_gender'), 0);

		$_elements_cleanables.filter('select#cbo_departamento').children('option:eq(0)').prop({selected:true});
		$_elements_cleanables.filter('select#cbo_departamento').change();
		$_elements_cleanables.filter('select.ubigeo:not(#cbo_departamento)').prop({disabled:true});
		
		// limpiando data_send
		data_send.gender = -1;
		data_send.id = 0;
		data_send.dni = '',
		data_send.distrito = 0;

		if ( config.is_customized == 0 ) data_send.entity = 0;
	},

	_fill_inputs = function( DATA ) {

		var $cbo_gender 		= $_inputs_watches.filter('#cbo_gender'),
			$txt_type_entity 	= $_elements_cleanables.filter("#txt_type_entity"),
			$txt_subtype_entity = $_elements_cleanables.filter("#txt_subtype_entity");

		$_inputs_watches.filter('#txt_name').val(DATA.name).change();
		$_inputs_watches.filter('#txt_lastname').val(DATA.lastname).change();
		$_inputs_watches.filter('#txt_profesion').val(DATA.profesion).change();
		$_inputs_watches.filter('#txt_email').val(DATA.email).change();
		$_inputs_watches.filter('#txt_cellphone').val(DATA.cellphone).change();
		$_inputs_watches.filter('#txt_phone').val(DATA.phone).change();
		$_inputs_watches.filter('#txt_annex').val(DATA.annex).change();
		$_inputs_watches.filter('#txt_position').val(DATA.position).change();

		$_chk_is_discapacty.prop({checked:DATA.dispacity}).change();
		$_inputs_watches.filter('#txt_dispacity_details').val(DATA.details_dispacity).change();

		_select_dropdwon($cbo_gender, (DATA.gender + 1));

		$_cbo_ubigeos.filter('#cbo_departamento')
			.children('option[value="'+DATA.departamento_id+'"]').prop({selected:true}).change();
		$_cbo_ubigeos.filter('#cbo_provincia')
			.children('option[value="'+DATA.provincia_id+'"]').prop({selected:true}).change();
		$_cbo_ubigeos.filter('#cbo_distrito')
			.children('option[value="'+DATA.distrito_id+'"]').prop({selected:true}).change();

		$_txt_entities.val(DATA.entity_name)
			.attr({title: DATA.entity_name,'data-id':DATA.entity_id}).change();
		
		data_send.entity = DATA.entity_id;

		$txt_subtype_entity.val(DATA.subtype_entity_name)
			.attr({title: DATA.subtype_entity_name}).change();
		$txt_type_entity.val(DATA.type_entity_name)
			.attr({title: DATA.type_entity_name}).change();

		data_send.id = DATA.id;

		// PARA CUANDO SE HA SELECCIONADO PREVIAMENTE UNA ENTIDAD
		// SE LIMPIA LA DATA PARA QUE SELECCIONE EL VALOR ENTRANTE
		entities_filter = [];
		$_inputs_watches.filter('#txt_name').focus();
	},
//=================================================================================================================

	_enable_details = function() {
		if ($(this).is(':checked')) {
			$_inputs_watches.filter('#txt_dispacity_details').prop({
				disabled: false
			}).val('').change().focus();
		} else {
			$_inputs_watches.filter('#txt_dispacity_details').prop({
				disabled: true
			}).val('').change();
		}
	},

	_select_dropdwon = function( SELECT, SELECTED_IXDEX ) { 

		var parent_gender 	= SELECT.parent('.select-wrapper'),
			select_dopdrown = parent_gender.children('.select-dropdown'),
			dropdown_cnt 	= parent_gender.children('.dropdown-content'),
			option 			= SELECT.children('option').eq(SELECTED_IXDEX);

		option.prop({selected:true});

		select_dopdrown.val(option.text());

		dropdown_cnt.children('li').removeClass('active selected');
		dropdown_cnt.children('li').eq(SELECTED_IXDEX).addClass('active selected');

		SELECT.change();
	},

	_humanize_data = function() {
		moment.locale("es");

		$('time.humanize').each(function(index, el) {
			var datetime = el.getAttributeNode("data-datetime").textContent,
				type = el.getAttributeNode("data-type").textContent;

			if ( type == 'date' ) {
				el.textContent = moment(datetime).format('ddd DD MMMM YYYY');
			} else {
				el.textContent = moment(datetime).format('h:mma');
			}
			
		});
	};

	return {
		init: _bind
	}

}(window, window.jQuery));

$(function() {
	if( $("html").attr('data-module') == 'create' )
		app.inscription.manage.init();
});