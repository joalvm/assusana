app.inscription = {};

app.inscription = (function(_window, $) {
    'use strict';

    var opt = $.extend(true, {}, options),
        hlp = $.extend(true, {}, helper);

    var mfp_program = $.extend(true, {}, opt.magnificPopup),
        mfp_delete = $.extend(true, {}, opt.magnificPopup),
        mfp_participant = $.extend(true, {}, opt.magnificPopup);

    var elements_list = [],
        elements_list_filter = [],
        entities = [],
        data_participant = {},
        timeOutID = 0;

    var $_input_programming = $('input[type=text]#txtprogramming'),
        $_cbo_filter = $('select#cbo_filters'),
        $_btn_options = $('button.btn-options'),
        $_search_participant = $('input[type=search]#search_inscription'),
        $_list_inscription = $('ul#list-inscription'),
        $_btn_del_inscription = $('button.delete-inscription'),
        $_btn_active_inscription = $('button.active-inscription'),
        $_md_programming = $('section#md-list-programming'),
        $_list_programing = $_md_programming.find('input[type=radio]'),

        $_md_deletion = $('section#md-delete-inscription'),
        $_btn_continue_del = $_md_deletion.find('button#btn_continue'),
        $_element_to_delete = {},

        $_md_participant = $('section#md-participant'),
        $_btn_modify_info = $_md_participant.find('button#btn_edit_info'),
        $_item_participant = {},

        $_container = $(document),
        $_body = $(document.body);

    var _bind = function() {

            $_btn_options.dropdown(opt.dropdown);
            $_input_programming.magnificPopup(mfp_program);

            mfp_delete.callbacks.beforeOpen = _beforeOpen_md_delete;
            $_btn_del_inscription.magnificPopup(mfp_delete);

            mfp_participant.callbacks.beforeOpen = _beforeOpen_md_participant;
            mfp_participant.callbacks.afterClose = _afterClose_md_participant;
            $_container.on('scroll.app.docente.inscription.module', _detected_botton);

            $_list_programing.on('change.app.inscription.module', _selected_programming);
            $_cbo_filter.on('change.app.inscription.module', _change_filter);
            $_btn_continue_del.on('click.app.inscription.module', _delete_inscription);
            $_btn_active_inscription.on('click.app.inscription.module', _active_inscription);
            $_search_participant.on('keyup.app.docente.inscription.module', _filter_participant);
            $_btn_modify_info.on('click.app.docente.inscription.module', _participant_to_modify);
            $_md_participant.find('form').on('submit.app.inscription.module', _send_data_participant_to_edit);

            if (_window.outerWidth < 1024)
                $_search_participant.on('change.app.docente.inscription.module', _filter_participant);

            hlp.humanize_DateTime();
        },

        _selected_programming = function() {
            var $this = $(this),
                id = parseInt($this.val()),
                resource = '/app/inscription/programming/' + id;

            elements_list = [];
            elements_list_filter = [];

            app.progress.show();

            $_list_inscription.children('li').remove();

            $.getJSON(resource, function(json, textStatus) {
                if (json) {
                    if (!json.error) {

                        $_input_programming.val($this.data('title'));

                        json.data.forEach(function(obj, index) {

                            var fullname = (obj.participant_name + " " + obj.participant_lastname).toLowerCase(),
                                item = _collection_item(obj, (index + 1));

                            elements_list.push({
                                textSearch: (hlp.remove_accent(fullname) + " " + obj.participant_dni),
                                elementList: item
                            });

                            if (index < 30) {
                                $_list_inscription.append(item);
                                item.find('button.btn-options').dropdown(opt.dropdown);
                                item.find('button.active-inscription')
                                    .on('click.app.inscription.module', _active_inscription);
                                item.find('button.delete-inscription').magnificPopup(mfp_delete);
                            }

                        });

                        // Copiar la data inicial a la data de filtros
                        elements_list_filter = elements_list;

                        if (json.data.length == 0) {
                            $_list_inscription.append('<li class="empty-list"><h4>Sin participantes</h4><p>Aun no se han registrado participantes a esta capacitación</p></li>');
                            $_search_participant.prop({
                                disabled: true
                            });
                        } else {
                            $_search_participant.prop({
                                disabled: false
                            });
                        }

                        if (!json.its_canceled) {
                            $('main').append(_button_create(json.url));
                        }
                    }
                }
            }).always(function() {
                $.magnificPopup.close();
                app.progress.close();
            });
        },

        _beforeOpen_md_delete = function() {
            var btn_active = $.magnificPopup.instance.st.el;

            $_btn_continue_del.data({
                'id': btn_active.data('id')
            });
            $_element_to_delete = btn_active.parents('li.collection-item');

            $_md_deletion.find('b.participant').text($_element_to_delete.find('.title').text());
            $_md_deletion.find('b.date-inscription').text($_element_to_delete.find('time').text());
        },

        _beforeOpen_md_participant = function() {
            var btn_active = $.magnificPopup.instance.st.el,
                resource = '/api/participant/' + btn_active.data('dni') + '.json';

            $_item_participant = btn_active.parent('li');

            $_md_participant.find('.fullname').text("");
            $_md_participant.find('.dni').text("");

            $_md_participant.append('<div class="progress"><div class="indeterminate"></div></div>');
            $_md_participant.find('.popup-body').slideUp(250);

            $.getJSON(resource, function(json, textStatus) {
                var item = json.data;
                
                data_participant = json.data;

                $_md_participant.find('.fullname').text(item.name + " " + item.lastname);
                $_md_participant.find('.dni').text(item.dni);
                $_md_participant.find('.gender').text((item.gender == 1) ? "Femenino" : "Masculino");
                $_md_participant.find('.profesion').text((item.profesion == null) ? "No Especifico" : item.profesion);
                $_md_participant.find('.cargo').text((item.position == null) ? "No Especifico" : item.position);
                $_md_participant.find('.departamento').text(item.departamento_name);
                $_md_participant.find('.provincia').text(item.provincia_name);
                $_md_participant.find('.distrito').text(item.distrito_name);
                $_md_participant.find('.celular').text((item.cellphone == null) ? "No Especifico" : item.cellphone);
                $_md_participant.find('.telefono').text((item.phone == null) ? "No Especifico" : item.phone);
                $_md_participant.find('.anexo').text((item.annex == null) ? "No Especifico" : item.annex);
                $_md_participant.find('.email').text(item.email);
                $_md_participant.find('.entidad').text(item.entity_name);
                $_md_participant.find('.subtipo').text(item.subtype_entity_name);
                $_md_participant.find('.tipo').text(item.type_entity_name);
                $_md_participant.find('.discapacidad').text(item.dispacity ? "SI" : "NO");
                $_md_participant.find('.detalle_discapacidad').html(hlp.nl2br(item.details_dispacity));
                $_md_participant.find('.creacion').text(moment(item.date_created).format('ddd DD MMMM YYYY h:mma'));

            }).always(function() {
                $_md_participant.find('.progress').remove();
                $_md_participant.find('.popup-body').slideDown(250);
            });
        },

        _afterClose_md_participant = function() {
            _cancel_edit_participant();
        },

        _delete_inscription = function() {
            var $this = $(this),
                id = $this.data('id'),
                resource = '/app/inscription/' + id + '/delete';

            var data_request = {};
            data_request[csrf.name] = csrf.value;

            $this.prop({
                disabled: true
            });

            $.post(resource, data_request, function(data, textStatus, xhr) {
                if (data) {
                    if (!data.error) {
                        Materialize.toast(data.message, 3000);

                        $_element_to_delete.slideUp(300, function() {

                            $_element_to_delete.remove();

                            if ($_list_inscription.children('li').length == 0) {
                                $_list_inscription.append('<li class="empty-list"><h4>Sin participantes</h4></li>');
                            }
                        });

                        $this.prop({
                            disabled: false
                        });
                    } else {
                        Materialize.toast(data.message, 3000);
                        $this.prop({
                            disabled: false
                        });
                    }
                } else {
                    Materialize.toast('Tu sessión ha terminado, Reiniciando...', 4000, '', function() {
                        location.reload(true);
                    });
                }

                app.progress.close();

            }).fail(function(objResponse, textStatus, responseText) {

                if (objResponse.readyState == 4) {

                    if (typeof objResponse.responseJSON != 'undefined') {
                        Materialize.toast(objResponse.responseJSON.message, 3000);
                    } else {
                        Materialize.toast('Ha ocurrido un error en el servidor, vuelve a intentarlo', 3000);
                    }

                    $this.prop({
                        disabled: false
                    });

                } else {
                    Materialize.toast('Ha ocurrido un error en el servidor, intentalo mas tarde', 4000, '', function() {
                        location.reload(true);
                    });
                }
            }).always(function() {
                app.progress.close();
                $.magnificPopup.close();
            });
        },

        _active_inscription = function() {
            var $this = $(this),
                parent = $this.parents('li.collection-item'),
                id = $this.data('id'),
                resource = '/app/inscription/' + id + '/active',
                data_request = {};

            $this.prop({
                disabled: true
            });
            app.progress.show();

            data_request[csrf.name] = csrf.value;

            $.post(resource, data_request, function(data, textStatus, xhr) {
                if (data) {
                    if (!data.error) {
                        Materialize.toast(data.message, 2500);

                        parent.find('.icons-status')
                            .removeClass('amber darken-3')
                            .addClass('green accent-4');

                        $this.parent('li').remove();
                    } else {
                        Materialize.toast(data.message, 3000);
                        $this.prop({
                            disabled: false
                        });
                    }
                } else {
                    Materialize.toast('Tu sessión ha terminado, Reiniciando...', 4000, '', function() {
                        location.reload(true);
                    });
                }

                app.progress.close();
            }).fail(function(objResponse, textStatus, responseText) {

                if (objResponse.readyState == 4) {

                    if (typeof objResponse.responseJSON != 'undefined') {
                        Materialize.toast(objResponse.responseJSON.message, 3000);
                    } else {
                        Materialize.toast('Ha ocurrido un error en el servidor, vuelve a intentarlo', 3000);
                    }

                    $this.prop({
                        disabled: false
                    });

                } else {
                    Materialize.toast('Ha ocurrido un error en el servidor, intentalo mas tarde', 4000, '', function() {
                        location.reload(true);
                    });
                }
            }).always(function() {
                app.progress.close();
            });
        },

        _change_filter = function() {
            var $this = $(this),
                valor = parseInt($this.children('option:selected').val());

            var data_request = {
                    filter_level: valor
                },
                resource = '/app/training/programming/filter';

            data_request[csrf.name] = csrf.value;

            $this.prop({
                disabled: true
            });
            app.progress.show();

            $.post(resource, data_request, function(data, textStatus, xhr) {
                if (data) {
                    location.reload(true);
                } else {
                    Materialize.toast('Tu sessión ha terminado, Reiniciando...', 2000, '', function() {
                        location.reload(true);
                    });
                }

                app.progress.close();

            }).fail(function(objResponse, textStatus, responseText) {

                if (objResponse.readyState == 4) {

                    if (typeof objResponse.responseJSON != 'undefined') {
                        Materialize.toast(objResponse.responseJSON.message + ". Reiniciando...", 3000);
                    } else {
                        Materialize.toast('Ha ocurrido un error en el servidor, vuelve a intentarlo', 3000);
                    }

                    $this.prop({
                        disabled: false
                    });

                } else {
                    Materialize.toast('Ha ocurrido un error en el servidor, intentalo mas tarde', 4000, '', function() {
                        location.reload(true);
                    });
                }

            }).always(function() {
                app.progress.close();
            });
        },

        _filter_participant = function() {

            var texto = hlp.remove_accent(this.value.toLowerCase());

            if (timeOutID != 0) clearTimeout(timeOutID);

            elements_list_filter = [];
            $_list_inscription.children('li').remove();

            timeOutID = _window.setTimeout(function() {

                var count = 0;

                elements_list.forEach(function(obj, index) {

                    if (obj.textSearch.indexOf(texto) != -1) {

                        elements_list_filter.push(obj);

                        if (count < 30) {
                            $_list_inscription.append(obj.elementList);
                            obj.elementList.find('button.btn-options').dropdown(opt.dropdown);
                            obj.elementList.find('button.active-inscription')
                                .on('click.app.inscription.module', _active_inscription);
                            obj.elementList.find('button.delete-inscription').magnificPopup(mfp_delete);
                            obj.elementList.find('.title').magnificPopup(mfp_participant);
                        }

                        ++count;
                    }
                })

            }, 300);
        },

        _detected_botton = function(e) {

            if ($_body.scrollTop() > (($_body.height() * 40) / 100)) {

                var lengthItems = $_list_inscription.children('li').length;

                if (lengthItems < elements_list.length) {
                    for (var i = lengthItems; i < (lengthItems + 30); i++) {

                        if (typeof(elements_list_filter[i]) != 'undefined') {

                            var item = elements_list_filter[i].elementList;
                            $_list_inscription.append(item);
                            item.find('button.btn-options').dropdown(opt.dropdown);
                            item.find('button.active-inscription')
                                .on('click.app.inscription.module', _active_inscription);
                            item.find('button.delete-inscription').magnificPopup(mfp_delete);
                            item.find('.title').magnificPopup(mfp_participant);

                        }
                    }
                }
            }
        },

        _collection_item = function(DATA, INDEX) {

            var fullname = INDEX + ". " + DATA.participant_name + " " + DATA.participant_lastname,
                date_real = moment(DATA.date_inscription).format('ddd DD MMMM YYYY h:mma');

            var jli = $('<li />', {
                    class: 'collection-item',
                    id: 'item_' + DATA.inscription_id,
                    'data-id': DATA.inscription_id,
                    'data-search': fullname + " " + DATA.participant_dni
                }),
                jtitle = $('<span />', {
                    class: 'title',
                    'data-dni': DATA.participant_dni,
                    'data-mfp-src': '#md-participant'
                }).text(fullname),
                jdni = $('<span />', {
                    class: 'dni'
                }).html('<b>DNI: </b>' + DATA.participant_dni),
                jemails = $('<span />', {
                    class: 'email'
                }).html('<b>Correo Electronico: </b>' + DATA.participant_emails),
                jdate = $('<span />').html('<b>Fecha de inscripción: </b><time>' + date_real + '</time>'),
                joptions = $('<button />', {
                    class: 'btn-options',
                    type: 'button',
                    'data-activates': 'options_' + DATA.inscription_id
                }).html('<i class="material-icons">&#xE5D4;</i>'),
                joption_cnt = _button_options(DATA.inscription_id, DATA.email_confirmed),
                jicons_status = $('<span />', {
                    class: 'icons-status ' + ((DATA.email_confirmed == 1) ? 'green accent-4' : 'amber darken-3')
                });

            jtitle.magnificPopup(mfp_participant);

            jli.append([joptions, joption_cnt, jicons_status, jtitle, jdni, jemails, jdate]);

            if (DATA.dispacity)
                jtitle.append($('<div>', {
                    class: 'chip disp',
                    title: 'Presenta una discapacidad'
                }).html('<i class="material-icons">&#xE914;</i>'));

            return jli;
        },

        _button_options = function(KEY, ACTIVATED) {
            var jul = $('<ul />', {
                    id: 'options_' + KEY,
                    class: 'dropdown-content'
                }),
                jliactive = $('<li />'),
                jbutton_active = $('<button />', {
                    type: 'button',
                    class: 'active-inscription',
                    'data-id': KEY
                }).html('<i class="material-icons">&#xE877;</i>Activar'),
                jlidelete = $('<li />'),
                jbutton_delete = $('<button />', {
                    type: 'button',
                    class: 'delete-inscription',
                    'data-mfp-src': "#md-delete-inscription",
                    'data-id': KEY
                }).html('<i class="material-icons">&#xE872;</i>Eliminar');

            jbutton_delete.magnificPopup(mfp_delete);

            jliactive.append(jbutton_active);
            jlidelete.append(jbutton_delete);

            if (ACTIVATED == 0)
                jul.append([jliactive, jlidelete]);
            else
                jul.append([jlidelete]);

            return jul;
        },

        _button_create = function(URL) {
            var jdiv_cnt = $('<div />', {
                    id: 'button_new',
                    class: 'fixed-action-btn tooltipped',
                    'data-delay': 50,
                    'data-position': "left",
                    'data-tooltip': "Programar nueva capacitación",
                    style: "bottom: 45px; right: 24px;"
                }),
                jlink = $('<a />', {
                    href: URL,
                    class: "btn-floating btn-large waves waves-effect"
                }),
                jicon = $('<i />', {
                    class: 'material-icons'
                }).html('&#xE145;');

            $('div#button_new').remove();

            jlink.append(jicon);
            jdiv_cnt.append(jlink);

            return jdiv_cnt;
        },

        _participant_to_modify = function() {
            var $this = $(this),
                edit_panel = $_md_participant.find('.edit');

            //Quitando el boton de update
            $this.remove();

            var btn_save_changes = $('<button />', {
                    type: 'submit',
                    form:'frm_edit_info',
                    id:'btn_update',
                    class:'btn-flat waves waves-effect'
                }).text('Guardar'),
                btn_cancel_changes = $('<button />', {
                    type: 'button',
                    id:'btn_cancel',
                    class:'btn-flat'
                }).text('Cancelar')
                .on('click.app.inscription.module', _cancel_edit_participant);

            $_md_participant.find('.popup-footer').append([btn_cancel_changes, btn_save_changes]);

            $_md_participant.find('.view').hide(400, function(){
                edit_panel.show(400, function(){
                    // agregando la accion al formulario
                    edit_panel.children('form').attr({
                        action: '/app/inscription/participant/'+ data_participant.id + '/update'
                    });

                    //llenando los inputs con los datos del participante
                    edit_panel.find('#txtdni').val(data_participant.dni).change();
                    edit_panel.find('#txtlastname').val(data_participant.lastname).change();
                    edit_panel.find('#txtname').val(data_participant.name).focus();
                });
            });
        },

        _send_data_participant_to_edit = function(e) {

            var $form = $(this),
                $btn_submit = $_md_participant.find('button[type=submit]'),
                name = $.trim($form.find('input#txtname').val()),
                lname = $.trim($form.find('input#txtlastname').val()),
                dni = $.trim($form.find('input#txtdni').val());
            // no debe estar vacio
            if ( name.length == 0 || lname.length == 0 || dni.length == 0 ) {
                Materialize.toast('Todos los campos son requeridos.', 2000);
                e.preventDefault();
                return false;
            }
            // el dni tiene que tener 8 diguitos numericos
            if( dni.length != 8 || !$.isNumeric(dni) ) {
                Materialize.toast('DNI requiere de 8 Digitos numericos.', 2000);
                e.preventDefault();
                return false;
            }
            // los nombres solo debe tener caracteres alfabeticos
            if( !hlp.is_alpha(name) || !hlp.is_alpha(lname) ) {
                Materialize.toast('Los nombres y apellidos solo deben contener caracteres alfabeticos', 2000);
                e.preventDefault();
                return false;
            }

            var data = {
                'name': name,
                'lastname': lname,
                'dni': dni
            };

            $btn_submit.prop({disabled:true});

            $.post($form.attr('action'), data, function(response, textStatus, xhr) {
                if( response ) {

                    //cambiando los datos que se muestran en el modal
                    $_md_participant.find('h4.title .fullname').html(name + ' ' + lname);
                    $_md_participant.find('h4.title small .dni').html(dni);

                    //reseteando toda la vista
                    _cancel_edit_participant();

                    //modificando en la lista general
                    var title = $_item_participant.children('.title').text().split(' ');

                    $_item_participant.children('.title')
                        .text(title[0] + ' ' + name + ' ' + lname)
                        .data({'dni':dni})
                        .attr({'data-dni':dni});

                    $_item_participant.children('.dni').html('<b>DNI:</b>' + dni);

                    // actualizando data guardada temporalmente
                    data_participant.name = name;
                    data_participant.lastname = lname;
                    data_participant.dni = dni;

                    Materialize.toast(response.message, 2000);

                } else {
                     Materialize.toast('Tu sessión ha terminado, Reiniciando...', 2000, '', function() {
                        location.reload(true);
                    });
                }
            });

            e.preventDefault();
        },

        _cancel_edit_participant = function(){
            var btn_edit = $('<button />', {
                    type:'button',
                    id: 'btn_edit_info',
                    class:'btn-flat waves waves-effect'
                }).text('Editar datos generales')
                    .on('click.app.docente.inscription.module', _participant_to_modify);

            $_md_participant.find('.popup-footer button').remove();
            $_md_participant.find('.popup-footer').append(btn_edit);

            $_md_participant.find('.edit').hide(200, function(){
                $_md_participant.find('.view').show(200);
                $_md_participant.find('.edit').find('input[type=text]').val('');
                $_md_participant.find('.edit form').removeAttr('action');
            });
        };

    return {
        init: _bind
    }

}(window, window.jQuery));

$(function() {
    if ($("html").attr('data-module') == 'index')
        app.inscription.init();
});