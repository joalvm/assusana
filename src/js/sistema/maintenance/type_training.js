app.training.type = (function(_window, $){
	'use strict';

	var opt 			= $.extend(true, {}, options);

	var $_btn_new 		= $("button.btn-floating, button.btn-update"),
		$_modal_mng 	= $('section#popup-manage'),
		$_modal_del 	= $('section#popup-delete'),
		$_form_mng 		= $_modal_mng.find("form"),
		$_form_del 		= $_modal_del.find("form"),
		$_txt_type 		= $_form_mng.find('input[type=text]#txt_type'),
		$_btn_save_mng 	= $_modal_mng.find('button#btn-save'),
		$_btn_save_del 	= $_modal_del.find('button#btn-save-del'),
		$_btn_cancel_del = $_modal_del.find('button#btn-cancel-del'),
		$_list_type 	= $("ul#list-type"),
		$_btn_more 		= $_list_type.find('button.btn-options'),
		$_btn_delete 	= $_list_type.find("button.btn-delete"),
		$_times 		= $('time');

		// Guarda la accion actual en la que se muestra el modal
	var $action 		= "",

		// guarda el item de la lista a la que pertenece el boton de
		// modificar el cual he pulsado - solo para modificar / Eliminar
		$li_current 	= {};

	var _bind = function() {

		opt.url = {
			create : "/app/maintenance/training/type/create",
			update : "/app/maintenance/training/type/{ID}/update",
			delete : "/app/maintenance/training/type/{ID}/delete",
			find : "/app/maintenance/training/type/{ID}/find.json"
		};

		// Configuraciones para el modal de mannage(create/update)
		var opt_mfp_mng = $.extend(true, {}, opt.magnificPopup);

		//Configuraciones para el modal de mannage(delete)
		var opt_mfp_del = $.extend(true, {}, opt.magnificPopup);

		opt_mfp_mng.focus = $_txt_type;
		opt_mfp_mng.callbacks.beforeOpen 	= _beforeOpen_modal;
		opt_mfp_mng.callbacks.beforeClose 	= _beforeClose_modal;

		$_btn_new.magnificPopup(opt_mfp_mng);

		opt_mfp_del.showCloseBtn = false;
		opt_mfp_del.enableEscapeKey = false;
		opt_mfp_del.callbacks.beforeOpen = _beforeOpen_modal_delete;
		$_btn_delete.magnificPopup(opt_mfp_del);

		$_btn_cancel_del.on('click.app.training.type.module', _close_modal_del);
		$_form_mng.on('submit.app.training.type.module', _submit_form_mng);
		$_form_del.on('submit.app.training.type.module', _submit_form_del);
		$_txt_type.on('change.app.training.type.module', _validate_input);

		$_btn_more.dropdown(opt.dropdown);

		moment.locale('es');
		opt.datetimeHumanize();
	},

	_beforeOpen_modal = function() {
		var btn_active = $.magnificPopup.instance.st.el;

		switch (btn_active.data('action')) {
			case "create":

				$_form_mng.attr('action', opt.url.create)
					.find('h4.title')
						.text("Nuevo");

				$action = 1;

			break;
			case "update":

				var resource_id = btn_active.data('id'),
					url_action 	= opt.url.update.replace(/{ID}/gi, resource_id);

				$_form_mng.attr('action', url_action)
					.find('h4.title')
						.text("Modificar");

				$_modal_mng.find('.popup-header')
					.append('<div class="progress"><div class="indeterminate"></div></div>');

				$_btn_save_mng.prop({disabled:true});
				$_txt_type.prop({disabled:true});
				$_form_mng.find('textarea').prop({disabled:true});

				$action = 2;

				$li_current = btn_active.parents('li.collection-item');

				_get_type_workshop(resource_id);

			break;
		}
	},

	_beforeClose_modal = function() {
		$_form_mng.removeAttr('action');
		$_txt_type.val("").removeClass('valid invalid');

		// Vuelve el textarea a su tamaño original al no detectar dato
		$_form_mng.find('textarea').val("").keyup();

		$_btn_save_mng.prop({disabled:false});
		$_txt_type.prop({disabled:false});
		$_form_mng.find('textarea').prop({disabled:false});
	},

	_beforeOpen_modal_delete = function() {
		var btn_active = $.magnificPopup.instance.st.el,
			resource_id = btn_active.data('id'),
			url_action 	= opt.url.delete.replace(/{ID}/gi, resource_id);

		$li_current = btn_active.parents('li.collection-item');

		var text 	= $li_current.find('a.name').text(),
			message = $_modal_del.find('p.message').html();

		$_modal_del.find('p.message').html( message.replace(/{TYPE}/gi, text) );

		$_form_del.attr('action', url_action);
	},

	_validate_input = function() {
		var $this = $(this);
		if ( $this.val().length > 0 )
		{
			if ( ($.trim($this.val())).length == 0 ) {
				$this.removeClass('valid');
				$this.addClass('invalid');
				$_btn_save_mng.prop({'disabled': true});
			} else {
				$this.removeClass('invalid');
				$this.addClass('valid');
				$_btn_save_mng.removeProp('disabled').removeAttr('disabled');
			}
		} else {
			$this.removeClass('valid invalid');
		}
	},

	_submit_form_mng = function( e ) {
		var request = {
			typeText : $.trim($_txt_type.val()),
			description: $.trim($_form_mng.find('textarea').val())
		};

		request[csrf.name] = csrf.value;

		$_btn_save_mng.prop({disabled:true});
		$_txt_type.prop({disabled:true});
		$_form_mng.find('textarea').prop({disabled:true});
		app.progress.show();

		$.post($_form_mng.attr('action'), request, function(response, textStatus, xhr) {

			if ( response ) {

				if ( ! response.error ) {

					// si la orden a sido crear
					if ( $action == 1 )
					{
						// Add Item to list
						_add_to_list(response.data);

						// Clear Form
						$_txt_type.val("").removeClass('valid invalid')

						$_txt_type.prop({disabled:false});
						$_form_mng.find('textarea').prop({disabled:false});

						$_txt_type.siblings('label').removeClass('active');
						$_form_mng.find('textarea').val("").keyup();

						// Show Message
						Materialize.toast(response.message, 5000);

						$_txt_type.focus();
					} else {
						$li_current.find('.name').text($_txt_type.val());
						$li_current.find('time').text(moment(Date.now()).calendar());

						$_txt_type.prop({disabled:false});
						$_form_mng.find('textarea').prop({disabled:false});

						// Show Message
						Materialize.toast(response.message, 5000);

						$.magnificPopup.close();
					}
				}

				$_btn_save_mng.removeProp('disabled').removeAttr('disabled');

			} else {
				document.location.reload(true);
			}

		}).fail(function(data, text, textStatus) {
			if ( data.status >= 500 )
			{
				Materialize.toast("Ocurrio un error en nuestro servidor, por favor intentalo mas tarde.", 5000);
			} else {
				Materialize.toast(data.responseJSON.message, 5000);
			}

			$.magnificPopup.close();
		}).always(function() {
			app.progress.close();
		});

		e.preventDefault();
	},

	_submit_form_del = function( e ) {
		var request = {};

		request[csrf.name] = csrf.value;

		$_btn_save_del.prop({disabled:true});

		app.progress.show();

		$.post($_form_del.attr('action'), request, function(response, textStatus, xhr) {

			if ( response ) {
				if ( ! response.error ) {

					// REMOVER LA LISTA
					$li_current.hide('500', function() {
						$li_current.remove();

						// Agregando visión de lista vacia
						if( $_list_type.children('li.collection-item').length == 0 )
							$_list_type.append($('<li class="collection-item empty"><p>No se registran datos.</p></li>'));
					});

					// Show Message
					Materialize.toast(response.message, 4000);

					$.magnificPopup.close();
				}

				$_btn_save_del.prop({disabled:false});

			} else {
				document.location.reload(true);
			}

		}).fail(function(data, text, textStatus){
			if ( data.status >= 500 )
			{
				Materialize.toast("Ocurrio un error en nuestro servidor, por favor intentalo mas tarde.", 5000);
			} else {
				Materialize.toast(data.responseJSON.message, 5000);
			}

			$.magnificPopup.close();
		}).always(function() {
			app.progress.close();
		});

		e.preventDefault();
	},

	_close_modal_del = function () {
		$.magnificPopup.close();
	},

	_add_to_list = function(data) {
		var li = $('<li />', {class: 'collection-item'}),
			name = $('<a />', {class:'title name'}).text($_txt_type.val()),
			span_time = $('<span />', {class:'last-modified'}).text('Modificado: '),
			time = $('<time />', {class:'last-modified'}).text(moment(Date.now()).calendar()),
			btn_more_opt = $('<button />', {
				type:"button",
				class:"btn-options",
				"data-activates": ('options_' + data)
			});

		span_time.append([time]);
		li.append([name, span_time, btn_more_opt]);
		$_list_type.append([li]);
	},

	_get_type_workshop = function( ID ) {
		var url_resource = opt.url.find.replace(/{ID}/gi, ID);
		$.getJSON(url_resource, function(json, textStatus) {
				if ( json ) {
					if( ! $.isArray(json.data) ) {

						$_txt_type.val(json.data.name)
							.prop({disabled:false})
							.siblings('label')
								.addClass('active');
						$_form_mng.find('textarea')
							.val(json.data.description)
							.prop({disabled:false})
							.keyup();

						if ( json.data.description != null )
							$_form_mng.find('textarea').siblings('label').addClass('active');

						$_btn_save_mng.prop({disabled:false});

						$_form_mng.find('.progress').remove();

						$_txt_type.focus();
					}
				} else {
					Materialize.toast("Tu sesión a caducado, vuelve a iniciar sesión", 5000, function(){
						document.location.reload(true);
					});
				}
		});
	};

	return {
		init : _bind
	};

}(window, window.jQuery));

$(function() {
	app.training.type.init();
});
