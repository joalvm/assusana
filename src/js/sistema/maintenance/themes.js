app.themes = ( function( _window, $ ) {

	'use strict';

	var config = {
		url : {
			theme : {
				find: '/app/maintenance/themes/{IDT}/find.json',
				create: '/app/maintenance/themes/create',
				update: '/app/maintenance/themes/{IDT}/update',
				delete: '/app/maintenance/themes/{IDT}/delete'
			},
			subtheme : {
				find: '/app/maintenance/themes/{IDT}/subtheme/{IDS}/find.json',
				create: '/app/maintenance/themes/{IDT}/subtheme/create',
				update: '/app/maintenance/themes/{IDT}/subtheme/{IDS}/update',
				delete: '/app/maintenance/themes/{IDT}/subtheme/{IDS}/delete'
			}
		}
	};

	var opt 					= $.extend(true, {}, options),
	hlp 					= $.extend(true, {}, helper);

	// 	MODAL PARA EL MANEJO CREATE/UPDATE DE LOS TEMAS
	var mfp_themes 				= $.extend(true, {}, opt.magnificPopup),

		// MODAL PARA EL MANEJO CREATE/UPDATE DE LOS SUBTEMAS
		mfp_subthemes 		= $.extend(true, {}, opt.magnificPopup),
		mfp_delete 			= $.extend(true, {}, opt.magnificPopup);

		var $_md_theme 				= $('section#popup-theme'),
		$_form_theme 			= $_md_theme.find('form'),
		$_btn_mng_theme 		= $('button#mng-theme, button.mng-theme'),

		$_md_subtheme 			= $('section#popup-subtheme'),
		$_form_subtheme 		= $_md_subtheme.find('form'),
		$_btn_mng_subtheme 		= $('button.mng-subtheme'),

		$_btn_mng_delete 		= $('button.btn-delete'),
		$_md_delete 			= $('section#popup-delete'),
		$_form_delete 			= $_md_delete.find('form'),

		$_btn_options 			= $('button.btn-options'),
		$_list_themes 			= $('ul#list-themes');

	// 	INICIANDO ELEMENTOS
	var _bind = function() {

		mfp_themes.callbacks.beforeOpen = _beforeOpen_mng_theme;
		mfp_themes.focus ='input[type=text]';
		$_btn_mng_theme.magnificPopup(mfp_themes);

		mfp_subthemes.callbacks.beforeOpen = _beforeOpen_mng_subtheme;
		mfp_subthemes.focus = 'input[type=text]';
		$_btn_mng_subtheme.magnificPopup(mfp_subthemes);

		mfp_delete.callbacks.beforeOpen = _beforeOpen_delete;
		$_btn_mng_delete.magnificPopup(mfp_delete);

		$_form_theme.on('submit.app.themes.module', _submit_theme);
		$_form_subtheme.on('submit.app.themes.module', _submit_subtheme);
		$_form_delete.on('submit.app.themes.module', _submit_delete);

		$_btn_options.dropdown(opt.dropdown);

		hlp.humanize_DateTime();

	},

	_beforeOpen_mng_theme = function () {

		var btn_active = $.magnificPopup.instance.st.el,
		action = btn_active.data('action');

		if ( action == 'create' ) {
			$_form_theme.find('h4.title').text('Nuevo Tema');
			$_form_theme.find('input, textarea').val('').keydown();
		} 
		else {
			$_form_theme.find('h4.title').text('Modificar Tema');

			$_form_theme.find('input, textarea')
			.val('')
			.prop({disabled:true})
			.change();

			$_form_theme.find('button[type=submit]').prop({disabled:true});

			var resource = config.url.theme.find.replace(/{IDT}/gi, btn_active.data('id'));


			$.getJSON(resource, function(json, textStatus) {

				if ( json ) {

					if ( ! json.error ) {

						$_form_theme.find('input').val(json.data.title).change();
						$_form_theme.find('textarea').val(json.data.description).change();

						$_form_theme.find('input, textarea, button[type=submit]').prop({disabled:false});

					} else {
						Materialize.toast( json.message, 3000 );
					}

				} else {
					Materialize.toast("Tu sesión a caducado, Reiniciando...", 3000, function() {
						location.reload(true);
					});
				}

			});
		}

		$_form_theme.data({
			'action': action,
			'theme': btn_active.data('id')
		});

	},

	_submit_theme = function(e) {

		var $this 			= $(this),
		action 				= $this.data('action'),
		url 					= config.url.theme[action],
		title 				= $.trim($this.find('#txt_title').val()),
		description 	= $.trim($this.find('#txt_description').val()),
		data_request 	= {};

		if ( title.length == 0 ) {

			Materialize.toast('Ingrese un titulo al tema', 2000);
			$this.find('#txt_title').focus();

			e.preventDefault();
			return false;

		}

		if ( action == 'update' )
			url = url.replace(/{IDT}/gi, $this.data('theme'));

		data_request = {
			title : title,
			description: description
		};

		$this.find('button[type=submit]').prop({disabled:true});

		$.post( url, data_request, function( data, textStatus, xhr ) {
			
			if( data ) {

				if( ! data.error ) {

					if( action == 'create' ) {

						Materialize.toast(data.message, 2500);

						var item = _get_item_theme( data.ID, title, description );

						$_list_themes.append(item);

						item.find('button.btn-options').dropdown(opt.dropdown);

						$this.find('input, textarea').val('').change().keydown();
						$this.find('button[type=submit]').prop({disabled:false});

					} else {
						Materialize.toast(data.message, 2500, '', function () {
							location.reload(true);
						});
					}

				} else {
					Materialize.toast(data.message, 2500);
					$this.find('button[type=submit]').prop({disabled:false});
				}

			} else {
				Materialize.toast('Tu sesión ha caducado, reiniciando...', 2500, '', function () {
					location.reload(true);
				});
			}

		}).fail(function(objResponse, textStatus, responseText) {
			if ( objResponse.readyState == 4 )
				$this.find('button[type=submit]').prop({disabled:false});
		});

		e.preventDefault();

	},

	_beforeOpen_mng_subtheme = function () {

		var btn_active 	= $.magnificPopup.instance.st.el,
		action 			= btn_active.data('action');

		if( action == 'create' ) {

			$_form_subtheme.find('h4.title').text('Nuevo Subtema');
			$_form_subtheme.find('input, textarea').val('').keydown();

		} else {

			$_form_subtheme.find('h4.title').text('Modificar Subtema');

			$_form_subtheme.find('input, textarea').val('')
			.prop({disabled:true})
			.change();

			$_form_subtheme.find('button[type=submit]').prop({disabled:true});

			var resource = config.url.subtheme.find.replace(/{IDS}/gi, btn_active.data('id'));

			resource = resource.replace(/{IDT}/gi, btn_active.data('theme'));

			$.getJSON(resource, function(json, textStatus) {

				if ( json ) {

					if ( ! json.error ) {
						
						$_form_subtheme.find('input').val(json.data.title).change();
						$_form_subtheme.find('textarea').val(json.data.description).change();

						$_form_subtheme.find('input, textarea, button[type=submit]').prop({disabled:false});

					} else {
						
						Materialize.toast(json.message, 3000);

					}

				} else {

					Materialize.toast("Tu sesión a caducado, Reiniciando...", 3000, function() {
						location.reload(true);
					});

				}

			});
		}

		$_form_subtheme.data({
			id: btn_active.data('id'),
			theme: btn_active.data('theme'),
			action: action
		});

	},

	_submit_subtheme = function (e) {

		var $this 				= $(this),
		action 			= $this.data('action'),
		url 			= config.url.subtheme[action],
		title 			= $.trim($this.find('#txt_title').val()),
		description 	= $.trim($this.find('#txt_description').val()),
		data_request 	= {};

		if ( title.length == 0 ) {

			Materialize.toast('Ingrese un titulo al subtema', 2000);
			$this.find('#txt_title').focus();

			e.preventDefault();
			return false;
		}

		var resource = url.replace(/{IDT}/g, $this.data('theme'));

		if( action == 'update' )
			resource = resource.replace(/{IDS}/gi, $this.data('id'));

		data_request = {
			title : title,
			description: description
		};

		$this.find('button[type=submit]').prop({disabled:true});

		$.post(resource, data_request, function(data, textStatus, xhr) {

			if( data ) {

				if( ! data.error ) {

					if( action == 'create' ) {

						Materialize.toast(data.message, 2500);

						var jul_theme 	= $_list_themes.find('#theme_' + $this.data('theme')),
						jul_subtheme 	= jul_theme.find('ul.list-subthemes'),
						item_st 			= _get_item_subtheme(data.ID, $this.data('theme'), title, description);

						jul_subtheme.children('li.empty').remove();

						jul_subtheme.append(item_st);

						item_st.find('button.btn-options').dropdown(opt.dropdown);

						$_form_subtheme.find('input, textarea').val('');

						$this.find('button[type=submit]').prop({disabled:false});

					} else {
						Materialize.toast(data.message, 2500, '', 
							function () {
								location.reload(true);
							});
					}

				} else {

					Materialize.toast(data.message, 2500);
					$this.find('button[type=submit]').prop({disabled:false});

				}

			} else {

				Materialize.toast('Tu sesión a concluido, reiniciando...', 2500, '', function () {
					location.reload(true);
				});

			}

		}).fail(function(objResponse, textStatus, responseText) {

			if ( objResponse.readyState == 4 )
				$this.find('button[type=submit]').prop({disabled:false});

		});

		e.preventDefault();

	},

	_beforeOpen_delete = function(e) {

		var btn_active 	= $.magnificPopup.instance.st.el,
			action 		= btn_active.data('action'),
			jerarquia 	= btn_active.data('hierarchy'),
			msg 		= "",
			jerq 		= "";

		if ( jerarquia == 'theme' ) {

			jerq = "tema";
			
			$_form_delete.data({ id: btn_active.data('id') });

			var resource = config.url.theme.delete.replace(/{IDT}/gi, btn_active.data('id'));
			$_form_delete.attr({action:resource});

		} else {

			jerq = "subtema";

			$_form_delete.data({
				id: btn_active.data('id'),
				theme: btn_active.data('theme')
			});

			var resource = config.url.subtheme.delete.replace(/{IDT}/gi, btn_active.data('theme'));
			resource = resource.replace(/{IDS}/gi, btn_active.data('id'));

			$_form_delete.attr({action:resource});

		}

		msg = "El " + jerq + ": <b>"+btn_active.data('title')+"</b>, será eliminado del sistema de forma permanente.";

		$_md_delete.find('p.message').html(msg);

	},

	_submit_delete = function(e) {

		var $this = $(this);

		$this.find('button[type=submit]').prop({disabled:true});

		$.post($this.attr('action'), $this.data(), function(data, textStatus, xhr) {

			if( data ) {

				if( ! data.error ) {

					$.magnificPopup.close();

					Materialize.toast(data.message, 2000, '', function () {
						location.reload(true);
					});

				} else {

					Materialize.toast(data.message, 2500);
					$this.find('button[type=submit]').prop({disabled:false});

				}

			} else {

				Materialize.toast('Tu sesión a concluido, reiniciando...', 2000, '', function () {
					location.reload(true);
				});

			}

		}).fail(function(objResponse, textStatus, responseText) {

			if ( objResponse.readyState == 4 )
				$this.find('button[type=submit]').prop({disabled:false});

		});

		e.preventDefault();
	},

	_get_item_theme = function ( ID, TITLE, DESCRIPTION ) {

		var jli 					= $('<li />', {class:'collection-item', id:'theme_' + ID}),

		jtitle 					= $('<a />', {class:'title name'}).text(TITLE),

		jdescr 					= $('<p />', {class:'description'}).text(hlp.nl2br(DESCRIPTION)),

		joptions				= $('<button />', {
			type: 'button',
			class:'btn-options',
			'data-activates': 'options_' + ID
		}).html('<i class="material-icons">&#xE5D4;</i>'),

		jheader_stheme 	= $('<span />', {class:'headertext-stheme'}).text('Subtemas'),

		jlist_sthemes 	= $('<ul />', {class:'list-subthemes collection collection-main'}),

		jlitem_empty 	= $('<li class="empty"><p>No se han incluido subtemas</p></li>');

		jlist_sthemes.append([jlitem_empty]);

		jli.append([
			joptions, 
			_get_options_theme(ID), 
			jtitle, 
			jdescr, 
			jheader_stheme, 
			jlist_sthemes
			]);

		return jli;

	},

	_get_item_subtheme = function( ID, ID_THEME, TITLE, DESCRIPTION ) {

		var jli 			= $('<li />', {class:'collection-item', id:'subtheme_' + ID}),

		jtitle 		= $('<h5 />', {class:'title'}).text(TITLE),

		jdescr 		= $('<p />', {class:'description'}).text(hlp.nl2br(DESCRIPTION)),

		joptions 	= $('<button />', {
			type: 'button',
			class:'btn-options',
			'data-activates': 'options_subtheme_' + ID
		}).html('<i class="material-icons">&#xE5D4;</i>');

		jli.append([
			joptions, 
			_get_options_subtheme(ID, ID_THEME), 
			jtitle, 
			jdescr
			]);

		return jli;

	},

	_get_options_theme = function ( ID ) {

		var jul = $('<ul />', {class:'dropdown-content', id:'options_' + ID}),

		jli_stheme = $('<li />'),

		jli_stheme_btn = $('<button />', {
			type: 'button',
			class: 'mng-subtheme',
			'data-action': 'create',
			'data-theme': ID,
			'data-mfp-src':'#popup-subtheme'
		}).html('<i class="material-icons">&#xE145;</i>Subtema'),

		jli_modify = $('<li />'),

		jli_modify_btn = $('<button />', {
			type: 'button',
			class: 'mng-theme',
			'data-action': 'update',
			'data-theme': ID,
			'data-mfp-src':'#popup-theme'
		}).html('<i class="material-icons">&#xE150;</i>Modificar'),

		jli_delete = $('<li />'),

		jli_delete_btn = $('<button />', {
			type: 'button',
			class: 'mng-delete',
			'data-action': 'delete',
			'data-theme': ID,
			'data-mfp-src':'#popup-delete'
		}).html('<i class="material-icons">&#xE872;</i>Eliminar');

		jli_modify_btn.magnificPopup(mfp_themes);
		jli_stheme_btn.magnificPopup(mfp_subthemes);

		jli_stheme.append(jli_stheme_btn);
		jli_modify.append(jli_modify_btn);
		jli_delete.append(jli_delete_btn);

		jul.append([jli_stheme, $('<li />', {class:'divider'}), jli_modify, jli_delete]);

		return jul;

	},

	_get_options_subtheme = function ( ID, IDTHEME ) {

		var jul = $('<ul />', {class:'dropdown-content', id:'options_subtheme_' + ID}),

		jli_modify = $('<li />'),

		jli_modify_btn = $('<button />', {
			type: 'button',
			class: 'mng-subtheme',
			'data-action': 'update',
			'data-theme': IDTHEME,
			'data-id': ID,
			'data-mfp-src':'#popup-subtheme'
		}).html('<i class="material-icons">&#xE150;</i>Modificar'),

		jli_delete = $('<li />'),

		jli_delete_btn = $('<button />', {
			type: 'button',
			class: 'mng-delete',
			'data-action': 'delete',
			'data-theme': IDTHEME,
			'data-id': ID,
			'data-mfp-src':'#popup-delete'
		}).html('<i class="material-icons">&#xE872;</i>Eliminar');

		jli_modify_btn.magnificPopup(mfp_subthemes);

		jli_modify.append(jli_modify_btn);
		jli_delete.append(jli_delete_btn);

		jul.append([jli_modify, jli_delete]);

		return jul;

	};

	return { init: _bind };

}( window, window.jQuery ) );

$(function() {
	app.themes.init();
});
