app.entities = {};

app.entities = (function(_window, $) {

	'use strict';

	var opt = $.extend(true, {}, options),
		hlp = $.extend(true, {}, helper),
		data_default = {
			type_id: 0,
			type_name: '',
			subtype_id: 0,
			subtype_name: '',
			entity_name: ''
		},
		data_send = {};

	var mfp_create = $.extend(true, {}, opt.magnificPopup);
	

	var elements_list 	= [],
		elements_list_filter = [],
		entities 		= [],
		type_entities 	= [],
		stype_entities 	= [],
		timeOutID 		= 0;

	var $_list_entities = $('ul#list-entities'),
		$_search_input 	= $('input[type=search]#search_entities'),
		$_cbo_types 	= $('select#cbotypes'),
		$_cbo_stypes 	= $('select#cbosubtypes'),

		$btn_add 		= $('button#btn-add'),
		$_md_create 	= $('section#popup-manage'),
		$_chk_type 		= $_md_create.find('input[type=checkbox]#chk_newtype'),
		$_chk_subtype 	= $_md_create.find('input[type=checkbox]#chk_newsubtype'),
		$_input_entity 	= $_md_create.find('input[type=text]#txt_entity'),
		$_btn_save 		= $_md_create.find('button#btn-save'),
		$_container 	= $(document),
		$_body 			= $('body');

	var _bind = function() {

		mfp_create.callbacks.beforeOpen = _beforeOpen_md_create;
		$btn_add.magnificPopup(mfp_create);

		$_container.on('scroll.app.entities.module', _detected_botton);
		$_search_input.on('keyup.app.entities.module', _filter_entities);
		$_cbo_types.on('change.app.entities.module', _change_types);
		$_cbo_stypes.on('change.app.entities.module', _change_subtypes);
		$_chk_type.on('change.app.entities.module', _change_insert_type);
		$_chk_subtype.on('change.app.entities.module', _change_insert_subtype);
		$_input_entity.on('change.app.entities.module', _watch_changes_data);
		$_btn_save.on('click.app.entities.module', _save_entity );

		_load_entities();
	},

	_save_entity = function() {
		var $this = $(this),
			resource = '/app/maintenance/entities/create';

		data_send = $.extend(true, data_default, data_send);
		
		if ( ! _verify_data_before_send() ) {
			Materialize.toast('Faltan datos antes de enviar', 3000);
			return false;
		}

		$this.prop({disabled:true});
		app.progress.show();

		data_send[csrf.name] = csrf.value;

		$.post(resource, data_send, function(data, textStatus, xhr) {
			
			if ( data )
			{
				if ( ! data.error ) {
					Materialize.toast(data.message, 2000, '', function(){
						location.reload(true);
					});
				} else {
					Materialize.toast(data.message, 3000);
					$this.prop({disabled:false});
				}
			} else {
				Materialize.toast('Tu sessión ha terminado, Reiniciando...', 3000, '', function(){
					location.reload(true);
				});
			}
			
		}).fail(function(objResponse, textStatus, responseText) {
			
			if ( objResponse.readyState == 4 ) {

				if ( typeof objResponse.responseJSON != 'undefined' ) {
					Materialize.toast(objResponse.responseJSON.message, 3000);
				} else {
					Materialize.toast('Ha ocurrido un error en el servidor, vuelve a intentarlo', 4000);
				}

				$_btn_submit.prop({disabled:false});

			} else {
				Materialize.toast('Ha ocurrido un error en el servidor, intentalo mas tarde', 4000, '', function(){
					location.reload(true);
				});
			}
			
		}).always(function(){
			app.progress.close();
		});
	},

	_verify_data_before_send = function() {

		if ( data_send.type_id == 0 && data_send.type_name == '' ) {
			return false;
		}

		if ( data_send.subtype_id == 0 && data_send.subtype_name == '' ) {
			return false;
		}

		if ( data_send.entity_name == '' ) {
			return false;
		}

		return true;
	},

	_watch_changes_data = function() {
		
		var $this = $(this),
			name = $this.attr('name'),
			tag = $this.prop("tagName").toLowerCase();

		if (tag == 'input') {
			data_send[name+"_name"] = $.trim($this.val());
			if ( name != 'entity' ) {
				data_send[name + "_id"] = 0;
			}
		} else {
			data_send[name + "_id"] = parseInt($this.children('option:selected').val());
			data_send[name+"_name"] = "";
		}

	},

	_load_entities = function() {

		elements_list = [];
		elements_list_filter = [];

		app.progress.show();

		$.getJSON('/api/entities/expanded.json', function(json, textStatus) {

			type_entities = json.type_entities;
			stype_entities = json.subtype_entities;
			entities = json.entities;

			json.entities.forEach( function(obj, index) {

				var stype 	= json.subtype_entities[obj.stype_ps],
					type 	= json.type_entities[stype.type_ps],
					litem 	= _get_item_list();

				var search = hlp.remove_accent((obj.entity + " " + obj.cue + " " + type.name + " " + stype.name).toLowerCase());

				litem.attr({ 'data-id' : obj.id });

				litem.find('span.entity').attr({
					title: obj.entity
				}).html(obj.entity);

				litem.find('span.subtype').html('<b>Subtipo:</b>' + stype.name);
				litem.find('span.type').html('<b>Tipo:</b>' + type.name);

				elements_list.push({
					textSearch : search,
					element: litem
				});

				if ( index < 50 ) $_list_entities.append(litem);
			});

			// Copiar la data inicial a la data de filtros
			elements_list_filter = elements_list;

			json.type_entities.forEach( function(obj, index) {
				$_cbo_types.append($('<option />', {
					value: obj.id,
					'data-index': index
				}).text(obj.name));
			});

		}).always(function() {
			app.progress.close();
		});
	},

	_change_types = function() {
		var $this = $(this),
			value = parseInt($this.children('option:selected').val());

		$_cbo_stypes.children('option:not(:eq(0))').remove();

		if ( value != 0 ) {
			stype_entities.forEach( function(obj, index) {
				if ( obj.type_id == value ) {
					$_cbo_stypes.append($('<option />', {
						value: obj.id
					}).text(obj.name));
				}
			});
		}

		$_cbo_stypes.change();
	},

	_change_subtypes = function() {
		var $this = $(this),
			count = 0,
			stype_id = parseInt($this.children('option:selected').val()),
			type_id = parseInt($_cbo_types.children('option:selected').val());

		elements_list 			= [];
		elements_list_filter 	= [];

		if ( timeOutID != 0) clearTimeout(timeOutID);

		$_list_entities.children('li').remove();

		timeOutID = _window.setTimeout(function() {

			entities.forEach( function(obj, index) {

				var stype 	= stype_entities[obj.stype_ps],
					type 	= type_entities[stype.type_ps],
					litem 	= _get_item_list();

				litem.attr({
					'data-search' : (obj.entity + " " + obj.cue),
					'data-id' : obj.id
				});

				litem.find('span.entity').attr({ title: obj.entity }).html(obj.entity);
				litem.find('span.subtype').html('<b>Subtipo:</b>' + stype.name);
				litem.find('span.type').html('<b>Tipo:</b>' + type.name);

				if ( stype_id == 0 ) {
					if ( type_id == 0 ) {
						elements_list.push(litem);
						if ( count < 50 ) $_list_entities.append(litem);
						++count;
					}else if ( type.id == type_id ) {
						elements_list.push(litem);
						if ( count < 50 ) $_list_entities.append(litem);
						++count;
					}
				} else if( obj.stype_id == stype_id ) {
					elements_list.push(litem);
					if ( count < 50 ) $_list_entities.append(litem);
					++count;
				}
			});

			// Copiar la data inicial a la data de filtros
			elements_list_filter = elements_list;

		}, 300);
	},

	_beforeOpen_md_create = function() {
		$_chk_type.prop({checked:false}).change();
		$_chk_subtype.prop({checked:false}).change();
	},

	_change_insert_type = function() {
		var $this = $(this);

		if ( $this.is(':checked') ) {
			var input_type = $('<input />', { type: 'text', name:'type', id: 'txt_type_create' }),
				label_type = $('<label />', {for:'txt_type_create'}).text('Tipo de Entidad');

			input_type.on('change.app.entities.module', _watch_changes_data);

			$_md_create.find('div#cnt-types').html('').append([input_type, label_type]);

			$_chk_subtype.prop({checked:true}).change();

			input_type.focus();
		} else {
			var select_type = $('<select />', { name:'type', class:'browser-default', id: 'cbo_type_create' }),
				label_type = $('<label />', {for:'cbo_type_create', class:'active'}).text('Tipo de Entidad');

			select_type.append($('<option />', {
				value: 0, 
				disabled:true, 
				selected:true
			}).text('Escoja su opción'));
			
			

			type_entities.forEach( function(obj, index) {
				select_type.append($('<option />', {value:obj.id}).text(obj.name));
			});

			select_type.on('change.app.entities.module', _choise_insert_type);
			select_type.on('change.app.entities.module', _watch_changes_data);

			$_md_create.find('div#cnt-types').html('').append([select_type, label_type]);
		}

		data_send['type_name'] = '';
		data_send['type_id'] = 0;
	},

	_choise_insert_type = function() {
		var $this = $(this),
			value = parseInt($this.children('option:selected').val()),
			cbo_subtype_create = $_md_create.find('select#cbo_subtype_create');

		if ( cbo_subtype_create.length > 0 ) {

			cbo_subtype_create.children('option:not(:eq(0))').remove();
			cbo_subtype_create.children('option:eq(0)').prop({selected:true});

			stype_entities.forEach( function(obj, index) {
				if ( obj.type_id == value ) {
					cbo_subtype_create.append($('<option />', {
						value: obj.id
					}).text(obj.name));
				}
			});
		}
	},

	_change_insert_subtype = function() {
		var $this = $(this),
			label_subtype = $('<label />').text('Subtipo de Entidad');

		if ( $this.is(':checked') ) {
			var input_subtype = $('<input />', { 
				type: 'text', 
				name:'subtype', 
				id: 'txt_subtype_create' 
			});

			input_subtype.on('change.app.entities.module', _watch_changes_data);

			label_subtype.prop({for:'txt_subtype_create'});

			$_md_create.find('div#cnt-subtypes')
				.html('')
				.append([input_subtype, label_subtype]);
		} else {
			var select_subtype = $('<select />', { 
				name:'subtype', 
				class:'browser-default', 
				id: 'cbo_subtype_create' 
			});

			label_subtype.prop({for:'cbo_subtype_create', class:'active'});

			select_subtype.append($('<option />', {
				value: 0,
				disabled:true,
				selected:true
			}).text('Escoja su opción'));
			
			select_subtype.on('change.app.entities.module', _watch_changes_data);
			
			$_md_create.find('div#cnt-subtypes').html('').append([select_subtype, label_subtype]);

			$_md_create.find('select#cbo_type_create').change();
		}

		data_send['subtype_name'] = '';
		data_send['subtype_id'] = 0;
	},

	_filter_entities = function() {

		var texto = hlp.remove_accent(this.value.toLowerCase());

		if ( timeOutID != 0) clearTimeout(timeOutID);

		elements_list_filter = [];
		$_list_entities.children('li').remove();

		timeOutID = _window.setTimeout(function() {

			var count = 0;

			elements_list.forEach(function(obj, index) {

				if ( obj.textSearch.indexOf(texto) != -1 ) {
					
					elements_list_filter.push(obj);

					if ( count < 50 ) $_list_entities.append(obj.element);

					++count;
				}

			})
			
		}, 300);
	},

	_detected_botton = function(e) {

		var bHeight = $_body.height(),
			scrollT = $_body.scrollTop() || $(document.documentElement).scrollTop();

		if ( scrollT > ((bHeight * 80) / 100) ) {

			var lengthItems = $_list_entities.children('li').length;
			
			for( var i = lengthItems; i < (lengthItems + 50); i++ ) {
				if( typeof elements_list_filter[i] != undefined ) {
					$_list_entities.append(elements_list_filter[i].element);
				}
			}
		}

	},

	_get_item_list = function() {
		return $('<li class="collection-item" data-search="" data-id="">'+
					'<span class="title entity" title=""></span>'+
					'<span class="title2 subtype"></span><br>'+
					'<span class="title2 type"></span>' +
				'</li>');
	};

//=================================================================================================================


	return {
		init: _bind
	}

}(window, window.jQuery));

$(function() {
	app.entities.init();
});