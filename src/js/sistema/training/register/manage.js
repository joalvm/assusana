app.training.register.manage = ( function( _window, $ ) {

	'use strict';

	var opt 			= $.extend(true, {}, options),
		hlp 			= $.extend(true, {}, helper),
		data_send 		= {},
		module 			= $('html').data('module'),
		data_default 	= {
			title : "",
			type_training: -1,
			description : "",
			theme : 0
		};

	var $_form 					= $("form#frm_training"),
		$_btn_submit 			= $('button[type=submit]#btn-submitdata'),
		$_input_req 			= $_form.find('.req'),
		$_input_data 			= $_form.find('.dataset'),
		$_list_themes			= $_form.find('ul#list-themes');

	var _bind = function() {

		$_input_data.on('change.app.training.register.manage.module', _watch_change_data);
		$_form.on('submit.app.training.register.manage.module', _submit_send_data);

	};

	var restore_data = function() {
		$_input_data.change();
	};

/**
 * REVISA LOS CAMBIOS PRODUCIDOS EN LOS ELEMENTO
 * @return {void}
 */
	var _watch_change_data = function() {
		var $this = $(this),
			name = $this.attr('name'),
			tag = $this.prop("tagName").toLowerCase();

		switch (tag) {
			case 'select':
				data_send[name] = parseInt($this.children('option:selected').val());
				break;
			case 'input':
				data_send[name] = $.trim($this.val());
				break;
			case 'textarea':
				data_send[name] = $.trim($this.val());
				break;
		}
	},

/**
| AGREGANDO TEMAS A LA LISTA
*/
	_submit_send_data = function( e ) {

		data_send = $.extend({}, data_default, data_send);
		var url = (module == 'create')
						? '/app/training/register/create'
						: '/app/training/register/'+$_form.data('id')+'/update';

		if ( ! _is_valid_data_before_send() ) {
			e.preventDefault();
			return false;
		}

		data_send[csrf.name] = csrf.value;

		$_btn_submit.prop({disabled:true});
		app.progress.show();

		$.post(url, data_send, function(data, textStatus, xhr) {
			if ( data )
			{
				if ( ! data.error ) {
					Materialize.toast(data.message, 2500, '', function(){
						if( module == 'create' ) {
							location.reload(true);
						} else {
							location.href = app.base_url + 'app/training/register.html';
						}

					});
				} else {
					Materialize.toast(data.message, 2500);
					$_btn_submit.prop({disabled:false});
				}
			} else {
				Materialize.toast('Tu sessión ha terminado, Reiniciando...', 2500, '', function(){
					location.reload(true);
				});
			}
		}).fail(function(objResponse, textStatus, responseText){

			if ( objResponse.readyState == 4 ) {
				if ( typeof objResponse.responseJSON != 'undefined' ) {
					Materialize.toast(objResponse.responseJSON.message, 5000);
				} else {
					Materialize.toast('Ha ocurrido un error en el servidor, vuelve a intentarlo', 5000);
				}

				$_btn_submit.prop({disabled:false});
			} else {
				Materialize.toast('Ha ocurrido un error en el servidor, intentalo mas tarde', 5000, '', function(){
					location.reload(true);
				});
			}
		}).always(function() {
			app.progress.close();
		});

		e.preventDefault();
	},


/**
| HELPERS
 */

 	/**
 	 * Verifica si la información que ha proporcionado el usuario
 	 * cumple los parametros de validación
 	 * @return {boolean} Confirmación de la validación
 	 */
 	_is_valid_data_before_send = function() {

 		var errors = [];

 		$.each(data_send, function(key, value) {

			switch (key) {
				case 'title':
					if( ($.trim(value)).length == 0 ) {
						errors.push({
							message : "El campo no debe estar vacio",
							elem : $_form.find('input[name="' + key + '"]')
						});
					}
					break;
				case 'type_training':
					if ( value < 0 ) {
						errors.push({
							message : "Debes escojer una opción",
							elem : $_form.find('select[name="' + key + '"]')
						});
					}
					break;
				case 'theme':
					if ( value == 0 ) {
						errors.push({
							message : "Debes escojer un tema",
							elem : $_form.find('input[name="' + key + '"]:eq(0)')
						});
					}
					break;
			}
 		});

 		if ( errors.length > 0 ) {
 			Materialize.toast(errors[0].message, 4000);
 			errors[0].elem.focus();
 			return false;
 		} else {
 			return true;
 		}

 		return false;
 	};

	return {
		create : _bind,
		update : function() {
			_bind();
			restore_data();
		}
	}

}(window, window.jQuery));

$(function() {
	if( $('html').data('module') == 'create' )
		app.training.register.manage.create();
	else if( $('html').data('module') == 'update' )
		app.training.register.manage.update();
});
