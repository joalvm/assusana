app.training.register = (function(_window, $) {

	'use strict';

	var config = {
		url : {
			themes : "/app/training/{ID}/themes.json"
		}
	};

	var opt = $.extend(true, {}, options),
		hlp = $.extend(true, {}, helper),

		mfp_delete = $.extend(true, {}, opt.magnificPopup);

	var timeoutID = 0;

	var $_general_list = $('ul#list-trainings'),
		$_items_list = $_general_list.children('li'),
		$_btn_delete = $_items_list.find('button.btn-delete'),
		$_md_delete = $('section#popup-delete'),
		$_form_delete = $_md_delete.children('form'),
		$_input_search = $('input[type=search]#search'),
		$_btn_options = $_general_list.find('button.btn-options'),
		$_header_coll = $_general_list.find('div.collapsible-header');

	var _bind = function() {

		$_header_coll.one("click.app.training.register.module", _show_themes);
		$_input_search.on('keyup.app.training.register.module', _searcher_list);
		$_form_delete.on('submit.app.training.register.module', _submit_delete);
		$_md_delete.find('button#btn-cancel').on('click.app.training.register.module', _close_md_delete);
		mfp_delete.callbacks.beforeOpen = _beforeOpen_delete;
		$_btn_delete.magnificPopup(mfp_delete);
		$_btn_options.dropdown(opt.dropdown);

		hlp.humanize_DateTime();
	};

	var _show_themes = function() {

		var $this = $(this),
			id = $this.data('id'),
			url = config.url.themes.replace(/{ID}/gi, id);

		$.getJSON(url, function(json, textStatus) {
			if ( json ) {
				if ( ! json.error ) {
					var container = $('<div />', {class:"container-themes", style:'display:none;'});

					$this.siblings('.collapsible-body').find('.progress').hide(500, function() {
						$this.siblings('.collapsible-body').find('.progress').remove();
					});

					json.data.forEach( function(element, index) {
						container.append(_build_themes(element, (index + 1)));
					});

					container.prepend('<span class="section-header">Temas</span>');

					$this.siblings('.collapsible-body').append([container]);

					container.show(500);
				}
			}
		});
	},

	_searcher_list = function() {
		var texto = hlp.remove_accent(this.value.toLowerCase());

		if( timeoutID != 0 ) _window.clearTimeout(timeoutID);

		timeoutID = _window.setTimeout(function(){
			$_items_list.each(function(index, el) {
				var search = hlp.remove_accent(el.getAttributeNode("data-search").textContent);

				if( search.indexOf(texto) != -1 ) {
					$(el).fadeIn(250);
				} else {
					$(el).fadeOut(250);
				}
			});
		}, 250);
	},

	_build_themes = function( DATA, INDEX ) {
		var jblockTheme = $('<blockquote />', {class:'blocktheme', id:'theme' + DATA.id}),
			jspanTitle = $('<span />', {class:'title'}).text(INDEX + ". " + DATA.title),
			jpDescription = $('<p />', {class: 'description'}).html(hlp.nl2br(DATA.description)),
			jspanSectionTitle = $('<span />', {class:'section-header'}).text('Subtemas'),
			julContainerSubthemes = $('<ul />', {
				class:'list-subthemes',
				id:'subthemes' + DATA.id,
				'data-theme-key': DATA.id
			});

		DATA.subthemes.forEach(function(element, index){
			julContainerSubthemes.append(_build_subthemes(element, (index + 1)));
		});

		if ( DATA.description == null ) {
			jblockTheme.append([jspanTitle, jspanSectionTitle, julContainerSubthemes]);
		} else {
			jblockTheme.append([jspanTitle, jpDescription, jspanSectionTitle, julContainerSubthemes]);
		}


		return jblockTheme;
	},

	_build_subthemes = function( DATA, INDEX ) {
		var jli_subthme = $('<li />', { id:'item'+DATA.id, 'data-key':DATA.id }),
			jblockSubTheme = $('<blockquote />', {class:'blocksubtheme'}),
			jspanTitle = $('<span />', {class:'stitle'}).text(INDEX + ". " + DATA.title),
			jpDescription = $('<p />', {class: 'sdescription'}).html(hlp.nl2br(DATA.description));

		if ( DATA.description == null ) {
			jblockSubTheme.append([jspanTitle]);
		} else {
			jblockSubTheme.append([jspanTitle, jpDescription]);
		}

		jli_subthme.append(jblockSubTheme);

		return jli_subthme;
	},

	_beforeOpen_delete = function (e) {
		
		var btn_active = $.magnificPopup.instance.st.el,
			title = btn_active.parents('ul.collapsible').find('span.title').text();

		$_md_delete.find('p.message b').html(title);
		$_md_delete.find('form')
			.attr({
				'id': btn_active.data('id'),
				action:'/app/training/register/'+btn_active.data('id')+'/delete'
			});

	},

	_submit_delete = function (e) {
		var $this 			= $(this),
			resource 		= $this.attr('action'),
			data_request 	= { id: $this.data('id') };

		data_request[csrf.name] = csrf.value;

		$this.find('button[type=submit]').prop({disabled:true});
		app.progress.show();

		$.post(resource, data_request, function(data, textStatus, xhr) {
			if( data ) {

				if( ! data.error ) {
					Materialize.toast(data.message, 2000, '', function () {
						location.reload(true);
					});

				} else {
					Materialize.toast(data.message, 2500);
					$this.find('button[type=submit]').prop({disabled:false});
				}

			} else {
				Materialize.toast('Tu sesión ha caducado, reiniciando...', 2500, '', function () {
					location.reload(true);
				});
			}
		}).fail(function(objResponse, textStatus, responseText) {

			if ( objResponse.readyState == 4 ) {

				if ( typeof objResponse.responseJSON != 'undefined' ) {
					Materialize.toast(objResponse.responseJSON.message, 3000);
					$this.find('button[type=submit]').prop({disabled:false});
				} else {
					Materialize.toast('Ha ocurrido un error en el servidor, vuelve a intentarlo', 4000);
				}

			} else {
				Materialize.toast('Ha ocurrido un error en el servidor, intentalo mas tarde', 4000, '', function() {
					location.reload(true);
				});
			}

		}).always(function () {
			app.progress.close();
		});

		e.preventDefault();
	},

	_close_md_delete = function () {
		$.magnificPopup.close();
	};

	return {
		init : _bind
	};

}(window, window.jQuery));


/**
 * MODULO PARA LA ELIMINACIÓN DE TODOS LOS PUNTOS(capacitación, tema y subtema) DEL MODULO
 */

app.training.register.delete = (function(_window, $) {

	'use strict';

	var config = {
		url : {
			delete : {
				training: "/app/training/{ID}/delete",
				theme: "/app/training/{IDTRAINING}/theme/{IDTHEME}/delete",
				subtheme: "/app/training/{IDTRAINING}/theme/{IDTHEME}/subtheme/{IDSUBTHEME}/delete"
			}
		},

		message : {
			training: "La Capacitación: <b>{name}</b>, será eliminado del sistema de forma permanente."
		}
	};

	var opt = $.extend(true, {}, options);

	var $_modal_delete = $('section#modal-delete'),
		$_form_delete 	= $_modal_delete.find('form'),
		$_btn_save 		= $_modal_delete.find('button#btn-save-del'),
		$_btn_cancel 	= $_modal_delete.find('button#btn-cancel-del');

	var _bind = function() {

	};

	return {
		init : _bind
	}

}(window, window.jQuery));



$(function() {
	if ( $('html').data('module') == 'index' )
		app.training.register.init();
});
