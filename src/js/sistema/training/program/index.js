app.training.program = {};

app.training.program = (function(_window, $){
	
	'use strict';

	var opt = $.extend(true, {}, options),
		hlp = $.extend(true, {}, helper);

	var time_finish = null,
		data_change_sch = {},
		old_data_change_sch = {};

	var mfp_cancel = $.extend(true, {}, opt.magnificPopup),
		mfp_delete = $.extend(true, {}, opt.magnificPopup),
		mfp_urls 	= $.extend(true, {}, opt.magnificPopup),
		mfp_caprox = $.extend(true, {}, opt.magnificPopup);

	var $_btn_option = $('button.btn-options'),
		$_btn_cancel_prg = $('button.cancel-prg'),
		$_btn_delete_prg = $('button.delete-prg'),
		$_btn_change_sch = $('button.change-schedule'),
		$_btn_getURL 	= $('button.get-url-prg'),
		$_cbo_filters = $('select#cbo_filters'),

		$_md_cancel_prg = $('section#md-cancel-programming'),
		$_btn_md_exit 	= $_md_cancel_prg.find('button#btn_exit'),
		$_btn_continue 	= $_md_cancel_prg.find('button#btn_continue'),
		$_curr_item_canceled = {},

		$_md_delete_prg = $('section#md-delete-programming'),
		$_btn_md_exit_del = $_md_delete_prg.find('button#btn_exit'),
		$_btn_md_continue_del = $_md_delete_prg.find('button#btn_continue'),
		$_curr_item_deleted = {},

		$_btn_caprox = $('.title-caprox'),
		$_md_caprox = $('section#md-caprox'),
		$_list_participants_prox = $_md_caprox.find('ul#list-participants'),

		$_md_get_urls 	= $('section#md-get-url');

	var _bind = function() {

		mfp_cancel.callbacks.beforeOpen = _beforeOpen_md_cancelation;
		mfp_delete.callbacks.beforeOpen = _beforeOpen_md_deletion;

		mfp_urls.callbacks.beforeOpen = _beforeOpen_md_geturl;

		$_btn_cancel_prg.magnificPopup(mfp_cancel);
		$_btn_delete_prg.magnificPopup(mfp_delete);
		$_btn_getURL.magnificPopup(mfp_urls);

		mfp_caprox.callbacks.beforeOpen = _beforeOpen_participants;
		$_btn_caprox.magnificPopup(mfp_caprox);

		$_btn_continue.on('click.app.training.program.module', _continue_cancelation);
		$_btn_md_continue_del.on('click.app.training.program.module', _continue_deletion);
		$_btn_change_sch.on('click.app.training.program.module', _change_schedule_prg);
		$_cbo_filters.on('change.app.training.program.module', _change_lists_programming);

		$_btn_option.dropdown(opt.dropdown);

		hlp.humanize_DateTime();
	},

	_beforeOpen_md_cancelation = function() {
		var btn_active = $.magnificPopup.instance.st.el;

		$_curr_item_canceled = btn_active.parents('dd.collection-item');
		$_btn_continue.data({id:btn_active.data('id')});
	},

	_continue_cancelation = function() {
		var $this = $(this),
			check_status = $_md_cancel_prg.find('input[type=checkbox]').is(':checked'),
			reason = $.trim($_md_cancel_prg.find('textarea').val()).replace(/[-|*|=|']/gi, ''),
			data_request = { 
				send_mail: (check_status ? 1 : 0),
				reason: reason
			},
			resource = "/app/training/programming/" + $this.data('id') + "/cancel";

		$this.prop({disabled: true});
		app.progress.show();

		data_request[csrf.name] = csrf.value;

		$.post(resource, data_request, function(data, textStatus, xhr) {
			if ( data )
			{
				if ( ! data.error ) {
					$_curr_item_canceled.hide(500);
					Materialize.toast(data.message + " Reiniciando...", 3000, '', function(){
						location.reload(true);
					});
				} else {
					Materialize.toast(data.message, 4000);
					$this.prop({disabled:false});
				}
			} else {
				Materialize.toast('Tu sessión ha terminado, Reiniciando...', 4000, '', function(){
					location.reload(true);
				});
			}

			app.progress.close();
		}).fail(function(objResponse, textStatus, responseText){
			
			if ( objResponse.readyState == 4 ) {

				if ( typeof objResponse.responseJSON != 'undefined' ) {
					Materialize.toast(objResponse.responseJSON.message + ". Reiniciando...", 3000);
				} else {
					Materialize.toast('Ha ocurrido un error en el servidor, vuelve a intentarlo', 3000);
				}

				$this.prop({disabled:false});

			} else {
				Materialize.toast('Ha ocurrido un error en el servidor, intentalo mas tarde', 4000, '', function(){
					location.reload(true);
				});
			}

		}).always(function() {
			app.progress.close();
			$.magnificPopup.close();
		});
	},

	_beforeOpen_md_deletion = function() {
		var btn_active = $.magnificPopup.instance.st.el;
		$_curr_item_deleted = btn_active.parents('dd.collection-item');

		var training_title = $_curr_item_deleted.find('.title').text();

		$_md_delete_prg.find('.message .text b').html(training_title);

		$_btn_md_continue_del.data({ id: btn_active.data('id') });
	},

	_beforeOpen_md_geturl = function() {
		var btn_active = $.magnificPopup.instance.st.el,
			resource = '/app/training/programming/' + btn_active.data('id') + '/url';

		$.getJSON(resource, function(json, textStatus) {
			$_md_get_urls.find('textarea').val(json);
		});
	},

	_continue_deletion = function() {
		var $this = $(this),
			data_request = {},
			resource = "/app/training/programming/" + $this.data('id') + "/delete";

		$this.prop({disabled: true});

		app.progress.show();

		data_request[csrf.name] = csrf.value;

		$.post(resource, data_request, function(data, textStatus, xhr) {
			if ( data )
			{
				if ( ! data.error ) {
					Materialize.toast(data.message, 4000, '', function(){
						location.reload(true);
					});
				} else {
					Materialize.toast(data.message, 4000);
					$this.prop({disabled:false});
				}
			} else {
				Materialize.toast('Tu sessión ha terminado, Reiniciando...', 4000, '', function(){
					location.reload(true);
				});
			}

			app.progress.close();

		}).fail(function(objResponse, textStatus, responseText) {
			
			if ( objResponse.readyState == 4 ) {

				if ( typeof objResponse.responseJSON != 'undefined' ) {
					Materialize.toast(objResponse.responseJSON.message, 3000);
				} else {
					Materialize.toast('Ha ocurrido un error en el servidor, vuelve a intentarlo', 3000);
				}

				$this.prop({ disabled: false });

			} else {
				Materialize.toast('Ha ocurrido un error en el servidor, intentalo mas tarde', 4000, '', function(){
					location.reload(true);
				});
			}

		}).always(function() {
			app.progress.close();
			$.magnificPopup.close();
		});
	},

	_beforeOpen_participants = function() {
		var $this = $.magnificPopup.instance.st.el;
		$_list_participants_prox.children('li').remove();
		$_list_participants_prox.fadeOut(200);

		app.progress.show();
		
		$.getJSON('/app/inscription/programming/'+$this.data('id')+'/participants', function(json, textStatus) {
			if( json ) {
				if( json.data.length > 0 ) {

					json.data.forEach( function(obj, index) {
						var item = _get_item_participant(obj, (index + 1));
						$_list_participants_prox.append(item);
					});

					$_list_participants_prox.fadeIn(200);

				} else {
					$_list_participants_prox.append('<li class="empty-list"><h4>Sin registro</h4></li>');
				}
			}
		}).always(function () {
			app.progress.close();
		});
	},

	_change_lists_programming = function() {
		var $this = $(this),
			valor = parseInt($this.children('option:selected').val());

		var data_request 	= { filter_level: valor },
			resource 		= '/app/training/programming/filter';

		data_request[csrf.name] = csrf.value;

		$this.prop({disabled:true});
		app.progress.show();

		$.post(resource, data_request, function(data, textStatus, xhr) {
			if ( data )
			{
				if ( ! data.error ) {
					Materialize.toast(data.message, 800, '', function(){
						location.reload(true);
					});
				} else {
					Materialize.toast(data.message, 4000);
					$this.prop({ disabled:false });
				}
			} else {
				Materialize.toast('Tu sessión ha terminado, Reiniciando...', 4000, '', function(){
					location.reload(true);
				});
			}

			app.progress.close();
		}).fail(function(objResponse, textStatus, responseText){
			
			if ( objResponse.readyState == 4 ) {

				if ( typeof objResponse.responseJSON != 'undefined' ) {
					Materialize.toast(objResponse.responseJSON.message + ". Reiniciando...", 3000);
				} else {
					Materialize.toast('Ha ocurrido un error en el servidor, vuelve a intentarlo', 3000);
				}

				$this.prop({disabled:false});

			} else {
				Materialize.toast('Ha ocurrido un error en el servidor, intentalo mas tarde', 4000, '', function(){
					location.reload(true);
				});
			}

		}).always(function() {
			app.progress.close();
		});
	},

	_change_schedule_prg = function() {
		var cnt_sch = $('.cnt-schedule');

		cnt_sch.slideUp(400, function(){
			cnt_sch.remove();
		});
		
		var $this = $(this),
			id 		= $this.data('id'),
			parent = $this.parents('dd.collection-item'),
			row 	= _get_inputs_schedule(id);

		parent.append(row);
		row.slideDown(500);

		var date = row.find('#txt_date_realization_' + id),
			start = row.find('#txt_time_start_' + id),
			finish = row.find('#txt_time_finish_' + id);

		date.data({value:$this.data('date')});
		date.pickadate(opt.picka.date);

		var pickatime_start = $.extend(true, {}, opt.picka.time),
			pickatime_finish = $.extend(true, {}, opt.picka.time);

		pickatime_start.hiddenName = 'time_start';
		pickatime_start['min'] = [7,0];
		pickatime_start['max'] = [21, 0];

		start.data({value:$this.data('start')});

		start.pickatime(pickatime_start);

		var start_arr = $this.data('start').split(":");

		pickatime_finish.hiddenName = 'time_finish';
		pickatime_finish['min'] = [(parseInt(start_arr[0]) + 1), parseInt(start_arr[1])];
		pickatime_finish['max'] = [22, 0];

		finish.data({value:$this.data('finish')});

		time_finish = finish.pickatime(pickatime_finish);

		data_change_sch = {
			'date_realization': $this.data('date'),
			'time_start' : $this.data('start'),
			'time_finish' : $this.data('finish')
		};

		old_data_change_sch = {
			'date_realization': $this.data('date'),
			'time_start' : $this.data('start'),
			'time_finish' : $this.data('finish')
		};
	},

	_get_inputs_schedule = function(ID) {
		var row_Style = "margin-left: 0;margin-right: 0;margin-top: 1rem; display:none;";

		var row_body 	= $('<div />', {
							class:'row cnt-schedule', 
							id:'cnt-schedule-'+ID, 
							style:row_Style
						}),
			footer 		= _get_column('s12 right-align'),
			btn_close 	= $('<button />', {
				type:'button', 
				class: 'btn-flat btn-small btn-close'
			}).text('Cerrar'),
			btn_save 	= $('<button />', {
				type: 'button', 
				class: 'btn btn-small btn-save',
				'data-id': ID
			}).text('Guardar');
		
		var col_1 = _get_column('input-field s12 m4'),
			icon_col1 = _get_icons('&#xE878;', 'prefix'),
			input_date = $('<input />', { 
				type:'text', 
				name: 'date_realization',
				id: 'txt_date_realization_' + ID,
				placeholder: 'Fecha de Realización'
			});

		var col_2 = _get_column('input-field s12 m4'),
			icon_col2 = _get_icons('&#xE878;', 'prefix'),
			input_start = $('<input />', {
				type:'text', 
				name:'time_start',
				id:'txt_time_start_' + ID,
				placeholder: 'Hora inicio'
			});

		var col_3 = _get_column('input-field s12 m4'),
			icon_col3 = _get_icons('&#xE878;', 'prefix'),
			input_finish = $('<input />', {
				type:'text', 
				name:'time_finish',
				id:'txt_time_finish_' + ID,
				placeholder:'Hora finalización'
			});

		input_start.on('change.app.training.program.module', _limit_time_finish);
		input_date.on('change.app.training.program.module', _change_data_schedule);
		input_start.on('change.app.training.program.module', _change_data_schedule);
		input_finish.on('change.app.training.program.module', _change_data_schedule);
		
		col_3.append([icon_col3, input_finish]);
		col_2.append([icon_col2, input_start]);
		col_1.append([icon_col1, input_date]);

		btn_close.on('click.app.training.program.module', _close_inner_change);
		btn_save.on('click.app.training.program.module', _save_change_schedule);

		footer.append([btn_close, btn_save]);

		row_body.append([col_1, col_2, col_3, footer]);

		return row_body;
	},

	_get_item_participant = function(DATA, INDEX) {

		var jli = $('<li />', {class:'collection-item'}),
			jname = $('<span />', { class:'title' }).text(INDEX + ". " + DATA.participant_lastname + ' ' + DATA.participant_name ),
			jdni = $('<span />', {class:'title2'}).text(DATA.participant_dni),
			jemail = $('<span />', {class:'title2'}).text(DATA.participant_emails),
			jconfirm = $('<div />', {class:'assis', title: DATA.email_confirmed ? 'Registro activado' : 'Registro no activo'});

		if ( DATA.email_confirmed )
			jconfirm.addClass('green');
		else
			jconfirm.addClass('yellow darken-2');

		jli.append([jname, jdni, jemail, jconfirm]);

		if( DATA.dispacity )
			jname.append($('<div>', {class:'chip disp', title:'Presenta una discapacidad'}).html('<i class="material-icons">&#xE914;</i>'));

		return jli;
	},

	_close_inner_change = function() {
		var $this = $(this),
			parent = $this.parents('.cnt-schedule');

		data_change_sch = {};
		old_data_change_sch = {};

		parent.slideUp(400, function() { parent.remove(); });
	},

	_change_data_schedule = function() {
		var $this = $(this),
			name = $this.attr('name'),
			type = $this.attr('type'),
			tag = $this.prop("tagName").toLowerCase();

		_window.setTimeout(function() {
			if ( tag == 'input' ) {
				data_change_sch[name] = $.trim($this.siblings('input[type=hidden]').val());
			}
		}, 100);
	},

	_save_change_schedule = function() {
		var $this = $(this),
			close = $this.siblings('button.btn-close'),
			resource = '/app/training/programming/' + $this.data('id') + '/schedule/change';

		if ( data_change_sch.date_realization == "" || 
			data_change_sch.time_start == "" || 
			data_change_sch.time_finish == "" ) {
			Materialize.toast('Debes completar todas las fechas', 3000);
			return false;
		}

		if ( data_change_sch.date_realization == old_data_change_sch.date_realization && 
			data_change_sch.time_start == old_data_change_sch.time_start && 
			data_change_sch.time_finish == old_data_change_sch.time_finish ) {
			Materialize.toast('No podemos guardar mientras no hayas hecho un cambio', 3000);
			return false;
		}

		data_change_sch[csrf.name] = csrf.value;

		$this.prop({disabled:true});
		close.prop({disabled:true});

		app.progress.show();

		$.post(resource, data_change_sch, function(data, textStatus, xhr) {
			if ( data )
			{
				if ( ! data.error ) {
					Materialize.toast(data.message, 4000, '', function(){
						location.reload(true);
					});
				} else {
					Materialize.toast(data.message, 4000);
					$this.prop({disabled:false});
				}
			} else {
				Materialize.toast('Tu sessión ha terminado, Reiniciando...', 4000, '', function(){
					location.reload(true);
				});
			}

			app.progress.close();
			
		}).fail(function(objResponse, textStatus, responseText) {
			
			if ( objResponse.readyState == 4 ) {

				if ( typeof objResponse.responseJSON != 'undefined' ) {
					Materialize.toast(objResponse.responseJSON.message, 3000);
				} else {
					Materialize.toast('Ha ocurrido un error en el servidor, vuelve a intentarlo', 3000);
				}

				$this.prop({ disabled: false });

			} else {
				Materialize.toast('Ha ocurrido un error en el servidor, intentalo mas tarde', 4000, '', function(){
					location.reload(true);
				});
			}

		}).always(function() {
			app.progress.close();
			$.magnificPopup.close();
		});
	},

	_limit_time_finish = function() {
		var $this 	= $(this);
		_window.setTimeout(function() {
			var tmin 	= $this.siblings('input[type=hidden]').val().split(":"),
				hmin 	= [ (parseInt(tmin[0]) + 1), parseInt(tmin[1]) ];

			time_finish.pickatime('picker').set('min', hmin).render(true).clear();
			time_finish.val("").prop({disabled:false}).siblings('label').removeClass('active');
		}, 100);
	},

	_get_icons = function(HEX, ADDCLASS) {
		if ( typeof(ADDCLASS) == 'undefined' ) ADDCLASS = '';
		return $('<i />', {class:('material-icons ' + ADDCLASS)}).html(HEX);
	},

	_get_column = function(KLASS) {
		return $('<div />', { 
			class: 'col ' + KLASS,
			style: "margin-bottom:0;"
		});
	};

	return {
		init: _bind
	}

}(window, window.jQuery));

$(function() {
	if( $("html").attr('data-module') == 'index' )
		app.training.program.init();
});