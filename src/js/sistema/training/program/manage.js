app.training.program.manage = ( function( _window, $ ) {

	'use strict';

	var data_default = {
		"training" : null,
		"date_realization" : null,
		"time_start" : null,
		"time_finish" : null,
		"place": null,
		"address" : null,
		"distrito" : null,
		"personalize": 0,
		"entity" : 0,
		"limit": 0 /*Si el valor es 0 no tiene limite*/,
		"docen_assign" : {}
	},

	data_send = {
		docen_assign : {}
	},

	config = {
		url: {
			create: '/app/training/programming/create',
			update: '/app/training/programming/{ID}/update'
		},
		idprog : 0
	};

	/**
	 * VARIABLES GLOBAL CON OPCIONES PARA LA CONFIGURACIÓN DE LOS DIFERENTE 
	 * PLUGINS
	 * @type {Oject JSON}
	 */
	var opt = $.extend(true, {}, options),

	/**
	 * VARIABLE GLOBAL CON FUNCIONES DE AYUDA PARA TODO EL APLICATIVO
	 * @type {Object}
	 */
		hlp = $.extend(true, {}, helper);


	var mfp_training 		= $.extend(true, {}, opt.magnificPopup),
		mfp_coordinator_1 	= $.extend(true, {}, opt.magnificPopup),
		mfp_entities 		= $.extend(true, {}, opt.magnificPopup),
		
		ubigeo_data 		= [],
		entities_data 		= [],
		entities_filter 	= [],
		entity_timeOutID 	= 0,
		module 				= '',
		$_item_checked 		= {};

	var $_form_main 			= $('form#frm_manage_program'),
		$_btn_submit 			= $('button[type=submit]#btn-submitdata'),
		$_input_watching 		= $_form_main.find('.dataset'),
		$_md_list_training 		= $('section#modal-list-training'),
		$_radio_training 		= $_md_list_training.find('input[type=radio]'),
		$_input_title_training 	= $_form_main.find('input[type=text]#txt_training'),
		$_input_type_training 	= $_form_main.find('input[type=text]#txt_type_training'),
		$_open_date 			= $_form_main.find('input[type=text]#txt_date_realization'),
		$_open_time_start 		= $_form_main.find('input[type=text]#txt_time_start'),
		$_open_time_finish 		= $_form_main.find('input[type=text]#txt_time_finish'),
		$_cbo_ubigeos 			= $_form_main.find('select.ubigeo'),

		$_list_themes 			= $_form_main.find('#list-themes-to-asign'),
		$_md_list_coordinator1 	= $('section#modal-asign-coordinator'),
		$_list_coordinator1 	= $_md_list_coordinator1.find('ul#list-coordinator'),
		$_curr_txt_coordinator 	= {},

		$_chk_active_entities 	= $_form_main.find('input[type=checkbox]#chk_personalize'),
		$_txt_entities 			= $_form_main.find('input#txt_entity_display'),
		$_modal_entities 		= $('section#popup-list-entities'),
		$_modal_list_entities 	= $_modal_entities.find('ul#list-entities'),
		$_searcher 				= $_modal_entities.find('input[type=search]'),
		$_btn_close_modal 		= $_modal_entities.find('#btn-cancel'),
		$_item_counts 			= $_modal_entities.find('.items-count');

	var time_finish = null;

	/**
	 * Enlaza todos los eventos y valores iniciales
	 * @return {void}
	 */
	var _bind = function() {

		module = $('html').data('module');

		mfp_training.focus = "input[type=search]";
		mfp_coordinator_1.modal = false;
		mfp_coordinator_1.callbacks.beforeOpen = _beforeOpen_modal_coordinator1;
		mfp_coordinator_1.callbacks.afterClose = _afterClose_modal_coordinator1;

		if( module == 'create' ) {
			$_open_date.pickadate(opt.picka.date);

			var pickatime_start = $.extend(true, {}, opt.picka.time),
				pickatime_finish = $.extend(true, {}, opt.picka.time);

			pickatime_start.hiddenName = 'time_start';
			pickatime_start['min'] = [7,0];
			pickatime_start['max'] = [21, 0];

			$_open_time_start.pickatime(pickatime_start);

			pickatime_finish.hiddenName = 'time_finish';
			time_finish = $_open_time_finish.pickatime(pickatime_finish);

			$_form_main
				.attr({action:config.url.create})
				.on('submit.app.training.program.manage.module', _submit_data_create);

		} else {

			config.idprog = $_form_main.data('programming');

			$_form_main
				.attr({action:config.url.update.replace(/{ID}/gi, config.idprog)})
				.on('submit.app.training.program.manage.module', _submit_data_create);
		}


		$_input_watching.on('change.app.training.program.manage.module', _watch_changes),
		$_open_time_start.on('change.app.training.program.manage.module', _limit_time_finish);
		$_cbo_ubigeos.on('change.app.training.program.manage.module', _changing_ubigeo_cascaded);
		$_radio_training.on('change.app.training.program.manage.module', _training_selected),
		$_chk_active_entities.on('change.app.training.program.manage.module', _active_entities),
		$_list_coordinator1.find('li a').on('click.app.training.program.manage.module', _asign_coordinator);


		mfp_entities.callbacks.beforeOpen = _beforeOpen_modal;
		mfp_entities.focus = "input[type=search]";
		mfp_entities.modal = true;
		$_txt_entities.magnificPopup(mfp_entities);

		$_searcher.on('keyup.app.coordinator.manage.module', _filter_entities);
		$_btn_close_modal.on('click.app.coordinator.manage.module', _close_modal_entities);
		$_modal_list_entities.on('scroll.app.coordinate.manage.module', _detected_botton);

		$_input_title_training.magnificPopup(mfp_training);
		//$_btn_asign_all_themes.magnificPopup(mfp_coordinator_1);

		_getUbigeo();
	},

	_watch_changes = function() {
		var $this = $(this),
			name = $this.attr('name'),
			type = $this.attr('type'),
			tag = $this.prop("tagName").toLowerCase();

		if ( tag == 'select' ) {
			data_send[name] = parseInt($this.children('option:selected').val());
		} else if( tag == 'input' && type == 'number' ) {
			data_send[name] = 0;
			if ( $.isNumeric($.trim($this.val())) ) {
				if( parseInt($.trim($this.val())) >= 0 ) {
					data_send[name] = parseInt($.trim($this.val()));
				}
			}
		} else if ( tag == 'input' ) {
			if ( $this.hasClass('hidden') ) {
				var hidden = $this.siblings('input[type=hidden]');
				data_send[name] = $.trim(hidden.val());
			} else {
				data_send[name] = $.trim($this.val());
			}
		}
	},

	_submit_data_create = function( e ) {

		var resource = '';

		if( module == 'create' ) {
			resource = config.url.create;
		} else {
			resource = config.url.update.replace(/{ID}/gi, config.idprog);
		}
		
		data_send = $.extend({}, data_default, data_send);

		if ( ! _is_valid_data_before_send() ) {
			e.preventDefault();
			return false;
		}

		data_send[csrf.name] = csrf.value;

		$_btn_submit.prop({disabled:true});

		app.progress.show();

		$.post(resource, data_send, function(data, textStatus, xhr) {
			if ( data )
			{
				if ( ! data.error ) {
					Materialize.toast(data.message, 4000, '', function(){
						location.reload(true);
					});
				} else {
					Materialize.toast(data.message, 4000);
					$_btn_submit.prop({disabled:false});
				}
			} else {
				Materialize.toast('Tu sessión ha terminado, Reiniciando...', 4000, '', function(){
					location.reload(true);
				});
			}

			app.progress.close();

		}).fail(function(objResponse, textStatus, responseText){
			
			if ( objResponse.readyState == 4 ) {

				if ( typeof objResponse.responseJSON != 'undefined' ) {
					Materialize.toast(objResponse.responseJSON.message, 4000);
				} else {
					Materialize.toast('Ha ocurrido un error en el servidor, vuelve a intentarlo', 4000);
				}

				$_btn_submit.prop({disabled:false});

			} else {
				Materialize.toast('Ha ocurrido un error en el servidor, intentalo mas tarde', 4000, '', function(){
					location.reload(true);
				});
			}
		}).always(function() {
			app.progress.close();
		});

		e.preventDefault();
	},

	_limit_time_finish = function() {
		var $this = $(this),
			tmin = $this.siblings('input[type=hidden]').val().split(":"),
			hmin = [
				(parseInt(tmin[0]) + 1),
				parseInt(tmin[1])
			];

		time_finish.pickatime('picker').set('min', hmin).render(true).clear();
		time_finish.val("").prop({disabled:false}).siblings('label').removeClass('active');
	},

	_is_valid_data_before_send = function() {

		var errors = [];

		$.each( data_send, function(key, value) {

			var elem = $_form_main.find('[name="'+key+'"]');

			switch (key) {
				case 'training':
					if ( value == null ) {
						errors.push({
							elem : elem,
							message: "Debes escoger una capacitación."
						});
					}
				break;
				case 'distrito':
					if ( value == null ) {
						errors.push({
							elem : elem,
							message: "Debes escoger un distrito."
						});
					}
				break;
				case 'place':
				case 'address':
					if ( value == null ) {
						errors.push({
							elem : elem,
							message: "Este campo es requerido."
						});
					} else if( $.trim(value).length == 0 ) {
						errors.push({
							elem : elem,
							message: "Este campo es requerido."
						});
					}
				break;
				case 'date_realization':
				case 'time_start':
				case 'time_finish':
					if ( (value == null || value == '') && module == 'create' ) {
						errors.push({
							elem : elem,
							message: "Este campo es requerido."
						});
					}
				break;
				case 'docen_assign': 
					var choise = 0;
					$.each(value, function(ikey, ivalue) {
						if ( ivalue.profesor > 0 ) {
							choise++;
							return false;
						}
					});

					if ( choise == 0 ) {
						errors.push({
							elem : elem,
							message: "Se debe asignar almenos un docente a un subtema."
						});
					}
				break;
				case "entity":
					if ( data_send.personalize ) {
						if ( value == 0 ) {
							errors.push({
								elem : elem,
								message: "Al ser personalizada necesita que escojas una entidad."
							});
						}
					}
				break;
			}
		});

		if ( errors.length > 0 ) {
			Materialize.toast(errors[0].message, 3000);
			errors[0].elem.focus();
			return false;
		}

		return true;
	},

/**
| METODOS PARA LA CARGA Y SELECCION DEL UBIGEO DE UBICACIÓN DE
| LA CAPACITACIÓN
*/
	_getUbigeo = function() {
		var depart = $_cbo_ubigeos.filter('#cbo_departamento');

		$.getJSON('/api/ubigeo.json', function(json, textStatus) {
			
			json.forEach( function(element, index) {
				var option = $('<option />', { value: element.id, 'data-index': index, }).html(element.departamento);

				if( depart.data('selected') == element.id ) {
					option.prop({selected:true});
				}

				depart.append(option);
			});

			ubigeo_data = json;

			depart.prop({disabled:false});

		}).always(function(){
			if( module == 'update' ) {
				var dept = $_cbo_ubigeos.filter('#cbo_departamento');
				var prov = $_cbo_ubigeos.filter('#cbo_provincia');
				var dist = $_cbo_ubigeos.filter('#cbo_distrito');
					
				dept.children('option[value="'+dept.data('selected')+'"]').prop({selected:true}).change();
				prov.children('option[value="'+prov.data('selected')+'"]').prop({selected:true}).change();
				dist.children('option[value="'+dist.data('selected')+'"]').prop({selected:true}).change();
			}
		});
	},

	_changing_ubigeo_cascaded = function() {
		var $this = $(this),
			name = $this.attr('name'),
			position = parseInt($this.children('option:selected').data('index'));

		if ( $this.children('option:selected').val() != '0' ) {
			switch (name) {
				case 'departamento':
					$_cbo_ubigeos
						.filter('#cbo_distrito')
							.children('option:not(.default)').remove();
					_load_provincia(position);
					$_cbo_ubigeos
						.filter('#cbo_provincia')
							.children('option.default').prop({selected:true});
					break;
				case 'provincia':
					_load_distrito(position);
					break;
			}
		} else {
			$_cbo_ubigeos.filter('#cbo_provincia').children('option:not(.default)').remove();
			$_cbo_ubigeos.filter('#cbo_distrito').children('option:not(.default)').remove();
		}
	},

	_load_provincia = function( POSITION ) {

		var prov = $_cbo_ubigeos.filter('#cbo_provincia');

		prov.children('option:not(.default)').remove();

		ubigeo_data[POSITION].provincias.forEach( function(element, index) {

			var option = $('<option />', { value: element.id, 'data-index': index }).html(element.provincia);

			prov.append(option);

		});

		prov.prop({disabled:false});
	},

	_load_distrito = function( POSITION ){
		var dist = $_cbo_ubigeos.filter('#cbo_distrito'),
			pos_depart = $_cbo_ubigeos.filter('#cbo_departamento').children('option:selected').data('index');

		dist.children('option:not(.default)').remove();

		ubigeo_data[parseInt(pos_depart)].provincias[POSITION].distritos.forEach( function(element, index) {

			var option = $('<option />', { value: element.id, 'data-index': index }).html(element.distrito);

			dist.append(option);
		});

		dist.prop({disabled:false});
	},
//==========================================================================================

	_training_selected = function() {
		var $this = $(this),
			name = $this.attr('name'),
			title = $this.data('title'),
			type = $this.data('type'),
			id = parseInt($this.val());

		$_input_title_training.val(title);
		$_input_type_training.val(type).siblings('label').addClass('active');

		_list_themes_for_asign( id );

		data_send[name] = parseInt(id);

		$.magnificPopup.close();
		$_cbo_ubigeos.filter("#cbo_departamento").focus();
	},

	_list_themes_for_asign = function( ID ) {
		
		$_list_themes.children('blockquote').remove();

		$.getJSON('/app/training/' + ID + '/themes.json', function(json, textStatus) {
			if ( json ) {
				if ( ! json.error ) {
					json.data.forEach( function(elem, index) {
						$_list_themes.append(_build_themes(elem, (index + 1)));
					});
				}
			}
		});
	},

	_build_themes = function( DATA, INDEX ) {
		var jblockTheme = $('<blockquote />', {class:'blocktheme', id:'theme' + DATA.id}),
			jspanTitle = $('<span />', {class:'title'}).text(DATA.title),
			jspanSectionTitle = $('<span />', {class:'section-header'}).text('Subtemas'),
			julContainerSubthemes = $('<ul />', {
				class:'list-subthemes', 
				id:'subthemes' + DATA.id, 
				'data-theme-key': DATA.id
			});

		DATA.subthemes.forEach(function(element, index){
			julContainerSubthemes.append(_build_subthemes(element, (index + 1)));
		});

		jblockTheme.append([jspanTitle, jspanSectionTitle, julContainerSubthemes]);
		
		return jblockTheme;
	},

	_build_subthemes = function( DATA, INDEX ) {
		var key = (hlp.key() + hlp.key()),
			jli_subthme = $('<li />', { 
				id:'item' + DATA.id, 
				'data-id': DATA.id, 
				'data-key': key
			}),
			jblockSubTheme = $('<blockquote />', {class:'blocksubtheme'}),
			jspanTitle = $('<span />', {class:'stitle'}).text(DATA.title),
			jinput 		= $('<input />', {
				type:'text', 
				placeholder:'Docente',
				'data-id':DATA.id,
				'data-mfp-src': '#modal-asign-coordinator',
				'data-key': key
			}),
			jbutton_clear = $('<button />', {
				type:'button',
				class:'clear_docente hoverable hide',
				'data-key': key
			}).html('<i class="material-icons">&#xE14C;</i>');

		jbutton_clear.on('click.app.training.program.manage.module', _clear_docente);

		jinput.prop({readonly:true}).magnificPopup(mfp_coordinator_1);
		jblockSubTheme.append([jspanTitle, jinput, jbutton_clear]);
		jli_subthme.append(jblockSubTheme);

		if( module == 'update' ) {

			var stheme_input = $_list_themes.find('input[type=hidden]#stheme_' + DATA.id);

			if (stheme_input.length > 0) {

				data_send.docen_assign[key] = {
					subtheme: parseInt(DATA.id),
					profesor: stheme_input.data('iddocente')
				};

				jinput.val(stheme_input.data('docentename'))
					.data( {'item-list': 'coordinator_'+stheme_input.data('iddocente')} );

				jbutton_clear.removeClass('hide');

			} else {

				data_send.docen_assign[key] = {
					subtheme : parseInt(DATA.id),
					profesor: 0
				};
			}

		} else {
			data_send.docen_assign[key] = {
				subtheme : parseInt(DATA.id),
				profesor: 0
			};
		}

		return jli_subthme;
	},

	_clear_docente = function () {
		
		var $this = $(this);
		
		data_send.docen_assign[$this.data('key')].profesor = 0;
		
		$this.siblings('input[type=text]').val('').removeData('item-list').focus();

		$this.addClass('hide');

	},

	_beforeOpen_modal_coordinator1 = function() {

		var btn_active = $.magnificPopup.instance.st.el,
			element_selected = btn_active.data('item-list');

		if ( typeof(element_selected) == 'undefined' ) {
			$_list_coordinator1.children('li').removeClass('active');
		}
		else {
			$_list_coordinator1.children('li#'+element_selected).addClass('active');
		}

		$_curr_txt_coordinator = btn_active;

	},

	_afterClose_modal_coordinator1 = function() {
		$_list_coordinator1.children('li').removeClass('active');
	},

	_asign_coordinator = function() {
		var $this = $(this),
			parent = $this.parent('li'),
			id = parent.data('id'),
			key = $_curr_txt_coordinator.data('key');

		parent
			.siblings('li')
			.removeClass('active')
				.children('a')
				.removeClass('active');

		parent.addClass('active');

		$this.addClass('active');
		$_curr_txt_coordinator
			.data( {'item-list': parent.attr('id')} )
			.val($this.text());

		var button_clear = $_curr_txt_coordinator.siblings('button.clear_docente');

		if( button_clear.hasClass('hide') )
			button_clear.removeClass('hide');

		data_send.docen_assign[key].profesor = parseInt(id);

		$.magnificPopup.close();
	},

/**
|  METODOS PARA LA FUNCION DE CREAR UNA CAPACITACIÓN PERSONALIZADA
*/
	_active_entities = function() {
		var $this = $(this);

		data_send['personalize'] = $this.is(':checked') ? 1 : 0;

		if ( $this.is(':checked') ) {
			$_txt_entities.prop({ disabled: false }).focus().click();
		} else {
			$_txt_entities.val('').prop({ disabled:true }).focusout();
			data_send['entity'] = undefined;
		}
	},

	_beforeOpen_modal = function() {

		var btn_active = $.magnificPopup.instance.st.el,
			ID = btn_active.data('selected');

		if ( entities_filter.length > 0 )
			return false;

		$_modal_entities.find('.popup-header')
			.append('<div class="progress"><div class="indeterminate"></div></div>');

		entities_data = [];
		entities_filter = [];
		$_modal_list_entities.children('li').remove();

		$.getJSON('/api/entities/expanded.json', function(json, textStatus) {

			json.entities.forEach( function(obj, index) {
				var stype 	= json.subtype_entities[obj.stype_ps],
					type 	= json.type_entities[stype.type_ps],
					litem 	= _get_item_list();

				litem.attr({
					'data-search' : (obj.entity + " " + obj.cue + " " + type.name + " " + stype.name),
					'data-id' : obj.id
				});

				litem.find('input[type=radio]').attr({
					'data-id' : "entity" + obj.id,
					'data-entity' : obj.entity,
					'data-type' : type.name,
					'data-subtype' : stype.name,
					id: "entity" + obj.id
				}).val(obj.id);

				if ( ID == obj.id )
					litem.find('input[type=radio]').prop({checked:true});

				litem.find('label').attr({ for:('entity' + obj.id) });
				litem.find('span.entity').attr({
					title: obj.entity
				}).html(obj.entity);

				litem.find('span.subtype').html('Subtipo : ' + stype.name);
				litem.find('span.type').html('Tipo &nbsp;&nbsp;&nbsp; : ' + type.name);

				litem.find('input[type=radio]').on('change.app.coordinator.manage.module', _choise_item);
				
				var strFilter = obj.entity + " " + stype.name + " " + type.name;

				entities_data.push({
					TextSearch: hlp.remove_accent(strFilter.toLowerCase()),
					element: litem
				});

				if ( index < 50 )
					$_modal_list_entities.append(litem);
			});

			// Copiar la data inicial a la data de filtros
			entities_filter = entities_data;

			$_item_counts.find('.curr').text(50);
			$_item_counts.find('.total').text(entities_data.length);

			$_modal_entities.find('.popup-header .progress').remove();
		});
	},

	_close_modal_entities = function() {
		$.magnificPopup.close();
	},

	_detected_botton = function(e) {

		//var scrollCount = $_modal_list_entities.scrollTop() + $_modal_list_entities.height(),
		//	initCallback = $_modal_list_entities[0].scrollHeight -  $_modal_list_entities.height();

		if ( parseInt($_modal_list_entities.scrollTop() + $_modal_list_entities.height()) > 
			parseInt($_modal_list_entities[0].scrollHeight -  $_modal_list_entities.height()) ) {

			var lengthItems = $_modal_list_entities.children('li').length;

			for( var i = lengthItems; i < (lengthItems + 50); i++ ) {

				var obj = entities_filter[i];

				if( typeof(obj) != 'undefined' ) {
					obj.element.find('input[type=radio]').on('change.app.coordinator.manage.module', _choise_item);
					$_modal_list_entities.append(obj.element);
				}
			}

			$_item_counts.find('.curr').text($_modal_list_entities.children('li').length);
		}
	},

	_choise_item = function() {
		var $this = $(this);
		var entity = $this.data('entity'),
			id = $this.val();

		$_item_checked = $this;

		$_txt_entities.attr({
			title: entity,
			'data-id': parseInt(id)
		}).val(entity);

		data_send['entity'] = parseInt(id);

		$.magnificPopup.close();
	},

	_filter_entities = function() {
		var texto = hlp.remove_accent(this.value.toLowerCase());

		if ( entity_timeOutID != 0) clearTimeout(entity_timeOutID);

		entities_filter = [];
		$_modal_list_entities.children('li').remove();

		entity_timeOutID = _window.setTimeout(function() {

			var count = 0;
			entities_data.forEach(function(obj, index) {

				if ( obj.TextSearch.indexOf(texto) != -1 ) {
					
					entities_filter.push(obj);

					if ( count < 50 ) {
						obj.element.find('input[type=radio]').on('change.app.coordinator.manage.module', _choise_item);
						$_modal_list_entities.append(obj.element);
					}

					++count;
				}

			})

			$_item_counts.find('.curr').text($_modal_list_entities.children('li').length);
			$_item_counts.find('.total').text(entities_filter.length);
			
		}, 300);
	},

	_get_item_list = function() {
		return $('<li class="collection-item" data-search="" data-id="">'+
			'<p>'+
				'<input class="with-gap" type="radio" id="" name="entities" data-name="" value="">'+
				'<label for="">'+
					'<span class="title entity truncate" title=""></span>'+
					'<span class="title2 type"></span>' +
					'<span class="title3 subtype"></span>'+
				'</label>'+
			'</p>'+
		'</li>');
	};

//==========================================================================================


	return {
		create : function() {
			_bind();
		},
		update : function() {

			_bind();

			$_radio_training.filter('[checked=checked]').change();
			
			$_input_watching.change();

		}
	}

}(window, window.jQuery));




searcher.modal_training = (function(_window, $){
	'use strict';

	var $_modal = $('section#modal-list-training'),
		$_list 	= $_modal.find('ul#list-training'),
		$_input = $_modal.find('input[type=search]'),
		$_close_modal = $_modal.find('button#btn-out-training');

	var timeOutID = 0;

	var _bind = function() {

		$_input.on('change.searcher.modal_training.module', _filter_list);
		$_input.on('keyup.searcher.modal_training.module', _filter_list);

		$_close_modal.on('click.searcher.modal_training.module', function(){
			$.magnificPopup.close();
		});
	},

	_filter_list = function() {
		var texto = this.value;

		if ( timeOutID != 0) clearTimeout(timeOutID);

		timeOutID = _window.setTimeout(function() {
			$_list.children('li').each(function(index, elem) {
				if ((elem.getAttributeNode('data-search').value.toLowerCase()).indexOf(texto) < 0 ) {
					$(elem).fadeOut('200');
				} else {
					$(elem).fadeIn('200');
				}
			});
		}, 200);
	};

	return {
		init: _bind
	}

}(window, window.jQuery));




$(function() {

	switch ($("html").attr('data-module')) {
		case "create":
			app.training.program.manage.create();
			break;
		case "update":
			app.training.program.manage.update();
			break;
	}

	searcher.modal_training.init();
});