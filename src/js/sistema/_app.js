var options = {
	magnificPopup : {
		type:'inline',
		mainClass: 'mfp-zoom-in mfp-img-mobile',
		midClick: true,
		closeOnBgClick: false,
		fixedBgPos:true,
		alignTop:true,
		callbacks : {}
	},
	dropdown : {
		inDuration: 300,
		outDuration: 225,
		constrain_width: false,
		alignment: 'right',
		hover: false,
		gutter: 0
	},
	picka : {
		date: {
			format:'ddd. dd mmmm yyyy',
			formatSubmit: "yyyy-mm-dd",
			selectYears: true,
			selectMonths: true,
			clear:false
		},
		time: {
			interval: 5,
			clear: false,
			hiddenName: '',
			hiddenSuffix: '_submit',
  			formatSubmit: 'H:i'
		}
	},
	datetimeHumanize : function() {
		$('time.humanize').each(function(index, el) {
			el.textContent = moment(el.getAttributeNode("data-datetime").textContent).calendar();
		});
	}
};

$.ajaxSetup({
	data : csrf.init(),
	error: function ( objResponse, textStatus, responseText ) {

		if ( objResponse.readyState == 4 ) {

			if ( typeof objResponse.responseJSON != 'undefined' )
				Materialize.toast(objResponse.responseJSON.message, 4000);
			else 
				Materialize.toast('Ocurrio un problema durante la ejecución, intentanlo más tarde', 3000);

		} else {

			Materialize.toast('Ha ocurrido un error en el servidor, intentalo más tarde', 3000, '', function() {
				location.reload(true);
			});

		}

	},
	beforeSend: function (jqXHR, settings) {

		var base_url 	= $('head meta[name="base-url"]').attr('content'),
			r_url 		= '';

		base_url = base_url.substr(0, base_url.length - 1);

		app.progress.show();
		
		if ( settings.type == 'GET' ) {

			var nurl 	= settings.url.split('?');

			r_url = nurl[0];

		} else {

			r_url = settings.url;

		}

		this.url = base_url + r_url;

	},
	complete: function () {

		app.progress.close();

	}
});


var app = ( function( window, $ ) {

	'use strict';
	
	var $_progress = $("<div />", {class:'progress pg-body'}),
		$_logout = $('header .side-nav .menu .menu-item a#logout-link'),
		$_meta_base = $('meta[name="base-url"]');

	var _bind = function() {
		$('.button-collapse, .active-menu').sideNav();
		$_logout.on('click.app.module', _logout_session);
	},

	_logout_session = function( e ) {
		e.preventDefault();

		var auth = {};
		$.post('/logout', auth, function(data, textStatus, xhr) {
			window.location.href = $_logout.attr('href');
		});
	},

	_show_progress = function() {
		$_progress.append( $('<div />', {class:'indeterminate'}) );
		$('body').prepend($_progress);
	},

	_close_progress = function() {
		$_progress.remove();
	};

	return {
		init : _bind,
		base_url: $_meta_base.attr('content'),
		progress: {
			show : _show_progress,
			close: _close_progress
		}
	};

}(window, window.jQuery));

// DECLARE MODULES
app.workshop 	= {};
app.coordinator = {};
app.training 	= {};
app.reports 	= {};

$(function() {
	app.init();
});