app.dashboard = (function(_window, $){
	
	'use strict';

	var opt = $.extend(true, {}, options),
		hlp = $.extend(true, {}, helper);
	
	var mfp_caprox = $.extend(true, {}, opt.magnificPopup);
	
	
	var cultureSpanish = {
		decimalSeparator: ".",
		digitGroupSeparator: ",",
		savePNGText: "Guardar como PNG",
		saveJPGText: "Guardar como JPG",
		days: ["Domingo", "Lunes", "Martes", "Miércoles", "Jueves", "Viernes", "Sábado"],
		shortDays: ["Dom.", "Lun.", "Mar.", "Mié.", "Jue.", "Vie.", "Sáb."],
		months: ["Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio", "Julio", "Agosto", "Septiembre", "Octubre", "Noviembre", "Diciembre"],
		shortMonths: ["Ene.", "Feb.", "Mar.", "Abr.", "May.", "Jun.", "Jul.", "Ago.", "Sep.", "Oct.", "Nov.", "Dic."]

	},

	canvasConfig = {
		culture: 'Spanish',
		animationEnabled: true, 
		animationDuration: 1500,
		exportFileName: "registromensual_de inscripciones",  //Give any name accordingly
		exportEnabled: true,
		title:{
			text: "",
			fontweight: 'lighter',
			fontFamily: "Roboto, arial, tahoma sans-serif",
			labelFontFamily: "Roboto",
			horizontalAlign: 2,
			fontSize: 15,
			fontColor: "#777777",
			margin: 5,
			padding:15
		}, 
		axisX:{
	        gridThickness: 1,
	        lineThickness: 1,
	        tickLength: 5,
			tickThickness: 1,
			gridColor: "#eeeeee",
			margin: 0
	    },
	    axisY:{
	        gridThickness: 1,
	        lineThickness: 1,
	        tickLength: 5,
			tickThickness: 1,
			gridColor: "#eeeeee",
			margin: 0
	    },
		data: [
			{        
				type: "",
				markerSize: 4,
				lineThickness: 1,
				indexLabelLineThickness: 1,
				xValueType: "dateTime",
				dataPoints: []
			}             
		]
	},

	splineAreaChart = {},
	PieChart = {};

	var $_btn_caprox = $('.title-caprox'),
		$_md_caprox = $('section#md-caprox'),
		$_list_participants_prox = $_md_caprox.find('ul#list-participants');


	var _bind = function() {
		CanvasJS.addCultureInfo("Spanish", cultureSpanish);

		splineAreaChart = $.extend(true, {}, canvasConfig);
		PieChart = $.extend(true, {}, canvasConfig);

		_chart_spline_area();
		_chart_pie();

		mfp_caprox.callbacks.beforeOpen = _beforeOpen_participants;
		$_btn_caprox.magnificPopup(mfp_caprox);
		
		hlp.humanize_DateTime();
	},

	_beforeOpen_participants = function() {

		var $this = $.magnificPopup.instance.st.el;

		$_list_participants_prox.children('li').remove();
		$_list_participants_prox.fadeOut(200);

		app.progress.show();
		
		$.getJSON('/app/inscription/programming/'+$this.data('id')+'/participants', function(json, textStatus) {
			if( json ) {

				if( json.data.length > 0 ) {

					json.data.forEach( function(obj, index) {
						var item = _get_item_participant(obj, (index + 1));
						$_list_participants_prox.append(item);
					});

					$_list_participants_prox.fadeIn(200);

				} else {
					$_list_participants_prox.append('<li class="empty-list"><h4>Aun sin participantes</h4></li>');
					$_list_participants_prox.fadeIn(200);
				}
			}
		}).always(function () {
			app.progress.close();
		});
	},

	_chart_spline_area = function() {

		splineAreaChart.data[0].type = "splineArea";
		splineAreaChart.data[0].color = "rgba(229, 57, 53, .7)";
		splineAreaChart.title.text = "INSCRIPCIONES MENSUALES";

		$.getJSON('/app/dashboard/timeline', function(json, textStatus) {

			splineAreaChart.data[0].dataPoints = json;
			splineAreaChart.axisX.valueFormatString = "MMMM";
			splineAreaChart.axisX.interval = 1;
			splineAreaChart.axisX.intervalType = "month";

			var chart = new CanvasJS.Chart("chartInscriptions", splineAreaChart);

			chart.render();

		});
	},

	_chart_pie = function() {

		CanvasJS.addColorSet('pie', [
			'rgba(244,67,54,0.7)',
			'rgba(4,167,72,0.7)',
			'#FF5E52',
			'#36F485',
			'#A71E14'
		]); 

		PieChart.colorSet = 'pie';
		PieChart.title.text = "GENEROS INSCRITOS";
		PieChart.data[0].type = "pie";
		PieChart.data[0].showInLegend = true;
		PieChart.data[0].legendText = "{y} {indexLabel}";
		PieChart.data[0].toolTipContent = "{y} {indexLabel}";	

		$.getJSON('/app/dashboard/genders', function(json, textStatus) {

			PieChart.data[0].dataPoints = json;
			var chart = new CanvasJS.Chart("chartGenders", PieChart);

			chart.render();
		});
	},

	_get_item_participant = function(DATA, INDEX) {

		var jli = $('<li />', {class:'collection-item'}),
			jname = $('<span />', { class:'title' }).text(INDEX + ". " + DATA.participant_lastname + ' ' + DATA.participant_name ),
			jdni = $('<span />', {class:'title2'}).text(DATA.participant_dni),
			jemail = $('<span />', {class:'title2'}).text(DATA.participant_emails),
			jconfirm = $('<div />', {class:'assis', title: DATA.email_confirmed ? 'Registro activado' : 'Registro no activo'});

		if ( DATA.email_confirmed )
			jconfirm.addClass('green');
		else
			jconfirm.addClass('yellow darken-2');

		jli.append([jname, jdni, jemail, jconfirm]);

		if( DATA.dispacity )
			jname.append($('<div>', {class:'chip disp', title:'Presenta una discapacidad'}).html('<i class="material-icons">&#xE914;</i>'));

		return jli;
	};

	return {
		init: _bind
	}

}(window, window.jQuery));

$(function() {
	app.dashboard.init();
});