app.dashboard.counts = ( function( _win, $ ) {

	var opt = $.extend(true, {}, options),
		hlp = $.extend(true, {}, helper);

	var mfp_participants 	= $.extend(true, {}, opt.magnificPopup),
		mfp_program			= $.extend(true, {}, opt.magnificPopup);

	var data_participants = [];

	var $_btn_participant = $('button#btn-invitados, button#btn-confirmados'),
		$_md_participant = $('section#md-participants');
	

	var _bind = function() {

		mfp_participants.callbacks.beforeOpen = _beforeOpen_participants;
		$_btn_participant.magnificPopup(mfp_participants);
		
	},

	_beforeOpen_participants = function(e) {

		var btn_active 	= $.magnificPopup.instance.st.el;

		$_md_participant.find('.popup-body').html('');
		
		$.getJSON('/app/dashboard/counts/'+ btn_active.data('content'), function(json, textStatus) {

			var collapsible = _get_collapsible( (hlp.key() + hlp.key()) );

			if( json ) {

				data_participants = json.data;

				$.each(json.data, function(years, months) {
					
					var ci = _get_collapsible_item();
					var cnt = $('<ul />', {class:'cnt-years collection'});
					var count = 0;

					$.each(months, function(month, items) {

						var btn_list = $('<a />', {
							href:'javascript:void(0)',
							'data-year': parseInt(years),
							'data-month': month,
							class:'view-list'
						}).text(hlp.getStrMonth( parseInt(month) ));

						btn_list.on('click.app.dashboard.counts.module', _view_list_participants);

						cnt.append($('<li />', {
							class:'collection-item'
						}).append([
							btn_list,
							'<span class="secondary-content" title="Cant. Inscripciones"><i class="material-icons">&#xE7FB;</i>'+items.length+'</span>' 
							])
						);

						count += items.length;
					});

					ci.find('.collapsible-header').append([
						'Año: ' + years, 
						'<span class="secondary-content" title="Cant. Inscripciones"><i class="material-icons">&#xE7FB;</i>'+count+'</span>'
					]);

					ci.find('.collapsible-body').append(cnt);
					collapsible.append(ci).collapsible({ accordion : false });

				});

				$_md_participant.find('.popup-body').append(collapsible);

			}
		});

	},

	_view_list_participants = function(e) {
		var $this = $(this),
			year = $this.data('year'),
			month = $this.data('month');

		var list = data_participants[year][month],
			cnt = $('<div />', {class:'cnt-list-persons'}),
			btn_close = $('<button />', {class:'close'}).html('<i class="material-icons">&#xE5CD;</i>');
			ul = $('<ul />', {class:'collection collection-main'});

		list.forEach( function(element, index) {
			ul.append(_get_item_person(element, index));
		});

		cnt.append([btn_close, ul]);

		$this.parent('.collection-item').append(cnt);

		$this.off('click.app.dashboard.counts.module');

		btn_close.on('click.app.dashboard.counts.module', function() {
			cnt.fadeOut(500, function() {
				$this.on('click.app.dashboard.counts.module', _view_list_participants);
				btn_close.remove();
				cnt.remove();
			});
		});

		e.preventDefault();
	},

	_get_item_person = function(obj, index) {
		var li = $('<li />', {class:'collection-item'}),
			names = $('<h4 />', {class:'title'}).text((index + 1) + '). ' + obj.name + ' ' + obj.lastname ),
			training = $('<span />', {class:'title2 truncate'}).text(obj.training_name),
			date_create = $('<time />', {class:'datetime'}).text(moment(obj.date_created).format('ddd DD MMMM YYYY h:mma'));
			small = $('<small />').append(['F. inscripción: ', date_create]),
			confim = $('<div />', {class:'confirm'});
		li.append([names, training, small]);

		return li;
	},

	_get_collapsible = function(KEY) {
		return $('<ul />', {class:'collapsible coll-participantes', 'data-collapsible':'accordion'});
	},

	_get_collapsible_item = function() {
		var li = $('<li />'),
			header = $('<div />', {class:'collapsible-header'}),
			body = $('<div />', {class:'collapsible-body'});

		li.append([header, body]);

		return li;
	};

	return {
		init : _bind
	}

}(window, window.jQuery));

$(function() {
	app.dashboard.counts.init();
});