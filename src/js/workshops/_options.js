var options = {
	magnificPopup : {
		type:'inline',
		mainClass: 'mfp-zoom-in mfp-img-mobile',
		midClick: true,
		closeOnBgClick: false,
		callbacks : {}
	},
	dropdown : {
		inDuration: 300,
		outDuration: 225,
		constrain_width: false,
		alignment: 'right',
		hover: false,
		gutter: 0
	}
};

$.ajaxSetup({

	error: function ( objResponse, textStatus, responseText ) {

		if ( objResponse.readyState == 4 ) {

			if ( typeof objResponse.responseJSON != 'undefined' )
				Materialize.toast(objResponse.responseJSON.message, 4000);
			else 
				Materialize.toast('Ocurrio un problema durante la ejecución, intentanlo más tarde', 3000);

		} else {

			Materialize.toast('Ha ocurrido un error en el servidor, intentalo más tarde', 3000, '', function() {
				location.reload(true);
			});

		}

	},
	beforeSend: function (jqXHR, settings) {

		var base_url 	= $('head meta[name="base-url"]').attr('content'),
			r_url 		= '';

		base_url = base_url.substr(0, base_url.length - 1);
		
		if ( settings.type == 'GET' ) {

			var nurl 	= settings.url.split('?');

			r_url = nurl[0];

		} else {

			r_url = settings.url;

		}

		this.url = base_url + r_url;

	}
});