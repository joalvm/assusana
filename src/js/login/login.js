$.ajaxSetup({
	beforeSend: function (jqXHR, settings) {

		var base_url 	= $('head meta[name="base-url"]').attr('content'),
			r_url 		= '';

		base_url = base_url.substr(0, base_url.length - 1);

		
		if ( settings.type == 'GET' ) {

			var nurl 	= settings.url.split('?');

			r_url = nurl[0];

		} else {

			r_url = settings.url;

		}

		this.url = base_url + r_url;

	}
});


var login = ( function( _window, $ ) {

	'use strict';

	var hlp 	= $.extend(true, {}, helper),
		crypto_ = "",
		href_	= "", 
		data_  	= {
			"username": "",
			"role" : 1,
			"password": ""
		};

	var $_form = $("form#frm_login"),
		$_input_req = $_form.find('.req');

	var _bind = function() {
		$_form.on('submit.login.module', _submit);
		$_input_req.on('change.login.module', _watcher_change);

		$('select').material_select();

		crypto_ = (hlp.key() + hlp.key());
		href_ 	= $_form.attr('action');
	},

	_watcher_change = function(e) {
		var $this = $(this),
			name = $this.attr('name'),
			type = $this.attr('type'),
			tag = $this.prop("tagName").toLowerCase();

		if ( tag == 'input' ) {
			data_[name] = $.trim($this.val());
		} else if ( tag == 'select' ) {
			data_[name] = parseInt($this.children('option:selected').val());
		}
	},

	_submit = function(e) {

		if ( ! _is_valid_data() ) {
			e.preventDefault();
			return false;
		}

		var resource_data = {
			data: _get_data_sha1()
		};

		resource_data[csrf.name] = csrf.value;

		$_input_req.prop({disabled: true});

		$_form.find('input.select-dropdown').prop({disabled:true});
		$_form.find("button[type=submit]").prop({disabled: true});

		$.post('/login/authenticate', resource_data, function(response, textStatus, xhr) {
			if ( ! response.error ) {
					Materialize.toast(response.message, 2500, 'error', function() {
						_window.location.href = response.href;
					});
			} else {
				Materialize.toast(response.message, 2500);

				$_input_req.prop({disabled: false});
				$_form.find('input.select-dropdown').prop({disabled:false});
				$_form.find("button[type=submit]").prop({disabled: false});

				$_form.find("#txtpass").val("").focus();
			}
		}).fail(function(dataError){
			if ( dataError.status == 404 ) {
				Materialize.toast("Ocurrió Algo Extraño, Por algun motivo no se encontro el recurso necesario. ¬¬", 2500, '', function(){
					location.reload(true);
				});
			} else if( dataError.status > 500 ) {
				window.alert("Ocurrio un error en nuestros servidor... Intentalo mas tarde");
				location.reload(true);
			} else {
				Materialize.toast(dataError.responseJSON.message, 2500, "error", function(){
					location.reload(true);
				});
			}
		});
		
		e.preventDefault();
	},

	is_alphaNumeric = function( TEXTO ) {
    	return !(/[^a-zA-Z0-9_]+/gmi).test( TEXTO );
    },

    _get_data_sha1 = function() {
    	return  (hlp.key() + hlp.b64EncodeUrl(JSON.stringify(data_)) + crypto_);
    },

    _is_valid_data = function() {
    	var errors_arr = [];

    	$.each(data_, function(key, val) {
    		var elem = $_form.find('input[name="' + key + '"]');
    		switch (key) {
    			case 'username':
    				if ( $.trim(val).length == 0 ) {
    					errors_arr.push({
    						elem: elem,
    						"message" : "Es campo es requerido"
    					});
    				} else if(! is_alphaNumeric(val)) {
    					errors_arr.push({
    						elem: elem,
    						"message" : "Solo caracteres Alfanumericos y _ estan permitidos."
    					});
    				}
    				break;
    			case 'password':
    				if ( $.trim(data_.password).length == 0 ) {
    					errors_arr.push({
    						elem: elem,
    						"message" : "Es campo es requerido"
    					});
    				}
    				break;
    			case 'role':
    				if(data_.role != 1 && data_.role != 2) {
    					errors_arr.push({
    						elem: elem,
    						"message" : "El rol especificado no esta permitido"
    					});
    				}
    				break;
    		}
    	});
    	
    	if ( errors_arr.length > 0 )
		{
			Materialize.toast(errors_arr[0].message, 4500);
			errors_arr[0].elem.focus();
			return false;
		} else {
			return true;
		}
    };

	return {
		init : _bind
	};
}(window, window.jQuery));

$(function() {
	login.init();
});